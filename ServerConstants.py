#
#  LoLorb Match Prediction Server Constants
# 


import sqlalchemy

from Constants import *


# Worker configuration (system context)
worker_conf = None
in_lolorb = os.path.exists(conf_fn_default)
if not in_lolorb:
    prev_wd = os.getcwd()
    os.chdir(os.path.dirname(sys.argv[0]))
if os.path.exists(conf_fn):
    with open(conf_fn, 'r') as f:
        worker_conf = json.load(f)
else:  # Import from defaults file if not found
    with open(conf_fn_default, 'r') as f:
        worker_conf = json.load(f)
if not in_lolorb:
    os.chdir(prev_wd)

# How many requests are allowed to accumulate in the rpc queue before rejecting new ones
mp_rpc_queue_maxsize = worker_conf["mp_rpc_qmaxsize"]
is_root = worker_conf["is_root"]        # Whether this machine is the LorbRoot (nginx, node.js, rabbitmq and postgres host machine)
root_addr = worker_conf["root_addr"]    # Address of the LorbRoot
serve_regions = worker_conf["regions"]  # Which regions we are serving in this environment (used for index.html/js modifications & op.gg status checks)
worker_pyex = "python3"                 # Python executable on remote worker servers (TODO: use command stored in remote $PYEXEC)


### Global options

spec_use_riot = True                        # If user specifies a specific timestamp (past, present or future), use riot match history
renew_sleep_time = 7                        # Seconds to wait for op.gg update renewals
op_load_time = 5                            # Seconds to wait for new profile data to be downloaded
restart_interval = 60                       # Seconds between restarts of workers
n_mp_threads = 50                           # How many worker threads per worker process (ThreadPool size)
# n_mp_workers = os.cpu_count()               # How many worker processes to spawn
n_mp_workers = 1
n_opgg_threads = 200                        # How many op.gg request threads per worker process
n_pgcn_threads = 50                         # How many postgres connection threads per worker process
cauldron_size = 70                          # Prediction RPC server process maximum SQLAlchemy connection pool size
cauldron_overflow = 300                     # Prediction RPC server process maximum SQLAlchemy connection pool overflow limit
if is_root:
    n_pgcn_threads = 100
    cauldron_size = 135
mp_rpc_queue_maxsize = 100                  # Default maximum match predition RPC request queue size (reject new if full)
mp_log_interval = 2.5                       # Seconds between op.gg threadpool traffic logging
opgg_expiry = 30 * 60                       # Number of seconds before op.gg profile caches expire
current_request_lifespan = 48 * 60 * 60     # Lifespan of the most recent "normal" (non-recc) request for a particular client
request_cache_lifespan = 7.5 * 60           # Lifespan of a cached request (that isn't the most recent "normal" type (non-recc))
shortlink_lifespan = 99999 * 24 * 60 * 60   # Lifespan of a shortlinked game (~300 years)
session_lifespan = 2 * 24 * 60 * 60         # Lifespan of response of last request submitted by a client
ptr_lifespan = 2 * 24 * 60 * 60             # Lifespan of a request for solo-queue-derived features of a past pro match
pg_clean_interval = 3                       # Seconds between cleanups of postgres database (delete expired entries)
op_check_interval = 30                      # Seconds between status checks of op.gg servers
log_clean_interval = 5 * 24 * 60 * 60       # Seconds between clearing of output logs

bet_caps = {                  # Caps for bets (fraction of bankroll)  [favourite, underdog]
    'nsqo': {
        1: [0.5, 0.5],
        2: [0.35, 0.35],
        3: [0.3, 0.3],
        4: [0.35, 0.35],
        5: [0.35, 0.35],
    },
    'sqo': {
        1: [0.5, 0.5],
        2: [0.3, 0.3],
        3: [0.25, 0.25],
        4: [0.25, 0.25],
        5: [0.25, 0.25],
    },
}
bet_mults = {
    'nsqo': {
        1: [1.0, 1.0],
        2: [0.80, 0.80],
        3: [0.67, 0.67],
        4: [0.6, 0.6],
        5: [0.6, 0.6],
    },
    'sqo': {
        1: [1.0, 1.0],
        2: [0.67, 0.67],
        3: [0.5, 0.5],
        4: [0.5, 0.5],
        5: [0.5, 0.5],
    },
}
# bet_caps = {                  # Caps for bets (fraction of bankroll)  [favourite, underdog]
#     'nsqo': {
#         1: [0.35, 0.5],
#         2: [0.25, 0.35],
#         3: [0.2, 0.3],
#         4: [0.25, 0.35],
#         5: [0.25, 0.35],
#     },
#     'sqo': {
#         1: [0.35, 0.5],
#         2: [0.25, 0.3],
#         3: [0.2, 0.25],
#         4: [0.2, 0.25],
#         5: [0.2, 0.25],
#     },
# }
# bet_mults = {
#     'nsqo': {
#         1: [1.0, 1.0],
#         2: [0.90, 0.80],
#         3: [0.75, 0.67],
#         4: [0.7, 0.6],
#         5: [0.6, 0.6],
#     },
#     'sqo': {
#         1: [1.0, 1.0],
#         2: [0.80, 0.67],
#         3: [0.6, 0.5],
#         4: [0.6, 0.5],
#         5: [0.6, 0.5],
#     },
# }


op_check_interval = (op_check_interval // mp_log_interval) * mp_log_interval  # Make sure they're multiples
pg_clean_interval = (pg_clean_interval // mp_log_interval) * mp_log_interval
log_clean_interval = (log_clean_interval // mp_log_interval) * mp_log_interval

### Configuration constants

# Postgres and RabbitMQ details
pg_user = PG_USER
pg_password = PG_PASS
pg_port = 5432
pg_name = PG_NAME
pg_host = root_addr
rabbit_host = root_addr
rmq_username = RMQ_USER
rmq_password = RMQ_PASS
mp_qname = "mp_rpc_queue"
pg_conn_str = 'postgresql://' + pg_user + ':' + pg_password + '@' + pg_host + ':' + str(pg_port) + '/' + pg_name

# Python executable and file names
worker_start_cmd = pyex + " worker.py 37420"
pem_dict = {"rest": "sf", "kr": "kr"}
host_pem = "~/lorb-" + pem_dict[serve_regions] + '.pem'
combined_log_path = logs_dir + "combined.log"
models_fn = "models_fin"                        # Models file
pro_model_fn = "pro_model"                # Pro model files
pro_data_fn = "pro_d"                # PRIMARY, "pro_data" = SECONDARY
pro_sq_models_fn = "pro_sq_models"
infill_data_fn = "infill_data"
rec_infill_data_fn = "rec_infill_data"

# Seasons and database columns to use
seasons_to_use = range((curr_season_id - n_seasons_to_use) + 1, curr_season_id + 1)
cols_to_use = [
    "timestamp",
    "summoner_id",
    "ranked_summary",
    "intermediate_features",
    "ranked_features",
    "champion_features"
] + ["season_" + str(i) for i in seasons_to_use]



### Static constants

team_li = 0  # Indexes of each feature in req_data
role_li = 1
cid_li = 2
name_li = 3

meta_url = "http://169.254.169.254/latest/meta-data"  # AWS metadata url

### Request format
# "data": data[0],
# "region_i": data[1],
# "avg_elo_i": data[2],
# "req_i": data[3],
# "session_id": data[4],
# "shortlink": data[5], # = failed chat log sometimes
# "comp": data[6],
# "source": data[7], # ADDED BY NODEJS

### Response format
# 0 prediction
# 1 champs only prediction
# 2 players only prediction
# 3 request index
# 4 player error codes
# 5 err code
# 6 renewals
# 7 player percentages
# 8 shortlink
# 9 composition
# 10 timestamp



# Error codes
mp_err_codes = {
    "Success": 200,
    "Server error (model key missing)": 500,
    "Need more data (low model accuracy)": 201,
    "Need more data (player(s) not found)": 300,
    "Stale request": 401,
    "Shortlink not found": 404,
    "Chat log fail": 301,
}
opgg_err_codes = {
    -1: "No summoner requested",
    505: "op.gg server unavailable",
    404: "Profile not found",
    200: "Found player",
    201: "Found player (refreshing)",
}
opgg_return_codes = {
    -1: "Player not found",
    0: "Found player (cache)",
    1: "Found player (new)",
    2: "Found player (cache updated)",
}
opgg_err_code_map = {
    -2: 505,
    -1: 404,
    0: 200,
    1: 201,
    2: 201,
}

# Match prediction response data format
mp_response_keys = [
    "percentage",
    "champions only percentage",
    "summoners only percentage",
    "client request index",
    "op.gg player err codes",
    "response error code",
    "op.gg renewals triggered",
]




