#
#  Log piper: Connects pipes to combine logs for a given host
#


from Monitoring import *


host = sys.argv[1]
pem = sys.argv[2]
ips = json.loads(sys.argv[3].replace("'", '"'))


# Open log file
logf = open(logs_dir + lorb_log_file, 'a')

# Open pipes to log files TODO: close old pipes on remote end
subprocess.Popen("ssh -i " + pem + " -o StrictHostKeyChecking=no ubuntu@" + host + " \"tail -f ~/lorb/logs/server.log\"",
    stdout=logf, stderr=logf)
subprocess.Popen("ssh -i " + pem + " -o StrictHostKeyChecking=no ubuntu@" + host + " \"tail -f ~/lorb/logs/errors.log\"",
    stdout=logf, stderr=logf)
for ip in ips:
    subprocess.Popen("ssh -i " + pem + " -o StrictHostKeyChecking=no ubuntu@" + ip + " \"tail -f ~/lolorb/logs/combined.log\"",
        stdout=logf, stderr=logf)

while True:
    time.sleep(0.1)


