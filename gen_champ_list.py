#
#  Generate champion info list for Lorb front end
#

from Server import *


# ddrag_ver = "8.22.1"

# url = "https://ddragon.leagueoflegends.com/cdn/" + ddrag_ver + "/data/en_US/champion.json"

# champions = requests.get(url).json()["data"]

idents = list(cdata_.keys())
idents.sort()

champ_list = [[-1, "None", "None [unknown]"]]

for ident in idents:
    ch = cdata_[ident]
    champ_list.append([int(ch['key']), ch["id"], ch["name"]])


# champ_role_pref_order = {}
# for cid in champion_names:
#     role_is = [role_champ_orderings[r].index(cid) for r in roles_all]
    # champ_role_pref_order[cid] = np.argsort(role_is)
champ_role_is_ordered = {cid: [role_champ_orderings[r].index(cid) \
    if cid in role_champ_orderings[r] else 500 for r in roles_all] for cid in champion_names}

patches = []
for i in range(len(patches_all)):
    patch = patches_all[i]
    patches.append(patch)
    if patch == earliest_patch:
        break

res = {
    "list": champ_list,
    # "role_fill_order": [roles_all.index(role) for role in role_fill_order],
    # "role_orderings": role_champ_orderings,
    "champ_role_is_ordered": champ_role_is_ordered,
    # "role_orderings": champ_role_pref_order,
    "patches": patches,
}

with open(data_dir + "champ_list.js", 'w') as f:
    f.write("const champ_data = '" + json.dumps(res).replace("'", "\\'") + "';\nchamp_data;")


