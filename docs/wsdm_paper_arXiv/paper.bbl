%%% -*-BibTeX-*-
%%% Do NOT edit. File created by BibTeX with style
%%% ACM-Reference-Format-Journals [18-Jan-2012].

\begin{thebibliography}{27}

%%% ====================================================================
%%% NOTE TO THE USER: you can override these defaults by providing
%%% customized versions of any of these macros before the \bibliography
%%% command.  Each of them MUST provide its own final punctuation,
%%% except for \shownote{}, \showDOI{}, and \showURL{}.  The latter two
%%% do not use final punctuation, in order to avoid confusing it with
%%% the Web address.
%%%
%%% To suppress output of a particular field, define its macro to expand
%%% to an empty string, or better, \unskip, like this:
%%%
%%% \newcommand{\showDOI}[1]{\unskip}   % LaTeX syntax
%%%
%%% \def \showDOI #1{\unskip}           % plain TeX syntax
%%%
%%% ====================================================================

\ifx \showCODEN    \undefined \def \showCODEN     #1{\unskip}     \fi
\ifx \showDOI      \undefined \def \showDOI       #1{#1}\fi
\ifx \showISBNx    \undefined \def \showISBNx     #1{\unskip}     \fi
\ifx \showISBNxiii \undefined \def \showISBNxiii  #1{\unskip}     \fi
\ifx \showISSN     \undefined \def \showISSN      #1{\unskip}     \fi
\ifx \showLCCN     \undefined \def \showLCCN      #1{\unskip}     \fi
\ifx \shownote     \undefined \def \shownote      #1{#1}          \fi
\ifx \showarticletitle \undefined \def \showarticletitle #1{#1}   \fi
\ifx \showURL      \undefined \def \showURL       {\relax}        \fi
% The following commands are used for tagged output and should be
% invisible to TeX
\providecommand\bibfield[2]{#2}
\providecommand\bibinfo[2]{#2}
\providecommand\natexlab[1]{#1}
\providecommand\showeprint[2][]{arXiv:#2}

\bibitem[\protect\citeauthoryear{??}{dot}{2013}]%
        {dota2cp}
 \bibinfo{year}{2013}\natexlab{}.
\newblock \bibinfo{booktitle}{\emph{Dota2 Counter-Pick}}.
\newblock
\urldef\tempurl%
\url{https://blog.liangchun.me/Dota2CP}
\showURL{%
Retrieved December 10, 2019 from \tempurl}
\newblock
\shownote{Originally hosted at \url{http://dota2cp.com}.}


\bibitem[\protect\citeauthoryear{Agarwala and Pearce}{Agarwala and
  Pearce}{2014}]%
        {agarwala}
\bibfield{author}{\bibinfo{person}{Atish Agarwala} {and}
  \bibinfo{person}{Michael Pearce}.} \bibinfo{year}{2014}\natexlab{}.
\newblock \bibinfo{booktitle}{\emph{Learning Dota 2 team compositions}}.
\newblock \bibinfo{type}{{T}echnical {R}eport}. \bibinfo{institution}{Stanford
  University}.
\newblock


\bibitem[\protect\citeauthoryear{Almeida, Correia, Eler, Olivete-Jr, Garci,
  Scabora, and Spadon}{Almeida et~al\mbox{.}}{2017}]%
        {almeida}
\bibfield{author}{\bibinfo{person}{Carlos~EM Almeida},
  \bibinfo{person}{Ronaldo~CM Correia}, \bibinfo{person}{Danilo~M Eler},
  \bibinfo{person}{Celso Olivete-Jr}, \bibinfo{person}{Rog{\'e}rio~E Garci},
  \bibinfo{person}{Lucas~C Scabora}, {and} \bibinfo{person}{Gabriel Spadon}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Prediction of winners in MOBA games}. In
  \bibinfo{booktitle}{\emph{2017 12th Iberian Conference on Information Systems
  and Technologies}}. IEEE, \bibinfo{pages}{1--6}.
\newblock


\bibitem[\protect\citeauthoryear{Bekir and {\c{C}}elik}{Bekir and
  {\c{C}}elik}{2019}]%
        {bekir}
\bibfield{author}{\bibinfo{person}{Seyhan Bekir} {and}
  \bibinfo{person}{Ey{\"u}p {\c{C}}elik}.} \bibinfo{year}{2019}\natexlab{}.
\newblock \showarticletitle{Examining the Factors Contributing to
  Adolescents’ Online Game Addiction}.
\newblock \bibinfo{journal}{\emph{Annals of Psychology}} \bibinfo{volume}{35},
  \bibinfo{number}{3} (\bibinfo{year}{2019}), \bibinfo{pages}{444--452}.
\newblock


\bibitem[\protect\citeauthoryear{Chen, Sun, El-Nasr, and Nguyen}{Chen
  et~al\mbox{.}}{2017}]%
        {chen}
\bibfield{author}{\bibinfo{person}{Zhengxing Chen}, \bibinfo{person}{Yizhou
  Sun}, \bibinfo{person}{Magy~Seif El-Nasr}, {and}
  \bibinfo{person}{Truong-Huy~D Nguyen}.} \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Player skill decomposition in multiplayer online
  battle arenas}.
\newblock \bibinfo{journal}{\emph{arXiv preprint arXiv:1702.06253}}
  (\bibinfo{year}{2017}).
\newblock


\bibitem[\protect\citeauthoryear{Christiano, Leike, Brown, Martic, Legg, and
  Amodei}{Christiano et~al\mbox{.}}{2017}]%
        {christiano}
\bibfield{author}{\bibinfo{person}{Paul~F Christiano}, \bibinfo{person}{Jan
  Leike}, \bibinfo{person}{Tom Brown}, \bibinfo{person}{Miljan Martic},
  \bibinfo{person}{Shane Legg}, {and} \bibinfo{person}{Dario Amodei}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Deep reinforcement learning from human
  preferences}. In \bibinfo{booktitle}{\emph{Advances in Neural Information
  Processing Systems}}. \bibinfo{pages}{4299--4307}.
\newblock


\bibitem[\protect\citeauthoryear{Conley and Perry}{Conley and Perry}{2013}]%
        {conley}
\bibfield{author}{\bibinfo{person}{Kevin Conley} {and} \bibinfo{person}{Daniel
  Perry}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \bibinfo{booktitle}{\emph{How Does He Saw Me? A Recommendation Engine
  for Picking Heroes in Dota 2}}.
\newblock \bibinfo{type}{CS229 Technical Report}.
  \bibinfo{institution}{Stanford University}.
\newblock


\bibitem[\protect\citeauthoryear{Crust and Nesti}{Crust and Nesti}{2006}]%
        {crust}
\bibfield{author}{\bibinfo{person}{Lee Crust} {and} \bibinfo{person}{Mark
  Nesti}.} \bibinfo{year}{2006}\natexlab{}.
\newblock \showarticletitle{A Review of Psychological Momentum in Sports: Why
  qualitative research is needed}.
\newblock \bibinfo{journal}{\emph{Athletic Insight}} \bibinfo{volume}{8},
  \bibinfo{number}{1} (\bibinfo{year}{2006}), \bibinfo{pages}{1--15}.
\newblock


\bibitem[\protect\citeauthoryear{Csapo, Avugos, Raab, and Bar-Eli}{Csapo
  et~al\mbox{.}}{2015}]%
        {csapo}
\bibfield{author}{\bibinfo{person}{Peter Csapo}, \bibinfo{person}{Simcha
  Avugos}, \bibinfo{person}{Markus Raab}, {and} \bibinfo{person}{Michael
  Bar-Eli}.} \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{The effect of perceived streakiness on the
  shot-taking behaviour of basketball players}.
\newblock \bibinfo{journal}{\emph{European Journal of Sport Science}}
  \bibinfo{volume}{15}, \bibinfo{number}{7} (\bibinfo{year}{2015}),
  \bibinfo{pages}{647--654}.
\newblock


\bibitem[\protect\citeauthoryear{Ebbinghaus}{Ebbinghaus}{1885}]%
        {ebbinghaus}
\bibfield{author}{\bibinfo{person}{Hermann Ebbinghaus}.}
  \bibinfo{year}{1885}\natexlab{}.
\newblock \showarticletitle{Forgetting curve}.
\newblock \bibinfo{journal}{\emph{Memory A Contribution to Experimental
  Psychology}} (\bibinfo{year}{1885}).
\newblock


\bibitem[\protect\citeauthoryear{Grutzik, Higgins, and Tran}{Grutzik
  et~al\mbox{.}}{2017}]%
        {grutzik}
\bibfield{author}{\bibinfo{person}{Petra Grutzik}, \bibinfo{person}{Joe
  Higgins}, {and} \bibinfo{person}{Long Tran}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \bibinfo{booktitle}{\emph{Predicting outcomes of professional DotA 2
  matches}}.
\newblock \bibinfo{type}{{T}echnical {R}eport}. \bibinfo{institution}{Stanford
  University}.
\newblock


\bibitem[\protect\citeauthoryear{Herce~Casta{\~n}{\'o}n, Bang, Moran, Ding,
  Egner, and Summerfield}{Herce~Casta{\~n}{\'o}n et~al\mbox{.}}{2018}]%
        {herce}
\bibfield{author}{\bibinfo{person}{Santiago Herce~Casta{\~n}{\'o}n},
  \bibinfo{person}{Dan Bang}, \bibinfo{person}{Rani Moran},
  \bibinfo{person}{Jacqueline Ding}, \bibinfo{person}{Tobias Egner}, {and}
  \bibinfo{person}{Christopher Summerfield}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Human noise blindness drives suboptimal cognitive
  inference}.
\newblock  (\bibinfo{year}{2018}).
\newblock


\bibitem[\protect\citeauthoryear{Kalyanaraman}{Kalyanaraman}{2014}]%
        {kalyanaraman}
\bibfield{author}{\bibinfo{person}{Kaushik Kalyanaraman}.}
  \bibinfo{year}{2014}\natexlab{}.
\newblock \bibinfo{booktitle}{\emph{To win or not to win? A prediction model to
  determine the outcome of a DotA2 match}}.
\newblock \bibinfo{type}{University of California San Diego}.
\newblock


\bibitem[\protect\citeauthoryear{Kou, Li, Gui, and Suzuki-Gill}{Kou
  et~al\mbox{.}}{2018}]%
        {kou}
\bibfield{author}{\bibinfo{person}{Yubo Kou}, \bibinfo{person}{Yao Li},
  \bibinfo{person}{Xinning Gui}, {and} \bibinfo{person}{Eli Suzuki-Gill}.}
  \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Playing with Streakiness in Online Games: How
  Players Perceive and React to Winning and Losing Streaks in League of
  Legends}. In \bibinfo{booktitle}{\emph{Proceedings of the 2018 CHI Conference
  on Human Factors in Computing Systems}}. ACM, \bibinfo{pages}{578}.
\newblock


\bibitem[\protect\citeauthoryear{Lan, Duan, Chen, Qin, Nummenmaa, and
  Nummenmaa}{Lan et~al\mbox{.}}{2018}]%
        {lan}
\bibfield{author}{\bibinfo{person}{Xuan Lan}, \bibinfo{person}{Lei Duan},
  \bibinfo{person}{Wen Chen}, \bibinfo{person}{Ruiqi Qin},
  \bibinfo{person}{Timo Nummenmaa}, {and} \bibinfo{person}{Jyrki Nummenmaa}.}
  \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{A Player Behavior Model for Predicting Win-Loss
  Outcome in MOBA Games}. In \bibinfo{booktitle}{\emph{International Conference
  on Advanced Data Mining and Applications}}. Springer,
  \bibinfo{pages}{474--488}.
\newblock


\bibitem[\protect\citeauthoryear{Murre and Dros}{Murre and Dros}{2015}]%
        {murre}
\bibfield{author}{\bibinfo{person}{Jaap M.~J. Murre} {and}
  \bibinfo{person}{Joeri Dros}.} \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Replication and Analysis of Ebbinghaus’
  Forgetting Curve}.
\newblock \bibinfo{journal}{\emph{PLOS ONE}} \bibinfo{volume}{10},
  \bibinfo{number}{7} (\bibinfo{date}{07} \bibinfo{year}{2015}),
  \bibinfo{pages}{1--23}.
\newblock
\urldef\tempurl%
\url{https://doi.org/10.1371/journal.pone.0120644}
\showDOI{\tempurl}


\bibitem[\protect\citeauthoryear{Ong, Deolalikar, and Peng}{Ong
  et~al\mbox{.}}{2015}]%
        {ong}
\bibfield{author}{\bibinfo{person}{Hao~Yi Ong}, \bibinfo{person}{Sunil
  Deolalikar}, {and} \bibinfo{person}{Mark Peng}.}
  \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Player Behavior and Optimal Team Composition for
  Online Multiplayer Games}.
\newblock \bibinfo{journal}{\emph{arXiv preprint arXiv:1503.02230}}
  (\bibinfo{year}{2015}).
\newblock


\bibitem[\protect\citeauthoryear{OpenAI}{OpenAI}{2019}]%
        {openai}
\bibfield{author}{\bibinfo{person}{OpenAI}.} \bibinfo{year}{2019}\natexlab{}.
\newblock \bibinfo{booktitle}{\emph{How to Train Your OpenAI Five}}.
\newblock
\urldef\tempurl%
\url{https://openai.com/blog/how-to-train-your-openai-five}
\showURL{%
Retrieved December 2019 from \tempurl}


\bibitem[\protect\citeauthoryear{Palom{\"a}ki, Laakasuo, and
  Salmela}{Palom{\"a}ki et~al\mbox{.}}{2013}]%
        {palomaki}
\bibfield{author}{\bibinfo{person}{Jussi Palom{\"a}ki},
  \bibinfo{person}{Michael Laakasuo}, {and} \bibinfo{person}{Mikko Salmela}.}
  \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{‘This is just so unfair!’: A qualitative
  analysis of loss-induced emotions and tilting in on-line poker}.
\newblock \bibinfo{journal}{\emph{International Gambling Studies}}
  \bibinfo{volume}{13}, \bibinfo{number}{2} (\bibinfo{year}{2013}),
  \bibinfo{pages}{255--270}.
\newblock


\bibitem[\protect\citeauthoryear{Reddi, Kale, and Kumar}{Reddi
  et~al\mbox{.}}{2019}]%
        {reddi}
\bibfield{author}{\bibinfo{person}{Sashank~J Reddi}, \bibinfo{person}{Satyen
  Kale}, {and} \bibinfo{person}{Sanjiv Kumar}.}
  \bibinfo{year}{2019}\natexlab{}.
\newblock \showarticletitle{On the convergence of adam and beyond}.
\newblock \bibinfo{journal}{\emph{arXiv preprint arXiv:1904.09237}}
  (\bibinfo{year}{2019}).
\newblock


\bibitem[\protect\citeauthoryear{Sapienza, Goyal, and Ferrara}{Sapienza
  et~al\mbox{.}}{2018}]%
        {sapienza}
\bibfield{author}{\bibinfo{person}{Anna Sapienza}, \bibinfo{person}{Palash
  Goyal}, {and} \bibinfo{person}{Emilio Ferrara}.}
  \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Deep Neural Networks for Optimal Team Composition}.
\newblock \bibinfo{journal}{\emph{arXiv preprint arXiv:1805.03285}}
  (\bibinfo{year}{2018}).
\newblock


\bibitem[\protect\citeauthoryear{{\v{S}}ka{\v{r}}upov{\'a} and
  Blinka}{{\v{S}}ka{\v{r}}upov{\'a} and Blinka}{2015}]%
        {skarupova}
\bibfield{author}{\bibinfo{person}{Kate{\v{r}}ina {\v{S}}ka{\v{r}}upov{\'a}}
  {and} \bibinfo{person}{Lukas Blinka}.} \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Interpersonal dependency and online gaming
  addiction}.
\newblock \bibinfo{journal}{\emph{Journal of Behavioral Addictions}}
  \bibinfo{volume}{5}, \bibinfo{number}{1} (\bibinfo{year}{2015}),
  \bibinfo{pages}{108--114}.
\newblock


\bibitem[\protect\citeauthoryear{Stannett, Sedeeq, and Romano}{Stannett
  et~al\mbox{.}}{2016}]%
        {stannett}
\bibfield{author}{\bibinfo{person}{Mike Stannett}, \bibinfo{person}{Alhasan
  Sedeeq}, {and} \bibinfo{person}{Daniela~M. Romano}.}
  \bibinfo{year}{2016}\natexlab{}.
\newblock \showarticletitle{Generic and Adaptive Gamification: A Panoramic
  Review}.
\newblock \bibinfo{journal}{\emph{Convention Proceedings of the Society for the
  Study of Artificial Intelligence and Simulation of Behaviour 2016}}.
\newblock


\bibitem[\protect\citeauthoryear{Swann, Keegan, Piggo, and Crust}{Swann
  et~al\mbox{.}}{2012}]%
        {swann}
\bibfield{author}{\bibinfo{person}{Christian~F. Swann},
  \bibinfo{person}{Richard~J. Keegan}, \bibinfo{person}{David Piggo}, {and}
  \bibinfo{person}{Lee Crust}.} \bibinfo{year}{2012}\natexlab{}.
\newblock \showarticletitle{A systematic review of the experience, occurrence,
  and controllability of flow states in elite sport}.
\newblock \bibinfo{journal}{\emph{Psychology of Sport and Exercise}}
  \bibinfo{volume}{13}, \bibinfo{number}{6} (\bibinfo{year}{2012}),
  \bibinfo{pages}{807--819}.
\newblock


\bibitem[\protect\citeauthoryear{Wei, Palomaki, Yan, and Robinson}{Wei
  et~al\mbox{.}}{2016}]%
        {wei}
\bibfield{author}{\bibinfo{person}{Xingjie Wei}, \bibinfo{person}{Jussi
  Palomaki}, \bibinfo{person}{Jeff Yan}, {and} \bibinfo{person}{Peter
  Robinson}.} \bibinfo{year}{2016}\natexlab{}.
\newblock \showarticletitle{The Science and Detection of Tilting}. In
  \bibinfo{booktitle}{\emph{Proceedings of the 2016 ACM on International
  Conference on Multimedia Retrieval}}. ACM, \bibinfo{pages}{79--86}.
\newblock


\bibitem[\protect\citeauthoryear{Yao, Liao, and Jin}{Yao et~al\mbox{.}}{2018}]%
        {yao}
\bibfield{author}{\bibinfo{person}{Qiongjie Yao}, \bibinfo{person}{Xiaofei
  Liao}, {and} \bibinfo{person}{Hai Jin}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Hierarchical Attention Based Recurrent Neural
  Network Framework for Mobile MOBA Game Recommender Systems}. In
  \bibinfo{booktitle}{\emph{2018 International Conference on Parallel \&
  Distributed Processing with Applications, Ubiquitous Computing \&
  Communications, Big Data \& Cloud Computing, Social Computing \& Networking,
  Sustainable Computing \& Communications
  (ISPA/IUCC/BDCloud/SocialCom/SustainCom)}}. IEEE, \bibinfo{pages}{338--345}.
\newblock


\bibitem[\protect\citeauthoryear{Zhang, Li, Zhang, Zheng, Zhang, Hao, Chen,
  Chen, Xiao, and Zhou}{Zhang et~al\mbox{.}}{2019}]%
        {zhang}
\bibfield{author}{\bibinfo{person}{Zhijian Zhang}, \bibinfo{person}{Haozheng
  Li}, \bibinfo{person}{Luo Zhang}, \bibinfo{person}{Tianyin Zheng},
  \bibinfo{person}{Ting Zhang}, \bibinfo{person}{Xiong Hao},
  \bibinfo{person}{Xiaoxin Chen}, \bibinfo{person}{Min Chen},
  \bibinfo{person}{Fangxu Xiao}, {and} \bibinfo{person}{Wei Zhou}.}
  \bibinfo{year}{2019}\natexlab{}.
\newblock \showarticletitle{Hierarchical Reinforcement Learning for Multi-agent
  MOBA Game}.
\newblock \bibinfo{journal}{\emph{arXiv preprint arXiv:1901.08004}}
  (\bibinfo{year}{2019}).
\newblock


\end{thebibliography}
