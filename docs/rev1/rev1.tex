\documentclass[a4paper]{article}

% \setcounter{tocdepth}{3}

% latex package inclusions here
% \usepackage{amsthm}
\usepackage{amsmath,amsthm,amssymb} % package matematici
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{tabulary}
\usepackage{amsfonts} % altri simbolo matematici stronzi
% \usepackage{textcomp} % copyrite
% \usepackage{fancyhdr}
% \usepackage{courier}

% set up BNF generator
\usepackage{syntax}
\setlength{\grammarparsep}{10pt plus 1pt minus 1pt}   % vertical distance between production rules
\setlength{\grammarindent}{10em}                      % horizontal indent distance

% set up source code inclusion
\usepackage{listings}
\lstset{
  tabsize=2,
  basicstyle = \ttfamily\small,
  columns=fullflexible
}

% in-line code styling
\newcommand{\shell}[1]{\lstinline{#1}}

\theoremstyle{definition}
\newtheorem{question}{Gap}

% tagged boxes for fill the gap exercise
\newcommand{\fillgap}[2]{
  \begin{center}
  \fbox{
    \begin{minipage}{4in}
      \begin{question}
        {\it #1} \hfill ({\bf #2})
      \end{question}
    \end{minipage}
  }
\end{center}
}
\newcommand{\ts}{\textsuperscript}
% superscript text

% \pagestyle{fancy}
% \renewcommand{\headrulewidth}{0pt}
% \lfoot{\textcopyright Alfonso White 2019}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{BlitzBrain Revision 1}
\date{April 2019}
\author{
Alfonso White \\
}

\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Updated requirements and completion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Existing requirements}

\begin{enumerate}
  \item \textit{Utilising all relevant available data from the LoL match history database (directly or through third party sites), to the extent that is possible given bandwidth constraints} \\
  This requirement can be considered complete for the context of solo/duo/flex queue matches between opponents. There is untapped (live) data available for the purpose of predicting professional matches, for both regular season games, and playoffs. In addition, with future sources of information from Blitz, more accurate versions of existing statistics may be leveraged for greater accuracy in all models.

  \item \textit{It can provide win probability estimates for any given subset of champion select information, i.e., 0-10 champion picks, and/or 0-10 players. Todo: include summoner spell picks} \\
  This requirement can be considered complete, except for the inclusion of summoner spells. The data science strategy for summoner spells has been mapped out, and can be implemented relatively easily. There are also small improvements, ready to implement, for the case of incomplete team compositions (multiple imputation with a decision tree model).

  \item \textit{The win predictions are accurate (up to 94\% win/loss classification accuracy given all 10 champion/player data, and up to 90\% given full champion select data (10 champions + 5 teammates)) and win probabilities are well calibrated} \\
  This requirement is complete.

  \item \textit{It can provide probability estimates within a reasonable time frame (a few seconds, with caching)} \\
  This requirement is complete. However, the total efficiency of the system could be improved significantly with a software rewrite in a language oriented more towards speed and maintainability, rather than ease of experimentation, which was only required in the research \& development stage.

  \item \textit{It can provide win probability estimates not only for the whole available champion select information, but also for informative individual subsets, such as for each player alone, for the champions in the match alone, and for the players in the match alone} \\
  This requirement can be considiered complete. There are 4 or 5 extra metrics which have yet to be added, though the framework is in place to make this a very simple process.
\end{enumerate}

\subsection{New requirements}

\begin{enumerate}
  \item \textit{Utilising live ``momentum/tilt'' data, and other data, in professional match prediction} \\
  While the system has been tested and, without any adjustments, appears to work well for regular LCS season matches, there is a significant lack of normally present data, when making predictions for pro games, especially during best-of-3 and best-of-5 games. This is due to the difficulty of obtaining the data, however, once this is solved, it is an easy integration. This lack of data would have a highly detrimental impact on the predictive accuracy, because momentum/tilt in Esports is known to be one of the largest factors affecting the outcome of a match. This has been shown in the literature \cite{hudson} \cite{olsson}, and has been observed in our experiments using solo queue data as the most influential factor \url{https://docs.google.com/spreadsheets/d/1fuvH1fSfbWuQXsWhQIzCD_r7LskV_8NVZM_wnK21Fuc/edit?usp=sharing}.

  \item \textit{Training a more complex pair of models specifically for regular season games and tournament format games} \\
  The current model currently has approximately 95\% accuracy for high elo (Master+) solo queue games, and at least 90\% in regular LCS season games. However, for season games, to some degree, but especially for tournament matches, the influence of momentum and tilt is weighted differently for this level of competitive team play \cite{olsson}. In some scenarios, it is more complex, while in others, there may be very little impact from tilt. For example, in best-of-5s, there is a certain amount of a contextual `fatigue' element faced by professional players, which is not present in solo queue. To learn these kinds of features, we propose a modification to the momentum/tilt normalization equation, which adds a linear `fatigue' factor, which scales with the (exponential) momentum/tilt, representing the ability of a team being able to recover. Then, using the output of the existing model together with the presence of individual pro players, as input to the new model, we can learn these features specific to players. This would also then be a useful model for scouting among solo queue players with low average tilt. We would also like to experiment with more complex models for prediction, which would learn the fatigue and momentum factors implicitly, and which were not feasible prior to the consideration of playoff games. This would include recurrent neural networks, random forests, and temporal gaussian processes.


\end{enumerate}

\section{Methods}

\subsection{Validation}
We first perform an initial training and testing phase with all of the features from the extraction phase (currently, $|x|$ = 2298 features).
\begin{enumerate}
  \item We extract a total of 80,000 datapoints, which have been gathered over the last 3-4 months, however this many can be gathered in 1-2 months given a higher crawler bandwidth (around 100Mbit)
  \item We initially experiment a little to see at which point training with more data no longer increases the accuracy (the remaining error being due to the model or irreducible error from unpredictability in the game). We start by training with 50,000 datapoints (graphing the learning curve), using 15,000 for probability calibration, and 15,000 for classification \% accuracy. The data is fully shuffled before each individual test to ensure generalisability
  \item From the results, we can see that around 45,000 are enough to max out the accuracy (and avoid overfitting), 5,000 are enough for testing to within $\pm$0.1\% (the variance in the learned model accuracy is low), and 15,000 is enough for probability calibration, using a Brier score sample size of 20
  \item We therefore train and test our final model using these sample sizes, on a total data subset of 65,000 samples, chosen by most recent first, but using the data crawler heuristics to select more diverse data (in elo and playerbase) when appropriate
  \item Multiple copies of the final model are trained for all possible combinations of included players and/or champions, considering the presence of 1-4 players or champions as one category. This produces an index of length 7 for the included features model key, defined in \lit*{new_model.py}, and representing \\
  (\\
  \hspace*{5mm} [0, 1-4, or 5] champions for blue team, \\
  \hspace*{5mm} [0, 1-4, or 5] champions for red team, \\
  \hspace*{5mm} [0, 1-4, or 5] players for blue team, \\
  \hspace*{5mm} [0, 1-4, or 5] players for red team, \\
  \hspace*{5mm} [0, 1-4, or 5] player champion overlaps for blue team, \\
  \hspace*{5mm} [0, 1-4, or 5] player champion overlaps for red team, \\
  \hspace*{5mm} [0, 1-4, or 5] champion matchup overlaps for either team \\
  )\\
  We perform the same set of tests for each of 115 possible cases of our case based testing method. When the model being trained is linear, this is mathetically equivalent to performing the `zeroing-out' single imputation for missing input and testing on those results. The case-based testing of the faster, single imputation method has not been implemented, because it will be superseded by the multiple imputation training and testing method, at which point, this case-based testing method will only be useful for performing testing, rather than training.
\end{enumerate}

\subsection{Parameter tuning}

The training and testing procedure described above (at least the version for a fully-featured model, with a key of (5,5,5,5,5,5,5)) was used to approximate the optimal values of various paramters of the learning system. These would include:
\begin{enumerate}
  \item The learned weight vector $\boldsymbol{w}$, which is learned for the fully-featured model, then the same weights applied to the input for all additional ``data subset'' models. This saves times in the training when using the case-based method.
  \item The Logistic Regression L-BFGS (scikit-learn implementation) training hyperparameters: the inverse regularization strength $C = 2.783$ (found by 10-fold cross validation using ten logarithmic values in the range $1e^{-4}$ - $1e^{4}$), stopping criterion tolerance \lit*{tol}$ = 0.00001$ (found by manual optimisation using values $[0.0001, 0.00005, 0.00001, 0.000005, 0.000001]$), and maximum iterations \lit*{max_iter}$ = 3000$ (also found manually, using values $[2000, 2500, 3000, 3500, 4000]$). There may be small accuracy gains in using better optimisation methods, possibly a different algorithm. However, we have found this configuration to work very well compared to other techniques.
  \item The momentum shape constants $C_1$ and $C_2$. These were optimised manually, so that the majority of the exponential decay begins after about 6-12 hours, and so that the influence has decayed almost completely by around the 2nd or 3rd day. While the current parameter values are correct in that they increase the system's final accuracy, it is not known how much improvement could be achieve using hyperoptimisation, and so this is something worth exploring.
  \item The Beta distribution parameters $\alpha = 1$ and $\beta = 1$. These have been determind using an approximate hyperparameter optimisation performed manually for various values such as [0, 0.5, 0.75, 1, 1.25, 1.5, 2, 3] etc. It was observed that the increase in accuracy from this was most stable at a value of about 1.0, though there may be more optimisation to do here.
  \item The number of samples used for average stats for champion performance: \\
  \hspace*{5mm} \lit*{n_opgg_pcs_games} = 100 \hspace*{1mm} should be enough for an accurate sample, without going too far back into the past (3 seasons ago) for a player \\
  \hspace*{5mm} \lit*{n_opgg_pcs_avg_games} = 1000 \hspace*{1mm} for average stats over all champions for a player, this should be enough (more would be outdated) \\
  \hspace*{5mm} \lit*{n_opgg_gcs_games} = 40 \hspace*{1mm} we want stats for rarely played champions in rare skill ratings (Challenger), for the current meta (new samples daily). For this purpose, a sample size of 40 should be optimal, as this is the absolute minimum to get accurate means, while not oversampling older data from previous patches \\
  \hspace*{5mm} \lit*{n_chgg_gms_cmn} = 10 \hspace*{1mm} Since the matchup stats are already averaged over many games, this number does not have to be very high, and a value of 10 gives a fair approximation to the average values that would be expected from the rarest matchups for a given champion and role
  \hspace*{5mm} \lit*{n_gsdb_load} = 90 \hspace*{1mm} This many past months of stats (around 3 months) should be enough for any given query, so long as it is not for a game that occurred, for example, 5 months ago.
  \hspace*{5mm} \lit*{n_gsdb_patches} = 4 \hspace*{1mm} This, again, depends on how far back in time we are making requests for - this value will make it possible to obtain accurate predictions for games from 4 or 5 patches ago. In the current implementation, however, this takes up a significant amount of system memory (RAM) in order to have the full database of statistics ready for querying by either the live prediction server or model training processes. A value of 2 or 3 would be more efficient for making present and future predictions.

\end{enumerate}

\section{Complex interactions}
Complex interactions are accounted for by the separation of features for individual roles (for example, measuring the importance of champion category dimensions in each position on the map), matchup and bot lane synergy stats, and champion role specific stats from Blitz. The initial feature set, prior to feature selection, contained many more features that may assist in prediction (such as the prescence of individual champions, and the performance of players on every champion), however, the gains to be made by trying to include this `raw' data may not be worth the difficulty required.

\section{Heuristic search depth}
The depth of our elo heuristic for searching for champion-roles and matchup stats has not been measured systematically, however, having observed these rare cases, we believe that the search drops down at the very most 2-3 levels, but usually only one - from Diamond to Platinum, for example. In addition, to a significant degree, the variance in the accuracy of these heuristics is accounted for in the magnitude of the weights for the matchup and synergy features, and all derivative features.

\begin{thebibliography}{99}



\bibitem{olsson}
  Josefine Olsson \& Riikka Vahlroos,
  \emph{``Co-operative Emotional Interaction in Virtual Competitive Group Activity''},
  Thesis for Master of Science in Psychology Oriented Towards
Sports, \\
  Ume\aa$ $ University,
  2018.

\bibitem{hudson}
  Matthew Hudson \& Paul Cairns,
  \emph{``The effects of winning and losing on social presence in team-based digital games''},
  Computers in Human Behavior 60, (pp. 1-12),
  2016.


\end{thebibliography}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}