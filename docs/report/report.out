\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.1.1}{Motivation}{chapter.1}% 2
\BOOKMARK [1][-]{section.1.2}{Contributions}{chapter.1}% 3
\BOOKMARK [0][-]{chapter.2}{Background}{}% 4
\BOOKMARK [1][-]{section.2.1}{Internet Gaming Addiction}{chapter.2}% 5
\BOOKMARK [1][-]{section.2.2}{Nudge Theory}{chapter.2}% 6
\BOOKMARK [1][-]{section.2.3}{Psychological Momentum and Tilt}{chapter.2}% 7
\BOOKMARK [1][-]{section.2.4}{Multiplayer Online Battle Arenas}{chapter.2}% 8
\BOOKMARK [2][-]{subsection.2.4.1}{League of Legends Game Structure}{section.2.4}% 9
\BOOKMARK [1][-]{section.2.5}{Win Prediction and Artificial Intelligence in MOBAs}{chapter.2}% 10
\BOOKMARK [1][-]{section.2.6}{Learning Techniques}{chapter.2}% 11
\BOOKMARK [2][-]{subsection.2.6.1}{Item Imputation}{section.2.6}% 12
\BOOKMARK [2][-]{subsection.2.6.2}{Sequential Floating Feature Selection}{section.2.6}% 13
\BOOKMARK [2][-]{subsection.2.6.3}{Principal Component Analysis}{section.2.6}% 14
\BOOKMARK [2][-]{subsection.2.6.4}{Logistic Regression}{section.2.6}% 15
\BOOKMARK [2][-]{subsection.2.6.5}{Support Vector Classification}{section.2.6}% 16
\BOOKMARK [2][-]{subsection.2.6.6}{Deep Learning}{section.2.6}% 17
\BOOKMARK [2][-]{subsection.2.6.7}{Multi-Task Learning}{section.2.6}% 18
\BOOKMARK [2][-]{subsection.2.6.8}{Transfer Learning}{section.2.6}% 19
\BOOKMARK [2][-]{subsection.2.6.9}{Reinforcement Learning}{section.2.6}% 20
\BOOKMARK [0][-]{chapter.3}{Methodology}{}% 21
\BOOKMARK [1][-]{section.3.1}{Player Performance Modelling}{chapter.3}% 22
\BOOKMARK [2][-]{subsection.3.1.1}{Model Inputs}{section.3.1}% 23
\BOOKMARK [2][-]{subsection.3.1.2}{Feature Engineering}{section.3.1}% 24
\BOOKMARK [2][-]{subsection.3.1.3}{Engineered Features}{section.3.1}% 25
\BOOKMARK [2][-]{subsection.3.1.4}{Feature Selection}{section.3.1}% 26
\BOOKMARK [2][-]{subsection.3.1.5}{Win Prediction Model}{section.3.1}% 27
\BOOKMARK [2][-]{subsection.3.1.6}{Partial Composition Heuristics}{section.3.1}% 28
\BOOKMARK [2][-]{subsection.3.1.7}{Transfer Learning for Professional Prediction}{section.3.1}% 29
\BOOKMARK [2][-]{subsection.3.1.8}{Influence and Momentum Models}{section.3.1}% 30
\BOOKMARK [1][-]{section.3.2}{Request Server and Training Loop}{chapter.3}% 31
\BOOKMARK [1][-]{section.3.3}{Team Composition User Interface}{chapter.3}% 32
\BOOKMARK [1][-]{section.3.4}{Momentum App}{chapter.3}% 33
\BOOKMARK [1][-]{section.3.5}{Adaptive Nudge Strategy}{chapter.3}% 34
\BOOKMARK [0][-]{chapter.4}{Experiments}{}% 35
\BOOKMARK [1][-]{section.4.1}{Data Collection}{chapter.4}% 36
\BOOKMARK [2][-]{subsection.4.1.1}{Data Sources}{section.4.1}% 37
\BOOKMARK [2][-]{subsection.4.1.2}{Data Crawling}{section.4.1}% 38
\BOOKMARK [1][-]{section.4.2}{Feature Extraction}{chapter.4}% 39
\BOOKMARK [1][-]{section.4.3}{Model Selection}{chapter.4}% 40
\BOOKMARK [1][-]{section.4.4}{Win Prediction Model Evaluation}{chapter.4}% 41
\BOOKMARK [1][-]{section.4.5}{Feature Importances}{chapter.4}% 42
\BOOKMARK [1][-]{section.4.6}{Professional Model}{chapter.4}% 43
\BOOKMARK [1][-]{section.4.7}{Influence and Momentum Conditioning}{chapter.4}% 44
\BOOKMARK [2][-]{subsection.4.7.1}{Training}{section.4.7}% 45
\BOOKMARK [2][-]{subsection.4.7.2}{Evaluation}{section.4.7}% 46
\BOOKMARK [0][-]{chapter.5}{Conclusions}{}% 47
\BOOKMARK [1][-]{section.5.1}{Limitations}{chapter.5}% 48
\BOOKMARK [2][-]{subsection.5.1.1}{Data Resolution}{section.5.1}% 49
\BOOKMARK [2][-]{subsection.5.1.2}{Specificity}{section.5.1}% 50
\BOOKMARK [2][-]{subsection.5.1.3}{Effect Size}{section.5.1}% 51
\BOOKMARK [1][-]{section.5.2}{Future Work}{chapter.5}% 52
\BOOKMARK [2][-]{subsection.5.2.1}{In-game Timeline Features}{section.5.2}% 53
\BOOKMARK [2][-]{subsection.5.2.2}{Implementation of Reinforcement Learning Framework}{section.5.2}% 54
\BOOKMARK [2][-]{subsection.5.2.3}{Longitudinal Survey}{section.5.2}% 55
\BOOKMARK [2][-]{subsection.5.2.4}{Pre-match Team Analysis for Subpar Composition Detection}{section.5.2}% 56
\BOOKMARK [2][-]{subsection.5.2.5}{Matchmaking Algorithm Balance Adjustments}{section.5.2}% 57
\BOOKMARK [2][-]{subsection.5.2.6}{Application to Other Video Games}{section.5.2}% 58
\BOOKMARK [2][-]{subsection.5.2.7}{Application to Gambling}{section.5.2}% 59
\BOOKMARK [2][-]{subsection.5.2.8}{Application to Other Areas}{section.5.2}% 60
\BOOKMARK [0][-]{chapter.6}{Appendices}{}% 61
\BOOKMARK [1][-]{section.6.1}{Feature List}{chapter.6}% 62
\BOOKMARK [1][-]{section.6.2}{Partial Composition Submodel Accuracies}{chapter.6}% 63
\BOOKMARK [1][-]{section.6.3}{TensorFlow Computational Graph}{chapter.6}% 64
\BOOKMARK [1][-]{section.6.4}{Extra graphs}{chapter.6}% 65
\BOOKMARK [0][-]{chapter*.55}{Bibliography}{}% 66
