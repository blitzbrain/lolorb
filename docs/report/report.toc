\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Contributions}{8}{section.1.2}
\contentsline {chapter}{\numberline {2}Background}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Internet Gaming Addiction}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Nudge Theory}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Psychological Momentum and Tilt}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Multiplayer Online Battle Arenas}{14}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}League of Legends Game Structure}{15}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Win Prediction and Artificial Intelligence in MOBAs}{17}{section.2.5}
\contentsline {section}{\numberline {2.6}Learning Techniques}{18}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Item Imputation}{18}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Sequential Floating Feature Selection}{19}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Principal Component Analysis}{19}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}Logistic Regression}{21}{subsection.2.6.4}
\contentsline {subsection}{\numberline {2.6.5}Support Vector Classification}{22}{subsection.2.6.5}
\contentsline {subsection}{\numberline {2.6.6}Deep Learning}{23}{subsection.2.6.6}
\contentsline {subsubsection}{Operations}{24}{section*.9}
\contentsline {subsubsection}{Regularisation}{24}{section*.10}
\contentsline {subsubsection}{Optimisation}{24}{section*.11}
\contentsline {subsubsection}{Recurrent Networks}{25}{section*.12}
\contentsline {subsection}{\numberline {2.6.7}Multi-Task Learning}{26}{subsection.2.6.7}
\contentsline {subsection}{\numberline {2.6.8}Transfer Learning}{26}{subsection.2.6.8}
\contentsline {subsection}{\numberline {2.6.9}Reinforcement Learning}{27}{subsection.2.6.9}
\contentsline {chapter}{\numberline {3}Methodology}{28}{chapter.3}
\contentsline {section}{\numberline {3.1}Player Performance Modelling}{28}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Model Inputs}{29}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Feature Engineering}{30}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Engineered Features}{31}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Feature Selection}{31}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Win Prediction Model}{32}{subsection.3.1.5}
\contentsline {subsubsection}{Automatic Logarithmic Scaling}{33}{section*.18}
\contentsline {subsection}{\numberline {3.1.6}Partial Composition Heuristics}{34}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Transfer Learning for Professional Prediction}{35}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}Influence and Momentum Models}{36}{subsection.3.1.8}
\contentsline {section}{\numberline {3.2}Request Server and Training Loop}{36}{section.3.2}
\contentsline {section}{\numberline {3.3}Team Composition User Interface}{37}{section.3.3}
\contentsline {section}{\numberline {3.4}Momentum App}{38}{section.3.4}
\contentsline {section}{\numberline {3.5}Adaptive Nudge Strategy}{39}{section.3.5}
\contentsline {chapter}{\numberline {4}Experiments}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Data Collection}{41}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Data Sources}{41}{subsection.4.1.1}
\contentsline {subsubsection}{Riot Games API}{41}{section*.22}
\contentsline {subsubsection}{Blitz.gg and Champion.gg APIs}{42}{section*.23}
\contentsline {subsubsection}{op.gg web scraping and AJAX API}{42}{section*.24}
\contentsline {subsection}{\numberline {4.1.2}Data Crawling}{43}{subsection.4.1.2}
\contentsline {subsubsection}{Requirements}{43}{section*.25}
\contentsline {subsubsection}{Collected match data format}{43}{section*.26}
\contentsline {subsubsection}{Crawler Specification}{44}{section*.27}
\contentsline {section}{\numberline {4.2}Feature Extraction}{45}{section.4.2}
\contentsline {section}{\numberline {4.3}Model Selection}{45}{section.4.3}
\contentsline {section}{\numberline {4.4}Win Prediction Model Evaluation}{45}{section.4.4}
\contentsline {section}{\numberline {4.5}Feature Importances}{47}{section.4.5}
\contentsline {section}{\numberline {4.6}Professional Model}{48}{section.4.6}
\contentsline {section}{\numberline {4.7}Influence and Momentum Conditioning}{50}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Training}{50}{subsection.4.7.1}
\contentsline {subsubsection}{Minibatching method}{50}{section*.36}
\contentsline {subsubsection}{Optimisation}{50}{section*.37}
\contentsline {subsection}{\numberline {4.7.2}Evaluation}{51}{subsection.4.7.2}
\contentsline {chapter}{\numberline {5}Conclusions}{56}{chapter.5}
\contentsline {section}{\numberline {5.1}Limitations}{56}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Data Resolution}{56}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Specificity}{56}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Effect Size}{57}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Future Work}{57}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}In-game Timeline Features}{57}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Implementation of Reinforcement Learning Framework}{58}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Longitudinal Survey}{58}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Pre-match Team Analysis for Subpar Composition Detection}{58}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Matchmaking Algorithm Balance Adjustments}{58}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}Application to Other Video Games}{59}{subsection.5.2.6}
\contentsline {subsection}{\numberline {5.2.7}Application to Gambling}{59}{subsection.5.2.7}
\contentsline {subsection}{\numberline {5.2.8}Application to Other Areas}{59}{subsection.5.2.8}
\contentsline {chapter}{\numberline {6}Appendices}{60}{chapter.6}
\contentsline {section}{\numberline {6.1}Feature List}{60}{section.6.1}
\contentsline {section}{\numberline {6.2}Partial Composition Submodel Accuracies}{65}{section.6.2}
\contentsline {section}{\numberline {6.3}TensorFlow Computational Graph}{66}{section.6.3}
\contentsline {section}{\numberline {6.4}Extra graphs}{67}{section.6.4}
\contentsline {chapter}{Bibliography}{69}{chapter*.55}
