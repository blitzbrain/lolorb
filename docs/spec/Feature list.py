  $Global stats for the chosen champion (in the current role)$
  \indent $Stats for today, very recent trends in the meta$
  \lit*{opgg_eloavg_champion_today_win_rate}
  \lit*{opgg_eloavg_champion_today_gold}
  \lit*{opgg_eloavg_champion_today_creep_score}
  \indent $Stats for this patch (stable trends based on the game version)$
  \lit*{chgg_eloavg_champion_win_rate}
  \lit*{chgg_eloavg_champion_play_rate}
  \lit*{chgg_eloavg_champion_kills}
  \lit*{chgg_eloavg_champion_deaths}
  \lit*{chgg_eloavg_champion_assists}
  \lit*{chgg_eloavg_champion_kill_sprees}
  \lit*{chgg_eloavg_champion_gold}
  \lit*{chgg_eloavg_champion_total_damage_taken}
  \lit*{chgg_eloavg_champion_total_heal}
  \lit*{chgg_eloavg_champion_wards_placed}
  \lit*{chgg_eloavg_champion_wards_killed}
  \lit*{chgg_eloavg_champion_total_damage}
  \lit*{chgg_eloavg_champion_total_magic_damage}
  \lit*{chgg_eloavg_champion_total_physical_damage}
  \lit*{chgg_eloavg_champion_total_true_damage}
  \indent $Since these are specific features for each role, the late game potential of the chosen champion is normalized by position$
  \lit*{chgg_eloavg_champion_duration_0_to_15_games_fraction}
  \lit*{chgg_eloavg_champion_duration_15_to_20_games_fraction}
  \lit*{chgg_eloavg_champion_duration_20_to_25_games_fraction}
  \lit*{chgg_eloavg_champion_duration_25_to_30_games_fraction}
  \lit*{chgg_eloavg_champion_duration_30_to_35_games_fraction}
  \lit*{chgg_eloavg_champion_duration_35_to_40_games_fraction}
  \lit*{chgg_eloavg_champion_duration_40_plus_games_fraction}
  \indent $The same is true here, but maybe representative when a balance of early and late game potential is needed$
  \lit*{chgg_eloavg_champion_duration_0_to_15_win_rate}
  \lit*{chgg_eloavg_champion_duration_15_to_20_win_rate}
  \lit*{chgg_eloavg_champion_duration_20_to_25_win_rate}
  \lit*{chgg_eloavg_champion_duration_25_to_30_win_rate}
  \lit*{chgg_eloavg_champion_duration_30_to_35_win_rate}
  \lit*{chgg_eloavg_champion_duration_35_to_40_win_rate}
  \lit*{chgg_eloavg_champion_duration_40_plus_win_rate}
  \indent $Global stats for the composition matchup (in the current role)$
  \lit*{chgg_eloavg_matchup_wins}
  \lit*{chgg_eloavg_matchup_win_rate}
  \lit*{chgg_eloavg_matchup_gold}
  \lit*{chgg_eloavg_matchup_creep_score}
  \lit*{chgg_eloavg_matchup_total_damage_dealt_to_champions}
  \lit*{chgg_eloavg_matchup_weighed_score}

  $Player\,\, recent\,\, game\,\, performance\,\, on\,\, their\,\, chosen\,\, champion\,\, (average\,\, for\,\, last\,\, 2\,\, seasons).$
  $Since\,\, this\,\, feature\,\, is\,\, not\,\, normalized\,\, for\,\, the\,\, champion,\,\, this\,\, is\,\, essentially\,\, their\,\, performance\,\, prediction\,\, for\,\, this\,\, game$
  \lit*{opgg_champion_recent_games} \\
  \lit*{opgg_champion_recent_wins} \\
  \lit*{opgg_champion_recent_losses} \\
  \lit*{opgg_champion_recent_bayes_win_rate} \\
  \lit*{opgg_champion_recent_alltotal_wins} \\
  \lit*{opgg_champion_recent_alltotal_losses} \\
  \lit*{opgg_champion_recent_alltotal_bayes_win_rate} \\
  \lit*{opgg_champion_recent_mean_rank} \\
  \lit*{opgg_champion_recent_kills} \\
  \lit*{opgg_champion_recent_deaths} \\
  \lit*{opgg_champion_recent_assists} \\
  \lit*{opgg_champion_recent_gold} \\
  \lit*{opgg_champion_recent_creep_score} \\
  \lit*{opgg_champion_recent_damage_taken} \\

  $The\,\, previous\,\, group\,\, of\,\, features,\,\, normalized\,\, by\,\, the\,\, global\,\, average\,\, (representing\,\, how\,\, good\,\, this\,\, play\,\, is\,\, at\,\, the\,\, champion\,\, he's\,\, chosen)$
  \lit*{opgg_champion_recent_kills_/_opgg_champion_recent_kills} \\
  \lit*{opgg_champion_recent_deaths_/_opgg_champion_recent_deaths} \\
  \lit*{opgg_champion_recent_assists_/_opgg_champion_recent_assists} \\
  \lit*{opgg_champion_recent_gold_/_opgg_eloavg_champion_today_gold} \\
  \lit*{opgg_champion_recent_creep_score_/_opgg_champion_recent_creep_score} \\
  \lit*{opgg_champion_recent_damage_taken_/_chgg_eloavg_champion_total_damage_taken} \\




  $How\,\, long\,\, this\,\, player's\,\, recent\,\, games\,\, have\,\, lasted,\,\, on\,\, average\,\, (longer\,\, is\,\, better\,\, -\,\, more\,\, carrying\,\, losing\,\, teams,\,\, less\,\, solo\,\, losing\,\, a\,\, game)$
  \lit*{ravg_duration} \\
  $Time\,\, since\,\, the\,\, recent\,\, matches\,\, (less\,\, is\,\, better\,\, -\,\, fresh\,\, experience)$
  \lit*{ravg_time_since_match} \\

  $Performance\,\, stats\,\, for\,\, recent\,\, games$
  \lit*{ravg_win} \\
  \lit*{ravg_kills} \\
  \lit*{ravg_deaths} \\
  \lit*{ravg_assists} \\
  \lit*{ravg_creep_score} \\
  \lit*{ravg_kill_participation} \\
  \lit*{ravg_pinks_purchased} \\

  $How\,\, good\,\, this\,\, player\,\, is\,\, (last\,\, 2\,\, seasons)\,\, at\,\, the\,\, champions\,\, he's\,\, been\,\, playing\,\, recently$
  \lit*{ravg_opgg_champion_recent_games} \\
  \lit*{ravg_opgg_champion_recent_wins} \\
  \lit*{ravg_opgg_champion_recent_losses} \\
  \lit*{ravg_opgg_champion_recent_bayes_win_rate} \\
  \lit*{ravg_opgg_champion_recent_mean_rank} \\

  $How\,\, good\,\, the\,\, champions\,\, that\,\, this\,\, player\,\, is\,\, playing\,\, are\,\, in\,\, the\,\, current\,\, meta$
  \lit*{ravg_opgg_eloavg_champion_today_win_rate} \\
  \lit*{ravg_chgg_eloavg_champion_win_rate} \\

  $How\,\, good\,\, this\,\, player\,\, is\,\, at\,\, the\,\, champions\,\, he's\,\, been\,\, playing\,\, recently\,\, compared\,\, to\,\, how\,\, well\,\, he\,\, normally\,\, plays\,\, them$
  \lit*{ravg_kills_/_opgg_champion_recent_kills} \\
  \lit*{ravg_deaths_/_opgg_champion_recent_deaths} \\
  \lit*{ravg_assists_/_opgg_champion_recent_assists} \\
  \lit*{ravg_creep_score_/_opgg_champion_recent_creep_score} \\

  $How\,\, good\,\, this\,\, player\,\, is\,\, at\,\, the\,\, champions\,\, he's\,\, been\,\, playing\,\, in\,\, the\,\, last\,\, 2\,\, seasons\,\, compared\,\, to\,\, the\,\, global\,\, average\,\, for\,\, these\,\, champions$
  \lit*{ravg_opgg_champion_recent_gold_/_opgg_eloavg_champion_today_gold} \\
  \lit*{ravg_opgg_champion_recent_damage_taken_/_chgg_eloavg_champion_total_damage_taken} \\

  $How\,\, good\,\, this\,\, player\,\, is\,\, at\,\, the\,\, champions\,\, he's\,\, been\,\, playing\,\, recently\,\, compared\,\, to\,\, the\,\, global\,\, average\,\, for\,\, these\,\, champions$
  \lit*{ravg_kills_/_chgg_eloavg_champion_kills} \\
  \lit*{ravg_deaths_/_chgg_eloavg_champion_deaths} \\
  \lit*{ravg_assists_/_chgg_eloavg_champion_assists} \\
  \lit*{ravg_creep_score_/_opgg_eloavg_champion_today_creep_score} \\
  \lit*{ravg_pinks_purchased_/_chgg_eloavg_champion_wards_placed} \\
  \lit*{ravg_pinks_purchased_/_chgg_eloavg_champion_wards_killed} \\
