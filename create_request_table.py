#
#  Create postgres request cache table
#


from sqlalchemy import Column, BigInteger, Text, DateTime
from sqlalchemy.dialects.postgresql import JSON, JSONB
import sqlalchemy

from ServerConstants import *


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()  
meta = sqlalchemy.MetaData(engine)

try:
    rcache = sqlalchemy.Table('rcache', meta, autoload=True, autoload_with=engine)
    rcache.drop()
    print("dropped")
except sqlalchemy.exc.NoSuchTableError as e:
    pass

meta = sqlalchemy.MetaData(engine)

sqlalchemy.Table("rcache", meta,  
    Column('request_hash', Text, primary_key=True),
    Column('shortlink', Text),
    Column('type', Text),
    Column('timestamp', BigInteger),
    Column('expiry_timestamp', BigInteger),
    # Column('composition', JSON), // stored as 10th member of result
    Column('rorder', JSON),
    Column('result', JSON),
)

meta.create_all()
engine.close()
db.dispose()


