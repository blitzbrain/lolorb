#
# Sequential backwards selection to get final features (print output)
#


import warnings

from new_model import *



if __name__ == "__main__":


    # Load pre-filtered dataset if command line argument passed. Otherwise, filter data
    load_data = sys.argv[1] == "-load" if len(sys.argv) == 2 else False

    X_, Y_, Y_tr, Y_te, X_ls, n_train, n_test = [None] * 7
    if load_data:
        pr_fl("Loading final_selection_data...")
        X_, Y_, Y_tr, Y_te, X_ls, n_train, n_test = load_ld("final_selection_data")

    else:

        ### Script options ###

        combine_players = True
        combine_teams = True
        combine_history = True

        n_train = 50000
        n_test = 14999
        n_pcal = 1


        X, Y, X_labels = get_final_features()
        X_train, Y_train, X_pcal, Y_pcal = X[:n_train], Y[:n_train], X[n_train:n_train + n_pcal], Y[n_train:n_train + n_pcal]
        X_test, Y_test = X[-n_test:], Y[-n_test:]
        X_ls = X_labels
        # X_train.shape, X_pcal.shape, X_test.shape


        # Combine features using mean for each team (makes selection faster by reducing number of features by a factor of ~5)
        pr_fl("Reducing features by mean...")
        if combine_players:
            new_xls = ["blue_" + l[9:] if l[:9] == "blue_top_" else "red_" + l[8:] if l[:8] == "red_top_" else l for l in X_ls if (
                '_'.join(l.split('_')[:2]) not in t_roles) or l[:9] == "blue_top_" or l[:8] == "red_top_"]
            new_X = np.zeros((X_train.shape[0], len(new_xls)))
            new_X_test = np.zeros((X_test.shape[0], len(new_xls)))
            for i in range(len(new_xls)):
                l = new_xls[i]
                
                if l not in X_ls:
                    col = l.split('_')[0]
                    l_trunc = '_'.join(l.split('_')[1:])
                    new_X[:, i] = np.mean(X_train[:, 
                        [X_ls.index(l_) for l_ in [col + '_' + r + '_' + l_trunc for r in roles_all]]], axis=1)
                    new_X_test[:, i] = np.mean(X_test[:,
                        [X_ls.index(l_) for l_ in [col + '_' + r + '_' + l_trunc for r in roles_all]]], axis=1)
                else:
                    new_X[:, i] = X_train[:, X_ls.index(l)]
                    new_X_test[:, i] = X_test[:, X_ls.index(l)]
            X_train = new_X
            X_test = new_X_test
            X_ls = new_xls
            # X_train.shape, X_test.shape


        # Combine recent match features for each team, into one time period instead of 5
        if combine_history:
            prev_nonrec_ls = [l for l in X_ls if "ravg_" not in l]
            # new_xls = ['ravg_' + '_'.join(l.split('_')[1:]) for l in X_ls if "1ravg_" in l]
            new_xls = ['_'.join(l.split('ravg_')[0].split('_')[:-1]) + '_ravg_' + l.split('ravg_')[1] for l in X_ls if "1ravg_" in l]
            new_X = np.zeros((X_train.shape[0], len(new_xls)))
            new_X_test = np.zeros((X_test.shape[0], len(new_xls)))
            rec_ns = [1,2,4,8,20]
            for i in range(len(new_xls)):
                l = new_xls[i]
                new_X[:, i] = np.mean(X_train[:, 
                    [X_ls.index(l.replace('ravg_', str(n) + 'ravg_')) for n in rec_ns]], axis=1)
                new_X_test[:, i] = np.mean(X_test[:, 
                    [X_ls.index(l.replace('ravg_', str(n) + 'ravg_')) for n in rec_ns]], axis=1)
            X_train = np.hstack([X_train[:, [X_ls.index(l) for l in prev_nonrec_ls]], new_X])
            X_test = np.hstack([X_test[:, [X_ls.index(l) for l in prev_nonrec_ls]], new_X_test])
            X_ls = prev_nonrec_ls + new_xls
            # X_train.shape, X_test.shape


        # Combine features for both teams into one by subtraction
        if combine_teams:
            prev_nonteam_ls = [l for l in X_ls if l.split('_')[0] not in cols_all]
            new_xls = ['_'.join(l.split('_')[1:]) for l in X_ls if l.split('_')[0] == "blue"]
            new_X = np.zeros((X_train.shape[0], len(new_xls)))
            new_X_test = np.zeros((X_test.shape[0], len(new_xls)))
            for i in range(len(new_xls)):
                l = new_xls[i]
                new_X[:, i] = X_train[:, X_ls.index("blue_" + l)] - X_train[:, X_ls.index("red_" + l)]
                new_X_test[:, i] = X_test[:, X_ls.index("blue_" + l)] - X_test[:, X_ls.index("red_" + l)]
            X_train = np.hstack([X_train[:, [X_ls.index(l) for l in prev_nonteam_ls]], new_X])
            X_test = np.hstack([X_test[:, [X_ls.index(l) for l in prev_nonteam_ls]], new_X_test])
            X_ls = prev_nonteam_ls + new_xls
            # X_train.shape, X_test.shape


        # Robust scaling
        pr_fl("Normalizing features (zero mean, unit variance)...")
        scaler = RobustScaler()
        X_tr_ = scaler.fit_transform(X_train)
        X_te_ = scaler.transform(X_test)


        # Use only a subset of data to make selection faster
        n_train = n_train
        n_test = n_test
        X_tr = X_tr_[:n_train]
        X_te = X_te_[:n_test]
        Y_tr = Y_train[:n_train]
        Y_te = Y_test[:n_test]
        X_ = np.vstack([X_tr, X_te])

        Y_ = np.vstack([Y_tr, Y_te])
        # X_.shape, Y_.shape


    # Save data for later outsourced computation
    if not load_data:
        pr_fl("Saving filtered data...")
        save_ld((X_, Y_, Y_tr, Y_te, X_ls, n_train, n_test), "final_selection_data")
        sys.exit()


    ### Model training/feature selection ###

    # m = get_model("LogisticRegression")
    # if isinstance(m, MultiOutputClassifier):
    #     m = m.estimator

    # m = LogisticRegression(solver='liblinear', C=2.783, tol=7e-6)
    # m = LogisticRegression(solver='saga', C=2.783, max_iter=100, tol=7e-6, penalty='l1')
    # m = LogisticRegression(solver='newton-cg', C=50.783, max_iter=6, tol=1e-6)
    # m = LogisticRegressionCV(solver='lbfgs', Cs=1, max_iter=1000, tol=7e-6)
    m = LogisticRegression(solver='lbfgs', C=2.783, max_iter=300, tol=1e-5)
    # m = LinearSVC()

    # m = Pipeline(steps=[
    # #     ("select", SelectPercentile(percentile=70, score_func=f_classif)),
    #     ("reduce", IncrementalPCA(n_components=500)),
    #     ("model", m),
    # ])

    selector = SFS(estimator=m, k_features=(1, X_.shape[1]), forward=False, floating=True, n_jobs=n_parallel_cpu, verbose=5)
    # selector = SelectKBest(k=7500, score_func=mutual_info_classif)
    # selector = SelectKBest(k=7900, score_func=f_classif)
    # selector = FactorAnalysis(n_components=100)
    # selector = SelectKBest(k=100, score_func=f_classif)
    # selector = SelectPercentile(percentile=50, score_func=f_classif)

    pr_fl("Selecting features...")
    X_s = X_
    error = False
    try:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            X_s = selector.fit_transform(X_, Y_.flatten())

    except:
        error = True
        # for line in traceback.format_tb(e.__traceback__):
        #     pr_fl(line)
        # pr_fl(type(e))
        # pr_fl(e)
    finally:

        if not error:
            X_train_ = X_s[:n_train]
            X_test_ = X_s[-n_test:]

            # m = FeatSelectWrapper(m, percentiles=[50])

            m.fit(X_train_, Y_tr.flatten())
            preds = m.predict(X_test_) # Test accuracy
            score = np.mean(preds == Y_te.flatten()) * 100
            pr_fl("### FINAL SCORE: ")
            pr_fl(score)
            # pr([X_ls[i] for i in m.inds])

        print()
        pr_fl("### SUBSETS: ")
        pr(selector.subsets_)
        print()
        pr_fl("### FEATURES: ")
        pr([(n, selector.subsets_[n]['avg_score'], [X_ls[i] for i in selector.subsets_[n]['feature_idx']]) for n in selector.subsets_])


