#
#  Refresh server script (git pull and restart)
#


import os

# os.chdir("lolorb/")


from Monitoring import *


os.chdir("../")
for host in hosts:
    subprocess.Popen("ssh -i " + host["pem"] + " -o StrictHostKeyChecking=no ubuntu@" + host["addr"] + \
        " \"" + worker_pyex + " ~/lolorb/refresh_minions.py\"",
        close_fds=True, shell=True, creationflags=DETACHED_PROCESS)


