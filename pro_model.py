import warnings
warnings.filterwarnings("ignore")

from Server import *

leagues_list = deepcopy(pro_leagues)


# Options

perf_patch = '10.1'      # Earliest patch to analyse pro history for
n_max_feats = 150       # Sequential feature selection options
n_min_feats = 1
# n_min_feats = 0.667
forward = True
floating = True
cv_k_folds = 3          # Cross validation options
cv_iter = 3
n_pca_thresh = 5        # Min. number of features to use PCA in feature selection
val_set_ln = 15         # Number of validation set games for each league (60 total)


extract_feats = sys.argv[1] == "--extract" if len(sys.argv) >= 2 else False # otherwise, load_features
# exclude_series = len(sys.argv) >= 2 and "--no_series" in sys.argv
train_nsqo = len(sys.argv) >= 2 and "--nsqo" in sys.argv
train_sqo = len(sys.argv) >= 2 and "--sqo" in sys.argv
select_sqo = len(sys.argv) >= 2 and "--sqo_select" in sys.argv
select_sqpco = len(sys.argv) >= 2 and "--sqpco_select" in sys.argv

if extract_feats:
    model_dict, model_strs, scales, n_train, n_test, n_pcal = load_ld(pro_sq_models_fn)
    _, _, full_5_centers, full_5_scales, full_5_coefs = model_dict[(5, 5, 5, 5, 5, 5, 5)][6]
    m5__ = model_dict[(5, 5, 5, 5, 5, 5, 5)]
    f5_x_ls = m5__[0]
    mkey = list(full_5_centers.keys())[0]
    f5_center_, f5_scale_ = full_5_centers[mkey], full_5_scales[mkey]
    # f5_center_, f5_scale_, f5_coef_ = full_5_centers[mkey], full_5_scales[mkey], full_5_coefs[mkey]
    m5__ = m5__[1][0]
    if isinstance(m5__, Pipeline):
        m5__ = m5__.steps[1][1]
    coef_vec = m5__.coef_.flatten()
    f5_coef_ = OrderedDict(zip(f5_x_ls, list(coef_vec)))

    data = load_ld("pro_d")
    pr_fl(len(data))

    # Load odds records
    # odds_data = pd.read_csv(data_dir + 'odds/odds.csv', index_col=0)
    # pr((odds_data.loc[1390213]['bd'], type(odds_data.loc[1390213]['bd'])))
    # odds = odds_data.loc[1390213]
    # pr((odds['bd'], type(odds['bd'])))
    # sys.exit()

    _ = load_global_stats()

    # # Temporary: set patch 9.15 games to 9.14 for rest of code to work
    # for i in range(len(data)):
    #     if data[i]['meta']['patch'] == '9.15' or data[i]['meta']['patch'] == '9.16':
    #         data[i]['meta']['patch'] = '9.14'

    # # First, filter out games with too many unknown solo queue names, or no solo queue data
    # data = [d for d in data if d["meta"]["n_sqnames_missing"] < 1]
    # pr_fl(len(data))
    # data = [d for d in data if 'X' in d]
    # pr_fl(len(data))

    # Specific league models
    # data = [d for d in data if d['meta']['league'] in pro_leagues]

    # Filter out games that were too long ago
    # data = [d for d in data if d["meta"]["timestamp"] < n_weeks_ago(5, milliseconds=False)]
    patches_list = []
    for i in range(len(patches_all)):
        patch = patches_all[i]
        patches_list.append(patch)
        if patch == earliest_patch:
            break
    patches_list = patches_list[::-1]
    patches_set = set(patches_list)
    perf_patches_list = []
    for i in range(len(patches_all)):
        patch = patches_all[i]
        perf_patches_list.append(patch)
        if patch == perf_patch:
            break
    perf_patches_list = perf_patches_list[::-1]
    perf_patches_set = set(perf_patches_list)
    # pr_fl(len(data))

    data = [d for d in data if d['meta']['patch'] in perf_patches_set]
    pr_fl(len(data))


    # Filter out game(s) with some missing features
    contain_nan = [
        'visionwardbuys',
        'fbassist',
        'doubles',
        'triples',
        'csat10',
        'goldat10',
        'xpat10',
    ]
    for l in contain_nan:
        data = [d for d in data if not any(np.isnan(pl[l]) if type(pl[l]) is np.float64 \
                                else pl[l] == ' ' or pl[l] is None for pl in d["players"])]
        pr_fl(len(data))


    # Get list of unique players
    pl_list = set()
    for d in data:
        for p in d["players"]:
            pl_list.add(p["player"])
    pl_list = list(pl_list)
    pr_fl("# unique players:" + str(len(pl_list)))

    def parsePBool(v): # Parse a numerical value that might also be a boolean represented as a string (T -> 1, F -> 0)
        if isinstance(v, str):
            return parseBool(v)
        return v

    # Create arrays to store gameid,player,role,league,patch_in_perfpatches,game_performance entries
    pro_correspondence = deepcopy(pro_correspondence_ratios)
    for l in pro_correspondence:
        key = pro_correspondence[l]
        pro_correspondence[l] = {
            role: (abs(f5_coef_['blue_'+role+'_'+key]) + abs(f5_coef_['red_'+role+'_'+key])) / 2
              for role in roles_all
        }
        pro_correspondence[l] = {
            role: {side: abs(f5_coef_[side + '_' + role + '_' + key]) / pro_correspondence[l][role]
              for side in cols_all} for role in roles_all
        }
    pl_perfs_meta = np.zeros((len(data) * 10, 5))
    n_perf_feats = len(pro_full_player_labels)
    pl_perfs = np.zeros((len(data) * 10, n_perf_feats))
    pl_perfs_i = 0
    gid_list = [d["meta"]["gameid"] for d in data]
    nans = 0
    for d in data:
        gid = d['meta']['gameid']
        patch = d["meta"]["patch"]
        if patch not in patches_all:
            patch = patches_all[0]
        league = pro_leagues.index(d['meta']['league'])
        for i in range(10):
            pl = d["players"][i]
            r = pro_role_ord[i]
            role = r.split('_')[1]
            side = r.split('_')[0]
            opp_r = cols_all[1 - cols_all.index(side)] + '_' + role
            opp_pl = d["players"][pro_role_ord.index(opp_r)]
            opp_cid = opp_pl["champion"]
            name = pl["player"]
            cid = pl["champion"]
            team = d[side + "_team"]
            
            # Get global stats for the matchup
            ms = get_chgg_gms(patch_tss[patch] * 1000, cid, len(chgg_elos) - 1, role, opp_cid)
            
            pl_perfs_meta[pl_perfs_i] = [gid_list.index(gid), pl_list.index(name), roles_all.index(role), league,
                                         int(d["meta"]["patch"] in perf_patches_set)]
            pl_perfs[pl_perfs_i] = [float(pl[l]) for l in X_pro_player_labels] + \
                                   [float(pl[l]) * pro_correspondence[l][role][side] for l in pro_correspondence] + \
                [float(pl[l]) / (ms['u'][pro_gms_ratios[l]] if ms['u'][pro_gms_ratios[l]] > 0 else 1.0) for l in pro_gms_ratios] + \
                                   [np.nan_to_num(float(parsePBool(team[l]))) for l in pro_team_player_labels]
    #         if np.any(np.isnan(pl_perfs[pl_perfs_i])):
    #             pr((pl_perfs[pl_perfs_i]))
    #             if i == 0:
    #                 nans += 1
            
            pl_perfs_i += 1
    pr_fl(pl_perfs_meta.shape, pl_perfs.shape, nans)



    # Construct final data array - encode categoricals with index
    sqo_labels = []
    for d in data:
        if 'X' in d:
            X_ls = d["X"]["X_labels"]
            if len(X_ls) > len(sqo_labels):
                sqo_labels = X_ls
    # sqo_labels = [l for l in sqo_labels if '_' in l]
    sqo_labels = deepcopy(f5_x_ls)
    sqo_labels = [l for l in sqo_labels if '1ravg_' not in l and "2ravg_" not in l]
    pr_fl(len(sqo_labels))
    f5_center = np.asarray([f5_center_[l] for l in sqo_labels])
    f5_scale = np.asarray([f5_scale_[l] for l in sqo_labels])
    f5_coef = np.asarray([f5_coef_[l] for l in sqo_labels])
    sqo_pl_labels = ['_'.join(l.split('_')[2:]) for l in sqo_labels if l[:8] == "blue_top"]
    sqo_bot_labels = ['_'.join(l.split('_')[1:]) for l in sqo_labels if l[:5] == "blue_" and l.split('_')[1] not in roles_all]
    role_league_perf_cache = {}
    for league_i in range(len(leagues_list)):
        for ri in range_5:
            onehot = np.all(pl_perfs_meta[:, [2,3,4]] == [ri, league_i, 1], axis=1)
            if sum(onehot) == 0:
                if leagues_list[league_i] != 'MSI':
                    pr_fl("Error: no pro historical stats for", leagues_list[league_i], roles_all[ri])
                onehot = np.ones(onehot.shape)
            role_league_perf_cache[str(ri) + '_' + str(league_i)] = np.mean(pl_perfs[np.nonzero(onehot)[0]], axis=0)
    player_role_perfs = {}
    D = []
    patches_makeup = defaultdict(int)
    leagues_makeup = defaultdict(int)
    missing_makeup = defaultdict(int)
    for d in data:
        x = OrderedDict()
        x['gameid'] = gid = d["meta"]["gameid"]
        x['timestamp'] = gid = d["meta"]["timestamp"]
        x['blue_win'] = d["meta"]["blue_win"]
        x['league'] = leagues_list.index(d["meta"]["league"])
    #     x['duration'] = d["meta"]["duration"]

        x['series_count'] = d["meta"]["series_count"]
        x['series_ingame_time'] = d["meta"]["series_ingame_time"]
        x['series_blue_wins'] = d["meta"]["series_blue_wins"]
        x['series_blue_winstreak'] = d["meta"]["series_blue_winstreak"]
        x['series_red_wins'] = d["meta"]["series_red_wins"]
        x['series_red_winstreak'] = d["meta"]["series_red_winstreak"]

        # Get player-role performances averages, excluding this game
        perfs = {}
        for i in range(10):
            pl = d["players"][i]
            r = pro_role_ord[i]
            
            gid_i = gid_list.index(d["meta"]["gameid"])
            name_i = pl_list.index(pl["player"])
            ri = roles_all.index(r.split('_')[1])
            league_i = x['league']
            
            onehot = np.logical_and(
                np.all(pl_perfs_meta[:, [1,2,4]] == [name_i, ri, 1], axis=1),
                pl_perfs_meta[:, 0] != gid_i)
            res = None
            if sum(onehot) < 3:
                res = role_league_perf_cache[str(ri) + '_' + str(league_i)]
            else:
                res = np.mean(pl_perfs[np.nonzero(onehot)[0]], axis=0)
            perfs[r] = res
            
            pl_r_key = pl["player"] + '_' + str(ri)
            if pl_r_key not in player_role_perfs:
                onehot = np.all(pl_perfs_meta[:, [1,2,4]] == [name_i, ri, 1], axis=1)
                if sum(onehot) < 3:
                    pass
    #                 pr_fl("Less than 3 samples (", sum(onehot), ") for", pl["player"], roles_all[ri])
                else:
                    player_role_perfs[pl_r_key] = np.mean(pl_perfs[np.nonzero(onehot)[0]], axis=0)
        x['performances'] = perfs

        # if exclude_series and x['series_count'] > 0:
        #     continue
        # if exclude_series and int(gid) not in odds_data.index:
        #     continue
        
        if 'X' in d:
            # Get model output
            x['sq_prediction'] = d["X"]["sq_pred"]

            # odds = odds_data.loc[int(gid)]
            # b_odds = float(odds['bd']) / float(odds['bn'])
            # r_odds = float(odds['rd']) / float(odds['rn'])
            # odds_prob = ((1 / (b_odds + 1)) + 1 - (1 / (r_odds + 1))) / 2
            # odds_margin = np.abs((1 / (b_odds + 1)) - (1 - (1 / (r_odds + 1))))
            # x['odds_prob'] = odds_prob
            # x['odds_margin'] = odds_margin

            x_f = d['X']['X'][0]
            x_labels = d['X']['X_labels']
        #     pr_fl(len(x_f), len(x_labels))
            out = np.zeros(len(sqo_labels))
            for l_i in range(len(x_labels)):
                l = x_labels[l_i]
                if l in sqo_labels:
                    sq_li = sqo_labels.index(l)
                    out[sq_li] = ((x_f[l_i] - f5_center[sq_li]) / f5_scale[sq_li]) * f5_coef[sq_li]
            x["sq_output"] = out

        # Add to dataset if criteria are met
    #     if 'X' in d and (("n_missing" in d["X"] and d["X"]["n_missing"] < 1) or \
    #                      ("n_missing" not in d["X"] and d["meta"]["n_sqnames_missing"] < 1)) and d["meta"]["patch"] in patches_set:
        if 'X' in d and ("n_missing" in d["X"] and d["X"]["n_missing"] <= n_max_missing) and d["meta"]["patch"] in patches_set:
            x['patch'] = patches_list.index(d["meta"]["patch"])
            D.append(x)
            patches_makeup[d["meta"]["patch"]] += 1
            leagues_makeup[d["meta"]["league"]] += 1
            missing_makeup[d["X"]["n_missing"]] += 1
    #         pr_fl(d["meta"]["patch"]) # Check the meta info of the data that makes it through the filter (ensure balance)
    #         pr_fl(d["blue_team"]["team"])
    #         pr_fl(d["red_team"]["team"])
    pr_fl(len(D))
    pr((patches_makeup))
    pr((leagues_makeup))
    pr((missing_makeup))





    # Create data arrays for fast indexing during feature selection model training
    noncat_grps = [
        'series_count',
        'series_ingame_time',
        'sq_prediction',
        # 'odds_prob',
        # 'odds_margin'
    ]
    series_wins_grp = [
        'series_blue_wins',
        'series_red_wins',
    ]
    series_winstreak_grp = [
        'series_blue_winstreak',
        'series_red_winstreak',
    ]
    series_grps = [
        "series_wins",
        "series_winstreak",
    ]
    sqo_pl_r_labels = sum([[r + '_' + l for l in sqo_pl_labels] for r in roles_all], [])
    X_pro_player_r_labels = sum([[r + '_' + l for l in pro_full_player_labels] for r in roles_all], [])
    X_pro_player_t_labels = sum([[r + '_' + l for l in pro_full_player_labels] for r in t_roles], [])
    grps_config = \
        sqo_pl_r_labels + sqo_bot_labels + X_pro_player_r_labels
    #     sqo_pl_labels + sqo_bot_labels + X_pro_player_r_labels
    #     ['diff_' + l for l in sqo_pl_r_labels] + \
    #     sqo_bot_labels + ['diff_' + l for l in sqo_bot_labels] + \
    #     X_pro_player_r_labels + ['diff_' + l for l in X_pro_player_r_labels]
    #     sqo_pl_r_labels + ['diff_' + l for l in sqo_pl_r_labels] + \
    X_groups = [
        "league",
        "patch",
    ] + noncat_grps + series_grps + grps_config
    llist = [l for l in leagues_list if l in leagues_makeup]
    plist = [l for l in patches_list if l in patches_makeup]
    leagues_arr = np.zeros((len(D), len(llist)))
    patches_arr = np.zeros((len(D), len(plist)))
    for i in range(len(D)):
        d = D[i]
        leagues_arr[i, llist.index(leagues_list[d["league"]])] = 1
        patches_arr[i, plist.index(patches_list[d["patch"]])] = 1
    X_arrs = [leagues_arr, patches_arr]
    X_arrs += [np.asarray([d[l] for d in D]).reshape(-1, 1) for l in noncat_grps]

    # Current configuration: take difference between sides in selection stage, keep side specific features in final training,
    # and allow selection among roles (no grouping or summing)

    # # X_arrs += [np.asarray([[d[l] for l in series_wins_grp] for d in D])]
    # # X_arrs += [np.asarray([[d[l] for l in series_winstreak_grp] for d in D])]
    # X_arrs += [np.asarray([d["series_blue_wins"] - d["series_red_wins"] for d in D]).reshape(-1, 1)]
    # X_arrs += [np.asarray([d["series_blue_winstreak"] - d["series_red_winstreak"] for d in D]).reshape(-1, 1)]

    # # X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index(r + '_' + l)] for r in t_roles] for d in D])
    # #        for l in sqo_pl_labels]                                                                  # raw, roles grouped
    # X_arrs += sum([[np.asarray([[d["sq_output"][sqo_labels.index(c + '_' + r + '_' + l)] for c in cols_all] for d in D])
    #        for l in sqo_pl_labels] for r in roles_all], [])                                           # raw, roles ungrouped
    # # X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for r in roles_all] for d in D]) - \
    # #            np.asarray([[d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for r in roles_all] for d in D])
    # #         for l in sqo_pl_labels]                                                                 # diff, roles grouped
    # # X_arrs += sum([[np.asarray([d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for d in D]).reshape(-1, 1) - \
    # #                 np.asarray([d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for d in D]).reshape(-1, 1)
    # #         for l in sqo_pl_labels] for r in roles_all], [])                                        # diff, roles ungrouped
    # # X_arrs += [np.sum([np.asarray([d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for d in D]).reshape(-1, 1) - \
    # #                    np.asarray([d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for d in D]).reshape(-1, 1)
    # #         for r in roles_all], axis=0) for l in sqo_pl_labels]                                    # diff, roles summed

    # X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index(c + '_' + l)] for c in cols_all] for d in D]) for l in sqo_bot_labels]
    # # X_arrs += [np.asarray([d["sq_output"][sqo_labels.index('blue_' + l)] for d in D]).reshape(-1, 1) - \
    # #            np.asarray([d["sq_output"][sqo_labels.index('red_' + l)] for d in D]).reshape(-1, 1) for l in sqo_bot_labels]

    # # X_arrs += [np.asarray([[d["performances"][r][i] for r in t_roles] for d in D])
    # #         for i in range(n_perf_feats)]                                               # raw, roles grouped
    # X_arrs += sum([[np.asarray([[d["performances"][c + '_' + r][i] for c in cols_all] for d in D])
    #         for i in range(n_perf_feats)] for r in roles_all], [])                        # raw, roles ungrouped
    # # X_arrs += [np.asarray([[d["performances"]["blue_" + r][i] for r in roles_all] for d in D]) - \
    # #            np.asarray([[d["performances"]["red_" + r][i] for r in roles_all] for d in D])
    # #         for i in range(n_perf_feats)]                                               # diff, roles grouped
    # # X_arrs += sum([[np.asarray([d["performances"]["blue_" + r][i] for d in D]).reshape(-1, 1) - \
    # #                 np.asarray([d["performances"]["red_" + r][i] for d in D]).reshape(-1, 1)
    # #         for i in range(n_perf_feats)] for r in roles_all], [])                      # diff, roles ungrouped
    # # X_arrs += [np.sum([np.asarray([d["performances"]["blue_" + r][i] for d in D]).reshape(-1, 1) - \
    # #                    np.asarray([d["performances"]["red_" + r][i] for d in D]).reshape(-1, 1)
    # #         for r in roles_all], axis=0) for i in range(n_perf_feats)]                  # diff, roles summed

    # old configuration: take difference between sides, allow selection among roles (no grouping or summing)

    # X_arrs += [np.asarray([[d[l] for l in series_wins_grp] for d in D])]
    # X_arrs += [np.asarray([[d[l] for l in series_winstreak_grp] for d in D])]
    X_arrs += [np.asarray([d["series_blue_wins"] - d["series_red_wins"] for d in D]).reshape(-1, 1)]
    X_arrs += [np.asarray([d["series_blue_winstreak"] - d["series_red_winstreak"] for d in D]).reshape(-1, 1)]

    # X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index(r + '_' + l)] for r in t_roles] for d in D])
    #        for l in sqo_pl_labels]                                                                  # raw, roles grouped
    # X_arrs += sum([[np.asarray([[d["sq_output"][sqo_labels.index(c + '_' + r + '_' + l)] for c in cols_all] for d in D])
    #        for l in sqo_pl_labels] for r in roles_all], [])                                           # raw, roles ungrouped
    # X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for r in roles_all] for d in D]) - \
    #            np.asarray([[d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for r in roles_all] for d in D])
    #         for l in sqo_pl_labels]                                                                 # diff, roles grouped
    X_arrs += sum([[np.asarray([d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for d in D]).reshape(-1, 1) - \
                    np.asarray([d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for d in D]).reshape(-1, 1)
            for l in sqo_pl_labels] for r in roles_all], [])                                        # diff, roles ungrouped
    # X_arrs += [np.sum([np.asarray([d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for d in D]).reshape(-1, 1) - \
    #                    np.asarray([d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for d in D]).reshape(-1, 1)
    #         for r in roles_all], axis=0) for l in sqo_pl_labels]                                    # diff, roles summed

    # X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index(c + '_' + l)] for c in cols_all] for d in D]) for l in sqo_bot_labels]
    X_arrs += [np.asarray([d["sq_output"][sqo_labels.index('blue_' + l)] for d in D]).reshape(-1, 1) - \
               np.asarray([d["sq_output"][sqo_labels.index('red_' + l)] for d in D]).reshape(-1, 1) for l in sqo_bot_labels]

    # X_arrs += [np.asarray([[d["performances"][r][i] for r in t_roles] for d in D])
    #         for i in range(n_perf_feats)]                                               # raw, roles grouped
    # X_arrs += sum([[np.asarray([[d["performances"][c + '_' + r][i] for c in cols_all] for d in D])
    #         for i in range(n_perf_feats)] for r in roles_all], [])                        # raw, roles ungrouped
    # X_arrs += [np.asarray([[d["performances"]["blue_" + r][i] for r in roles_all] for d in D]) - \
    #            np.asarray([[d["performances"]["red_" + r][i] for r in roles_all] for d in D])
    #         for i in range(n_perf_feats)]                                               # diff, roles grouped
    X_arrs += sum([[np.asarray([d["performances"]["blue_" + r][i] for d in D]).reshape(-1, 1) - \
                    np.asarray([d["performances"]["red_" + r][i] for d in D]).reshape(-1, 1)
            for i in range(n_perf_feats)] for r in roles_all], [])                      # diff, roles ungrouped
    # X_arrs += [np.sum([np.asarray([d["performances"]["blue_" + r][i] for d in D]).reshape(-1, 1) - \
    #                    np.asarray([d["performances"]["red_" + r][i] for d in D]).reshape(-1, 1)
    #         for r in roles_all], axis=0) for i in range(n_perf_feats)]                  # diff, roles summed




    # Remove a random subset of data

    # tried_is = [
    #     138, 129,  54,   2,  66,  86,  99,  32
    #     107,  59,  70, 103, 123,   0,  76,  95
    #      32,  48,  42, 143,  70, 131,  34,  90
    #       0,   3,  79,  26,  53, 122, 113, 110
    #     118,  42,  52,  30,  16,  72,  70, 100
    #       8, 107,  48,  18,  53, 102,  78, 140
    # ]

    n_data = X_arrs[0].shape[0]
    # rem_is = np.random.choice(list(set(range(n_data)) - set(tried_is)), 8, False)
    # rem_is = np.random.choice(n_data, 8, False)
    # rem_is = [107, 78, 140, 138, 129]
    rem_is = []
    inds = [j for j in range(n_data) if j not in rem_is]
    X_a = [X_arr[inds] for X_arr in X_arrs]

    # Apply robust scaling for appropriate feature groups
    scale_groups = noncat_grps + series_grps + grps_config
    scalers = {}
    for l in scale_groups:
        l_i = X_groups.index(l)
        scaler = RobustScaler()
        X_a[l_i] = scaler.fit_transform(X_a[l_i])
        scalers[l] = scaler

    # Create final X index representation
    X = np.tile(range(len(inds)), (len(X_groups), 1)).T.astype(float)
    for j in range(len(X_groups)):
        X[:, j] += j / 10000

    Y = np.asarray([d["blue_win"] for d in D])[inds]
    pr_fl(X.shape, Y.shape, len(X_a), len(X_groups), sum([arr.shape[1] for arr in X_a]), X_a[0].shape)


    # # Save/load existing dataset to save time
    save_ld((d, D, X_a, X, Y, X_groups, scalers, plist, llist, player_role_perfs, role_league_perf_cache), "pro_data")
    sys.exit()
else:
    d, D, X_a, X, Y, X_groups, scalers, plist, llist, player_role_perfs, role_league_perf_cache = load_ld("pro_data")


pr_fl("Dataset shape", (X_a[0].shape[0], len(X_a)))
# sys.exit()

# pr_fl("Selecting patch 10.1 or later data...")
# pat_idx = np.nonzero(np.nonzero(X_a[X_groups.index("patch")])[1] >= plist.index("10.1"))[0]
# pr(len(pat_idx))
# # pr((pat_idx, len(pat_idx)))
# # sys.exit()
# X = X[pat_idx]
# Y = Y[pat_idx]
# D = [D[i] for i in pat_idx]
# # d = [d[i] for i in pat_idx]
# X_a = [X_a_[pat_idx] for X_a_ in X_a]
# pr((len(D), X.shape, len(X_a)))

# pr(plist)
# pats = np.nonzero(X_a[X_groups.index("patch")])[1]
# pr(sum(pats <= 0))
# pr(sum(pats >= 2))
# pr(np.nonzero(X_a[X_groups.index("patch")])[1])
# sys.exit()

# # Test predicted probabilities
# (pro_models, fscalers, patches, leagues, perfs, lperfs), pro_scores = load_ld(pro_model_fn)
# X_flabels = pro_models["LCK"][1]
# feat_is = [X_groups.index(l) for l in X_flabels]
# X_f = np.hstack([X_a[i] for i in feat_is])
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# m = ZeroLossPCA(m, pca_n=110)
# # m = Pipeline(steps=[
# #     ("pca", PCA(n_components=110)),
# #     ("model", m),
# # ])
# print("Testing probabilities...")
# res = cross_val_predict(m, X_f, Y, cv=StratifiedKFold(n_splits=20, shuffle=True), method="predict_proba")
# m = ZeroLossPCA(m, pca_n=110)
# # m = Pipeline(steps=[
# #     ("pca", PCA(n_components=110)),
# #     ("model", m),
# # ])
# # print("Calibrating probabilities...") # Doesn't work with, probs bc only ~200 samples
# # m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# # m = CalibratedClassifierCV(base_estimator=m, cv=StratifiedKFold(n_splits=20, shuffle=True), method='sigmoid')
# # m.fit(X_f, Y.flatten())
# res_calibrated = res#m.predict_proba(X_f)
# save_ld((res, res_calibrated), "pcal_results")
# # sys.exit()



# (pro_models, fscalers, patches, leagues, perfs, lperfs), pro_scores = load_ld(pro_model_fn)


# # Try PCA with different numbers of components
# n_splits = 20
# n_repeats = 30
# selector = load_ld("pro_model_selector")
# fsets = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
# results = {}
# for j in list(selector.subsets_.keys())[::-1]:
#     pr_fl("Running PCA for the " + str(j) + " best features...")
#     feat_is = selector.subsets_[j]["feature_idx"]
#     X_f = np.hstack([X_a[i] for i in feat_is])
#     # print(leag, len(feat_is), X_f.shape)
#     m_ = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
#     def test_comp(m, X, Y, n_pca):
#         m = ZeroLossPCA(m, pca_n=n_pca)
#         return cross_val_score(m, X, Y, cv=RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats))
#     pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - 1/n_splits)))))[::-1]
#     res = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_comp)(m_, X_f, Y, n) for n in pca_ns)
#     results[j] = (pca_ns, res)
# save_ld(results, "pca_results")
# pr_fl("Done PCA Testing")
# selector = load_ld("pro_model_sqo_selector")
# fsets = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
# results = {}
# for j in list(selector.subsets_.keys())[::-1]:
#     pr_fl("Running PCA for the " + str(j) + " best features...")
#     feat_is = selector.subsets_[j]["feature_idx"]
#     X_f = np.hstack([X_a[i] for i in feat_is])
#     # print(leag, len(feat_is), X_f.shape)
#     m_ = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
#     def test_comp(m, X, Y, n_pca):
#         m = ZeroLossPCA(m, pca_n=n_pca)
#         return cross_val_score(m, X, Y, cv=RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats))
#     pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - 1/n_splits)))))[::-1]
#     res = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_comp)(m_, X_f, Y, n) for n in pca_ns)
#     results[j] = (pca_ns, res)
# save_ld(results, "pca_results_sqo")
# pr_fl("Done PCA Testing SQO")
# sys.exit()
# print(X_a[X_groups.index("series_wins")])


# # # Train and save tuned models for each league
if train_nsqo:
    selector = load_ld("pro_model_nsqo_selector")
    fsets = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
    pro_models = {}
    final_scalers = {}
    m_ = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
    # m_ = Pipeline(steps=[
    #     ('scale', StandardScaler()),
    #     ('model', m_),
    # ])
    # for leag, peak in [('MSI', 79), ("LEC", 71), ("LCS", 101), ("LCK", 88), ("CBLoL", 65), ("LMS", 76)]:
    fscores = {}
    for leag, n_pca, peak, score in [
            # ("MSI", 28, 56, -1),
            # ("LCS", 23, 42, -1),
            # ("LEC", 34, 64, -1),
            # ("LCK", 28, 56, -1),
            # ("LMS", 28, 56, -1),
            # ("CBLoL", 28, 56, -1),

            # ("MSI", pca_n_max, 57, -1),
            # ("LCS", pca_n_max, 51, -1),
            ("LEC", pca_n_max, 51, -1),
            ("LCK", pca_n_max, 64, -1),
            ("NAAc", pca_n_max, 72, -1),
            # ("CBLoL", pca_n_max, 57, -1),
      ]:
        fscores[leag] = score
        m = deepcopy(m_)
        X_flabels = fsets[peak]
        X_flabels = [l for l in X_flabels if "series_" not in l]
        if n_pca == -1: n_pca = round(peak * pca_frac)
        # X_f = np.hstack(X_a)
        # n_pca = 30
        m = ZeroLossPCA(m, pca_n=n_pca)
        # if leag == "MSI" and "league" in X_flabels:
        #     X_flabels = [l for l in X_flabels if l != "league"]
        X_flabels = [l for l in X_flabels if l != "series_wins"]
        X_flabels = [l for l in X_flabels if l != "series_winstreak"]
        if league == "MSI":
            print(len(X_flabels), "features total")
        feat_is = [X_groups.index(l) for l in X_flabels]
        leag_idx = [i for i in range(len(D)) if pro_leagues[D[i]["league"]] == leag] if leag != 'MSI' else range(len(D))
        X_f = np.hstack([X_a[i] for i in feat_is])
        scores = []
        for i in range(20):
            preds = cross_val_predict(deepcopy(m), X_f, Y, cv=StratifiedKFold(n_splits=3, shuffle=True))[leag_idx]
            score = np.mean(preds == Y[leag_idx])
            scores.append(score)
        score = np.mean(scores)

        # Compute feature importances
        # for j in range(len(feat_is)):
        #     j_ = feat_is[j]
        #     X_f = np.hstack([X_a[i] for i in feat_is if i != j_])
        #     preds = cross_val_predict(deepcopy(m), X_f, Y, cv=StratifiedKFold(n_splits=20, shuffle=True))[leag_idx]
        #     score_ = np.mean(preds == Y[leag_idx])
        #     print(X_flabels[j], score - score_)

        preds = cross_val_predict(deepcopy(m), X_f, Y, cv=LeaveOneOut())[leag_idx]
        new_fscore = np.mean(preds == Y[leag_idx])
        pr_fl(leag, "20fcvPCA accuracy:", score, "LOOCV accuracy:", new_fscore)
        fscores[leag] = new_fscore
        m.fit(X_f, Y)
        pro_models[leag] = (m, X_flabels)
        for l in X_flabels:
            if l in scalers and l not in final_scalers:
                final_scalers[l] = scalers[l]
    obj = ((pro_models, final_scalers, plist, llist, player_role_perfs, role_league_perf_cache), fscores)
    print(len(obj[0]))
    save_ld(obj, "pro_model_nsqo")
    sys.exit()

if train_sqo:
    selector = load_ld("pro_model_sqo_selector")
    sqlabels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels]
    fsets = [[sqlabels[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_]
    pro_models = {}
    final_scalers = {}
    m_ = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
    # m_ = Pipeline(steps=[
    #     ('scale', StandardScaler()),
    #     ('model', m_),
    # ])
    # for leag, peak in [('MSI', 79), ("LEC", 71), ("LCS", 101), ("LCK", 88), ("CBLoL", 65), ("LMS", 76)]:
    fscores = {}
    for leag, n_pca, peak, score in [

            # ("MSI", pca_n_max, 31, -1),
            ("LCS", pca_n_max, 26, -1),
            ("LEC", pca_n_max, 40, -1),
            ("LCK", pca_n_max, 53, -1),
            # ("NAAc", pca_n_max, 55, -1),
            # ("CBLoL", pca_n_max, 56, -1),
      ]:
        fscores[leag] = score
        m = deepcopy(m_)
        X_flabels = fsets[peak]
        X_flabels = [l for l in X_flabels if "series_" not in l]
        if n_pca == -1: n_pca = round(peak * pca_frac)
        # X_f = np.hstack(X_a)
        # n_pca = 30
        m = ZeroLossPCA(m, pca_n=n_pca)
        # if leag == "MSI" and "league" in X_flabels:
        #     X_flabels = [l for l in X_flabels if l != "league"]
        feat_is = [X_groups.index(l) for l in X_flabels]
        leag_idx = [i for i in range(len(D)) if pro_leagues[D[i]["league"]] == leag] if leag != 'MSI' else range(len(D))
        X_f = np.hstack([X_a[i] for i in feat_is])
        scores = []
        for i in range(20):
            preds = cross_val_predict(deepcopy(m), X_f, Y, cv=StratifiedKFold(n_splits=3, shuffle=True))[leag_idx]
            score = np.mean(preds == Y[leag_idx])
            scores.append(score)
        score = np.mean(scores)

        # Compute feature importances
        # for j in range(len(feat_is)):
        #     j_ = feat_is[j]
        #     X_f = np.hstack([X_a[i] for i in feat_is if i != j_])
        #     preds = cross_val_predict(deepcopy(m), X_f, Y, cv=StratifiedKFold(n_splits=20, shuffle=True))[leag_idx]
        #     score_ = np.mean(preds == Y[leag_idx])
        #     print(X_flabels[j], score - score_)

        preds = cross_val_predict(deepcopy(m), X_f, Y, cv=LeaveOneOut())[leag_idx]
        new_fscore = np.mean(preds == Y[leag_idx])
        pr_fl(leag, "20fcvPCA accuracy:", score, "LOOCV accuracy:", new_fscore)
        fscores[leag] = new_fscore
        m.fit(X_f, Y)
        pro_models[leag] = (m, X_flabels)
        for l in X_flabels:
            if l in scalers and l not in final_scalers:
                final_scalers[l] = scalers[l]
    save_ld(((pro_models, final_scalers, plist, llist, player_role_perfs, role_league_perf_cache), fscores), "pro_model_sqo")
    sys.exit()

# if train_sqo:
#     selector = load_ld("pro_model_sqpco_selector")
#     sqpc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels and \
#                    "_opgg_champion_recent" not in l and "_eloavg_" not in l]
#     fsets = [[sqpc_labels[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_]
#     pro_models = {}
#     final_scalers = {}
#     m_ = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
#     # m_ = Pipeline(steps=[
#     #     ('scale', StandardScaler()),
#     #     ('model', m_),
#     # ])
#     # for leag, peak in [('MSI', 79), ("LEC", 71), ("LCS", 101), ("LCK", 88), ("CBLoL", 65), ("LMS", 76)]:
#     fscores = {}
#     for leag, n_pca, peak, score in [

#             ("MSI", pca_n_max, 31, -1),
#             ("LCS", pca_n_max, 38, -1),
#             ("LEC", pca_n_max, 43, -1),
#             ("LCK", pca_n_max, 53, -1),
#             ("NAAc", pca_n_max, 55, -1),
#             ("CBLoL", pca_n_max, 56, -1),
#       ]:
#         fscores[leag] = score
#         m = deepcopy(m_)
#         X_flabels = fsets[peak]
#         X_flabels = [l for l in X_flabels if "series_" not in l]
#         if n_pca == -1: n_pca = round(peak * pca_frac)
#         # X_f = np.hstack(X_a)
#         # n_pca = 30
#         m = ZeroLossPCA(m, pca_n=n_pca)
#         # if leag == "MSI" and "league" in X_flabels:
#         #     X_flabels = [l for l in X_flabels if l != "league"]
#         feat_is = [X_groups.index(l) for l in X_flabels]
#         leag_idx = [i for i in range(len(D)) if pro_leagues[D[i]["league"]] == leag] if leag != 'MSI' else range(len(D))
#         X_f = np.hstack([X_a[i] for i in feat_is])
#         scores = []
#         for i in range(20):
#             preds = cross_val_predict(deepcopy(m), X_f, Y, cv=StratifiedKFold(n_splits=3, shuffle=True))[leag_idx]
#             score = np.mean(preds == Y[leag_idx])
#             scores.append(score)
#         score = np.mean(scores)

#         # Compute feature importances
#         # for j in range(len(feat_is)):
#         #     j_ = feat_is[j]
#         #     X_f = np.hstack([X_a[i] for i in feat_is if i != j_])
#         #     preds = cross_val_predict(deepcopy(m), X_f, Y, cv=StratifiedKFold(n_splits=20, shuffle=True))[leag_idx]
#         #     score_ = np.mean(preds == Y[leag_idx])
#         #     print(X_flabels[j], score - score_)

#         preds = cross_val_predict(deepcopy(m), X_f, Y, cv=LeaveOneOut())[leag_idx]
#         new_fscore = np.mean(preds == Y[leag_idx])
#         pr_fl(leag, "20fcvPCA accuracy:", score, "LOOCV accuracy:", new_fscore)
#         fscores[leag] = new_fscore
#         m.fit(X_f, Y)
#         pro_models[leag] = (m, X_flabels)
#         for l in X_flabels:
#             if l in scalers and l not in final_scalers:
#                 final_scalers[l] = scalers[l]
#     save_ld(((pro_models, final_scalers, plist, llist, player_role_perfs, role_league_perf_cache), fscores), "pro_model_sqo")
#     sys.exit()

# selector = load_ld("pro_model_sqo_selector")
# fsets = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
# pro_models = {}
# final_scalers = {}
# m_ = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# # for leag, peak in [('MSI', 79), ("LEC", 71), ("LCS", 101), ("LCK", 88), ("CBLoL", 65), ("LMS", 76)]:
# fscores = {}
# for leag, n_pca, peak, score in [
#         ("MSI", 19, 31, -1),
#         ("LCS", 55, 58, -1),
#         ("LEC", 15, 30, -1),
#         ("LCK", 17, 35, -1),
#         ("LMS", 16, 36, -1),
#         ("CBLoL", 20, 38, -1),
#   ]:
#     fscores[leag] = score
#     m = deepcopy(m_)
#     X_flabels = fsets[peak]
#     if n_pca == -1: n_pca = round(peak * pca_frac)
#     m = ZeroLossPCA(m, pca_n=n_pca)
#     # if leag == "MSI" and "league" in X_flabels:
#     #     X_flabels = [l for l in X_flabels if l != "league"]
#     feat_is = [X_groups.index(l) for l in X_flabels]
#     leag_idx = [i for i in range(len(D)) if pro_leagues[D[i]["league"]] == leag] if leag != 'MSI' else range(len(D))
#     X_f = np.hstack([X_a[i] for i in feat_is])
#     preds = cross_val_predict(deepcopy(m), X_f, Y, cv=StratifiedKFold(n_splits=20, shuffle=True))[leag_idx]
#     score = np.mean(preds == Y[leag_idx])
#     preds = cross_val_predict(deepcopy(m), X_f, Y, cv=LeaveOneOut())[leag_idx]
#     new_fscore = np.mean(preds == Y[leag_idx])
#     pr_fl(leag, "20fcvPCA accuracy:", score, "LOOCV accuracy:", new_fscore)
#     fscores[leag] = new_fscore
#     m.fit(X_f, Y)
#     pro_models[leag] = (m, X_flabels)
#     for l in X_flabels:
#         if l in scalers and l not in final_scalers:
#             final_scalers[l] = scalers[l]
# # save_ld(((pro_models, final_scalers, plist, llist, player_role_perfs, role_league_perf_cache), fscores), "pro_model_sqo")
# sys.exit()

# selector = load_ld("pro_model_sqpco_selector")
# sqpc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels and \
#                "_opgg_champion_recent" not in l and "_eloavg_" not in l]
# fsets = [[sqpc_labels[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_]
# pro_models = {}
# final_scalers = {}
# m_ = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# # for leag, peak in [('MSI', 79), ("LEC", 71), ("LCS", 101), ("LCK", 88), ("CBLoL", 65), ("LMS", 76)]:
# fscores = {}
# for leag, n_pca, peak, score in [
#         ("MSI", 54, 65, 0.7801075268817205),
#         # ("LCS", 29, 87, 0.8443396226415094),
#         # ("LEC", 47, 67, 0.8791666666666667),
#         # ("LCK", 73, 86, 0.833125),
#         # ("LMS", 38, 65, 0.7896551724137931),
#         # ("CBLoL", 71, 86, 0.9181818181818182),
#   ]:
#     fscores[leag] = score
#     m = deepcopy(m_)
#     X_flabels = fsets[peak]
#     # m = ZeroLossPCA(m, pca_n=n_pca)
#     feat_is = [X_groups.index(l) for l in X_flabels]
#     X_f = np.hstack([X_a[i] for i in feat_is])
    # new_fscore = np.mean(cross_val_score(deepcopy(m), X_f, Y, cv=LeaveOneOut()))
    # pr_fl(leag, "20fcvPCA accuracy:", score, "LOOCV accuracy:", new_fscore)
    # fscores[leag] = new_fscore
#     m.fit(X_f, Y)
#     pro_models[leag] = (m, X_flabels)
#     for l in X_flabels:
#         if l in scalers and l not in final_scalers:
#             final_scalers[l] = scalers[l]
# save_ld(((pro_models, final_scalers, plist, llist, player_role_perfs, role_league_perf_cache), fscores), "pro_model_sqpco")
# sys.exit()


# # Test accuracy with all features
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = FoldingWrapper(m, X_a, pca=n_pca_thresh, pca_n=pca_frac, pca_max=pca_n_max)
# # scores = np.hstack([cross_val_score(m, X, Y, cv=StratifiedShuffleSplit(n_splits=20)) for _ in range(10)])
# scores = cross_val_score(m, X, Y, cv=LeaveOneOut())
# fscore = np.mean(scores)
# pr_fl("Accuracy test w/ all data: ", fscore)

# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# X_f = np.hstack(X_a)
# m = ZeroLossPCA(m, pca_n=min(300, X_f.shape[0] - 1, X_f.shape[1]))
# # scores = np.hstack([cross_val_score(m, X, Y, cv=StratifiedShuffleSplit(n_splits=20)) for _ in range(10)])
# scores = cross_val_score(m, X_f, Y, cv=LeaveOneOut())
# fscore = np.mean(scores)
# pr_fl("Accuracy test w/ all data and PCA: ", fscore)


# Split dataset into training and validation subsets
val_idx = []
# pr_fl("Validation set:")
for league in pro_leagues:
    shortlist = []
    for i in range(len(D)):
        if D[i]["league"] == pro_leagues.index(league):
            shortlist.append((D[i]["timestamp"], i))
    shortlist = sorted(shortlist)[::-1]
    # pr_fl(league, shortlist)
    val_idx += [i for _, i in shortlist[:val_set_ln]]
train_idx = [i for i in range(len(X)) if i not in val_idx]
X_train, X_val = X[train_idx], X[val_idx]
Y_train, Y_val = Y[train_idx], Y[val_idx]
# sys.exit()


# # # Feature selection (with PCA above n components)
results = {}
if not select_sqo:
    m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
    m = FoldingWrapper(m, X_a, pca=n_pca_thresh, pca_n=pca_frac, pca_max=pca_n_max)

    n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(X_groups))
    n_max_feats_ = min(n_max_feats, len(X_groups)) if isinstance(n_max_feats, int) else int(n_max_feats * len(X_groups))
    selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=forward, floating=floating,
                   cv=RepeatedStratifiedKFold(n_splits=cv_k_folds, n_repeats=cv_iter),
                   estimator=m, n_jobs=n_parallel_cpu, verbose=5)
    X_selected = selector.fit_transform(X_train, Y_train)
    scores = cross_val_score(m, X_selected, Y_train, cv=LeaveOneOut())
    fscore = np.mean(scores)
    feat_is = selector.k_feature_idx_
    val_score = accuracy_score(Y_val, m.fit(X_selected, Y_train).predict(X_val[:, feat_is]))
    pr_fl("nsqo accuracy (training, validation):", fscore, val_score)
    pr(([X_groups[i] for i in feat_is], len(feat_is)))


    # Train and save final model and all infill data
    # m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
    # if len(feat_is) > n_pca_thresh:
    #     m = Pipeline(steps=[
    #         ("pca", PCA(n_components=n_pca_thresh)),
    #         ("model", m),
    #     ])
    # X_f = np.hstack([X_a[i] for i in feat_is])
    # m.fit(X_f, Y)
    X_flabels = [X_groups[i] for i in feat_is]
    fscalers = [scalers[l] if l in scalers else None for l in X_flabels]
    save_ld(selector, "pro_model_nsqo_selector")
    flabels = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
    save_ld(flabels, "pro_selector_fsets_nsqo")
    # save_ld(((m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore), "pro_model_nsqo")


    # Test selected feature sets for different cross validation configurations to choose least-overfitted, highest accuracy model
    # Also, collect results together for each league, to see which feature sets are best tuned to each one
    # selector = load_ld("pro_model_nsqo_selector")
    subset_is = [i for i in selector.subsets_]
    subset_is.sort()
    fsets = [selector.subsets_[i]["feature_idx"] for i in subset_is]

    # Print first few feature sets to validate selection (manual reality check)
    # for i in list(range(10)) + [20, 75, 105, 200]:
    #     subset_i = subset_is[i]
    #     feat_is = selector.subsets_[subset_i]["feature_idx"]
    #     pr((subset_i, [X_groups[j] for j in feat_is]))

    def test_subset(X_a, Y, fs, iz, k, n, pca=False, val=False):
        m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
        X_f = np.hstack([X_a[i] for i in fs])
        if pca:
            pca_num = min(300, X_f.shape[1], int(X_f.shape[0] * (1 - (1/k)) * pca_frac) - 1)
            if pca_num > pca_n_max: pca_num = pca_n_max
            m = ZeroLossPCA(m, pca_n=pca_num)

        # X = X[:, fs]
        res = [[] for _ in iz]
        for _ in range(n):
            if val:
                m.fit(X_f[train_idx], Y[train_idx])
                preds = m.predict(X_f[val_idx])
                corr = preds == Y[val_idx]
            else:
                preds = cross_val_predict(m, X_f, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
                # preds = cross_val_predict(m, X, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
                corr = preds == Y
            for i in range(len(iz)):
                res[i].append(corr[iz[i]])
        return [np.mean(np.hstack(r)) for r in res]

    izz = [list(range(len(Y_train)))] + [[i for i in range(len(Y_train)) if leagues_list[D[train_idx[i]]["league"]] == leag] for leag in llist]
    izz_val = [list(range(len(Y_val)))] + [[i for i in range(len(Y_val)) if leagues_list[D[val_idx[i]]["league"]] == leag] for leag in llist]

    res_val = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz_val, 10, 1, pca=True, val=True) for fs in fsets)
    pr_fl("Done time validation")
    time.sleep(10)
    res_loocv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 'l', 1) for fs in fsets)
    pr_fl("Done LeaveOneOut CV")
    time.sleep(10)
    res_20fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 30) for fs in fsets)
    pr_fl("Done 20-fold CV")
    time.sleep(10)
    res_20fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 20, pca=True) for fs in fsets)
    pr_fl("Done 20-fold PCA CV")
    time.sleep(10)
    res_10fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 10, 50) for fs in fsets)
    pr_fl("Done 10-fold CV")
    time.sleep(10)
    res_7fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 7, 70) for fs in fsets)
    pr_fl("Done 7-fold CV")
    time.sleep(10)
    res_5fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 130) for fs in fsets)
    pr_fl("Done 5-fold CV")
    time.sleep(10)
    res_5fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 50, pca=True) for fs in fsets)
    pr_fl("Done 5-fold PCA CV")
    time.sleep(10)
    res_3fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 3, 200) for fs in fsets)
    pr_fl("Done 3-fold CV, saving results...")
    time.sleep(10)
    pr_fl("")

    res_5fcv_training = [selector.subsets_[i]["avg_score"] for i in subset_is]
    save_ld((['MSI'] + llist, subset_is, res_val,
        res_loocv, res_20fcv, res_20fcv_pca, res_10fcv, res_7fcv, res_5fcv, res_5fcv_pca, res_3fcv, res_5fcv_training), "pro_validation_nsqo")


    n_splits = 20           # CV parameters
    n_repeats = 20

    # Define testing function to be parallelised
    def test_pca(X, Y, n_pca, zero_loss=True):
        m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
        if zero_loss:
            m = ZeroLossPCA(m, pca_n=n_pca)
        else:
            m = Pipeline(steps=[
                ('pca', PCA(n_components=n_pca)),
                ('model', SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol))
            ])

        return [cross_val_predict(m, X, Y, cv=StratifiedKFold(n_splits=n_splits, shuffle=True)) for _ in range(n_repeats)]
        # return cross_val_score(m, X, Y, cv=RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats))


    # First, run for nsqo (fully fledged) model
    selector = load_ld("pro_model_nsqo_selector")
    res = {}
    for j in selector.subsets_:
        feat_is = selector.subsets_[j]["feature_idx"]
        X_f = np.hstack([X_a[i] for i in feat_is])
        pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
        pr_fl("Running nsqo PCA tests for fset", j)
        res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
        if j >= 50 and j % 25 == 0:
            results["nsqo"] = res
            save_ld(results, "pca_results")
            # sys.exit()
    results["nsqo"] = res
    save_ld(results, "pca_results")
    pr_fl("nsqo PCA testing finished")

# sys.exit()


# # Perform a backwards selection starting from the existing feature set
# (m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore = load_ld("pro_model")
# selector = load_ld("pro_model_selector")
# subset_is = [i for i in selector.subsets_ if i > 980]
# subsets = [selector.subsets_[i] for i in subset_is]
# best_i = np.argmax([subset["avg_score"] for subset in subsets])
# subset = subsets[best_i]
# feat_is = subset["feature_idx"]
# fscore = subset["avg_score"]
# pr(([X_groups[i] for i in feat_is], len(feat_is)))
# X_selected = X[:, feat_is]

# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = FoldingWrapper(m, X_a, pca=n_pca_thresh, pca_n=pca_frac, pca_max=pca_n_max)

# n_min_feats_ = int(0.667 * X_selected.shape[1])
# n_max_feats_ = X_selected.shape[1]
# selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=False, floating=True,
#                cv=RepeatedStratifiedKFold(n_splits=cv_k_folds, n_repeats=cv_iter),
#                estimator=m, n_jobs=n_parallel_cpu, verbose=5)
# X_selected = selector.fit_transform(X_selected, Y)
# scores = cross_val_score(m, X_selected, Y, cv=LeaveOneOut())
# fscore = np.mean(scores)
# pr_fl("nsqo bw accuracy:", fscore)

# feat_is = [feat_is[i] for i in selector.k_feature_idx_]
# pr(([X_groups[i] for i in feat_is], len(feat_is)))

# # Train and save final model and all infill data
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# if len(feat_is) > n_pca_thresh:
#     m = Pipeline(steps=[
#         ("pca", PCA(n_components=n_pca_thresh)),
#         ("model", m),
#     ])
# X_f = np.hstack([X_a[i] for i in feat_is])
# m.fit(X_f, Y)
# X_flabels = [X_groups[i] for i in feat_is]
# fscalers = [scalers[l] if l in scalers else None for l in X_flabels]
# save_ld(selector, "pro_model_selector_bw")
# save_ld(((m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore), "pro_model_bw")

# sys.exit()


# old code
# selector = load_ld("pro_model_selector")
# feat_is = selector.subsets_[max(list(selector.subsets_.keys()))]["feature_idx"]
# selector = load_ld("pro_model_selector_bw")
# subsets = [i for i in selector.subsets_ if i > 280]
# best_i = np.argmax([selector.subsets_[i]["avg_score"] for i in subsets])
# subset = selector.subsets_[subsets[best_i]]
# feat_is = [feat_is[i] for i in subset["feature_idx"]]
# fscore = subset["avg_score"]
# pr(([X_groups[i] for i in feat_is], len(feat_is)))





# # old code for final model selection (also tested C values)
# Select backwards-tuned feature set that performed best above a minimum number of features (chosen to avoid overfitting)
# selector = load_ld("pro_model_selector")
# feat_is = selector.subsets_[max(list(selector.subsets_.keys()))]["feature_idx"]
# selector = load_ld("pro_model_selector_bw")
# subsets = [i for i in selector.subsets_ if i > 280]
# best_i = np.argmax([selector.subsets_[i]["avg_score"] for i in subsets])
# subset = selector.subsets_[subsets[best_i]]
# feat_is = [feat_is[i] for i in subset["feature_idx"]]
# fscore = subset["avg_score"]
# pr(([X_groups[i] for i in feat_is], len(feat_is)))

# # Train and save final model and all infill data
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# if len(feat_is) > n_pca_thresh:
#     m = Pipeline(steps=[
#         ("pca", PCA(n_components=n_pca_thresh)),
#         ("model", m),
#     ])
# X_f = np.hstack([X_a[i] for i in feat_is])
# m.fit(X_f, Y)
# pr_fl(fscore)

# # Cs = np.arange(0.85, 0.9+1e-7, 0.005)
# # Cs = [0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15, 1.2]
# # scores = [np.mean(np.hstack([cross_val_score(SVC(gamma='auto', kernel='rbf', C=C + ((np.random.random() - 0.5) * 0.01), tol=svm_tol),
# #     X_f, Y, cv=10, n_jobs=n_parallel_cpu) for _ in range(50)])) for C in Cs]
# # for i in range(len(Cs)):
# #     pr_fl(Cs[i], scores[i])

# X_flabels = [X_groups[i] for i in feat_is]
# fscalers = [scalers[l] if l in scalers else None for l in X_flabels]
# save_ld(((m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore), "pro_model_better")

# sys.exit()



# # # Select backwards-tuned feature set that performs best when excluding pro stats (bad version of sqo model)
# # selector = load_ld("pro_model_selector_bw") # actual version of code (current version is a workaround for a dead bug)
# # subsets = [i for i in selector.subsets_ if i > 280]
# # best_i = np.argmax([selector.subsets_[i]["avg_score"] for i in subsets])
# # subset = selector.subsets_[subsets[best_i]]
# # orig_feat_is = subset["feature_idx"]
# # fscore = subset["avg_score"]
# # pr(([X_groups[i] for i in orig_feat_is], len(orig_feat_is)))
# selector = load_ld("pro_model_selector")
# orig_feat_is = selector.subsets_[max(list(selector.subsets_.keys()))]["feature_idx"]
# selector = load_ld("pro_model_selector_bw")
# subset_is = [i for i in selector.subsets_ if i > 280]
# subsets = [selector.subsets_[i] for i in subset_is]

# # Train and save final model and all infill data
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# m = FoldingWrapper(m, X_a, pca=n_pca_thresh, pca_n=pca_frac, pca_max=pca_n_max)

# feat_sets = [[orig_feat_is[i] for i in subset["feature_idx"] if \
#     '_'.join(X_groups[orig_feat_is[i]].split('_')[1:]) not in pro_full_player_labels] for subset in subsets]
# def test_fset(X, Y):
#     return np.mean(np.hstack([cross_val_score(m, X, Y, cv=10) for _ in range(20)]))
# scores = Parallel(n_jobs=n_parallel_cpu)(delayed(test_fset)(X[:, feat_set], Y) for feat_set in feat_sets)
# for i in range(len(feat_sets)):
#     pr_fl(subset_is[i], scores[i])

# best_i = np.argmax(scores)
# fscore = scores[best_i]
# feat_is = feat_sets[best_i]

# pr_fl("sqo score:", fscore)
# pr_fl("n feats:", len(feat_is), "/", len(orig_feat_is))
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# X_f = np.hstack([X_a[i] for i in feat_is])
# m.fit(X_f, Y)

# X_flabels = [X_groups[i] for i in feat_is]
# fscalers = [scalers[l] if l in scalers else None for l in X_flabels]
# save_ld(((m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore), "pro_model_sqo_bw")

# sys.exit()





# # # Solo-queue data only prediction
if not select_sqpco:
    sqlabels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels]
    X_sq = X[:, [X_groups.index(l) for l in sqlabels]]
    X_sq_train, X_sq_val = X_sq[train_idx], X_sq[val_idx]
    pr_fl("Solo-queue only training...", X_sq.shape, len(sqlabels))
    m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
    m = FoldingWrapper(m, X_a, pca=n_pca_thresh, pca_n=pca_frac_sqo, pca_max=pca_n_max)

    n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(sqlabels))
    n_max_feats_ = min(n_max_feats, len(sqlabels)) if isinstance(n_max_feats, int) else int(n_max_feats * len(sqlabels))
    selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=forward, floating=floating,
                   cv=RepeatedStratifiedKFold(n_splits=cv_k_folds, n_repeats=cv_iter),
                   estimator=m, n_jobs=n_parallel_cpu, verbose=5)
    X_sq_selected = selector.fit_transform(X_sq_train, Y_train)
    scores = cross_val_score(m, X_sq_selected, Y_train, cv=LeaveOneOut())
    fscore = np.mean(scores)
    sqfeat_is = [X_groups.index(sqlabels[i]) for i in selector.k_feature_idx_]
    val_score = accuracy_score(Y_val, m.fit(X_sq_selected, Y_train).predict(X_val[:, sqfeat_is]))
    pr_fl("nsqo accuracy (training, validation):", fscore, val_score)
    pr_fl("sqo accuracy:", fscore)

    # Train and save final model and all infill data
    m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
    if len(sqfeat_is) > n_pca_thresh:
        m = Pipeline(steps=[
            ("pca", PCA(n_components=n_pca_thresh)),
            ("model", m),
        ])
    X_f = np.hstack([X_a[i] for i in sqfeat_is])
    m.fit(X_f, Y)
    X_flabels = [X_groups[i] for i in sqfeat_is]
    fscalers = [scalers[l] if l in scalers else None for l in X_flabels]
    save_ld(selector, "pro_model_sqo_selector")
    flabels = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
    save_ld(flabels, "pro_selector_fsets_sqo")
    save_ld(((m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore), "pro_model_sqo")


    # Test selected feature sets for different cross validation configurations to choose least-overfitted, highest accuracy model
    # selector = load_ld("pro_model_sqo_selector")
    subset_is = [i for i in selector.subsets_]
    subset_is.sort()
    fsets = [[X_groups.index(sqlabels[i]) for i in selector.subsets_[j]["feature_idx"]] for j in subset_is]

    # Print first few feature sets to validate selection (check we aren't overfitting with spurious relationships)
    # for i in list(range(10)) + [20, 75, 105, 200]:
    #     subset_i = subset_is[i]
    #     feat_is = selector.subsets_[subset_i]["feature_idx"]
    #     pr((subset_i, [X_groups[j] for j in feat_is]))

    def test_subset(X_a, Y, fs, iz, k, n, pca=False, val=False):
        m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
        X_f = np.hstack([X_a[i] for i in fs])
        if pca:
            pca_num = min(300, X_f.shape[1], int(X_f.shape[0] * (1 - (1/k)) * pca_frac) - 1)
            if pca_num > pca_n_max: pca_num = pca_n_max
            m = ZeroLossPCA(m, pca_n=pca_num)

        # X = X[:, fs]
        res = [[] for _ in iz]
        for _ in range(n):
            if val:
                m.fit(X_f[train_idx], Y[train_idx])
                preds = m.predict(X_f[val_idx])
                corr = preds == Y[val_idx]
            else:
                preds = cross_val_predict(m, X_f, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
                # preds = cross_val_predict(m, X, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
                corr = preds == Y
            for i in range(len(iz)):
                res[i].append(corr[iz[i]])
        return [np.mean(np.hstack(r)) for r in res]

    izz = [list(range(len(Y_train)))] + [[i for i in range(len(Y_train)) if leagues_list[D[train_idx[i]]["league"]] == leag] for leag in llist]
    izz_val = [list(range(len(Y_val)))] + [[i for i in range(len(Y_val)) if leagues_list[D[val_idx[i]]["league"]] == leag] for leag in llist]

    res_val = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz_val, 10, 1, pca=True, val=True) for fs in fsets)
    pr_fl("Done time validation")
    time.sleep(10)
    res_loocv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 'l', 1) for fs in fsets)
    pr_fl("Done LeaveOneOut CV")
    time.sleep(10)
    res_20fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 30) for fs in fsets)
    pr_fl("Done 20-fold CV")
    time.sleep(10)
    res_20fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 20, pca=True) for fs in fsets)
    pr_fl("Done 20-fold PCA CV")
    time.sleep(10)
    res_10fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 10, 50) for fs in fsets)
    pr_fl("Done 10-fold CV")
    time.sleep(10)
    res_7fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 7, 70) for fs in fsets)
    pr_fl("Done 7-fold CV")
    time.sleep(10)
    res_5fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 130) for fs in fsets)
    pr_fl("Done 5-fold CV")
    time.sleep(10)
    res_5fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 50, pca=True) for fs in fsets)
    pr_fl("Done 5-fold PCA CV")
    time.sleep(10)
    res_3fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 3, 200) for fs in fsets)
    pr_fl("Done 3-fold CV, saving results...")
    time.sleep(10)

    res_5fcv_training = [selector.subsets_[i]["avg_score"] for i in subset_is]
    save_ld((['MSI'] + llist, subset_is, res_val,
        res_loocv, res_20fcv, res_20fcv_pca, res_10fcv, res_7fcv, res_5fcv, res_5fcv_pca, res_3fcv, res_5fcv_training), "pro_validation_sqo")


    n_splits = 20           # CV parameters
    n_repeats = 20

    # Define testing function to be parallelised
    def test_pca(X, Y, n_pca, zero_loss=True):
        m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
        if zero_loss:
            m = ZeroLossPCA(m, pca_n=n_pca)
        else:
            m = Pipeline(steps=[
                ('pca', PCA(n_components=n_pca)),
                ('model', SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol))
            ])

        return [cross_val_predict(m, X, Y, cv=StratifiedKFold(n_splits=n_splits, shuffle=True)) for _ in range(n_repeats)]
        # return cross_val_score(m, X, Y, cv=RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats))


    # selector = load_ld("pro_model_sqo_selector")
    res = {}
    for j in selector.subsets_:
        # if j > 125:
        #     continue
        feat_is = selector.subsets_[j]["feature_idx"]
        X_f = np.hstack([X_a[i] for i in feat_is])
        pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
        pr_fl("Running sqo PCA tests for fset", j)
        res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
        if j > 50 and j % 25 == 0:
            save_ld(results, "pca_results")
    results["sqo"] = res
    save_ld(results, "pca_results")
    pr_fl("sqo PCA testing finished")



# # Solo-queue pre-champ-select data only prediction
sqpc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels and \
               "_opgg_champion_recent" not in l and "_eloavg_" not in l]
X_sqpc = X[:, [i for i in range(X.shape[1]) if X_groups[i] in sqpc_labels]]
pr_fl("Solo-queue pre-champ-select only training...", len(sqpc_labels), X_sqpc.shape)

# Solo-queue pre-champ-select only feature selection (with PCA above n components)
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
m = FoldingWrapper(m, X_a, pca=n_pca_thresh, pca_n=pca_frac_sqo, pca_max=pca_n_max)

n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(sqpc_labels))
n_max_feats_ = min(n_max_feats, len(sqpc_labels)) if isinstance(n_max_feats, int) else int(n_max_feats * len(sqpc_labels))
selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=forward, floating=floating,
               cv=RepeatedStratifiedKFold(n_splits=cv_k_folds, n_repeats=cv_iter),
               estimator=m, n_jobs=n_parallel_cpu, verbose=5)
X_sqpc_selected = selector.fit_transform(X_sqpc, Y)
scores = cross_val_score(m, X_sqpc_selected, Y, cv=LeaveOneOut())
fscore = np.mean(scores)
pr_fl("sqpc accuracy:", fscore)

sqpcfeat_is = [X_groups.index(sqpc_labels[i]) for i in selector.k_feature_idx_]
# Train and save final model and all infill data
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
if len(sqpcfeat_is) > n_pca_thresh:
    m = Pipeline(steps=[
        ("pca", PCA(n_components=n_pca_thresh)),
        ("model", m),
    ])
X_f = np.hstack([X_a[i] for i in sqpcfeat_is])
m.fit(X_f, Y)
X_flabels = [X_groups[i] for i in sqpcfeat_is]
fscalers = [scalers[l] if l in scalers else None for l in X_flabels]
save_ld(selector, "pro_model_sqpco_selector")
flabels = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
save_ld(flabels, "pro_selector_fsets_sqpco")
save_ld(((m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore), "pro_model_sqpco")


# Test selected feature sets for different cross validation configurations to choose least-overfitted, highest accuracy model
# selector = load_ld("pro_model_sqpco_selector")
subset_is = [i for i in selector.subsets_]
subset_is.sort()
fsets = [[X_groups.index(sqpc_labels[i]) for i in selector.subsets_[j]["feature_idx"]] for j in subset_is]

# Print first few feature sets to validate selection (check we aren't overfitting with spurious relationships)
# for i in list(range(10)) + [20, 75, 105, 200]:
#     subset_i = subset_is[i]
#     feat_is = selector.subsets_[subset_i]["feature_idx"]
#     pr((subset_i, [X_groups[j] for j in feat_is]))

def test_subset(X_a, Y, fs, iz, k, n, pca=False, val=False):
    m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
    X_f = np.hstack([X_a[i] for i in fs])
    if pca:
        pca_num = min(300, X_f.shape[1], int(X_f.shape[0] * (1 - (1/k)) * pca_frac) - 1)
        if pca_num > pca_n_max: pca_num = pca_n_max
        m = ZeroLossPCA(m, pca_n=pca_num)

    # X = X[:, fs]
    res = [[] for _ in iz]
    for _ in range(n):
        if val:
            m.fit(X_f[train_idx], Y[train_idx])
            preds = m.predict(X_f[val_idx])
            corr = preds == Y[val_idx]
        else:
            preds = cross_val_predict(m, X_f, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
            # preds = cross_val_predict(m, X, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
            corr = preds == Y
        for i in range(len(iz)):
            res[i].append(corr[iz[i]])
    return [np.mean(np.hstack(r)) for r in res]

izz = [list(range(len(Y_train)))] + [[i for i in range(len(Y_train)) if leagues_list[D[train_idx[i]]["league"]] == leag] for leag in llist]
izz_val = [list(range(len(Y_val)))] + [[i for i in range(len(Y_val)) if leagues_list[D[val_idx[i]]["league"]] == leag] for leag in llist]

res_val = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz_val, 10, 1, pca=True, val=True) for fs in fsets)
pr_fl("Done time validation")
time.sleep(10)
res_loocv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 'l', 1) for fs in fsets)
pr_fl("Done LeaveOneOut CV")
time.sleep(10)
res_20fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 30) for fs in fsets)
pr_fl("Done 20-fold CV")
time.sleep(10)
res_20fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 20, pca=True) for fs in fsets)
pr_fl("Done 20-fold PCA CV")
time.sleep(10)
res_10fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 10, 50) for fs in fsets)
pr_fl("Done 10-fold CV")
time.sleep(10)
res_7fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 7, 70) for fs in fsets)
pr_fl("Done 7-fold CV")
time.sleep(10)
res_5fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 130) for fs in fsets)
pr_fl("Done 5-fold CV")
time.sleep(10)
res_5fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 50, pca=True) for fs in fsets)
pr_fl("Done 5-fold PCA CV")
time.sleep(10)
res_3fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 3, 200) for fs in fsets)
pr_fl("Done 3-fold CV, saving results...")
time.sleep(10)

res_5fcv_training = [selector.subsets_[i]["avg_score"] for i in subset_is]
save_ld((['MSI'] + llist, subset_is,
    res_loocv, res_20fcv, res_20fcv_pca, res_10fcv, res_7fcv, res_5fcv, res_5fcv_pca, res_3fcv, res_5fcv_training), "pro_validation_sqpco")


# selector = load_ld("pro_model_sqpco_selector")
res = {}
for j in selector.subsets_:
    # if j > 125:
    #     continue
    feat_is = selector.subsets_[j]["feature_idx"]
    X_f = np.hstack([X_a[i] for i in feat_is])
    pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
    pr_fl("Running sqpco PCA tests for fset", j)
    res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
    # if j > 50 and j % 25 == 0:
    #     save_ld(results, "pca_results")
results["sqpco"] = res
save_ld(results, "pca_results")
pr_fl("sqpco PCA testing finished")



# # Pro pre-champ-select data only prediction
ppc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) in pro_full_player_labels or \
               ("_opgg_champion_recent" not in l and "_eloavg_" not in l)]
X_ppc = X[:, [i for i in range(X.shape[1]) if X_groups[i] in ppc_labels]]
pr_fl("Pre-champ-select only training...", len(ppc_labels), X_ppc.shape)

# Pro pre-champ-select data only feature selection (with PCA above n components)
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
m = FoldingWrapper(m, X_a, pca=n_pca_thresh, pca_n=pca_frac, pca_max=pca_n_max)

n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(ppc_labels))
n_max_feats_ = min(n_max_feats, len(ppc_labels)) if isinstance(n_max_feats, int) else int(n_max_feats * len(ppc_labels))
selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=forward, floating=floating,
               cv=RepeatedStratifiedKFold(n_splits=cv_k_folds, n_repeats=cv_iter),
               estimator=m, n_jobs=n_parallel_cpu, verbose=5)
X_ppc_selected = selector.fit_transform(X_ppc, Y)
scores = cross_val_score(m, X_ppc_selected, Y, cv=LeaveOneOut())
fscore = np.mean(scores)
pr_fl("ppc accuracy:", fscore)

ppcfeat_is = [X_groups.index(ppc_labels[i]) for i in selector.k_feature_idx_]

# Train and save final model and all infill data
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
if len(ppcfeat_is) > n_pca_thresh:
    m = Pipeline(steps=[
        ("pca", PCA(n_components=n_pca_thresh)),
        ("model", m),
    ])
X_f = np.hstack([X_a[i] for i in ppcfeat_is])
m.fit(X_f, Y)
X_flabels = [X_groups[i] for i in ppcfeat_is]
fscalers = [scalers[l] if l in scalers else None for l in X_flabels]
save_ld(selector, "pro_model_ppco_selector")
flabels = {j: [X_groups[i] for i in selector.subsets_[j]["feature_idx"]] for j in selector.subsets_}
save_ld(flabels, "pro_selector_fsets_ppco")
save_ld(((m, X_flabels, fscalers, plist, llist, player_role_perfs, role_league_perf_cache), fscore), "pro_model_ppco")


# Test selected feature sets for different cross validation configurations to choose least-overfitted, highest accuracy model
# selector = load_ld("pro_model_ppco_selector")
subset_is = [i for i in selector.subsets_]
subset_is.sort()
fsets = [[X_groups.index(ppc_labels[i]) for i in selector.subsets_[j]["feature_idx"]] for j in subset_is]

# Print first few feature sets to validate selection (check we aren't overfitting with spurious relationships)
# for i in list(range(10)) + [20, 75, 105, 200]:
#     subset_i = subset_is[i]
#     feat_is = selector.subsets_[subset_i]["feature_idx"]
#     pr((subset_i, [X_groups[j] for j in feat_is]))

def test_subset(X_a, Y, fs, iz, k, n, pca=False, val=False):
    m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
    X_f = np.hstack([X_a[i] for i in fs])
    if pca:
        pca_num = min(300, X_f.shape[1], int(X_f.shape[0] * (1 - (1/k)) * pca_frac) - 1)
        if pca_num > pca_n_max: pca_num = pca_n_max
        m = ZeroLossPCA(m, pca_n=pca_num)

    # X = X[:, fs]
    res = [[] for _ in iz]
    for _ in range(n):
        if val:
            m.fit(X_f[train_idx], Y[train_idx])
            preds = m.predict(X_f[val_idx])
            corr = preds == Y[val_idx]
        else:
            preds = cross_val_predict(m, X_f, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
            # preds = cross_val_predict(m, X, Y, cv=LeaveOneOut() if k == 'l' else StratifiedKFold(n_splits=k, shuffle=True))
            corr = preds == Y
        for i in range(len(iz)):
            res[i].append(corr[iz[i]])
    return [np.mean(np.hstack(r)) for r in res]

izz = [list(range(len(Y_train)))] + [[i for i in range(len(Y_train)) if leagues_list[D[train_idx[i]]["league"]] == leag] for leag in llist]
izz_val = [list(range(len(Y_val)))] + [[i for i in range(len(Y_val)) if leagues_list[D[val_idx[i]]["league"]] == leag] for leag in llist]

res_val = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz_val, 10, 1, pca=True, val=True) for fs in fsets)
pr_fl("Done time validation")
time.sleep(10)
res_loocv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 'l', 1) for fs in fsets)
pr_fl("Done LeaveOneOut CV")
time.sleep(10)
res_20fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 30) for fs in fsets)
pr_fl("Done 20-fold CV")
time.sleep(10)
res_20fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 20, 20, pca=True) for fs in fsets)
pr_fl("Done 20-fold PCA CV")
time.sleep(10)
res_10fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 10, 50) for fs in fsets)
pr_fl("Done 10-fold CV")
time.sleep(10)
res_7fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 7, 70) for fs in fsets)
pr_fl("Done 7-fold CV")
time.sleep(10)
res_5fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 130) for fs in fsets)
pr_fl("Done 5-fold CV")
time.sleep(10)
res_5fcv_pca = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 5, 50, pca=True) for fs in fsets)
pr_fl("Done 5-fold PCA CV")
time.sleep(10)
res_3fcv = Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(test_subset)(X_a, Y, fs, izz, 3, 200) for fs in fsets)
pr_fl("Done 3-fold CV, saving results...")
time.sleep(10)

res_5fcv_training = [selector.subsets_[i]["avg_score"] for i in subset_is]
save_ld((['MSI'] + llist, subset_is,
    res_loocv, res_20fcv, res_20fcv_pca, res_10fcv, res_7fcv, res_5fcv, res_5fcv_pca, res_3fcv, res_5fcv_training), "pro_validation_ppco")


# selector = load_ld("pro_model_ppco_selector")
res = {}
for j in selector.subsets_:
    # if j > 125:
    #     continue
    feat_is = selector.subsets_[j]["feature_idx"]
    X_f = np.hstack([X_a[i] for i in feat_is])
    pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
    pr_fl("Running ppco PCA tests for fset", j)
    res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
    # if j > 50 and j % 25 == 0:
    #     save_ld(results, "pca_results")
results["ppco"] = res
save_ld(results, "pca_results")
pr_fl("ppco PCA testing finished")
















