#
#  Create postgres request cache table
#


from sqlalchemy import Column, BigInteger, Text, DateTime
from sqlalchemy.dialects.postgresql import JSON, JSONB
import sqlalchemy

from ServerConstants import *


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()
meta = sqlalchemy.MetaData(engine)

try:
    wlogs = sqlalchemy.Table('wlogs', meta, autoload=True, autoload_with=engine)
    wlogs.drop()
    print("dropped")
except sqlalchemy.exc.NoSuchTableError as e:
    pass

meta = sqlalchemy.MetaData(engine)

sqlalchemy.Table("wlogs", meta,  
    Column('worker_key', Text, primary_key=True),
    Column('timestamp', BigInteger),
    # Column('regions', Text), # Regions resolved for by the worker (either kr or rest now depending on postgres db ip)
    Column('log', Text),
)

meta.create_all()
engine.close()
db.dispose()


