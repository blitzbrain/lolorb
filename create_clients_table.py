#
#  Create postgres clients table
#


from sqlalchemy import Column, Integer, BigInteger, Text, DateTime
from sqlalchemy.dialects.postgresql import JSON, JSONB
import sqlalchemy

from ServerConstants import *


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()  
meta = sqlalchemy.MetaData(engine)

try:
    clients = sqlalchemy.Table('clients', meta, autoload=True, autoload_with=engine)
    clients.drop()
    print("dropped")
except sqlalchemy.exc.NoSuchTableError as e:
    pass

meta = sqlalchemy.MetaData(engine)

sqlalchemy.Table("clients", meta,  
    Column('session_id', Text, primary_key=True),
    Column('expiry_timestamp', BigInteger),
    Column('request_index', Integer),
    Column('request_hash', Text),
)

meta.create_all()
engine.close()
db.dispose()


