#
# Tests requests
#

from DataRequests import *


def test(req, **kwargs):
    res = requests.get(req, **kwargs)
    # print(res.url)
    # l1, l2 = res.headers['Content-Length'], len(res.text)
    # eq = int(l1) == int(l2)
    # if not eq:
    #     print(l1, l2, eq)
    return res.json()


# Champion to get test data for (68 = rumble, 412 = thresh)
cid = 412





# Rito static data
# champion_by_id = "lol/static-data/v3/champion/" + str(cid)
# test(rito_serv + champion_by_id, params={"api_key": rito_key})



# Matchlist with multiple queue ids test
# acc_id = "233791841"

# url = https + "euw1" + matchlist_by_acc_id + str(acc_id)
# print(url)
# a = test(url, params={"api_key": rito_key, "queue": [1010, 420]})
# pr([x["queue"] for x in a["matches"]])

# a = ranked_matchlist(make_r_conds(), make_r_timestamps(), "EUW1", acc_id)
# pr([x["queue"] for x in a["matches"]])





# Champion.gg
def chgg(req, **kwargs):
    kwargs["params"]["api_key"] = chgg_key
    return test(req, **kwargs)

# Possible elo parameter values (last one is the default)
# 'BRONZE', 'SILVER', 'GOLD', 'PLATINUM', 'PLATINUM,DIAMOND,MASTER,CHALLENGER'
# (apparently the last one can only be selected by not specifying anything)



# Champions - info for each champion in each role w/ >11% pr & >=1000 gp
# a = chgg(chgg_serv + "champions",
#      params={"limit": 1,
#              # "elo": '',                 # (save all data tho)
#              "elo": 'IRON',                 # (save all data tho)
#              # "champData": "kda",          # use
#              # "champData": "damage",       # use
#              # "champData": "minions",      # use
#              # "champData": "wins",         # use
#              # "champData": "goldEarned",   # use
#              # "champData": "hashes",       # use (summs, r&m, etc)
#              # "champData": "matchups",     # use
#              # "champData": "sprees",
#              # "champData": "wards",
#              # "champData": "totalHeal",
#              # "champData": "overallPerformanceScore",    # use?
#              # "champData": "positions",    # prob dont use.
#              # "champData": "normalized",   # prob not useful (no new info)
#              # "champData": "averageGames", # wtf is this? avg per player?
#              # "champData": "maxMins",      # max/mins over what?

#              # All of the above
#              "champData": "kda,damage,minions,wins,goldEarned," + \
#              "hashes,matchups,sprees,wards,totalHeal," + \
#              "overallPerformanceScore,positions,normalized" + \
#              "averageGames,maxMins",
#      })
# pr(len(a))
# pr(a[0]["patch"])


# Champion (specific)
# for cid in get_champions():
#     time.sleep(chgg_cd)
#     b = chgg(chgg_serv + "champions/" + str(cid),
#          params={"elo": 'GOLD',                 # (save all data tho)
#                  # "champData": "kda",          # use
#                  # "champData": "damage",       # use
#                  # "champData": "minions",      # use
#                  # "champData": "wins",         # use
#                  # "champData": "goldEarned",   # use
#                  # "champData": "hashes",       # use (summs, r&m, etc)
#                  # "champData": "matchups",     # use
#                  # "champData": "sprees",
#                  # "champData": "wards",
#                  # "champData": "totalHeal",
#                  # "champData": "overallPerformanceScore",    # use?
#                  # "champData": "positions",    # prob dont use.
#                  # "champData": "normalized",   # prob not useful (no new info)
#                  # "champData": "averageGames", # wtf is this? avg per player?
#                  # "champData": "maxMins",      # max/mins over what?


#                  # All of the above
#                  # "champData": "kda,damage,minions,wins,goldEarned," + \
#                  # "hashes,matchups,sprees,wards,totalHeal," + \
#                  # "overallPerformanceScore,positions,normalized" + \
#                  # "averageGames,maxMins",


#                  # All except matchups (WORKS)
#                  "champData": "kda,damage,minions,wins,goldEarned," + \
#                  "hashes,sprees,wards,totalHeal," + \
#                  "overallPerformanceScore,positions,normalized" + \
#                  "averageGames,maxMins",


#                  # All useful?
#                  # "champData": "kda,damage,minions,wins,goldEarned"# + \

#                  # "champData": "hashes,sprees,wards,totalHeal"

#                  # "hashes,matchups,sprees,wards,totalHeal," + \
#                  # "overallPerformanceScore" #,positions,normalized" + \
#                  # "averageGames,maxMins",
#          })

cid = 157
# b = chgg(chgg_serv + "champions/" + str(cid),
#     params={
#             # "limit": 1,
#             "elo": 'PLATINUM',
#             # "elo": 'PLATINUM,DIAMOND,MASTER,CHALLENGER',                 # (save all data tho)
#             # "elo": '',
#             # "champData": "kda",          # use
#             # "champData": "damage",       # use
#             # "champData": "minions",      # use
#             # "champData": "wins",         # use
#             # "champData": "goldEarned",   # use
#             # "champData": "hashes",       # use (summs, r&m, etc)
#             # "champData": "matchups",     # use
#             # "champData": "sprees",
#             # "champData": "wards",
#             # "champData": "totalHeal",
#             # "champData": "overallPerformanceScore",    # use?
#             # "champData": "positions",    # prob dont use.
#             # "champData": "normalized",   # prob not useful (no new info)
#             # "champData": "averageGames", # wtf is this? avg per player?
#             # "champData": "maxMins",      # max/mins over what?


#             # Works
#             "champData": "kda,damage,minions,wins,goldEarned," + \
#             "hashes,sprees,wards,totalHeal"#," + \
#             # "overallPerformanceScore,positions,normalized" + \
#             # "averageGames,maxMins",

#             # All of the above
#             # "champData": "kda,damage,minions,wins,goldEarned," + \
#             # "hashes,matchups,sprees,wards,totalHeal," + \
#             # "overallPerformanceScore,positions,normalized" + \
#             # "averageGames,maxMins",

#             # # All except matchups (WORKS)
#             # "champData": "kda,damage,minions,wins,goldEarned," + \
#             # "hashes,sprees,wards,totalHeal," + \
#             # "overallPerformanceScore,positions,normalized" + \
#             # "averageGames,maxMins",

#             # All useful?
#             # "champData": "kda,damage,minions,wins,goldEarned"# + \
#             # "champData": "hashes,sprees,wards,totalHeal"
#             # "hashes,matchups,sprees,wards,totalHeal," + \
#             # "overallPerformanceScore" #,positions,normalized" + \
#             # "averageGames,maxMins",
#      })
# pr(len(b))
# pr([ch['role'] for ch in b])
# pr(b[0])
# pr([(ch['role'], ch['championId'], ch['minionsKilled']) for ch in b if ch['role'] == 'MIDDLE'])

# Print roles summary of matchups returned
# for role in b:
#     print('\n' + role['_id']["role"] + ":")
#     matchups = role["matchups"]
#     summary = dict([(key, len(matchups[key])) for key in matchups])
#     pr(summary)

# Compare champion/champions requests
# print("\n" + str(a == b) + "\n")
# Findings: the two requests return the same exact info for each champion



# Matchups (for a specific champion, all roles)
# url = chgg_serv + "champions/" + str(67) + "/MIDDLE/matchups"
# print(url + "&elo=GOLD&api_key=" + chgg_key)
# c = chgg(url,
#      params={"limit": 100,
#              "elo": '',                 # (save all data tho)
#      })
# pr(c)
# pr(len(c))
# Print roles summary of matchups returned
# matchup_summary = defaultdict(int)
# for matchup in c:
#     matchup_summary[matchup['_id']["role"]] += 1
# print()
# pr(matchup_summary)



# Matchups (for a specific champion in a specific role)
# d = chgg(chgg_serv + "champions/" + str(cid) + "/DUO_CARRY/matchups",
#      params={"limit": 99999999999,
#              "elo": 'GOLD',                 # (save all data tho)
#      })
# pr(d)
# pr(len(d))

# Print roles summary of matchups returned
# matchup_summary = defaultdict(int)
# for matchup in d:
#     matchup_summary[matchup['_id']["role"]] += 1
# print()
# pr(matchup_summary)




# pr_fl("\uc591\uac31\uc774\ub294\ud638\ub450")
# with open('blah.txt', 'w', encoding="utf-8-sig") as f:
#     f.write("y\u00fausha")

# op.gg
# summoner = "Phggy"
# summoner = "TSM BB"
summoner = "EUWent"
# summoner = "Quadro Komalord"
acc_id = "41341986"
summoner_id = "39146998"
region = "euw"

# summoner = "\ub864\ub9c8\uc5d0"
# summoner_id = "7037612"
# summoner = "mikyxd"
# region = "kr"

# summoner = "alvinlin"
# region = "jp"

url = "http://localhost:1337/" + region

# pr(opgg_global_champ_stats()[-1][0][0])


# TEST RENEW SUMMONER UPDATE DELAY -> result: about 4 seconds (avg?) on EUW

# res = []
# res += [ test(url + "/summary/" + summoner + "/ranked") ]

# # Renew Summoner
# t = time.time()
# pr(test(url + "/renew",
#      params={"summonerId": summoner_id}))
# print(time.time() - t)

# # # SummaryRanked
# a = test(url + "/summary/" + summoner + "/ranked")
# # a = test(url + "/summary/" + summoner)
# a = a["data"]
# for k in a["games"][0]:
#     pr((k, a["games"][0][k]))
# # for k in a:
# #     pr((k, a[k]))
#     # pr(k)
#     # if k != "games":
#     #     pr((k, a[k]))

## SummaryCombined
# pr(test(url + "/summary/" + summoner + "/")	)

# for i in range(1, 5):
#     print(time.time() - t)
#     res += [ test(url + "/summary/" + summoner + "/ranked") ]

# pr([r["data"]["recent"]["kdaRatio"] for r in res])
# pr(a)
# pr(a.keys())
# pr(a["data"]["games"][0]["timestamp"] > a["data"]["games"][1]["timestamp"])
# print(max([game["timestamp"] for game in a['data']["games"]]))

# Champions (player specific stats)
# a = test(url + "/champions/" + summoner,
#         params={"season": 13})
# pr(a)
# pr(a[0]["pentaKill"])
# Champions by summoner id
# pr(test(url + "/championsSummId/" + summoner_id,
#         params={"season": 11}))

# Stats (global champion stats)
# pr(test(url + "/stats",
#      params={"type": "win",
#              "league": "iron",
#              "period": "month",
#              "mapId": 1,
#              "queue": "ranked"}))

# AnalyticsByChampion (more global champion stats, for a specific champ)
# pr(test(url + "/analytics/champion",
#      params={"champion": "rumble",
#              "role": "top"}))

# AnalyticsByChampionItems (same but more info on items)
# pr(test(url + "/analytics/champion/items",
#      params={"champion": "rumble",
#              "role": "top"}))

# AnalyticsByChampionSkills (same but more info on skills)
# pr(test(url + "/analytics/champion/skills",
#      params={"champion": "rumble",
#              "role": "top"}))

# AnalyticsByChampionRunes (same but more info on runes)
# pr(test(url + "/analytics/champion/runes",
#      params={"champion": "rumble",
#              "role": "top"}))

# AnalyticsByChampionMasteries (same but more info on masteries)
# pr(test(url + "/analytics/champion/masteries",
#      params={"champion": "rumble",
#              "role": "top"}))

# Analytics (top 5 wr/pr champs in each role, top 10 br champs (kinda useless))
# pr(test(url + "/analytics/summary"))

# Match
# a = test(url + "/match/" + str(4842221062),
#      params={"summoner": summoner})['data']
# pr(a.keys())
# pr(a['team1'].keys())
# pr(a['team1']['players'][0].keys())

# Match graphs
a = test(url + "/matchgraphs/" + str(4842221062),
     params={"summoner": summoner})['data']
pr(a)
# pr(a.keys())
# pr(a['team1'].keys())
# pr(a['team1']['players'][0].keys())

# Live games
# pr(test(url + "/live"))

