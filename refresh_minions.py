#
#  Refresh minions (refresh op.gg API servers and Lorb worker processes)
#


import os

os.chdir("lolorb/")


from Monitoring import *


# git pull & restart workers on host
print(os.getcwd())
res = subprocess.run("./refresh_server.sh", stdout=subprocess.PIPE, shell=True)
res = res.stdout.decode('utf-8')


# Get all worker server ips
res = subprocess.run("sudo rabbitmqctl list_connections > /home/ubuntu/lorb/logs/connections.log", stdout=subprocess.PIPE, shell=True)
res = res.stdout.decode('utf-8')
res = subprocess.run(worker_pyex + " unique_connections.py", stdout=subprocess.PIPE, shell=True)
worker_ips = json.loads(res.stdout.decode('utf-8'))
print(worker_ips)

if len(worker_ips) == 0:
    sys.exit()

# For each worker server
for ip in worker_ips:

    # Run worker refresh script
    kwargs = {
        "close_fds": True,
        "shell": True,
    }
    if os.name == 'nt':
        kwargs["creationflags"] = DETACHED_PROCESS
    subprocess.Popen("ssh -i " + host_pem + " -o StrictHostKeyChecking=no ubuntu@" + ip + " ./lolorb/refresh_worker.sh", **kwargs)


