# Transfer stats databases to remote server


import glob
import subprocess
import os

os.chdir("lolorb/")


from LearningConstants import *
from ServerConstants import *


# to_ip = '18.219.214.152'
# to_ip = 'ec2-18-221-194-8.us-east-2.compute.amazonaws.com'

to_ip = sys.argv[1]
to_pem = sys.argv[2]
n_pats = 18
n_op = int((time.time() - list(patch_tss.values())[0]) / (60 * 60 * 24)) + 1
# n_op = n_gsdb_load
if len(sys.argv) >= 4:
    n_pats = int(sys.argv[3])
    n_op = n_pats


by_patches=True
n_patches=n_pats
oldest_patch=-np.inf
load_max=99


oc = sorted(glob.glob(data_dir+r_cache["opgg_stats"]["path"] + "/*.json"))[::-1][:n_op]
cc_ = sorted(glob.glob(data_dir+r_cache["chgg_champions"]["path"]+"/*.json"))[::-1]
cm_ = sorted(glob.glob(data_dir+r_cache["chgg_matchups"]["path"] +"/*.json"))[::-1]

cc_df = [deglob_fn(fn, '.json') for fn in cc_]
cm_df = [deglob_fn(fn, '.json') for fn in cm_]

if by_patches:
    cc, cm = [], []
    cc_patches, cm_patches = [], []
    patch_count = 0
    for fn in cc_df:
        patch = patch_s2n(fn.split('_')[1])
        if patch < oldest_patch:
            break
        if patch not in cc_patches:
            cc_patches.append(patch)
            cc.append(cc_[cc_df.index(fn)])
            if len(cc) >= min(n_patches, load_max):
                break

    patch_count = 0
    for fn in cm_df:
        patch = patch_s2n(fn.split('_')[1])
        if patch < oldest_patch:
            break
        if patch not in cm_patches:
            cm_patches.append(patch)
            cm.append(cm_[cm_df.index(fn)])
            if len(cm) >= min(n_patches, load_max):
                break

else:
    cc = cc_
    cm = cm_


# for path in cc:

pr_fl(cc + cm)
for path in cc + cm:
    cmd = 'scp -i ' + to_pem + ' -o StrictHostKeyChecking=no -r ' + path + ' ubuntu@' + to_ip + ':/home/ubuntu/lolorb/' + path
    pr_fl(path)
    res = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')

pr_fl(oc)
for path in oc:
    cmd = 'scp -i ' + to_pem + ' -o StrictHostKeyChecking=no -r ' + path + ' ubuntu@' + to_ip + ':/home/ubuntu/lolorb/' + path
    pr_fl(path)
    res = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')


