#
#  Read connections.log and return unique ips
#


from ServerConstants import *


ips = set()
with open("/home/ubuntu/lorb/logs/connections.log", 'r') as f:
    for line in f.readlines():

        if rmq_username in line:
            vals = line.split('\t')
            ip = vals[1]
            if ip != "127.0.0.1":
                ips.add(ip)

print(json.dumps(list(ips)))


