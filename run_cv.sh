for RUN in {0..4}
do
    for FOLD in {0..4}
    do
        if [[ $FOLD == 0 && $RUN == 0 ]]; then
            echo "5x5 cross validation starting from run zero fold zero..."
        fi
        python new_model.py --run=$RUN --fold=$FOLD --triplet --bayop
        # python new_model.py --run=$RUN --fold=$FOLD --rnn --triplet --preprocessed --rnn --nopca
        # python new_model.py --run=$RUN --fold=$FOLD --rnn --triplet --preprocessed --nomtm
        # python new_model.py --run=$RUN --fold=$FOLD --rnn --triplet --preprocessed --rnn --pca
    done
done


