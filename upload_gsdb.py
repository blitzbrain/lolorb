#
#  Upload global stats database to host servers, trigger the upload from them to worker servers
#


import os

# os.chdir("lolorb/")


from Monitoring import *
# sys.exit()

os.chdir("../")
for host in hosts:

    # Transfer front end data (champ_list.js)
    res = subprocess.run("lolorb\\transfer_lorb_data.sh " + host["addr"] + ' ' + host["pem"], stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    pr_fl(res)

    # Transfer backend data (models, infill data, role champion orderings)
    res = subprocess.run("lolorb\\transfer_data.sh " + host["addr"] + ' ' + host["pem"], stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    pr_fl(res)

    # # Transfer backend data (global stats database)
    # res = subprocess.run(pyex + " lolorb\\transfer_data.py " + host["addr"] + ' ' + host["pem"], stdout=subprocess.PIPE, shell=True)
    # res = res.stdout.decode('utf-8')
    # pr_fl(res)

    # Trigger uplaod of data to minion worker servers
    res = subprocess.run("ssh -i " + host["pem"] + " -o StrictHostKeyChecking=no ubuntu@" + host["addr"] + \
        " \"" + worker_pyex + " ~/lolorb/upload_gsdb_minions.py\"",
        stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    pr_fl(res)

    # Refresh the front end site after having refreshed the host worker processes so that they can handle new requests (ie, for new champion)
    res = subprocess.run("ssh -i " + host["pem"] + " -o StrictHostKeyChecking=no ubuntu@" + host["addr"] + \
        " ./lolorb/update_lorb.sh",
        stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    pr_fl(res)


