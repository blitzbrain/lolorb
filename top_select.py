# Final selection for AutoLog + Rolling + LR + RNN-TL

from new_model import *

meta, X, Yc, Yr, Y, meta_labels, X_labels, alpha, beta, X_scales, X_scalesdict, Yr_scales, Yr_scalesdict = load_ld("d_lorb_scaled_l")

# meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels) = load_ld("d_lorbnn")
# shuf_idx = load_ld("model_formats/rnn_mt_model.data", pad=False)["shuf_idx"]
# meta, X, Yc, Yr, shuf_idx = shuffle_data([meta, X, Yc, Yr], return_idx=True, indices=np.argsort(shuf_idx))  #deshuffle

# all_inds = load_ld("model_logs/cv_idx.data", pad=False)
# run_i = 0
# fold_i = 0
# train_idx, test_idx = all_inds[run_i][fold_i][0], all_inds[run_i][fold_i][1]
# shuf_idx = np.hstack([train_idx, test_idx])
# n_train = len(train_idx)
# n_test = X.shape[0] - n_train
# meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)



# for i in range(len(pcs_ratios)):
#     k_key = pcs_ratios_keys[i]
#     k, k_ = pcs_ratios[i]
#     for r in t_roles:
#         if r + '_' + k in X_labels:
#             X[:, X_labels.index(r + '_' + k_key)] = X[:, X_labels.index(r + '_' + k)] - X[:, X_labels.index(r + '_' + k_)]
# pr_fl("Fixed pcs deviations")


# Import post-rnn components
# shuf_idx, _, X_recEnc = load_ld("postRnnEnc4_4")
shuf_idx, X_recEnc, _ = load_ld("postRnnEnc4_4")
meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)
new_X = np.empty((X_recEnc[0].shape[0], X_recEnc[0].shape[1] * 10))
new_labels = sum([[r + '_recEnc__x' + str(i) for i in range(X_recEnc[0].shape[1])] for r in t_roles], [])
for i in N_PL_r:
    new_X[:, (i * X_recEnc[0].shape[1]):((i + 1) * X_recEnc[0].shape[1])] = X_recEnc[i]
# _, _, X_recEnc = load_ld("postMtmEnc")
# X_recEnc = X_recEnc[0]
# new_X = np.empty((X_recEnc.shape[0] // 10, X_recEnc.shape[1] * 10))
# new_labels = sum([[r + '_recEnc__x' + str(i) for i in range(X_recEnc.shape[1])] for r in t_roles], [])
# for i in N_PL_r:
#     new_X[:, (i * X_recEnc.shape[1]):((i + 1) * X_recEnc.shape[1])] = X_recEnc[i::10]

# selector_pre = SelectKBest(k=5)
# n_train = 70000
# selector_pre.fit(new_X[:n_train], Y[:n_train])
# # selector_pre.fit(new_X, Y)
# selected_ls = list(set(['_'.join(new_labels[i].split('_')[2:]) for i in selector_pre.get_support(indices=True)]))
# selected_is = [new_labels.index(l) for l in new_labels if '_'.join(l.split('_')[2:]) in selected_ls]
# new_labels = [new_labels[i] for i in selected_is]
# print("Selected", len(selected_is), "/", new_X.shape[1], "postRnn dimensions")
# print(selected_is)
# sys.exit()
# new_X = new_X[:, selected_is]

X_labels += new_labels
X = np.hstack([X, new_X])
pr_fl("Imported post-RNN encodings")


# Reorder by t_roles
new_ord_pre = [X_labels.index(l) for l in sum([[r + '_' + k[9:] for k in X_labels if "blue_top_" in k and "recEnc__" not in k] for r in t_roles], [])]
new_ord_rec = [X_labels.index(l) for l in sum([[r + '_' + k[9:] for k in X_labels if "blue_top_" in k and "recEnc__" in k] for r in t_roles], [])]
new_ord = new_ord_pre + new_ord_rec
X_labels_rec = [X_labels[i] for i in new_ord_rec]
X_labels_rec_trim = [l for i in X_labels_rec if "blue_top_" in l and "recEnc__" in l]
# print(X_labels_rec_trim)
# print(X_labels_rec)
X_labels = [X_labels[i] for i in new_ord]
X_labels_trim = [k[9:] for k in X_labels if "blue_top_" in k]
# sys.exit()
# print(X_labels_trim.index('opgg_eloavg_champion_today_win_rate'), X_labels_trim.index('chgg_eloavg_champion_kills'))
# sys.exit()
X = X[:, new_ord]


# scaler, selector, X_labels_trim = load_ld("top_selector_recEnt")
scaler = StandardScaler()

X = scaler.fit_transform(X)
n_train = 40000
n_test = 10000
n_try = n_train + n_test
Y = Y.flatten()
X_arrs = [X[:n_try, :len(new_ord_pre)]] + \
         [X[:n_try, [X_labels.index(r + '_' + l[9:]) for r in t_roles]] for l in X_labels if "blue_top_" in l and "recEnc__" in l]
m = get_model("LogisticRegression")
if isinstance(m, MultiOutputClassifier):
    m = m.estimator
m = FoldingWrapper(m, X_arrs)
X_ = np.tile(np.arange(n_try), (len(X_arrs), 1)).T.astype(float)
X_ += np.tile(np.arange(X_.shape[1]), (X_.shape[0], 1)) / 10000
print(X_.shape)


# n_train = 70000
# n_test = 17743


n_feats_try = 32  # Number of transfer learned components to select from
selector = SFS(m, k_features=(2, n_feats_try + 1), forward=True, floating=True, n_jobs=n_parallel_cpu,
    cv=RepeatedStratifiedKFold(n_splits=3, n_repeats=3), verbose=5)
selector.fit(X_[:n_train, :n_feats_try + 1], Y[:n_train], n_jobs=1)
save_ld((scaler, selector, X_labels_trim), "top_selector_recEnt0_0")
print(len(selector.k_feature_idx_), selector.k_score_)
print(selector.k_feature_idx_)
print([X_labels_trim[i] for i in selector.k_feature_idx_])




# new_is = [0, 5]


# X_recEnc_try = ['recEnc__x5', 'recEnc__x51', 'recEnc__x55', 'recEnc__x58', 'recEnc__x107']
# X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if 'recEnc__' in l])
# print(X.shape, len(X_labels))
# X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if 'recEnc__' in l and '_'.join(l.split('_')[2:]) not in X_recEnc_try])
# new_ls_try = [X_labels_trim[i] for i in new_is]
# feat_i = 5
# new_ls_try = X_labels[:len(new_ord_pre)] #+ [l for l in X_labels if l[-len("recEnc__x" + str(feat_i)):] == "recEnc__x" + str(feat_i)]
# X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if l not in new_ls_try])
# print(X.shape, len(X_labels))
# X_train, Y_train = X[:n_train], Y[:n_train]
# X_test, Y_test = X[n_train:n_train + n_test], Y[n_train:n_train + n_test]
# m = get_model("LogisticRegression")
# if isinstance(m, MultiOutputClassifier):
#     m = m.estimator
# m.fit(X_train, Y_train)
# preds = m.predict(X_test)
# score = np.mean(preds == Y_test) * 100
# print(score)


# X_ = selector.transform(X_)  # Doesn't work because we need full dataset within X_arrs
# X_train, Y_train = X_[:n_train], Y[:n_train]
# X_test, Y_test = X_[n_train:n_train + n_test], Y[n_train:n_train + n_test]
# m.fit(X_train, Y_train)
# preds = m.predict(X_test)
# score = np.mean(preds == Y_test)
# print(score)


