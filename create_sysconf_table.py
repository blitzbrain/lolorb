#
#  Create postgres system configuration table (just status of each op.gg region server for now)
#


from sqlalchemy import Column, BigInteger, Integer, SmallInteger, Text, DateTime, Boolean
from sqlalchemy.dialects.postgresql import JSON, JSONB
import sqlalchemy

from ServerConstants import *


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()
meta = sqlalchemy.MetaData(engine)

try:
    sysconf = sqlalchemy.Table('sysconf', meta, autoload=True, autoload_with=engine)
    sysconf.drop()
    print("dropped")
except sqlalchemy.exc.NoSuchTableError as e:
    pass

meta = sqlalchemy.MetaData(engine)

sysconf = sqlalchemy.Table("sysconf", meta,  
    Column('region', Text, primary_key=True),
    Column('status_code', SmallInteger),
    Column('status_text', Text),
    Column('enabled', Boolean),
    Column('message', Text),
)

meta.create_all()


for region in opgg_regions:
    ins = sysconf.insert().values(region=region, status_code=200, status_text="OK", enabled=True, message='')
    engine.execute(ins)

engine.close()
db.dispose()


