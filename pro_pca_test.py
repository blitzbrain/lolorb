#
# Run PCA for 1 to N components, for 1 to 200-size best feature groups,
# saving all predictions, for all repeat runs, so that we can probe results
#


import warnings
warnings.filterwarnings("ignore")

from Server import *
# from Graphing import *


n_splits = 20           # CV parameters
n_repeats = 20


d, D, X_a, X, Y, X_groups, scalers, plist, llist, player_role_perfs, role_league_perf_cache = load_ld("pro_data")
# results = load_ld("pca_results")
results = {}

# Define testing function to be parallelised
def test_pca(X, Y, n_pca, zero_loss=True):
    m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
    if zero_loss:
        m = ZeroLossPCA(m, pca_n=n_pca)
    else:
        m = Pipeline(steps=[
            ('pca', PCA(n_components=n_pca)),
            ('model', SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol))
        ])

    return [cross_val_predict(m, X, Y, cv=StratifiedKFold(n_splits=n_splits, shuffle=True)) for _ in range(n_repeats)]
    # return cross_val_score(m, X, Y, cv=RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats))


# First, run for nsqo (fully fledged) model
selector = load_ld("pro_model_nsqo_selector")
res = {}
for j in selector.subsets_:
    if j > 125:
        continue
    feat_is = selector.subsets_[j]["feature_idx"]
    X_f = np.hstack([X_a[i] for i in feat_is])
    pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
    pr_fl("Running nsqo PCA tests for fset", j)
    res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
    # if j > 50 and j % 25 == 0:
    #     save_ld(results, "pca_results")
results["nsqo"] = res
save_ld(results, "pca_results")
pr_fl("nsqo PCA testing finished")
sys.exit()


# Second, run for sqo (solo-queue only) model
selector = load_ld("pro_model_sqo_selector")
sqlabels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels]
res = {}
for j in selector.subsets_:
    if j > 125:
        continue
    feat_is = selector.subsets_[j]["feature_idx"]
    feat_is = [X_groups.index(sqlabels[i]) for i in feat_is]
    X_f = np.hstack([X_a[i] for i in feat_is])
    pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
    pr_fl("Running sqo PCA tests for fset", j)
    res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
    # if j > 50 and j % 25 == 0:
    #     save_ld(results, "pca_results")
results["sqo"] = res
save_ld(results, "pca_results")
pr_fl("sqo PCA testing finished")
# sys.exit()


# sqpco
selector = load_ld("pro_model_sqpco_selector")
sqpc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels and \
               "_opgg_champion_recent" not in l and "_eloavg_" not in l]
res = {}
for j in selector.subsets_:
    if j > 125:
        continue
    feat_is = selector.subsets_[j]["feature_idx"]
    feat_is = [X_groups.index(sqpc_labels[i]) for i in feat_is]
    X_f = np.hstack([X_a[i] for i in feat_is])
    pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
    pr_fl("Running sqpco PCA tests for fset", j)
    res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
    results["sqpco"] = res
    if j > 50 and j % 25 == 0:
        save_ld(results, "pca_results")
results["sqpco"] = res
pr_fl("sqpco PCA testing finished")


# ppco
selector = load_ld("pro_model_ppco_selector")
ppc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) in pro_full_player_labels or \
               ("_opgg_champion_recent" not in l and "_eloavg_" not in l)]
res = {}
for j in selector.subsets_:
    if j > 125:
        continue
    feat_is = selector.subsets_[j]["feature_idx"]
    feat_is = [X_groups.index(ppc_labels[i]) for i in feat_is]
    X_f = np.hstack([X_a[i] for i in feat_is])
    pca_ns = list(range(1, min(X_f.shape[1], int(X_f.shape[0] * (1 - (1/n_splits))) - 1)))[::-1]
    pr_fl("Running ppco PCA tests for fset", j)
    res[j] = Parallel(n_jobs=n_parallel_cpu, verbose=3)(delayed(test_pca)(X_f, Y, n) for n in pca_ns)
    results["ppco"] = res
    if j > 50 and j % 25 == 0:
        save_ld(results, "pca_results")
pr_fl("ppco PCA testing finished")


