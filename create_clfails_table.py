#
#  Create postgres request cache table
#


from sqlalchemy import Column, BigInteger, Text, DateTime
from sqlalchemy.dialects.postgresql import JSON, JSONB
import sqlalchemy

from ServerConstants import *


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()
meta = sqlalchemy.MetaData(engine)

try:
    clfails = sqlalchemy.Table('clfails', meta, autoload=True, autoload_with=engine)
    clfails.drop()
    print("dropped")
except sqlalchemy.exc.NoSuchTableError as e:
    pass

meta = sqlalchemy.MetaData(engine)

sqlalchemy.Table("clfails", meta,  
    Column('id', Text, primary_key=True),
    Column('source', Text),
    Column('chat_log', Text),
)

meta.create_all()
engine.close()
db.dispose()


