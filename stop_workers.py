#
#  Python script to stop all Match Prediction worker processes
#


from ServerConstants import *


for process in psutil.process_iter():
    cmd = None
    try:
        cmd = process.cmdline()
    except Exception as e:
        # print(e) # Access denied errors
        continue

    if type(cmd) is list and len(cmd) > 0 and isinstance(cmd[0], str) and ' '.join(cmd[:3]) == worker_start_cmd:
        # print(cmd)
        pid = process.pid
        print(cmd, pid, type(pid))
        os.kill(pid, signal.SIGTERM)


