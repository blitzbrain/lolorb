# locally or on host


addr=$1
pem=$2

# scp -i $pem -o StrictHostKeyChecking=no -r mlxtend ubuntu@$addr:/home/ubuntu/

cd ./lolorb

scp -i $pem -o StrictHostKeyChecking=no patches_all.json ubuntu@$addr:/home/ubuntu/lolorb/
cd data
cd learning_data
scp -i $pem -o StrictHostKeyChecking=no models_fin.data ubuntu@$addr:/home/ubuntu/lolorb/data/learning_data/
scp -i $pem -o StrictHostKeyChecking=no pro_model_nsqo.data ubuntu@$addr:/home/ubuntu/lolorb/data/learning_data/
scp -i $pem -o StrictHostKeyChecking=no pro_model_sqo.data ubuntu@$addr:/home/ubuntu/lolorb/data/learning_data/
# scp -i $pem -o StrictHostKeyChecking=no pro_d.data ubuntu@$addr:/home/ubuntu/lolorb/data/learning_data/
scp -i $pem -o StrictHostKeyChecking=no pro_sq_models.data ubuntu@$addr:/home/ubuntu/lolorb/data/learning_data/
cd ../
cd infill_data
scp -i $pem -o StrictHostKeyChecking=no infill_data.json ubuntu@$addr:/home/ubuntu/lolorb/data/infill_data/
scp -i $pem -o StrictHostKeyChecking=no rec_infill_data.json ubuntu@$addr:/home/ubuntu/lolorb/data/infill_data/
cd ../
scp -i $pem -o StrictHostKeyChecking=no -r riot_cache ubuntu@$addr:/home/ubuntu/lolorb/data/
scp -i $pem -o StrictHostKeyChecking=no -r datadragon_cache ubuntu@$addr:/home/ubuntu/lolorb/data/
scp -i $pem -o StrictHostKeyChecking=no role_champion_orderings.json ubuntu@$addr:/home/ubuntu/lolorb/data/


