#
#  Model Definitions
#

from copy import deepcopy

import os
import scipy

from sklearn.multioutput import *
from sklearn.metrics import *
from sklearn.pipeline import *
from sklearn.preprocessing import *
from sklearn.feature_selection import *
from sklearn.model_selection import *
from sklearn.calibration import *

from sklearn.tree import *
from sklearn.ensemble import *
from sklearn.cluster import *
from sklearn.neighbors import *
from sklearn.svm import *
from sklearn.linear_model import *
from sklearn.gaussian_process import *
from sklearn.gaussian_process.kernels import *
from sklearn.neural_network import *
from sklearn.decomposition import *
from sklearn.discriminant_analysis import *
from sklearn.naive_bayes import *

import os, sys
mlx_path = os.getcwd().split('/lolorb')[0] + '/mlxtend'
# mlx_remote_path = '/home/ubuntu/mlxtend'
# succ = False
try:
    if os.path.exists(mlx_path):
        sys.path.insert(0, mlx_path)
except Exception as e:
    pass
# try:
#     if os.path.exists(mlx_remote_path):
#         sys.path.insert(0, mlx_remote_path)
# except:
#     pass
from mlxtend.feature_selection import SequentialFeatureSelector as SFS


class kMeansSVC(BaseEstimator, ClassifierMixin):

    def __init__(self, k=17, n_init=20, n_jobs=1):
        self.k = k
        self.n_init = 20
        self.n_jobs = n_jobs
        self.classes_ = [0, 1]

    def _conv_X(self, X):  # convert to players format (assumes t_roles ordering of label groups)
        return X.reshape((X.shape[0] * 10, X.shape[1] // 10))

    def _deconv_X(self, X):
        X = X.reshape((X.shape[0] // 10, 10, self.k))
        return np.hstack([np.sum(X[:, :5], axis=1), np.sum(X[:, 5:], axis=1)])

    def transform_X(self, X):
        X = self._conv_X(X)
        X = self.km.transform(X)
        X = self._deconv_X(X)
        return X

    def fit(self, X, y):
        X = self._conv_X(X)
        self.km = KMeans(init='k-means++', n_clusters=self.k, n_init=self.n_init, n_jobs=self.n_jobs)
        X = self.km.fit_transform(X)  # Use cluster distances
        X = self._deconv_X(X)
        self.svc = SVC(gamma='auto', kernel='rbf', C=1.0, tol=1e-3)
        self.svc.fit(X, y)
        return self

    def predict(self, X):
        X = self.transform_X(X)
        return self.svc.predict(X)

    def predict_proba(self, X):
        X = self.transform_X(X)
        return self.svc.predict_proba(X)

    # def __init__(self, *args, **kwargs)

#     class 
# res = []
# for k in range(2, 15):
    
#     n_train, n_test = 5000, 17743
#     Y_train, Y_test = Y[:n_train], Y[-n_test:]
#     X_train = X_pl[:(n_train * 10)]
#     X_test = X_pl[-(n_test * 10):]
#     print(X_train.shape)
#     print("Fitting kmeans...")
# #     X_train = km.fit_transform(X_train)  # Use cluster distances
# #     X_test = km.transform(X_test)
#     km.fit(X_train)                         # Use cluster classification
#     X_train = km.predict(X_train)
#     X_test = km.predict(X_test)
#     X_train = np.eye(k)[X_train]
#     X_test = np.eye(k)[X_test]
#     X_train = X_train.reshape((n_train, 10, k))
#     X_test = X_test.reshape((n_test, 10, k))
#     X_train = np.hstack([np.sum(X_train[:, :5], axis=1), np.sum(X_train[:, 5:], axis=1)])
#     X_test = np.hstack([np.sum(X_test[:, :5], axis=1), np.sum(X_test[:, 5:], axis=1)])
#     print(X_train.shape, X_test.shape, "Fitting SVM...")
# #     m = SVC(gamma='auto', kernel='rbf', C=1.0, tol=1e-3)
#     m = LinearSVC(dual=False, max_iter=3000, tol=1e-5, penalty='l1')
#     print(X_train[:10])
#     m.fit(X_train, Y_train)
#     preds = m.predict(X_test)
#     score = np.mean(preds == Y_test) * 100
#     res.append((k, km.inertia_, score))
#     print(res[-1])


class SoftmaxRidgeClassifier(RidgeClassifier):  # Convert a decision function into a probability (> or < mean of decision function)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def predict_proba(self, X): # Implemented for n_classes = 2 only
        pr = self.decision_function(X)
        max_pr = max(np.abs(pr))
        pr += max_pr
        max_pr *= 2
        pr = np.atleast_2d(pr).T
        pr = np.hstack([max_pr-pr, pr])
        return np.exp(pr) / np.sum(np.exp(pr), axis=1, keepdims=True)

# class ProbabiliseClassifier(BaseEstimator, ClassifierMixin):  # Convert a decision function into a probability (> or < 0)

#     def __init__(self, base_estimator):
#         self.base_estimator = base_estimator

#     def fit(self, X, Y):
#         self.base_estimator.fit(X, Y)

#     def predict(self, X):
#         return self.base_estimator.predict(X)

#     def predict_proba(self, X): # Implemented for n_classes = 2 only
#         pr = self.base_estimator.decision_function(X)
#         max_pr = max(np.abs(pr))
#         pr /= max_pr * 2
#         pr += 0.5
#         pr = np.atleast_2d(pr).T
#         pr = np.hstack([max_pr-pr, pr])
#         return np.exp(pr) / np.sum(np.exp(pr), axis=1, keepdims=True)

class MeanProbPseudo(BaseEstimator, ClassifierMixin):
    
    def __init__(self, X_labels, alpha=None):
        self.alpha = alpha
        ch_labels = [l for l in X_labels if "_chgg_eloavg_champion_win_rate" in l]
        self.ch_blue_lis = [X_labels.index(l) for l in ch_labels if l[:5] == "blue_"]
        self.ch_red_lis = [X_labels.index(l) for l in ch_labels if l[:4] == "red_"]
        if alpha != 0:
            mu_labels = [l for l in X_labels if "_chgg_eloavg_matchup_win_rate" in l]
            self.mu_blue_lis = [X_labels.index(l) for l in mu_labels if l[:5] == "blue_"]
            self.mu_red_lis = [X_labels.index(l) for l in mu_labels if l[:4] == "red_"]
    
    def get_probability(self, X, a):
        res = np.mean(np.hstack([X[:, self.ch_blue_lis], 1.0 - X[:, self.ch_red_lis]]), axis=1, keepdims=True)
        if a != 0:
            res = (res * (1.0 - a)) + (a * \
                np.mean(np.hstack([X[:, self.mu_blue_lis], 1.0 - X[:, self.mu_red_lis]]), axis=1, keepdims=True))
        return np.hstack([1.0 - res, res])
    
    def fit(self, X, Y):
        if self.alpha != 0:
            def get_loss(a):
                a = a / 1
                probs = self.get_probability(X, a)[:, 1]
                preds = np.round(probs).astype(int)
                return 1.0 - (sum(preds == Y) / len(preds)) * 1
            sol = scipy.optimize.minimize(get_loss, [0.05], bounds=[(0, 1)], options={'eps': 0.06, 'gtol': 1e-10})
            # print(sol)
            self.alpha = sol.x
        return self
        
    def predict_proba(self, X):
        return self.get_probability(X, self.alpha)
    
    def predict(self, X):
        return np.round(self.predict_proba(X)[:, 1])

class ApproximalWrapper(BaseEstimator, ClassifierMixin):

    def __init__(self, base_estimator, max_bias=0.15):
        self.base_estimator = base_estimator
        self.max_bias = max_bias

    def predict_proba(self, X):
        return (0.5 - self.max_bias) + (self.base_estimator.predict_proba(X) * (self.max_bias * 2))

class FeatSelectWrapper(BaseEstimator, ClassifierMixin):

    def __init__(self, base_estimator,
      # percentiles=[50, 75, 87.5, 100]):
      # percentiles=[12.5, 25, 50, 75, 87.5, 100]):
      # percentiles=[25, 37.5, 50, 62.5, 75, 87.5, 100]):
      # percentiles=[18.75, 25, 37.5, 50, 62.5, 75, 87.5, 100]):
      # percentiles=[12.5, 18.75, 25, 37.5, 50, 62.5, 75, 87.5, 100]):
      # percentiles=[6.25, 12.5, 18.75, 25, 37.5, 50, 62.5, 75, 87.5, 100]):
      percentiles=[50, 62.5, 75, 87.5, 100]):
      # percentiles=[75, 87.5, 100]):
        self.base_estimator = base_estimator
        self.percentiles = percentiles
        self.model = None
        self.inds = None
        self.scores = None
        self.classes_ = [0, 1]
        self.perc = 100

    def fit(self, X, Y, X_test=None, Y_test=None):
        if X_test is None:
            X_test, Y_test = X, Y
        curr_acc = 0
        # selector = SelectPercentile(percentile=p, score_func=f_classif)
        # selector.fit(X, Y)
        scores_, _ = f_classif(X, Y)
        n_feats = X.shape[1]
        _, inds = zip(*sorted(zip(scores_, range(n_feats)))[::-1])
        self.scores, inds = zip(*sorted(zip(scores_, range(n_feats)))[::-1])
        inds = list(inds)
        for p in self.percentiles:
            model = self.base_estimator
            inds_, preds = None, None
            if p < 100:
                inds_ = inds[:int((p / 100.0) * n_feats)]
                # model = Pipeline([
                #     ('select', SelectPercentile(percentile=p, score_func=f_classif)),
                #     ('model', self.base_estimator),
                # ])
            else:
                inds_ = inds

            np.random.shuffle(inds_)
            model.fit(X[:, inds_], Y)
            preds = model.predict(X_test[:, inds_])
            acc = np.mean(preds == Y_test.flatten()) * 100.0
            if acc > curr_acc:
                curr_acc = acc
                self.model = deepcopy(model)
                self.inds = inds_
                self.perc = p
        return self

    def predict_proba(self, X):
        return self.model.predict_proba(X[:, self.inds])

    def predict(self, X):
        return self.model.predict(X[:, self.inds])

class ZeroWrapper(BaseEstimator, ClassifierMixin):

    def __init__(self, base_estimator, all_labels, labels, centers, scales):
        self.base_estimator = base_estimator
        self.all_labels = all_labels
        self.labels = labels
        incl_is = [all_labels.index(l) for l in labels]
        self.incl_is = incl_is
        self.centers = centers[incl_is]
        self.scales = scales[incl_is]
        self.coef_ = self.base_estimator.coef_[:, incl_is]
        self.intercept_ = self.base_estimator.intercept_
        self.classes_ = [0, 1]

    def _transform(self, X):
        X = (X - self.centers) / self.scales
        X_ = np.zeros((X.shape[0], len(self.all_labels)))
        X_[:, self.incl_is] = X
        return X_

    def fit(self, X, y):
        return self

    def predict_proba(self, X):
        return self.base_estimator.predict_proba(self._transform(X))

    def predict(self, X):
        return self.base_estimator.predict(self._transform(X))


# Shuffle dataset (list of arrays + X_rec (recent games list of lists))
def shuffle_data(data, X_rec=None, return_idx=False, indices=None):
    if indices is None:
        indices = np.arange(data[0].shape[0]);
        np.random.shuffle(indices)
    res = [d[indices] for d in data]
    if X_rec is not None:
        X_rec = [X_rec[i] for i in indices]
        res.append(X_rec)
    if return_idx:
        res.append(indices)
    return res

class ZeroLossPCA(BaseEstimator, ClassifierMixin):
    def __init__(self, base_estimator, pca_n=1, tol=0.01, incremental_pca=False):
        self.base_estimator = base_estimator
        self.pca_n = pca_n
        self.tol = tol
        self.incremental_pca = incremental_pca
        self.classes_ = [0, 1]

    def transform(self, X):
        if self.pca is None:
            return X
        if len(self.zero_components) < 1:
            return self.pca.transform(X)
        return np.hstack([self.pca.transform(X[:, self.nonzero_components]), X[:, self.zero_components]])

    def fit(self, X, Y):
        # self.pca = PCA(n_components=self.pca_n, svd_solver='full').fit(X)
        self.pca = PCA(n_components=self.pca_n).fit(X)
        components = self.pca.components_
        n = components.shape[1]
        self.zero_components = [i for i in range(n) if np.abs(components[:, i]).sum() < self.tol]
        if len(self.zero_components) < 1:
            self.base_estimator.fit(self.pca.transform(X), Y)
            return self
        self.nonzero_components = [i for i in range(n) if i not in self.zero_components]
        # if len(self.zero_components) > self.pca_n:
        #     # raise ValueError("ZeroLossPCA Error: Too many zero components (check data?)")
        #     print("ZeroLossPCA Error: Too many zero components (check data?), skipping PCA....")
        #     self.pca = None
        #     self.base_estimator.fit(X, Y)
        #     return self
        pca_n = self.pca_n
        if len(self.nonzero_components) < self.pca_n:
            pca_n = len(self.nonzero_components)
        X_new = X[:, self.nonzero_components]
        X_rest = X[:, self.zero_components]
        self.pca = IncrementalPCA(n_components=pca_n) if self.incremental_pca else PCA(n_components=pca_n)
        # self.pca = IncrementalPCA(n_components=pca_n) if self.incremental_pca else PCA(n_components=pca_n, svd_solver='full')
        X_new = self.pca.fit_transform(X_new)
        X_new = np.hstack([X_new, X_rest])
        self.base_estimator.fit(X_new, Y)
        return self

    def predict(self, X):
        return self.base_estimator.predict(self.transform(X))

    def predict_proba(self, X):
        return self.base_estimator.predict_proba(self.transform(X))

# Folding model wrapper
#
# This wrapper makes it possible to select groups of features, instead of one feature at a time.
# It is useful for feature selection methods when there are groups of related but independent features, which should be selected
# as a group because if one of the members is chosen by the selection algorithm, it is highly likely that the others will be too.
# This can speed up the selection by a factor equal to the average group size.
#
# This wrapper also implements an optional PCA dimensionality reduction, which can be set to trigger only above a certain
# total feature count threshold. It can also be set to reduce the number of features by a fraction of the total count
#
# if pca is None,
#     do not use PCA
# if pca is an integer and pca_n is None,
#     use PCA when we have more than pca features, reduce the dimensionality to pca features
# if pca is an integer and pca_n is an integer,
#     use PCA when we have more than pca features, reduce the dimensionality to min( pca_n, the number of input features )
# if pca is an integer and pca_n is a float (between 0 and 1),
#     use PCA when we have more than pca features, reduce the dimensionality to round( pca_n * the number of input features )
class FoldingWrapper(BaseEstimator, ClassifierMixin):
    def __init__(self, base_estimator, X_arrs, pca=None, pca_n=None, pca_max=None):
        self.base_estimator = base_estimator
        self.X_arrs = X_arrs
        self.pca = pca
        self.pca_n = pca_n
        self.pca_max = np.inf if pca_max is None else pca_max
        self.classes_ = [0, 1]

    def _conv_X(self, X):
        data_is = np.round(X[:, 0]).astype(int)
        feat_is = np.round((X[0] - np.round(X[0])) * 10000).astype(int)
        return np.hstack([self.X_arrs[j][data_is] for j in feat_is])
        
    def fit(self, X, Y, shuffle=True):
        X = self._conv_X(X)
        if shuffle:
            X, Y = shuffle_data([X, Y])
        if self.pca is not None:
            if X.shape[1] <= self.pca:  # If we have fewer features than the pca threshold
                # if isinstance(self.base_estimator, Pipeline):
                #     self.base_estimator = self.base_estimator.steps[1][1]
                if isinstance(self.base_estimator, ZeroLossPCA):
                    self.base_estimator = self.base_estimator.base_estimator
            else:
                # if not isinstance(self.base_estimator, Pipeline):
                #     self.base_estimator = Pipeline(steps=[
                #         ('pca', PCA(n_components=self.pca)),
                #     #     ('pca', KernelPCA(kernel='linear', n_components=self.pca)),
                #     #     ('pca', IncrementalPCA(n_components=self.pca)),
                #         ('model', self.base_estimator),
                #     ])
                if not isinstance(self.base_estimator, ZeroLossPCA):
                    self.base_estimator = ZeroLossPCA(self.base_estimator,
                        pca_n=self.pca if self.pca_n is None else (
                            min(round(self.pca_n * X.shape[1]), self.pca_max) if isinstance(self.pca_n, float) else self.pca_n))
        self.base_estimator.fit(X, Y)
        return self

    def predict_proba(self, X):
        return self.base_estimator.predict_proba(self._conv_X(X))

    def predict(self, X):
        return self.base_estimator.predict(self._conv_X(X))


_cv_n_jobs = os.cpu_count()
if _cv_n_jobs < 16:
    _cv_n_jobs = 4


# Models library
models_lib = {
    "AdaBoostClassifier": MultiOutputClassifier(AdaBoostClassifier(n_estimators=50)),
    "AdaBoostRegressor": MultiOutputClassifier(AdaBoostRegressor()),
    "GradientBoostingClassifier": MultiOutputClassifier(GradientBoostingClassifier()),
    "GradientBoostingRegressor":  MultiOutputClassifier(GradientBoostingRegressor()),
    "RandomForestClassifier": MultiOutputClassifier(RandomForestClassifier()),
    "RandomForestRegressor": MultiOutputRegressor(RandomForestRegressor()),
    "DecisionTreeClassifier": MultiOutputClassifier(DecisionTreeClassifier()),
    "DecisionTreeRegressor": MultiOutputRegressor(DecisionTreeRegressor()),

    "KNeighborsRegressor": MultiOutputRegressor(KNeighborsRegressor()),
    "KNeighborsClassifier": MultiOutputClassifier(KNeighborsClassifier()),

    "MLPRegressor": MLPRegressor(hidden_layer_sizes=(1024, 1024, 1024, 1024)),
    # "MLPClassifier": MLPClassifier(hidden_layer_sizes=(1024, 1024, 1024, 1024)),
    "MLPClassifier": MLPClassifier(hidden_layer_sizes=(256, 256), verbose=True,
        max_iter=50, early_stopping=True, learning_rate_init=0.001),

    "GaussianProcessClassifier": MultiOutputClassifier(GaussianProcessClassifier(n_restarts_optimizer=5)),
    "GaussianProcessRegressor": MultiOutputClassifier(GaussianProcessRegressor(n_restarts_optimizer=5)),

    "SVC": MultiOutputClassifier(SVC(gamma='auto', kernel='rbf', C=0.9, tol=1e-3)),
    "SVR": MultiOutputRegressor(SVR(gamma='auto', kernel='rbf', C=0.9, tol=1e-3)),
    "NuSVC": MultiOutputClassifier(NuSVC(nu=0.5, gamma='auto', kernel='rbf', tol=1e-3)),
    "NuSVR": MultiOutputClassifier(NuSVR(nu=0.5, gamma='auto', kernel='rbf', tol=1e-3)),
    "LinearSVC": MultiOutputClassifier(LinearSVC(dual=False, max_iter=3000, tol=1e-5, penalty='l1')),
    "LinearSVR": MultiOutputRegressor(LinearSVR(dual=False, max_iter=3000, tol=1e-5)),

    "LogisticRegression": MultiOutputClassifier(LogisticRegression(solver='lbfgs', C=2.783, tol=1e-5, max_iter=3000)),
    "LogisticRegressionCV": (LogisticRegressionCV(solver='lbfgs', cv=RepeatedStratifiedKFold(n_splits=3, n_repeats=3),
                                tol=1e-5, max_iter=3000, Cs=[np.e ** (v) for v in (np.arange(-2, 4) / 2)], n_jobs=_cv_n_jobs)),
    "SoftmaxRidgeClassifier": MultiOutputClassifier(SoftmaxRidgeClassifier(alpha=0.1)),
    "RidgeClassifier": MultiOutputClassifier(RidgeClassifier()),
    "RidgeClassifierCV": MultiOutputClassifier(RidgeClassifierCV()),

    "LinearDiscriminantAnalysis": MultiOutputClassifier(LinearDiscriminantAnalysis()),
    "QuadraticDiscriminantAnalysis": MultiOutputClassifier(QuadraticDiscriminantAnalysis()),

    "GaussianNB": MultiOutputClassifier(GaussianNB()),
    "BernoulliNB": MultiOutputClassifier(BernoulliNB()),

    "MultiTaskLasso": MultiTaskLasso(),
    "MultiTaskLassoCV": MultiTaskLassoCV(),
    "MultiTaskElasticNet": MultiTaskElasticNet(),
    "MultiTaskElasticNetCV": MultiTaskElasticNetCV(),
}

model_decision_funcs = {
    "AdaBoostClassifier": ["decision_function", "predict_proba"],
    "LogisticRegression": ["decision_function", "predict_proba"],
    "LogisticRegressionCV": ["decision_function", "predict_proba"],
    "LinearDiscriminantAnalysis": ["decision_function", "predict_proba"],
    "RandomForestClassifier": ["predict_proba"],
    "LinearSVC": ["decision_function"],
    "RidgeClassifier": ["decision_function"],
    "RidgeClassifierCV": ["decision_function"],
    "MLPClassifier": ["predict_proba"],
}

# Add versions of each model with prior dimensionality reductions
_ndim = 77 # Latent space dimensionality
for key in list(models_lib.keys()):
    models_lib[key + "+PCA"] = Pipeline(steps=[
        ("pca", IncrementalPCA(n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCAlinear"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='linear', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCArbf"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='rbf', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCApoly"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='poly', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCAsigmoid"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='sigmoid', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+FA"] = Pipeline(steps=[
        ("pca", FactorAnalysis(n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+NMF"] = Pipeline(steps=[
        ("pca", NMF(n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+LinearDA"] = Pipeline(steps=[
        ("pca", LinearDiscriminantAnalysis(n_components=_ndim)),
        ("model", models_lib[key]),
    ])


# Canoncial models to test
models_c = [
    "LogisticRegression",
    "MLPClassifier",
    "AdaBoostClassifier",
    # "GradientBoostingClassifier",
    # "RandomForestClassifier",
    # "DecisionTreeClassifier",
    # "SGDClassifier",
    # "SVC",
    # "GaussianProcessClassifier",
    # "KNeighborsClassifier",
]
models_r = [
    "MLPRegressor",
    "AdaBoostRegressor",
    # "RandomForestRegressor",
    # "DecisionTreeRegressor",
    # "KNeighborsRegressor",
    # "SGDRegressor",
    # "GaussianProcessRegressor",
]


def get_model(m):
    return deepcopy(models_lib[m])


