#
#  Restart workers
#


from ServerConstants import *


n_restart = int(sys.argv[1]) if len(sys.argv) >= 2 else n_mp_workers

create_folder(logs_dir)
logf = open(combined_log_path, 'a')


for i in range(n_restart):
    start_command = worker_start_cmd + ' ' + str(i)
    # start_command += ('' if len(sys.argv) >= 3 else ' 1> logs/worker_' + str(i) + '/output.log 2>&1')
    process_is_running = False

    create_folder(logs_dir)
    create_folder(logs_dir + "worker_" + str(i) + '/')

    for process in psutil.process_iter():
        cmd = None
        try:
            cmd = process.cmdline()
        except Exception as e:
            # print(e) # Access denied errors
            continue

        if type(cmd) is list and len(cmd) > 0 and isinstance(cmd[0], str) and ' '.join(cmd) == start_command:
            # print(cmd)
            process_is_running = True
            pid = process.pid
            # print(cmd, pid, type(pid))
            os.kill(pid, signal.SIGTERM)
            subprocess.Popen(start_command, stdout=logf, stderr=logf, close_fds=True, shell=True)
            print("worker " + str(i) + " restarted, waiting " + str(restart_interval) + "s...")
            time.sleep(restart_interval)
            break

    if not process_is_running:
        subprocess.Popen(start_command, stdout=logf, stderr=logf, close_fds=True, shell=True)
        print("worker " + str(i) + " started")
        time.sleep(1)
        # time.sleep(restart_interval)


