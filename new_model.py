#
#  Trains new model (if launched with option -extract, also perform feature extraction)
#


from Learning import *


def preprocess_features(meta, X, Yc, Yr, meta_labels, X_labels, Yc_labels, Yr_labels, n_train,
            shuffle=True, premade_shuf=False, autolog=True, rollAL=True, a=0.1, b=0.4, g=0.1, verbose=True):
            # shuffle=True, premade_shuf=False, autolog=True, aa=0.02, ab=0.35, ag=0.02):
    if shuffle:
        if verbose:
            pr_fl("Shuffling...")
        if premade_shuf:
            shuf_idx = load_ld("model_formats/rnn_mt_model.data", pad=False)["shuf_idx"]
            meta, X, Yc, Yr, shuf_idx = shuffle_data([meta, X, Yc, Yr], return_idx=True, indices=shuf_idx)
        else:
            meta, X, Yc, Yr = shuffle_data([meta, X, Yc, Yr])

    alpha, beta = 1.0, 1.0
    # # pr_fl("Adding beta winrates...")
    # X, X_labels = add_beta_winrates(alpha, beta, X, X_labels, None)

    X_scales, X_scalesdict = None, None
    Yr_scales, Yr_scalesdict = None, None
    if not autolog:
        # pr_fl("Scalin|g...")
        # pr_fl(Yr.min(), X.min(), Yr.max(), X.max()) # Scale data to between 0 & 1 (remember scaling values)
        X_scales, X_scalesdict = scale_data(X, X_labels, n_train)
        Yr_scales, Yr_scalesdict = scale_data(Yr, Yr_labels, n_train)
        # pr_fl(Yr.min(), X.min(), Yr.max(), X.max())
        # X_scales, X_scalesdict, Yr_scales, Yr_scalesdict = [None] * 4
    else:
        # X_labels_al, _, _, X_mins, X_alphas, X_betas, X_gammas, _, _, _, \
        #      X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler = \
        #     load_ld("model_logs/AutoLogTransf2_1.data", pad=False)
        # X_alphas_dict = dict(zip(X_labels_al, X_alphas))
        # X_betas_dict = dict(zip(X_labels_al, X_betas))
        # X_gammas_dict = dict(zip(X_labels_al, X_gammas))
        # X_mins_dict = dict(zip(X_labels_al, X_mins))
        # X_center_dict = dict(zip(X_labels_al, scaler.center_))
        # X_scales_dict = dict(zip(X_labels_al, scaler.scale_))

        # When only need X_mins
        # X_labels_al, X_mins, \
        #      X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler = \
        #     load_ld("model_logs/Xmins0_1.data", pad=False)
        # X_mins_dict = dict(zip(X_labels_al, X_mins))
        # X_center_dict = dict(zip(X_labels_al, scaler.center_))
        # X_scales_dict = dict(zip(X_labels_al, scaler.scale_))
        # X = scale_data_with_dict(X, X_labels, X_scalesdict)

        X_scales, X_scalesdict = scale_data(X, X_labels, n_train)
        Yr_scales, Yr_scalesdict = scale_data(Yr, Yr_labels, n_train)

        scaler = RobustScaler()
        scaler.fit(X[:n_train])
        X = scaler.transform(X)

        X_mins = np.min(X[:n_train], axis=0)
        # X_mins_dict = dict(zip(X_labels, X_mins))

        for i in range(len(X_labels)):
            k = X_labels[i]
            if (not rollAL) and "ravg_" in k:
                continue

            # X[:, i] = (X[:, i] - X_center_dict[k]) / X_scales_dict[k]

            X[:, i] = ( a * X[:, i]) + np.log((b * \
                      (max(g, 1e-8) + np.maximum(X[:, i] - X_mins[i], 0.))).astype(np.float32))

            # X[:, i] = (X_alphas_dict[k] * X[:, i]) + np.log((X_betas_dict[k] * \
            #           (max(X_gammas_dict[k], 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = (0.0544893670430943 * X[:, i]) + np.log((0.4725959878229357 * \
            #           (max(0.06129410022362503, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = (0.1 * X[:, i]) + np.log((0.4 * \
            #           (max(0.1, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = (0.02 * X[:, i]) + np.log((0.35 * \
            #           (max(0.02, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = ( 0.01171 * X[:, i]) + np.log((0.4325 * \
            #           (max(0.01161, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = ( 0.15  * X[:, i]) + np.log(( 0.336 * \
            #           (max(0.05012, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = ( 0.001 * X[:, i]) + np.log((0.3 * \
            #           (max(0.15, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = ( 0.04623 * X[:, i]) + np.log((0.5 * \
            #           (max(0.02707, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))
            # X[:, i] = np.log((0.35 * \
            #           (max(0.02, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))

    y_label = "blue_win"
    Y = Yc[:, Yc_labels.index(y_label)].reshape(-1, 1)

    X_autolog = (a, b, g)
    return meta, X, Yc, Yr, Y, X_labels, alpha, beta, X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins


# Clean features (non-rolling)
def clean_features(meta, X, meta_labels, X_labels):
    # Add features as metadata and remove redundant features
    new_meta_ls = [r + "_elo" for r in t_roles]
    meta_labels += new_meta_ls
    meta = np.hstack([meta, X[:, [X_labels.index(l) for l in new_meta_ls]]])
    # new_meta_ls = [r + "_pick_position" for r in t_roles]
    # meta_labels += new_meta_ls
    # meta = np.hstack([meta, X[:, [X_labels.index(l) for l in new_meta_ls]]])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_pick_position" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_spell_" in l])

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_recent_first_season" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_recent_last_season" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_first_ranked_season" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_last_ranked_season" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_highest_achieved_season_tier" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_champion__x" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "patch__x" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "season__x" in l])

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_" in l and (
                                                    "_total_wins" in l or "_total_losses" in l)])

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_tier" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_league_points" in l])

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_rs_wins" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_rs_losses" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_rs_win_rate" in l])

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_division" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_rank" in l and "_champion_" not in l])

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_all_" in l and l.split('_')[-2] in champ_dict])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_champion_season_" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_wins" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_losses" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_winrate" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_win" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_kda_ratio" in l])

    X, X_labels = remove_data_subset(X, X_labels, duration_labels)

    return meta, X, meta_labels, X_labels


def filter_features(X, X_labels):
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_avg_bayes_win_rate" in l]) # just redundant
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_avg_alltotal_bayes_win_rate" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_recent_games" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_recent_bayes_win_rate" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_recent_total_games" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_recent_total_bayes_win_rate" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_recent_alltotal_bayes_win_rate" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_recent_alltotal_games" in l]) # better version using champion_recent_games

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_division" in l]) # equivalent elo_ features
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_rank" in l and "_champion_" not in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_rs_recent_" in l])         # better repr by Nravg
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_champion_season_" in l]) # better repr by champion_recent
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_champion_seasonavg_" in l]) # better repr by champion_avg
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_opgg_champion_all_" in l and l.split('_')[-2] in champ_dict])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_kda_ratio" in l]) # low granularity approximation
    # we have a better version using opgg
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_creep_score_/_chgg_eloavg_champion_creep_score" in l])
    # better version in ravg_time_since_match
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "n_recent_matches" in l])
    # better version in opgg_champion_recent_mean_rank
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_rank" in l])
    # not included idk why its noisy, missing at random and misleading
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_mean_rank" in l])
    # better version in ravg_creep_score
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_creep_score_per_second" in l])
    # better version in ravg_opgg_champion_recent_mean_rank
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_mean_max_rank" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_max_rank" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_opgg_champion_recent_rank" in l])
    # better version in opgg_champion_avg_alltotal_games
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_games" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_alltotal_games" in l])
    # gold is basically a less informative derivative of kills, assists and creep score
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_gold" in l])
    # better version using just the means, rather than the maximal values, which may just be really noisy versions
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_max_kills" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_max_deaths" in l])
    # better version in opgg_champion_avg_alltotal_games
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "rs_games" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "season_games" in l])
    # not representative because value averages will have increased since training
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "season_wins" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "season_losses" in l])
    # not useful player champion stats
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_rank" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_mean_rank" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_max_rank" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_mean_max_rank" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_double_kills" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_triple_kills" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_quadra_kills" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_double_kills" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_triple_kills" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_quadra_kills" in l])
    # better repr by _today_
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_week_" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_month_" in l])
    # better repr by total
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_percent_" in l])
    # better repr by games_fraction
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_eloavg_" in l and "_games" in l and "_games_" not in l])
    # better repr by weighed_score
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_kill_sprees" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_jungle_creep_score_team" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_total_damage_dealt_to_champions" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_jungle_creep_score_team" in l])
    # better repr by chgg_eloavg_champion_play_rate
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_eloavg_champion_today_games_fraction" in l])
    # better repr by opgg_eloavg_champion_today_creep_score
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_creep_score" in l])
    # better repr by chgg_eloavg_matchup_adc_support
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_deaths" in l])
    # better repr by weighed_score and kda etc
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_total_damage_dealt_to_champions" in l])
    # better repr by other bot synergy features
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_kill_sprees" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_jungle_creep_score_team" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_weighed_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_total_damage_dealt_to_champions" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_kill_sprees" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_jungle_creep_score_team" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_weighed_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_support_adc_total_damage_dealt_to_champions" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_kill_sprees" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_jungle_creep_score_team" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_weighed_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_total_damage_dealt_to_champions" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_kill_sprees" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_jungle_creep_score_team" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_weighed_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_total_damage_dealt_to_champions" in l])
    # better repr by ravg_opgg_champion_recent_gold_/_chgg_eloavg_champion_gold
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_opgg_champion_recent_gold_/_chgg_eloavg_champion_gold" in l])

    # not useful global champion stats
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_largest_kill_spree" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_jungle_creep_score_team" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_jungle_creep_score_enemy" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_chgg_eloavg_champion_play_rate" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_opgg_eloavg_champion_today_games_fraction" in l])

    # try using _elodeviation instead (need to test more for what works best)
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_elo" == l[-4:]])

    # Features to be included in final system (don't filter out)
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_wins" in l]) # Includes category win/losses
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_losses" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_winrate" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_win_rate" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_win" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "elo" == l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "queue__" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "region__" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_duration" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_time_since_match" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_level" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_pinks_purchased" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_kill_participation" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_opgg_champion_recent_mean_rank" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_mean_rank" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_games" in l])


    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_alltotal_games" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_death" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_damage_dealt" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_avg_damage_taken" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_kills_/_opgg_champion_recent_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_opgg_champion_recent_gold_/_chgg_eloavg_champion_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_deaths_/_chgg_eloavg_champion_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_assists_/_chgg_eloavg_champion_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_assists_/_chgg_eloavg_champion_assists" in l])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_play_rate" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_assists" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_total_" in l and "_damage" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_kill_sprees" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_total_damage_taken" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_total_heal" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_wards_placed" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_wards_killed" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_duration_" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_eloavg_champion_today_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_eloavg_champion_today_gold" in l])

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_champion_duration_" in l and "_games_fraction" in l]) # Temporary: api.champion.gg is down & champion.gg doesn't have this data

    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_weighed_score" in l]) # Temporary: api.champion.gg is down & champion.gg doesn't have this data
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_creep_score" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_total_damage_dealt_to_champions" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_adc_support_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_support_gold" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_kills" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_deaths" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_gold" in l])

    # TODO: adjust post-testing (train multiple models - do A/B testing for exclusion of each candidate feature)
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_gold" in l])  # Because of possible leakage
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_damage_taken" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "opgg_champion_recent_damage_dealt" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "_elo" == l[-len("_elo"):]])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "elodeviation" == l[-len("elodeviation"):]])

    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_win" in l])


    # Not sure how this got in
    # X, X_labels = remove_data_subset(X, X_labels, ["season_bayes_win_rate"])

    # Found this actually doesn't help?
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l and "_eloavg_" in l and "_/_" in l])

    # Trying out without these. Actually they're more significant than I thought
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_time_of_day" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_duration" in l])

    # Non-useful metadata
    X, X_labels = remove_data_subset(X, X_labels, duration_labels)
    X, X_labels = remove_data_subset(X, X_labels, ["timestamp"])  # Use time_of_day
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "region__" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "queue__" in l])
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "pick_position" in l])
    X, X_labels = remove_data_subset(X, X_labels, ["season_games"])
    X, X_labels = remove_data_subset(X, X_labels, ["division", "tier", "rank", "league_points"])  # include avg elo & mean_hAST though

    return X, X_labels


def filter_linearonly(X, X_labels):
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if l[-len("_champion"):] == "_champion" or \
        l[-len("_pick_position"):] == "_pick_position" or \
        l[-len("_spell_1"):] == "_spell_1" or \
        l[-len("_spell_2"):] == "_spell_2"])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "__x" in l])
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if l == "elo" or l == "time_of_day" or l == "mean_hAST"])
    return X, X_labels

def filter_recfeats(X, X_labels):
    X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if \
        "ravg_opgg_champion_recent_gold_/_chgg_eloavg_champion_gold" in l or \
        "chgg_eloavg_champion_largest_kill_spree" in l or \
        "ravg_opgg_eloavg_champion_today_games_fraction" in l or \
        "opgg_champion_recent_mean_max_rank" in l or \
        "ravg_opgg_champion_recent_rank" in l or \
        "opgg_champion_recent_gold" in l or \
        "opgg_champion_recent_damage_taken" in l or \
        "opgg_champion_recent_damage_dealt" in l or \
        "_elo" == l[-len("_elo"):] or \
        "elodeviation" == l[-len("elodeviation"):]])
    return X, X_labels


# Train the models using the partitioned data
def train_models(meta, X, Y, meta_labels, X_labels, model_strs, n_train, n_test, n_pcal, alpha, beta,
  X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, X_scaler, X_autolog, X_mins, resume, bayopt=False, verbose=True, validation=False,
  testing_pre_post=False, testing_sp=False, save_lr_probs=True, save_suffix="generic", model_params={}):

    model_dict = {}
    if resume:
        model_dict = load_ld("models_unf")[0]

    # Define model keys for different combinations of input features (incomplete team compositions)
    model_dict_keys = ["n_blue", "n_red", "opgg_blue", "opgg_red", "opgg_blue_prof", "opgg_red_prof", "matchup_stats"]
    model_cases_init = list(itertools.product(*([[0, 1 ,5]] * 4)))
    model_cases = []
    for c in model_cases_init:
        # Skip null case
        if all([c_ == 0 for c_ in c]):
            continue
        
        c_ = list(c)
        
        # 0 = 0
        # 1 = between 1 & 4 (inclusive)
        # 5 = 5
        
        # Matchup features case indicator
        m = [0] # Default is that we have 0 champions for one or both teams, and thus we have no matchup features (0)
        if c[0] == 1 and c[1] == 1: # If we have the ambiguous case where matchups are possible but not guaranteed (1)
            m = [0, 1] # Create two model cases instead of one, for when we do & don't have matchups (uses mean if so)
        elif (c[0] == 1 and c[1] == 5) or (c[0] == 5 and c[1] == 1): # We know between 1 & 4 matchups (2)
            m = [1] # Include matchups feature means
        elif c[0] == 5 and c[1] == 5: # If we have all champions (3)
            m = [5] # Include all matchup features
            
        # Player champion proficiency features indicator (blue team)
        o_b = [0] # Default is that we have 0 opgg profiles or 0 champions, and thus we have no proficiency features (0)
        if c[0] == 1 and c[2] == 1: # If we have the ambiguous case where overlap is possible but not guaranteed (1)
            o_b = [0, 1] # Create two model cases instead of one, for when we do & don't have overlap (uses mean if so)
        elif (c[0] == 1 and c[2] == 5) or (c[0] == 5 and c[2] == 1): # We know between 1 & 4 overlap players (2)
            o_b = [1] # Include proficiency feature means
        elif c[0] == 5 and c[2] == 5: # If we have all champions & opgg profiles (3)
            o_b = [5] # Include all proficiency features
        # Player champion proficiency features indicator (red team)
        o_r = [0]
        if c[1] == 1 and c[3] == 1:
            o_r = [0, 1]
        elif (c[1] == 1 and c[3] == 5) or (c[1] == 5 and c[3] == 1):
            o_r = [1]
        elif c[1] == 5 and c[3] == 5:
            o_r = [5]
        
        # Now, create the final model case(s)
        for c_suffix in itertools.product(o_b, o_r, m):
            model_cases.append(tuple(c_ + list(c_suffix)))

    rs_labels = [l for l in X_labels if l in X_c_player_labels_ranked_final_full]
    all_matchup_labels = [l for l in X_labels if "chgg_" in l and "matchup_" in l]
    matchup_labels = [l for l in X_labels if "chgg_" in l and "matchup_" in l and "synergy" not in l \
                                  and "adc_support" not in l and "support_adc" not in l]
    synergy_labels = [l for l in X_labels if "chgg_" in l and "matchup_" in l and "synergy" in l]
    synergy_labels_blue = [l for l in synergy_labels if l[:5] == "blue_"]
    synergy_label_is_blue = [X_labels.index(l) for l in synergy_labels_blue]
    synergy_labels_red = [l for l in synergy_labels if l[:4] == "red_"]
    synergy_label_is_red = [X_labels.index(l) for l in synergy_labels_red]
    adcsupport_labels = [l for l in X_labels if "chgg_" in l and "matchup_" in l and ("adc_support" in l or "support_adc" in l)]
    adcsupport_labels_blue = [l for l in adcsupport_labels if l[:5] == "blue_"]
    adcsupport_label_is_blue = [X_labels.index(l) for l in adcsupport_labels_blue]
    adcsupport_labels_red = [l for l in adcsupport_labels if l[:4] == "red_"]
    adcsupport_label_is_red = [X_labels.index(l) for l in adcsupport_labels_red]
    botsynergies_labels = synergy_labels + adcsupport_labels
    botsynergies_labels_blue = [l for l in botsynergies_labels if l[:5] == "blue_"]
    botsynergies_labels_red = [l for l in botsynergies_labels if l[:4] == "red_"]
    botsynergies_label_is_blue = [X_labels.index(l) for l in botsynergies_labels_blue]
    botsynergies_label_is_red = [X_labels.index(l) for l in botsynergies_labels_red]
    matchup_ls_roles = OrderedDict([(r, [l for l in matchup_labels if r in l]) for r in t_roles])
    matchup_ls_avgs_blue = [l.replace("blue_top", "blue") for l in matchup_ls_roles["blue_top"]]
    matchup_ls_avgs_red = [l.replace("blue_top", "red") for l in matchup_ls_roles["blue_top"]]
    matchup_ls_roles_is_blue = \
        dict([(ri, [X_labels.index(l) for l in matchup_ls_roles["blue_" + roles_all[ri]]]) for ri in range_5])
    matchup_ls_roles_is_red = \
        dict([(ri, [X_labels.index(l) for l in matchup_ls_roles["red_" + roles_all[ri]]]) for ri in range_5])
    matchup_labels_blue = [l for l in matchup_labels if l[:5] == "blue_"]
    matchup_labels_red = [l for l in matchup_labels if l[:4] == "red_"]
    matchup_label_is_blue = [X_labels.index(l) for l in matchup_labels_blue]
    matchup_label_is_red = [X_labels.index(l) for l in matchup_labels_red]
    rec_labels = [l for l in X_labels if any([str(n) + "ravg_" in l for n in range(20)])] + \
        [l for l in X_labels if "_recEnc" in l] + [l for l in X_labels if "_mtmEnc" in l]
    opgg_r_labels = [l for l in X_labels if "opgg_rs_" in l or "opgg_champion_" in l or l in rec_labels or l in rs_labels]
    opgg_r_ls_roles = OrderedDict([(r, [l for l in opgg_r_labels if r in l]) for r in t_roles])
    opgg_r_ls_roles_champ = OrderedDict([(r, [l for l in opgg_r_ls_roles[r] if \
        ("_opgg_champion_recent_" in l or "_opgg_champion_season_" in l) and \
        "ravg_" not in l and "_recEnc" not in l and "_mtmEnc" not in l]) for r in t_roles])
    opgg_r_ls_roles_non_champ = OrderedDict([(r, [l for l in opgg_r_ls_roles[r] if \
        l not in opgg_r_ls_roles_champ[r]]) for r in t_roles])
    opgg_r_ls_avgs_blue_champ = [l.replace("blue_top", "blue") for l in opgg_r_ls_roles_champ["blue_top"]]
    opgg_r_ls_avgs_blue_non_champ = [l.replace("blue_top", "blue") for l in opgg_r_ls_roles_non_champ["blue_top"]]
    opgg_r_ls_avgs_red_champ = [l.replace("blue_top", "red") for l in opgg_r_ls_roles_champ["blue_top"]]
    opgg_r_ls_avgs_red_non_champ = [l.replace("blue_top", "red") for l in opgg_r_ls_roles_non_champ["blue_top"]]
    opgg_r_ls_roles_is_blue = \
        dict([(ri, [X_labels.index(l) for l in opgg_r_ls_roles["blue_" + roles_all[ri]]]) for ri in range_5])
    opgg_r_ls_roles_is_red = \
        dict([(ri, [X_labels.index(l) for l in opgg_r_ls_roles["red_" + roles_all[ri]]]) for ri in range_5])
    opgg_r_ls_roles_is_blue_champ = \
        dict([(ri, [X_labels.index(l) for l in opgg_r_ls_roles_champ["blue_" + roles_all[ri]]]) for ri in range_5])
    opgg_r_ls_roles_is_blue_non_champ = \
        dict([(ri, [X_labels.index(l) for l in opgg_r_ls_roles_non_champ["blue_" + roles_all[ri]]]) for ri in range_5])
    opgg_r_ls_roles_is_red_champ = \
        dict([(ri, [X_labels.index(l) for l in opgg_r_ls_roles_champ["red_" + roles_all[ri]]]) for ri in range_5])
    opgg_r_ls_roles_is_red_non_champ = \
        dict([(ri, [X_labels.index(l) for l in opgg_r_ls_roles_non_champ["red_" + roles_all[ri]]]) for ri in range_5])
    opgg_r_labels_blue = [l for l in opgg_r_labels if l[:5] == "blue_"]
    opgg_r_labels_red = [l for l in opgg_r_labels if l[:4] == "red_"]
    opgg_r_labels_blue_champ = sum([opgg_r_ls_roles_champ[r] for r in t_roles if "blue_" in r], [])
    opgg_r_labels_red_champ = sum([opgg_r_ls_roles_champ[r] for r in t_roles if "red_" in r], [])
    opgg_r_labels_blue_non_champ = [l for l in opgg_r_labels_blue if l not in opgg_r_labels_blue_champ]
    opgg_r_labels_red_non_champ = [l for l in opgg_r_labels_red if l not in opgg_r_labels_red_champ]
    opgg_r_label_is_blue = [X_labels.index(l) for l in opgg_r_labels_blue]
    opgg_r_label_is_red = [X_labels.index(l) for l in opgg_r_labels_red]
    opgg_r_label_is_blue_champ = [X_labels.index(l) for l in opgg_r_labels_blue_champ]
    opgg_r_label_is_red_champ = [X_labels.index(l) for l in opgg_r_labels_red_champ]
    opgg_r_label_is_blue_non_champ = [X_labels.index(l) for l in opgg_r_labels_blue_non_champ]
    opgg_r_label_is_red_non_champ = [X_labels.index(l) for l in opgg_r_labels_red_non_champ]
    opgg_r_ls_roles = OrderedDict([(r, [l for l in opgg_r_labels if r in l]) for r in t_roles])
    champion_labels_roles = OrderedDict([(r, [l for l in X_labels if \
        r in l and l not in all_matchup_labels and l not in opgg_r_labels]) for r in t_roles])
    champion_labels_roles_is = OrderedDict([(r, [X_labels.index(l) for l in champion_labels_roles[r]]) for r in t_roles])
    champion_labels_roles_is_cat = OrderedDict([(r, [X_labels.index(l) for l in champion_labels_roles[r] if "__x" in l]) for r in t_roles])
    champion_labels_roles_is_ncat = OrderedDict([(r, [X_labels.index(l) for l in champion_labels_roles[r] if "__x" not in l]) for r in t_roles])
    champion_labels_roles_is_blue = OrderedDict([(ri, champion_labels_roles_is["blue_" + roles_all[ri]]) for ri in range_5])
    champion_labels_roles_is_blue_cat = OrderedDict([(ri, champion_labels_roles_is_cat["blue_" + roles_all[ri]]) for ri in range_5])
    champion_labels_roles_is_blue_ncat = OrderedDict([(ri, champion_labels_roles_is_ncat["blue_" + roles_all[ri]]) for ri in range_5])
    champion_labels_roles_is_red = OrderedDict([(ri, champion_labels_roles_is["red_" + roles_all[ri]]) for ri in range_5])
    champion_labels_roles_is_red_cat = OrderedDict([(ri, champion_labels_roles_is_cat["red_" + roles_all[ri]]) for ri in range_5])
    champion_labels_roles_is_red_ncat = OrderedDict([(ri, champion_labels_roles_is_ncat["red_" + roles_all[ri]]) for ri in range_5])
    champion_ls_avgs_blue = [l.replace("blue_top", "blue") for l in champion_labels_roles["blue_top"]]
    champion_ls_avgs_blue_cat = [l.replace("blue_top", "blue") for l in champion_labels_roles["blue_top"] if "__x" in l]
    champion_ls_avgs_blue_ncat = [l.replace("blue_top", "blue") for l in champion_labels_roles["blue_top"] if "__x" not in l]
    champion_ls_avgs_red = [l.replace("blue_top", "red") for l in champion_labels_roles["blue_top"]]
    champion_ls_avgs_red_cat = [l.replace("blue_top", "red") for l in champion_labels_roles["blue_top"] if "__x" in l]
    champion_ls_avgs_red_ncat = [l.replace("blue_top", "red") for l in champion_labels_roles["blue_top"] if "__x" not in l]
    # pr_fl(len(model_cases))
    # pr_fl(opgg_r_labels_blue_non_champ)
    np.random.shuffle(model_cases)


    # model_cases = [(1,0,0,0,0,0,0)]
    model_cases_pre = [
        (5,5,5,5,5,5,5),
        (0,0,5,5,0,0,0),
        (5,5,0,0,0,0,5),
        (5,0,5,0,5,0,0),
        (0,5,0,5,0,5,0),
        (5,5,5,0,5,0,5),
        (5,5,0,5,0,5,5),
        (5,5,5,1,5,1,5),
        (5,5,1,5,1,5,5),
        (5,5,1,0,1,0,5),
        (5,5,0,1,0,1,5),
        (0,0,0,5,0,0,0),
        (0,0,5,0,0,0,0),
        (1,0,0,0,0,0,0),
        (1,0,1,0,0,0,0),
        (1,0,1,0,1,0,0),
        (1,1,0,0,0,0,0),
        (1,1,0,0,0,0,1),
    ]
    # model_cases = model_cases_pre
    model_cases_nonpre = [case for case in model_cases if case not in model_cases_pre]
    # model_cases = model_cases_nonpre
    model_cases = model_cases_pre + model_cases_nonpre
    # pr_fl(len(model_cases))

    # % = Classification accuracy
    if verbose and not testing_sp:
        pr_fl("shape | key | match | test% | train% | Master+% | recent% | Pr err | Pr cal% | Pr cal Pr err | mean wr model")
    scales = [X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, X_scaler, X_autolog, X_mins]
    # for model_key in model_cases:
    # def train_models(model_key, X, Y):
    full_5_centers = {}
    full_5_scales = {}
    full_5_coefs = {}
    full_5_centers_dict = {}
    full_5_scales_dict = {}
    full_5_coefs_dict = {}
    full_5_params = None
    full_5_cents = None
    zeroing = False
    folding_func = np.mean
    n_tot = X.shape[0]
    for model_key in model_cases:

        # if model_key != (5,5,5,5,5,5,5):
        #     sys.exit()

        if model_key != (5,5,5,5,5,5,5) and model_key in model_dict: # If we're resuming training after an outage
            continue

        n_blue_m, n_red_m, opgg_blue, opgg_red, opgg_blue_prof, opgg_red_prof, matchup_stats = model_key
        X_ = [] if not zeroing else deepcopy(X)
        x_ls = [] if not zeroing else deepcopy(X_labels)
        first = True
    #     print(model_key)
        if model_key != (5,5,5,5,5,5,5):
            full_5_cents = full_5_centers[model_strs[0]]
        x_i = 0
        for x_i in range(len(X))[:n_tot]:
            if model_key == (5,5,5,5,5,5,5) and zeroing:
                break
    #         sys_print("\r" + str(x_i) + "           ", flush=False)
            x = X[x_i] if not zeroing else None
            matchup_ris = []
            
            # Choose which players' champions we want to include
            blue_ri, red_ri = [], []
            n_blue, n_red = n_blue_m, n_red_m
            if n_blue == 1:
                n_blue = np.random.randint(1, 5)
            if n_red == 1:
                n_red = np.random.randint(1, ((1 + 5 - n_blue) if (n_blue_m == 1 and not matchup_stats) else 5))
            if n_blue == 5:
                blue_ri = range_5
            elif n_blue > 0: # Choose players to randomly include
                blue_ri = np.random.choice(range_5, n_blue, False)
            if n_red == 5:
                red_ri = range_5
                if matchup_stats:
                    matchup_ris = [ri for ri in blue_ri if ri in red_ri]
            elif n_red > 0:
                choices = range_5
                if matchup_stats:
                    choices = blue_ri
                    red_ri = np.random.choice(choices, 1, False)
                    if n_red > 1:
                        choices = list(set(range_5) - set(red_ri))
                        red_ri = np.hstack([red_ri, np.random.choice(choices, n_red - 1, False)])
                    matchup_ris = [ri for ri in blue_ri if ri in red_ri]
                else:
                    choices = list(set(range_5) - set(blue_ri))
                    red_ri = np.random.choice(choices, n_red, False)

            opgg_blue_ris, opgg_red_ris = [], []
            opgg_blue_prof_ris, opgg_red_prof_ris = [], []

            if opgg_blue == 1:
                if n_blue_m == 1:
                    if opgg_blue_prof == 0:
                        choices = list(set(range_5) - set(blue_ri))
                        opgg_blue_ris = np.random.choice(choices, np.random.randint(1, len(choices) + 1), False)
                    else: # opgg_blue_prof == 1
                        n_opgg_blue = np.random.randint(1, 5)
                        choices = blue_ri
                        opgg_blue_ris = np.random.choice(choices, 1, False)
                        if n_opgg_blue > 1:
                            choices = list(set(range_5) - set(opgg_blue_ris))
                            opgg_blue_ris = np.hstack([opgg_blue_ris, np.random.choice(choices, n_opgg_blue - 1, False)])
                else:
                    opgg_blue_ris = np.random.choice(range_5, np.random.randint(1, 5), False)
            elif opgg_blue == 5:
                opgg_blue_ris = range_5
            opgg_blue_prof_ris = [ri for ri in blue_ri if ri in opgg_blue_ris]
                    
            if opgg_red == 1:
                if n_red_m == 1:
                    if opgg_red_prof == 0:
                        choices = list(set(range_5) - set(red_ri))
                        opgg_red_ris = np.random.choice(choices, np.random.randint(1, len(choices) + 1), False)
                    else: # opgg_red_prof == 1
                        n_opgg_red = np.random.randint(1, 5)
                        choices = red_ri
                        opgg_red_ris = np.random.choice(choices, 1, False)
                        if n_opgg_red > 1:
                            choices = list(set(range_5) - set(opgg_red_ris))
                            opgg_red_ris = np.hstack([opgg_red_ris, np.random.choice(choices, n_opgg_red - 1, False)])
                else:
                    opgg_red_ris = np.random.choice(range_5, np.random.randint(1, 5), False)
            elif opgg_red == 5:
                opgg_red_ris = range_5
            opgg_red_prof_ris = [ri for ri in red_ri if ri in opgg_red_ris]
                            
    #         if opgg_blue == 1:
    #             opgg_blue_ris = np.random.choice(range_5, np.random.randint(1, 5), False)
    #         if opgg_red == 1:
    #             opgg_red_ris = np.random.choice(range_5, np.random.randint(1, 5), False)
            
    #         all_champs = n_blue == 5 and n_red == 5
    #         all_opgg_r = len(opgg_blue_ris) == 5 and len(opgg_red_ris) == 5
    #         all_opgg_r = False
    #         all_opgg_r = opgg_blue == 5 and opgg_red == 5


            if zeroing:

                blue_synergy = roles_all.index("adc") in blue_ri and roles_all.index("support") in blue_ri
                red_synergy = roles_all.index("adc") in red_ri and roles_all.index("support") in red_ri
                blue_adcsupport = roles_all.index("adc") in blue_ri and roles_all.index("support") in red_ri
                red_adcsupport = roles_all.index("adc") in red_ri and roles_all.index("support") in blue_ri

                for ri in range_5:
                    role = roles_all[ri]

                    if ri not in blue_ri:
                        for i in champion_labels_roles_is_blue[ri]:
                            X_[x_i, i] = full_5_cents[i]
                    if ri not in opgg_blue_ris:
                        for i in opgg_r_ls_roles_is_blue_non_champ[ri]:
                            X_[x_i, i] = full_5_cents[i]
                    if ri not in opgg_blue_prof_ris:
                        for i in opgg_r_ls_roles_is_blue_champ[ri]:
                            X_[x_i, i] = full_5_cents[i]

                    if ri not in red_ri:
                        for i in champion_labels_roles_is_red[ri]:
                            X_[x_i, i] = full_5_cents[i]
                    if ri not in opgg_red_ris:
                        for i in opgg_r_ls_roles_is_red_non_champ[ri]:
                            X_[x_i, i] = full_5_cents[i]
                    if ri not in opgg_red_prof_ris:
                        for i in opgg_r_ls_roles_is_red_champ[ri]:
                            X_[x_i, i] = full_5_cents[i]

                    if not blue_synergy:
                        for i in synergy_label_is_blue:
                            X_[x_i, i] = full_5_cents[i]
                    if not red_synergy:
                        for i in synergy_label_is_red:
                            X_[x_i, i] = full_5_cents[i]
                    if not blue_adcsupport:
                        for i in adcsupport_label_is_blue:
                            X_[x_i, i] = full_5_cents[i]
                    if not red_adcsupport:
                        for i in adcsupport_label_is_red:
                            X_[x_i, i] = full_5_cents[i]

            else:

                x_pl = []

                if n_blue == 5:
                    if first:
                        for ri in blue_ri:
                            x_ls += champion_labels_roles["blue_" + roles_all[ri]]
                    x_pl.append(np.hstack([X[x_i, champion_labels_roles_is_blue[ri]] for ri in blue_ri]))
                elif n_blue > 0:
                    if first:
                        x_ls += champion_ls_avgs_blue_cat   # Categorical
                        x_ls += champion_ls_avgs_blue_ncat  # Non-categorical
                    # x_pl.append(np.sum([X[x_i, champion_labels_roles_is_blue_cat[ri]] for ri in blue_ri], axis=0))
                    x_pl.append(np.sum([full_5_params(champion_labels_roles_is_blue_cat[ri],
                        X[x_i, champion_labels_roles_is_blue_cat[ri]]) for ri in blue_ri], axis=0))
                    x_pl.append(folding_func([full_5_params(champion_labels_roles_is_blue_ncat[ri],
                        X[x_i, champion_labels_roles_is_blue_ncat[ri]]) for ri in blue_ri], axis=0))
                if matchup_stats:
                    if matchup_stats == 5:
                        x_pl += [X[x_i, matchup_label_is_blue]]
                        x_pl += [X[x_i, [i for i in botsynergies_label_is_blue]]]
                        if first:
                            x_ls += matchup_labels_blue + botsynergies_labels_blue
                    else:
                        x_pl += [folding_func([full_5_params(matchup_ls_roles_is_blue[ri], X[x_i,
                            matchup_ls_roles_is_blue[ri]]) for ri in matchup_ris], axis=0)]
                        if first:
                            x_ls += matchup_ls_avgs_blue
                if opgg_blue:
                    if opgg_blue == 5:
                        x_pl += [X[x_i, opgg_r_label_is_blue_non_champ]]
                        if first:
                            x_ls += opgg_r_labels_blue_non_champ
                    else: # opgg_blue == 1
                        x_pl += [folding_func([full_5_params(opgg_r_ls_roles_is_blue_non_champ[ri],
                            X[x_i, opgg_r_ls_roles_is_blue_non_champ[ri]]) for ri in opgg_blue_ris], axis=0)]
                        if first:
                            x_ls += opgg_r_ls_avgs_blue_non_champ
                if opgg_blue_prof:
                    if opgg_blue_prof == 5:
                        x_pl += [X[x_i, opgg_r_label_is_blue_champ]]
                        if first:
                            x_ls += opgg_r_labels_blue_champ
                    else: # opgg_blue_prof == 1
                        x_pl += [folding_func([full_5_params(opgg_r_ls_roles_is_blue_champ[ri],
                            X[x_i, opgg_r_ls_roles_is_blue_champ[ri]]) for ri in opgg_blue_prof_ris], axis=0)]
                        if first:
                            x_ls += opgg_r_ls_avgs_blue_champ
                    

                if n_red == 5:
                    if first:
                        for ri in red_ri:
                            x_ls += champion_labels_roles["red_" + roles_all[ri]]
                    x_pl.append(np.hstack([X[x_i, champion_labels_roles_is_red[ri]] for ri in red_ri]))
                elif n_red > 0:
                    if first:
                        x_ls += champion_ls_avgs_red_cat
                        x_ls += champion_ls_avgs_red_ncat
                    x_pl.append(np.sum([X[x_i, champion_labels_roles_is_red_cat[ri]] for ri in red_ri], axis=0))
                    x_pl.append(folding_func([full_5_params(champion_labels_roles_is_red_ncat[ri],
                        X[x_i, champion_labels_roles_is_red_ncat[ri]]) for ri in red_ri], axis=0))
                if matchup_stats:
                    if matchup_stats == 5:
                        x_pl += [X[x_i, matchup_label_is_red]]
                        x_pl += [X[x_i, [i for i in botsynergies_label_is_red]]]
                        if first:
                            x_ls += matchup_labels_red + botsynergies_labels_red
                    else:
                        x_pl += [folding_func([full_5_params(matchup_ls_roles_is_red[ri],
                            X[x_i, matchup_ls_roles_is_red[ri]]) for ri in matchup_ris], axis=0)]
                        if first:
                            x_ls += matchup_ls_avgs_red
                if opgg_red:
                    if opgg_red == 5:
                        x_pl += [X[x_i, opgg_r_label_is_red_non_champ]]
                        if first:
                            x_ls += opgg_r_labels_red_non_champ
                    else: # opgg_red == 1
                        x_pl += [folding_func([full_5_params(opgg_r_ls_roles_is_red_non_champ[ri],
                            X[x_i, opgg_r_ls_roles_is_red_non_champ[ri]]) for ri in opgg_red_ris], axis=0)]
                        if first:
                            x_ls += opgg_r_ls_avgs_red_non_champ
                if opgg_red_prof:
                    if opgg_red_prof == 5:
                        x_pl += [X[x_i, opgg_r_label_is_red_champ]]
                        if first:
                            x_ls += opgg_r_labels_red_champ
                    else: # opgg_red_prof == 1
                        x_pl += [folding_func([full_5_params(opgg_r_ls_roles_is_red_champ[ri],
                            X[x_i, opgg_r_ls_roles_is_red_champ[ri]]) for ri in opgg_red_prof_ris], axis=0)]
                        if first:
                            x_ls += opgg_r_ls_avgs_red_champ

            x_ = None
            if not zeroing:
                x_ = np.hstack(x_pl)
            
    #         if first: print(x_ls)

    #         print([l for l in X_labels if l not in x_ls])
    #         print(x_.shape, len([l for l in X_labels if l not in x_ls]))
    #         break
            if not zeroing:
                X_.append(x_)
            # if (x_i + 1) % 10 == 0:
            # # if (x_i + 1) % n_train + n_test + n_pcal - 1 == 0:
            #     sys_print("\r" + str(x_i + 1) + ', ' + str(len(x_ls)) + '  ' + str(model_key) + "   ", flush=False)
            first = False
    #     break

        # if model_key == (5,5,5,5,5,5,5):
            # with open("actualFinalFeatsFinalLinear.txt", 'w') as f:
            #     f.write(str(x_ls).replace(' ', '\n'))
            # sys.exit()

        if verbose:
            sys_print("\r" + str(x_i + 1) + ', ' + str(len(x_ls)) + '  ' + str(model_key) + "   ", flush=False)

    #     sys_print([len(x_) for x_ in X_], flush=False)
        if not zeroing:
            X_ = np.asarray(X_)
    #     sys_print(X_.shape, flush=False)
        if model_key == (5,5,5,5,5,5,5) and import_probs:
            X_ = np.hstack([X_, X[:, [-1]]])
            x_ls.append("blue_top_sqWinProb")
        if verbose:
            sys_print(X_.shape[1] == len(x_ls), flush=False)
            sys_print(" ", flush=False)
            sys_print(get_datetime(), flush=False)
            sys_print("  ", flush=False)

        testing_MLP_predraft = False        # Test simple neural network for 10-player pre-draft prediction (2nd model key)
        # testing_sp = True                  # Test linear model(s) for single player pre-draft prediction

        # print([l for l in x_ls if "time_of_day" in l])
        # print([l for l in x_ls if "time_since_match" in l])
        # print([l for l in x_ls if "ravg_duration" in l])

        if testing_sp:

            # pr_fl()
            # pr_fl(len(X_labels), len(x_ls))
            # pr_fl([l for l in X_labels if l not in x_ls])

            rem_ls = [l for l in x_ls if
                                                  ("_champion_recent_" in l and "ravg_" not in l) or \
                                                  ("_champion_season_" in l and "ravg_" not in l) or \
                                                  # ("_season_" in l and "_/_" not in l) or \
                                                  ("_eloavg_" in l and "_/_" not in l and "ravg_" not in l)
                      ]
            j = 0
            # for l in rem_ls:
            #     pr_fl(l)
                # if l[:9] == "blue_top_":
                #     j += 1
            # pr_fl(len(rem_ls))
            X_, x_ls = remove_data_subset(X_, x_ls, rem_ls)
            # for l in x_ls:
            #     pr_fl(l)
            # pr_fl(len(X_labels), len(rem_ls), len(x_ls))

            # Test single player prediction
            X_ = StandardScaler().fit_transform(X_)
            # X_ = RobustScaler().fit_transform(X_)
            l_ord = np.argsort(x_ls)
            x_ls = [x_ls[i] for i in l_ord]
            X_ = X_[:, l_ord]
            x_ls = [l for l in x_ls if l[:9] == "blue_top_"]
            X_labels = x_ls
            X_ = X_.reshape((10 * X_.shape[0], len(x_ls)))
            X_ = StandardScaler().fit_transform(X_)
            # X_ = RobustScaler().fit_transform(X_)
            meta = np.tile(meta, (10, 1))
            Y_ = np.repeat(Y.flatten(), 10)
            idx = [i for i in range(X_.shape[0]) if (i % 10) >= 5]
            Y_[idx] = 1 - Y_[idx]
            # pr_fl("SP Test:", X_.shape, Y_.shape)
            # score = np.mean(preds == Y_test.flatten()) * 100
            # pr_fl(score)
            n_train *= 10
            n_test *= 10
            # n_test = 100000
            # n_test = 177430

            # return

        else:
            Y_ = Y[:len(X_)]


        X_train, Y_train, X_pcal, Y_pcal = X_[:n_train], Y_[:n_train], X_[n_train:n_train + n_pcal], Y_[n_train:n_train + n_pcal]
        X_test, Y_test = X_[-n_test:], Y_[-n_test:]
        if validation:
            X_test, Y_test = X_[n_train:n_train + n_test], Y_[n_train:n_train + n_test]
        meta_train, meta_pcal, meta_test = meta[:n_train], meta[n_train:n_train + n_pcal], meta[-n_test:]

        scores, scores_tr, high_elo_scores, recent_scores = [], [], [], []
        pscores_before, pscores = [], []
        models = []
        for mkey in model_strs:

            # if model_key != (5,5,5,5,5,5,5):
            #     m = FeatSelectWrapper(m, percentiles=[37.5, 50, 62.5, 75, 87.5, 100])
            #     # m = FeatSelectWrapper(m, percentiles=[25, 37.5, 50, 62.5, 75, 87.5, 100])
            #     # m = FeatSelectWrapper(m, percentiles=[18.75, 25, 37.5, 50, 62.5, 75, 87.5, 100])
            #     # m = FeatSelectWrapper(m, percentiles=[6.25, 12.5, 18.75, 25, 37.5, 50, 62.5, 75, 87.5, 100])

            # m.fit(X_train, Y_train.flatten(), model__X_test=X_test, model__Y_test=Y_test) # for FeatureSelectWrapper

            # If we just trained the full-5 (all data) model, 
            if model_key == (5,5,5,5,5,5,5):

                # pr_fl([l for l in X_labels if l not in x_ls])

                m = get_model(mkey)
                if isinstance(m, MultiOutputClassifier):
                    m = m.estimator

                if len(model_params) > 0:
                    m.set_params(**model_params)

                m = Pipeline(steps=
                [
                    ('scale', StandardScaler()),
                    # ('scale', RobustScaler()),
                    ('model', m),
                ])

                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    m.fit(X_train, Y_train.flatten())

                # save coefficients for non-categorical role-specific features
                full_5_centers[mkey] = np.zeros((len(X_labels)))
                full_5_scales[mkey] = np.zeros((len(X_labels)))
                full_5_coefs[mkey] = np.zeros((len(X_labels)))
                full_5_centers_dict[mkey] = {}
                full_5_scales_dict[mkey] = {}
                full_5_coefs_dict[mkey] = {}
                f5_scaler = m.steps[0][1]
                f5_centers = f5_scaler.center_ if isinstance(f5_scaler, RobustScaler) else f5_scaler.mean_
                f5_scales = f5_scaler.scale_
                f5_coefs = m.steps[-1][1].coef_.flatten()
                for i in range(len(x_ls)):
                    l = x_ls[i]
                    # full_5_coefs_by_label[l] = f5_centers[i], f5_scales[i], f5_coefs[i]
                    full_5_centers[mkey][X_labels.index(l)] = f5_centers[i]
                    full_5_scales[mkey][X_labels.index(l)] = f5_scales[i]
                    full_5_coefs[mkey][X_labels.index(l)] = f5_coefs[i]
                    full_5_centers_dict[mkey][l] = f5_centers[i]
                    full_5_scales_dict[mkey][l] = f5_scales[i]
                    full_5_coefs_dict[mkey][l] = f5_coefs[i]
                full_5_params = lambda lis, x__: ((x__ - full_5_centers[mkey][lis]) / full_5_scales[mkey][lis]) #* full_5_coefs[mkey][lis]
                # full_5_params = lambda lis, x__: x__ #* full_5_coefs[mkey][lis]

            else:

                if zeroing:

                    x_ls_, m = m5_tup[:2]
                    m = m[0]
                    # m = m.steps[-1][1]
                    # m = ZeroWrapper(m, x_ls_, x_ls, full_5_centers[mkey], full_5_scales[mkey])

                else:

                    m = get_model(mkey)
                    if testing_MLP_predraft:
                        m = MLPClassifier(hidden_layer_sizes=(1024, 1024), verbose=True,
                            max_iter=50, early_stopping=True, learning_rate_init=0.0005)
                    if isinstance(m, MultiOutputClassifier):
                        m = m.estimator

                    m = Pipeline(steps=
                    [
                        ('scale', StandardScaler()),
                        # ('scale', RobustScaler()),
                        ('model', m),
                    ])

                    with warnings.catch_warnings():
                        warnings.simplefilter("ignore")
                        m.fit(X_train, Y_train.flatten())

            m_ = m
            if isinstance(m, Pipeline):
                m_ = m.steps[1][1]
            elif isinstance(m, ZeroWrapper):
                m_ = m.base_estimator
            # if verbose:
            #     sys_print(m_.perc + ' ', flush=False)
            # if verbose:
            #     sys_print('100% ', flush=False)
            # if verbose:
            #     sys_print((str(m_.perc) if isinstance(m_, FeatSelectWrapper) else '100%') + ' ', flush=False)
            # if verbose:
            #     sys_print((str(m_.perc) if isinstance(m_, FeatSelectWrapper) else '') + ' ', flush=False)
            preds = m.predict(X_test) # Test accuracy
            score = np.mean(preds == Y_test.flatten()) * 100
            if verbose:
                sys_print(str(round(score, 4)) + ' ', flush=False)
            if bayopt:
                return score
            scores.append(score)
            preds_tr = m.predict(X_train) # Training accuracy
            score_tr = np.mean(preds_tr == Y_train.flatten()) * 100
            probs_full = m.predict_proba(X_)
            if model_key == (5,5,5,5,5,5,5) and testing_pre_post and save_lr_probs:
                save_ld(probs_full, "model_logs/LRprobs_" + save_suffix + ".data", pad=False)
            scores_tr.append(score)
            if verbose:
                sys_print(str(round(score_tr, 4)) + ' ', flush=False)
            high_elo_is = np.nonzero(meta_test[:, meta_labels.index("elo")] >= 2400)[0]
            high_elo_score = np.mean(preds[high_elo_is] == Y_test[high_elo_is].flatten()) * 100
            high_elo_scores.append(high_elo_score)
            if verbose:
                sys_print(str(round(high_elo_score, 4)) + ' ', flush=False)
            high_elo_meta_test = meta_test[high_elo_is]
            high_elo_X_test = X_test[high_elo_is]
            high_elo_Y_test = Y_test[high_elo_is]
            recent_is = np.argsort(high_elo_meta_test[:, meta_labels.index("timestamp")].flatten())[-100:]
            recent_preds = m.predict(high_elo_X_test[recent_is]) # Most recent games accuracy (up-to-date-ness)
            recent_score = np.mean(recent_preds == high_elo_Y_test[recent_is].flatten()) * 100
            recent_scores.append(recent_score)
            if verbose:
                sys_print(str(round(recent_score, 4)) + ' ', flush=False)

            prob = m.predict_proba(X_test)
            probs_classes = np.vstack([prob[:, 1], Y_test.flatten()]).T
            inds = np.argsort(probs_classes[:, 0])
            probs_classes = probs_classes[inds]
            bucket_perc = 5      # Percentile size for each bucket
            n_buckets = 100 // bucket_perc
            n_per_bucket = int(0.01 * bucket_perc * X_test.shape[0])

            i = 0
            diffs = []
            for j in range(n_buckets):
                pc = probs_classes[i:(i + n_per_bucket) if j < n_buckets - 1 else len(probs_classes)]
                i += n_per_bucket
                probs, classes = pc[:, 0], pc[:, 1]
                mean_prob = np.mean(probs)
                frac_pos = np.mean(classes)
                diffs.append( abs(mean_prob - frac_pos) )
            pscore = np.mean(diffs) * 100.0
            if verbose:
                sys_print(str(round(pscore, 4)) + ' ', flush=False)
            pscores_before.append(diffs)

            ##### Probability calibration #####

            # m = CalibratedClassifierCV(base_estimator=m, cv="prefit", method='isotonic')
            # m.fit(X_pcal, Y_pcal.flatten())
            # preds = m.predict(X_test) # Test accuracy
            # prob = m.predict_proba(X_test)
            # score = np.mean(preds == Y_test.flatten()) * 100
            # probs_classes = np.vstack([prob[:, 1], Y_test.flatten()]).T
            # inds = np.argsort(probs_classes[:, 0])
            # probs_classes = probs_classes[inds]
            if verbose:
                sys_print(str(round(score, 2)) + ' ', flush=False)
            
            # i = 0
            # diffs = []
            # for j in range(n_buckets):
            #     pc = probs_classes[i:(i + n_per_bucket) if j < n_buckets - 1 else len(probs_classes)]
            #     i += n_per_bucket
            #     probs, classes = pc[:, 0], pc[:, 1]
            #     mean_prob = np.mean(probs)
            #     frac_pos = np.mean(classes)
            #     diffs.append( abs(mean_prob - frac_pos) )
            # pscore = np.mean(diffs) * 100.0
            # pscores.append(diffs)
            if verbose:
                sys_print(str(round(pscore, 2)) + '   ', flush=False)

            if score < 60.0 and opgg_blue == 0 and opgg_red == 0:
                # Make win rate averaging model and compare it's accuracy
                wr_m = MeanProbPseudo(x_ls, alpha=0 if matchup_stats == 0 else None)
                wr_m.fit(X_train, Y_train.flatten())
                wr_prob = wr_m.predict_proba(X_test)[:, 1]
                wr_pred = np.round(wr_prob).astype(int)
                wr_score = np.mean(wr_pred == Y_test.flatten()) * 100.0
                if verbose:
                    sys_print(wr_score, flush=False)
                if verbose:
                    sys_print('    ', flush=False)
                
                if wr_score > score:  # If win rate model better, use this
                    if verbose:
                        sys_print("WR Model", flush=False)
                    m = wr_m
                else:  # Otherwise, wrap the innaccurate model so it's probabilities are less confident
                    if verbose:
                        sys_print(False, flush=False)
                    m = ApproximalWrapper(m)


            if testing_pre_post and model_cases.index(model_key) == 1:
                return
            if testing_sp:
                return

            models.append(m)
        if verbose:
            pr_fl()
        
        m_tuple = [x_ls, models, scores, high_elo_scores, pscores_before, pscores] + \
            ([(alpha, beta, full_5_centers_dict, full_5_scales_dict, full_5_coefs_dict)] if model_key == (5,5,5,5,5,5,5) else [None])
        model_dict[model_key] = m_tuple
        if model_key == (5,5,5,5,5,5,5):
            m5_tup = m_tuple

        save_ld((model_dict, model_strs, scales, n_train, n_test, n_pcal), "models_unf")

    if verbose:
        pr_fl("Saving final model!")
    save_ld((model_dict, model_strs, scales, n_train, n_test, n_pcal), "models_fin")
    # save_ld((model_dict, model_strs, scales, n_train, n_test, n_pcal), "models_fin_" + int(time.time()) + '_' + patches_all[0])
    #     return [model_key, x_ls, models, scores, high_elo_scores, pscores_before, pscores]
    # results = Parallel(n_jobs=2, verbose=5000)(delayed(train_models)(model_key, X, Y) for model_key in model_cases)
    # model_dict = {}
    # for res in results:
    #     pr_fl(model_key, round(np.mean(res[3][0]),2), round(np.mean(res[4][0]),2), round(np.mean(res[5][0]),2), round(np.mean(res[6][0]),2),
    #                      round(np.mean(res[3][1]),2), round(np.mean(res[4][1]),2), round(np.mean(res[5][1]),2), round(np.mean(res[6][1]),2),)
    #     model_dict[res[0]] = res[1:]


    # Write out all final x features
    # with open('full_x_ls', 'w') as f:
    #     f.write(str(model_dict[(5,5,5,5,5,5,5)][0]))


import_rnn = False
import_probs = False
no_pca = False
shuf_idx = None
if __name__ == "__main__":

    # Get launch option(s)
    extract_feats = len(sys.argv) >= 2 and "--extract" in sys.argv
    preloaded =  len(sys.argv) >= 3 and "--preloaded" in sys.argv
    wubba_store = len(sys.argv) >= 2 and "--wubba" in sys.argv
    preprocessed = len(sys.argv) >= 2 and "--preprocessed" in sys.argv
    resume = len(sys.argv) >= 2 and "--resume" in sys.argv
    solo = len(sys.argv) >= 2 and "--solo" in sys.argv
    import_rnn = len(sys.argv) >= 2 and "--rnn" in sys.argv
    import_probs = len(sys.argv) >= 2 and "--probs" in sys.argv
    no_pca = len(sys.argv) >= 2 and "--nopca" in sys.argv
    bayop = len(sys.argv) >= 2 and "--bayop" in sys.argv
    bayopC = len(sys.argv) >= 2 and "--opC" in sys.argv
    trip_test = len(sys.argv) >= 2 and "--triplet" in sys.argv
    noravg = len(sys.argv) >= 2 and "--noravg" in sys.argv
    nomtm = len(sys.argv) >= 2 and "--nomtm" in sys.argv
    fan = len(sys.argv) >= 2 and "--fa" in sys.argv
    ica = len(sys.argv) >= 2 and "--ica" in sys.argv
    nmf = len(sys.argv) >= 2 and "--nmf" in sys.argv
    kpca = len(sys.argv) >= 2 and "--kpca" in sys.argv
    lda = len(sys.argv) >= 2 and "--lda" in sys.argv
    shuf = len(sys.argv) >= 2 and "--shuffle" in sys.argv
    norollAL = len(sys.argv) >= 2 and "--norollAL" in sys.argv

    n_select_k = 1
    for w in sys.argv:
        if '--select=' in w:
            n_select_k = int(w.split('=')[1])
    n_pca_comps = 2
    for w in sys.argv:
        if '--comps=' in w:
            n_pca_comps = int(w.split('=')[1])

    run_i = -1
    for w in sys.argv:
        if '--run=' in w:
            run_i = int(w.split('=')[1])
    fold_i = -1
    for w in sys.argv:
        if '--fold=' in w:
            fold_i = int(w.split('=')[1])

    n_tot = None
    n_pcal = 0
    n_train = n_main_training
    if run_i != -1:
        pr_fl("Run", run_i, "Fold", fold_i, "       Comps", n_pca_comps, 'Select', n_select_k)
        all_inds = load_ld("model_logs/cv_idx.data", pad=False)
        train_idx, test_idx = all_inds[run_i][fold_i][0], all_inds[run_i][fold_i][1]
        shuf_idx = np.hstack([train_idx, test_idx])
        n_train = len(train_idx)
        n_train = 77743

    # Feature extraction
    if extract_feats:

        if not preloaded:
            meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels) = \
                load_games(N=n_main_train, n_occ='temporal', save_every=10000, min_save_n=50000, save_dir=store_folder)
                # load_games(N=n_main_train, n_occ=5, n_occ_elo=(2000, 35), save_every=13750, min_save_n=100000)
        else:
            pr_fl("Loading d_all...")      # Load preloaded data
            if wubba_store:
                meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels) = load_ld(store_folder + "d_all.data", pad=False)
            else:
                meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels) = load_ld("d_all")
        gc.collect()

        # Select recent patches only data
        oldest_p = patches_all[n_main_patches - 1]
        pr_fl("Selecting " + oldest_p + " or later data...")
        oldest_p = patch_s2n(oldest_p)
        data_del = np.nonzero(meta[:, meta_labels.index("game_version")] < oldest_p)[0]
        meta, X, X_rec, Yc, Yr = remove_data_inds(meta, X, X_rec, Yc, Yr, data_del)

        rem_is = []
        for i in range(len(meta)):
            if all([len(X_rec[i][j]) == 0 for j in N_PL_r]):
                rem_is.append(i)
        meta, X, X_rec, Yc, Yr = remove_data_inds(meta, X, X_rec, Yc, Yr, rem_is)
        pr_fl(len(rem_is), "no-recent-games samples removed")

        # Extract derivative features
        if "time_since_match" not in X_rec_labels:
            X_rec_labels = add_recent_match_features(X, X_rec, X_labels, X_rec_labels)
            gc.collect()
        pr_fl(len(X_rec_labels))
        if n_parallel_cpu < 16:
            X_rec_avgs, X_rec_avgs_labels = get_recent_match_avgs(X, X_rec, X_rec_labels)
        else:
            pr_fl("Parallel-computing 20-game rolling stats...")
            X_rec_avgs, X_rec_avgs_labels = get_recent_match_avgs_hypopt(X, X_rec, X_rec_labels, n_jobs=n_parallel_cpu)
        gc.collect()
        X, X_labels = np.hstack((X, X_rec_avgs)), X_labels + X_rec_avgs_labels
        gc.collect()

        pr_fl("Saving 20-game rolling stats...")
        if wubba_store:
            save_ld((meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels)),store_folder + "d1,2,4,8,20_all.data", pad=False)
        else:
            save_ld((meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels)),"d1,2,4,8,20_all")#+str(int(time.time())))
            # save_ld((meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels)),"d20_all_l")#+str(int(time.time())))

        # meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels) = load_ld("d20_all")  # Load preloaded data
        # meta,X,X_rec,Yc,Yr,(meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels) = load_ld("d20_all_")
        pr_fl("Saved 20-game rolling stats. Cleaning...")

        # Clear memory of unused variables
        del X_rec
        del X_rec_avgs
        gc.collect()

        meta, X, meta_labels, X_labels = clean_features(meta, X, meta_labels, X_labels)

        if wubba_store:
            save_ld((meta,X,Yc,Yr,(meta_labels,X_labels,Yc_labels,Yr_labels)),store_folder + "d_lorb.data", pad=False)#+str(int(time.time())))
        else:
            save_ld((meta,X,Yc,Yr,(meta_labels,X_labels,Yc_labels,Yr_labels)),"d_lorb")#+str(int(time.time())))
            # save_ld((meta,X,Yc,Yr,(meta_labels,X_labels,Yc_labels,Yr_labels)),"d_lorb_l")#+str(int(time.time())))
        pr_fl("Saved cleaned stats.")
        sys.exit()

    else: # Load last saved extracted features

        if not preprocessed:
            pr_fl("Loading d_lorb...")
            meta,X,Yc,Yr,(meta_labels,X_labels,Yc_labels,Yr_labels) = load_ld("d_lorb")
            # meta,X,Yc,Yr,(meta_labels,X_labels,Yc_labels,Yr_labels) = load_ld("d_lorb_l")
            pr_fl("# samples:", X.shape[0])
            n_tot = len(X)

    if not preprocessed:
        X, X_labels = filter_linearonly(X, X_labels)
        X, X_labels = filter_features(X, X_labels)

        if noravg:
            X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l])
            pr_fl("Shape after removing rolling statistics:", X.shape[1], len(X_labels))
        if nomtm:
            X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l and "_champion_recent_" in l and "_/_" in l])
            pr_fl("Shape after removing rolling momentum statistics:", X.shape[1], len(X_labels))
        # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l and "_eloavg_" in l and "_/_" in l])

        #     # shuf_idx, X_recEnc, X_recEnc_pca = load_ld("postRnnEnc" + str(run_i) + '_' + str(fold_i))  # Concatenating doesn't work :(
        #     # for i in range(len(X_recEnc)):
        #     #     X_recEnc[i] = np.hstack([X_recEnc[i], X_recEnc_pca[i]])
        # # meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)
        # # print(meta[:10])
        # # sys.exit()
        # # if import_probs:
        # #     shuf_idx, X_probs = load_ld("rnnProbs")
        # #     X_probs = X_probs[0]
        # #     X = np.hstack([X, X_probs[:, [0]]])
        # #     X_labels += ["blue_top_sqWinProb"]
        # #     pr_fl("Imported MTL-RNN output probabilities")
        # if import_rnn:
        #     shuf_idx_, X_recEnc = None, None
        #     if no_pca:
        #         shuf_idx_, X_recEnc, _ = load_ld("postRnnEnc" + str(run_i) + '_' + str(fold_i))
        #     else:
        #         shuf_idx_, _, X_recEnc = load_ld("postRnnEnc" + str(run_i) + '_' + str(fold_i))
        #     # for i in range(len(X_recEnc)):
        #     #     X_recEnc[i] = X_recEnc[i][:, :10]
        #     new_X = np.empty((X_recEnc[0].shape[0], X_recEnc[0].shape[1] * 10))
        #     new_labels = sum([[r + '_recEnc__v' + str(i) for i in range(X_recEnc[0].shape[1])] for r in t_roles], [])
        #     for i in N_PL_r:
        #         new_X[:, (i * X_recEnc[0].shape[1]):((i + 1) * X_recEnc[0].shape[1])] = X_recEnc[i]
        #     # new_X, = shuffle_data([new_X], indices=np.argsort(shuf_idx_))

        #     meta, X, Yc, Yr = shuffle_data([meta, X, Yc, Yr], indices=shuf_idx_)
        #     selector_pre = SelectKBest(k=1)
        #     # n_train = 70194
        #     Y = Yc[:, Yc_labels.index("blue_win")].reshape(-1, 1)
        #     # print(new_X.shape, Y.shape)
        #     selector_pre.fit(new_X[:n_train], Y.flatten()[:n_train])
        #     # selector_pre.fit(new_X, Y)
        #     selected_ls = list(set(['_'.join(new_labels[i].split('_')[2:]) for i in selector_pre.get_support(indices=True)]))
        #     selected_is = [new_labels.index(l) for l in new_labels if '_'.join(l.split('_')[2:]) in selected_ls]
        #     new_labels = [new_labels[i] for i in selected_is]
        #     print("Selected", len(selected_is), "/", new_X.shape[1], "postRnn dimensions")
        #     print(selected_is)

        #     X_labels += new_labels
        #     X = np.hstack([X, new_X])
        #     X_recEnc_try = ['recEnc__v' + str(min(selected_is))]
        #     # X_recEnc_try = ['recEnc__v17']F
        #     # X_recEnc_try = ['recEnc__v2', 'recEnc__v7']
        #     # X_recEnc_try = ['recEnc__v2', 'recEnc__v5']
        #     # X_recEnc_try = ['recEnc__v3']
        #     # X_recEnc_try = ['recEnc__v5']
        #     # X_recEnc_try = ['recEnc__v5', 'recEnc__v55']
        #     # X_recEnc_try = ['recEnc__v5', 'recEnc__v51', 'recEnc__v55', 'recEnc__v58', 'recEnc__v107']
        #     X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if 'recEnc__' in l and '_'.join(l.split('_')[2:]) not in X_recEnc_try])
        #     pr_fl("Imported post-RNN encodings")
        #     meta, X, Yc, Yr = shuffle_data([meta, X, Yc, Yr], indices=np.argsort(shuf_idx_))

        n_tot = len(X)

        if bayopC:
            def eval_al(a, b, g, C):
                global meta, X, Yc, Yr, meta_labels, X_labels, Yc_labels, Yr_labels, shuf_idx, n_tot, n_train
                a /= 10
                b /= 3
                g /= 10
                meta_, X_, Yc_, Yr_, meta_labels_, X_labels_, Yc_labels_, Yr_labels_ = deepcopy((meta, X, Yc, Yr, meta_labels, X_labels, Yc_labels, Yr_labels))
                meta_, X_, Yc_, Yr_, Y_, X_labels_, alpha_, beta_, X_scales_, X_scalesdict_, Yr_scales_, Yr_scalesdict_, scaler_, X_autolog_, X_mins_ = \
                    preprocess_features(meta_, X_, Yc_, Yr_, meta_labels_, X_labels_, Yc_labels_, Yr_labels_, n_train,
                        shuffle=False, autolog=True, rollAL=not norollAL, a=a, b=b, g=g, verbose=False)

                meta_, X_, Yc_, Yr_, Y_ = shuffle_data([meta_, X_, Yc_, Yr_, Y_], indices=shuf_idx)

                model_strs = [
                    # "LogisticRegression",
                    "LogisticRegression",
                #     "TF",
                ]

                # all_inds = load_ld("model_logs/cv_idx.data", pad=False)
                # run_i = 0
                # fold_i = 0
                # train_idx, test_idx = all_inds[run_i][fold_i][0], all_inds[run_i][fold_i][1]
                # shuf_idx = np.hstack([train_idx, test_idx])
                n_train = len(train_idx)
                # n_test = n_tot - n_train
                # meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)

                # n_train = 70194
                n_test = n_tot - n_train - 10000

                # n_test = 17743
                # n_train = n_tot - (n_test + n_pcal)

                z = train_models(meta_, X_, Y_, meta_labels_, X_labels_, model_strs, n_train, n_test, n_pcal,
                    alpha_, beta_, X_scales_, X_scalesdict_, Yr_scales_, Yr_scalesdict_, scaler_, X_autolog_, X_mins_, False,
                    bayopt=True, verbose=False, validation=True, model_params={'C': np.e ** C})
                sys_print(str(z) + '   ')
                return (z - 73.0) * 5.0
        else:
            def eval_al(a, b, g):
                global meta, X, Yc, Yr, meta_labels, X_labels, Yc_labels, Yr_labels, shuf_idx, n_tot, n_train
                a /= 10
                b /= 3
                g /= 10
                meta_, X_, Yc_, Yr_, meta_labels_, X_labels_, Yc_labels_, Yr_labels_ = deepcopy((meta, X, Yc, Yr, meta_labels, X_labels, Yc_labels, Yr_labels))
                meta_, X_, Yc_, Yr_, Y_, X_labels_, alpha_, beta_, X_scales_, X_scalesdict_, Yr_scales_, Yr_scalesdict_, scaler_, X_autolog_, X_mins_ = \
                    preprocess_features(meta_, X_, Yc_, Yr_, meta_labels_, X_labels_, Yc_labels_, Yr_labels_, n_train,
                        shuffle=False, autolog=True, rollAL=not norollAL, a=a, b=b, g=g, verbose=False)

                meta_, X_, Yc_, Yr_, Y_ = shuffle_data([meta_, X_, Yc_, Yr_, Y_], indices=shuf_idx)

                model_strs = [
                    # "LogisticRegression",
                    "LogisticRegressionCV",
                #     "TF",
                ]

                # all_inds = load_ld("model_logs/cv_idx.data", pad=False)
                # run_i = 0
                # fold_i = 0
                # train_idx, test_idx = all_inds[run_i][fold_i][0], all_inds[run_i][fold_i][1]
                # shuf_idx = np.hstack([train_idx, test_idx])
                n_train = len(train_idx)
                # n_test = n_tot - n_train
                # meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)

                # n_train = 70194
                n_test = n_tot - n_train - 10000

                # n_test = 17743
                # n_train = n_tot - (n_test + n_pcal)

                z = train_models(meta_, X_, Y_, meta_labels_, X_labels_, model_strs, n_train, n_test, n_pcal,
                    alpha_, beta_, X_scales_, X_scalesdict_, Yr_scales_, Yr_scalesdict_, scaler_, X_autolog_, X_mins_, False,
                    bayopt=True, verbose=False, validation=True)
                sys_print(str(z) + '   ')
                return (z - 73.0) * 5.0

        if bayop:
            boundz = {
                "a": (0.001 * 10, 0.15 * 10), 
                "b": (0.2 * 3, 0.5 * 3), 
                "g": (0.001 * 10, 0.01 * 10)
            }
            if bayopC:
                boundz['C'] = (-2.0, 4.0)
            optimizer = BayesianOptimization(f=eval_al, pbounds=boundz)
            optimizer.maximize(init_points=8, n_iter=7)
            peak = deepcopy(optimizer.max)
            pr_fl(("PEAK (raw):", peak["params"]["a"], peak["params"]["b"], peak["params"]["g"]))
            if bayopC: pr_fl(peak["params"]["C"])
            peak["params"]["a"] /= 10
            peak["params"]["b"] /= 3
            peak["params"]["g"] /= 10
            pr_fl(("PEAK:", peak["params"]["a"], peak["params"]["b"], peak["params"]["g"]))
            del peak["params"]["C"]
            meta, X, Yc, Yr, Y, X_labels, alpha, beta, X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins = \
                preprocess_features(meta, X, Yc, Yr, meta_labels, X_labels, Yc_labels, Yr_labels, n_train,
                    premade_shuf=False, shuffle=False, autolog=True, **peak["params"])
        else:
            a, b, g = None, None, None
            if run_i != -1:
                # a, b, g = 0.8272   ,  1.382    ,  0.03178
                # a, b, g = 1.5      ,  1.163    ,  0.01
                # a, b, g = 0.06892  ,  0.4984   ,  0.02242
                # a, b, g = 0.01278  ,  0.3234   ,  0.01031    # 70.6-70.8  on  2-4
                # a, b, g = 0.001    ,  0.4179   ,  0.001
                # a, b, g = 0.1112   ,  0.3396   ,  0.02506
                # a, b, g = 0.02  ,  0.35   ,  0.02
                a, b, g = run_ps[(run_i, fold_i)]
                pr((run_i, fold_i, a, b, g))
                a /= 10
                b /= 3
                g /= 10
            else:
                # a, b, g = np.mean([val[0] for val in run_ps.values()]), \
                #           np.mean([val[1] for val in run_ps.values()]), \
                #           np.mean([val[2] for val in run_ps.values()])
                # a, b, g = 0.012442057326872726, 0.20000000000000004, 0.01
                # a, b, g = 0.1363    ,  0.323   ,  0.007019
                a, b, g = 0.1009    , 0.3467     ,  0.004477
            pr((a, b, g))
            params = {'a': a, 'b': b, 'g': g}
            meta, X, Yc, Yr, Y, X_labels, alpha, beta, X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins = \
                preprocess_features(meta, X, Yc, Yr, meta_labels, X_labels, Yc_labels, Yr_labels, n_train,
                    premade_shuf=False, shuffle=shuf, autolog=True, rollAL=not norollAL, **params)

        save_ld((meta, X, Yc, Yr, Y, meta_labels, X_labels, alpha, beta, \
                X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins), "d_lorb_scaled")
        # save_ld((meta, X, Yc, Yr, Y, meta_labels, X_labels, alpha, beta, \
        #     X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins), "d_lorb_scaled_l")
    else:
        meta, X, Yc, Yr, Y, meta_labels, X_labels, alpha, beta, \
            X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins = load_ld("d_lorb_scaled")
        # meta, X, Yc, Yr, Y, meta_labels, X_labels, alpha, beta, \
        #     X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins = load_ld("d_lorb_scaled_l")
        n_tot = X.shape[0]
        pr_fl("# samples:", n_tot)

    if noravg:
        X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l])
        pr_fl("Shape after removing rolling statistics:", X.shape[1], len(X_labels))
    if nomtm:
        X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l and "_champion_recent_" in l and "_/_" in l])
        pr_fl("Shape after removing rolling momentum statistics:", X.shape[1], len(X_labels))
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l and "_eloavg_" in l and "_/_" in l])

    # meta = meta[:1000]
    # X = X[:1000]
    # Yc = Yc[:1000]
    # Yr = Yr[:1000]

    # pr_fl(np.nonzero(np.isnan(X)))
    # sys.exit()


    # shuf_idx = load_ld("model_formats/rnn_mt_model.data", pad=False)["shuf_idx"]
    # meta, X, Yc, Yr, Y, shuf_idx = shuffle_data([meta, X, Yc, Yr, Y], return_idx=True, indices=np.argsort(shuf_idx))  #deshuffle

    # meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y])

    # with open("actualFinalFeatsFinal.txt", 'w') as f:
    #     f.write(str(X_labels).replace(' ', '\n'))
    # sys.exit()

    # save_ld((X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins), "d_lorb_scales")
    # sys.exit()


    # new_Xs = []
    # for i in range(len(pcs_ratios)):
    #     k_key = pcs_ratios_keys[i]
    #     k, k_ = pcs_ratios[i]
    #     for r in t_roles:
    #         if r + '_' + k in X_labels:
    #             new_Xs.append(X[:, [X_labels.index(r + '_' + k)]] - X[:, [X_labels.index(r + '_' + k_)]])
    #             X_labels.append(k_key + '_dev')
    # X = np.hstack([X] + new_Xs)
    # pr_fl("Added pcs deviations", X.shape, len(X_labels))

    # for i in range(len(pcs_ratios)):
    #     k_key = pcs_ratios_keys[i]
    #     k, k_ = pcs_ratios[i]
    #     for r in t_roles:
    #         if r + '_' + k in X_labels:
    #             X[:, X_labels.index(r + '_' + k_key)] = X[:, X_labels.index(r + '_' + k)] - X[:, X_labels.index(r + '_' + k_)]
    # pr_fl("Fixed pcs deviations")



    if run_i != -1:
        train_idx, test_idx = all_inds[run_i][fold_i][0], all_inds[run_i][fold_i][1]
        shuf_idx = np.hstack([train_idx, test_idx])
        n_train = len(train_idx)
        n_test = X.shape[0] - n_train
        meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)
        n_train = 77743
        n_test = 10000
    else:
        n_test = n_tot - n_train

    # # # Import post-rnn components
        # shuf_idx, X_recEnc, X_recEnc_pca = load_ld("postRnnEnc" + str(run_i) + '_' + str(fold_i))  # Concatenating doesn't work :(
        # for i in range(len(X_recEnc)):
        #     X_recEnc[i] = np.hstack([X_recEnc[i], X_recEnc_pca[i]])
    # meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)
    # print(meta[:10])
    # sys.exit()
    # if import_probs:
    #     shuf_idx, X_probs = load_ld("rnnProbs")
    #     X_probs = X_probs[0]
    #     X = np.hstack([X, X_probs[:, [0]]])
    #     X_labels += ["blue_top_sqWinProb"]
    #     pr_fl("Imported MTL-RNN output probabilities")


    if import_rnn:
        shuf_idx, X_recEnc = None, None
        # if no_pca:
        shuf_idx, X_recEnc, _ = load_ld("postRnnEnc" + str(run_i) + '_' + str(fold_i))
        # else:
        #     shuf_idx, _, X_recEnc = load_ld("postRnnEnc" + str(run_i) + '_' + str(fold_i))
        # for i in range(len(X_recEnc)):
        #     X_recEnc[i] = X_recEnc[i][:, :10]
        n_recDims = X_recEnc[0].shape[1]
        new_X = np.empty((X_recEnc[0].shape[0], n_recDims * 10))
        new_labels = sum([[r + '_recEnc__v' + str(i) for i in range(n_recDims)] for r in t_roles], [])
        for i in N_PL_r:
            new_X[:, (i * n_recDims):((i + 1) * n_recDims)] = X_recEnc[i]

        if no_pca:
            # Select the top F value component for each of the 10 positions
            comp_idx = []
            orig_idx = []
            Y_sel = Y.flatten()[:n_train]
            Y_sel = np.hstack([Y_sel, 1.0 - Y_sel])
            new_Xs = []
            new_ls = []
            # for i in N_PL_r:
            for i in range(5):
                i_ = i + 5
                selector_pre = SelectKBest(k=n_select_k)
                # selector_pre = SelectKBest(k=2, score_func=mutual_info_classif)
                # n_train = 70194
                role_X = new_X[:n_train, (i * n_recDims):((i + 1) * n_recDims)]
                role_X = np.vstack([role_X, new_X[:n_train, (i_ * n_recDims):((i_ + 1) * n_recDims)]])
                role_X_all = new_X[:, (i * n_recDims):((i + 1) * n_recDims)]
                role_X_all = np.vstack([role_X_all, new_X[:, (i_ * n_recDims):((i_ + 1) * n_recDims)]])

                # pca = IncrementalPCA(n_components=24)
                # # pca.fit(role_X)
                # role_X = pca.fit_transform(role_X)
                # role_X_all = pca.transform(role_X_all)

                selector_pre.fit(role_X, Y_sel)
                # selector_pre.fit(new_X, Y)
                # selected_ls = list(set(['_'.join(new_labels[i].split('_')[2:]) for i in selector_pre.get_support(indices=True)]))
                # selected_is = [new_labels.index(l) for l in new_labels if '_'.join(l.split('_')[2:]) in selected_ls]
                # new_labels = [new_labels[i] for i in selected_is]
                # selected_is = sorted(selected_is)
                selected_is = selector_pre.get_support(indices=True).tolist()
                # print("Selected", len(selected_is), "/", new_X.shape[1], "postRnn dimensions")
                # print(selected_is)
                
                for sel_i in selected_is:
                    orig_idx.append(sel_i)
                    # orig_idx.append(selected_is[1])
                    comp_idx.append(sel_i + (i * n_recDims))
                    # comp_idx.append(selected_is[1] + (i * n_recDims))
                    # if i < 5:
                    #     i += 5
                    # else:
                    #     i -= 5
                    comp_idx.append(sel_i + (i_ * n_recDims))
                    # comp_idx.append(selected_is[1] + (i_ * n_recDims))
                    role_X_all_ = role_X_all[:, [sel_i]]
                    n_half = len(role_X_all_) // 2
                    role_X_all_ = np.hstack([role_X_all_[:n_half], role_X_all_[n_half:]])
                    # pr_fl("PCA for role", i)
                    new_Xs.append(role_X_all_)
                    new_ls.append('blue_' + roles_all[i] + '_recEnc__vF' + str(selected_is.index(sel_i)))
                    new_ls.append('red_' + roles_all[i] + '_recEnc__vF' + str(selected_is.index(sel_i)))
            new_X = np.hstack(new_Xs)
            new_labels = new_ls

        else:
            # Select the top F value component for each of the 10 positions
            comp_idx = []
            orig_idx = []
            Y_sel = Y.flatten()[:n_train]
            Y_sel = np.hstack([Y_sel, 1.0 - Y_sel])
            new_Xs = []
            new_ls = []
            # for i in N_PL_r:
            if fan:
                pr_fl("Performing Factor Analysis (FA)")
            if ica:
                pr_fl("Performing Independent Component Analysis (ICA)")
            if nmf:
                pr_fl("Performing Nonnegative Matrix Factorization (NMF)")
            if kpca:
                pr_fl("Performing Kernel PCA (RBF)")
            if lda:
                pr_fl("Performing Linear Discriminant Analysis (LDA)")
            for i in range(5):
                i_ = i + 5
                selector_pre = SelectKBest(k=n_select_k)
                # selector_pre = SelectKBest(k=2, score_func=mutual_info_classif)
                # n_train = 70194
                role_X = new_X[:n_train, (i * n_recDims):((i + 1) * n_recDims)]
                role_X = np.vstack([role_X, new_X[:n_train, (i_ * n_recDims):((i_ + 1) * n_recDims)]])
                role_X_all = new_X[:, (i * n_recDims):((i + 1) * n_recDims)]
                role_X_all = np.vstack([role_X_all, new_X[:, (i_ * n_recDims):((i_ + 1) * n_recDims)]])

                pca = IncrementalPCA(n_components=n_pca_comps)
                if fan:
                    pca = FactorAnalysis(n_components=n_pca_comps)
                if ica:
                    pca = FastICA(n_components=n_pca_comps)
                if nmf:
                    pca = NMF(n_components=n_pca_comps)
                if kpca:
                    pca = KernelPCA(n_components=n_pca_comps, kernel="rbf", n_jobs=n_parallel_cpu, eigen_solver='arpack')
                # pca.fit(role_X)
                n_pca_tr = 40000 if kpca else n_train
                pca.fit(role_X[:n_pca_tr])
                role_X = pca.transform(role_X)
                role_X_all = pca.transform(role_X_all)

                selector_pre.fit(role_X, Y_sel)
                selected_is = selector_pre.get_support(indices=True).tolist()
                # selected_is = [1]

                # selector_pre.fit(new_X, Y)
                # selected_ls = list(set(['_'.join(new_labels[i].split('_')[2:]) for i in selector_pre.get_support(indices=True).tolist()]))
                # selected_is = [new_labels.index(l) for l in new_labels if '_'.join(l.split('_')[2:]) in selected_ls]
                # new_labels = [new_labels[i] for i in selected_is]
                # selected_is = sorted(selected_is)
                # print("Selected", len(selected_is), "/", new_X.shape[1], "postRnn dimensions")
                # print(selected_is)
                # sel_i = selected_is[0]
                for sel_i in selected_is:
                    orig_idx.append(sel_i)
                    # orig_idx.append(selected_is[1])
                    comp_idx.append(sel_i + (i * n_recDims))
                    # comp_idx.append(selected_is[1] + (i * n_recDims))
                    # if i < 5:
                    #     i += 5
                    # else:
                    #     i -= 5
                    comp_idx.append(sel_i + (i_ * n_recDims))
                    # comp_idx.append(selected_is[1] + (i_ * n_recDims))
                    role_X_all_ = role_X_all[:, [sel_i]]
                    n_half = len(role_X_all_) // 2
                    role_X_all_ = np.hstack([role_X_all_[:n_half], role_X_all_[n_half:]])
                    # pr_fl("PCA for role", i)
                    new_Xs.append(role_X_all_)
                    new_ls.append('blue_' + roles_all[i] + '_recEnc__vF' + str(selected_is.index(sel_i)))
                    new_ls.append('red_' + roles_all[i] + '_recEnc__vF' + str(selected_is.index(sel_i)))
            new_X = np.hstack(new_Xs)
            new_labels = new_ls

        X_labels += new_labels
        X = np.hstack([X, new_X])
        # X = np.hstack([X, np.vstack([new_X, new_X[-(X.shape[0] - new_X.shape[0]):]])])
        print(orig_idx)
        pr_fl("Imported post-RNN encodings")


        # selector_pre = SelectKBest(k=1)
        # # n_train = 70194
        # Y_sel = Y.flatten()[:n_train]
        # # print(new_X.shape, Y.shape)
        # selector_pre.fit(new_X[:n_train], Y_sel)
        # # selector_pre.fit(new_X, Y)
        # selected_ls = list(set(['_'.join(new_labels[i].split('_')[2:]) for i in selector_pre.get_support(indices=True)]))
        # selected_is = [new_labels.index(l) for l in new_labels if '_'.join(l.split('_')[2:]) in selected_ls]
        # # new_labels = [new_labels[i] for i in selected_is]
        # print("Selected", len(selected_is), "/", new_X.shape[1], "postRnn dimensions")
        # print(selected_is)

        # X_labels += new_labels
        # X = np.hstack([X, new_X])
        # X_recEnc_try = ['recEnc__v' + str(min(selected_is))]
        # # X_recEnc_try = ['recEnc__v1']
        # # X_recEnc_try = ['recEnc__v2', 'recEnc__v7']
        # # X_recEnc_try = ['recEnc__v2', 'recEnc__v5']
        # # X_recEnc_try = ['recEnc__v3']
        # # X_recEnc_try = ['recEnc__v5']
        # # X_recEnc_try = ['recEnc__v5', 'recEnc__v55']
        # # X_recEnc_try = ['recEnc__v5', 'recEnc__v51', 'recEnc__v55', 'recEnc__v58', 'recEnc__v107']
        # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if 'recEnc__' in l and '_'.join(l.split('_')[2:]) not in X_recEnc_try])
        # pr_fl("Imported post-RNN encodings")


    #     # _, _, X_recEnc = load_ld("postMtmEnc")
    #     # X_recEnc = X_recEnc[0]
    #     # new_X = np.empty((X_recEnc.shape[0] // 10, X_recEnc.shape[1] * 10))
    #     # new_labels = sum([[r + '_mtmEnc__x' + str(i) for i in range(X_recEnc.shape[1])] for r in t_roles], [])
    #     # for i in N_PL_r:
    #     #     new_X[:, (i * X_recEnc.shape[1]):((i + 1) * X_recEnc.shape[1])] = X_recEnc[i::10]
        
        # X_labels += new_labels
        # X = np.hstack([X, new_X])
        # # X_recEnc_try = ['mtmEnc__x' + str(selected_is[0])]
        # X_recEnc_try = ['mtmEnc__x60']
        # # X_recEnc_try = ['mtmEnc__x60', 'mtmEnc__x101']
        # # X_recEnc_try = ['mtmEnc__x60', 'mtmEnc__x101', 'mtmEnc__x106']
        # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if 'mtmEnc__' in l and '_'.join(l.split('_')[2:]) not in X_recEnc_try])
        # pr_fl("Imported momentum encodings")

    # pr_fl("BEFORE:", X.shape[1], len(X_labels))
    # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l])
    # # # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l and "_champion_recent_" in l and "_/_" in l])
    # # X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "ravg_" in l and "_eloavg_" in l and "_/_" in l])
    # pr_fl("AFTER:", X.shape[1], len(X_labels))


    # meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y])


    if any("recEnc__v" in l for l in X_labels) and not import_rnn:
        pr_fl("Removing recEnc features...")
        pr_fl(X.shape, len(X_labels))
        X, X_labels = remove_data_subset(X, X_labels, [l for l in X_labels if "recEnc__v" in l])
        pr_fl(X.shape, len(X_labels))


    model_strs = [
        # "LogisticRegression",
        "LogisticRegressionCV",
        # "SoftmaxRidgeClassifier",
        # "BernoulliNB"
        # "GaussianNB"
        # "LinearDiscriminantAnalysis",
        # "QuadraticDiscriminantAnalysis",
        # "LinearSVC"
        # "SVC"
        # "GaussianProcessClassifier"
        # "KNeighborsClassifier"
        # "AdaBoostClassifier",
        # "MLPClassifier",
    #     "TF",
    ]

    n_tot = X.shape[0]
    n_pcal = 0

    # all_inds = load_ld("model_logs/cv_idx.data", pad=False)
    # run_i = 0
    # fold_i = 0
    # train_idx, test_idx = all_inds[run_i][fold_i][0], all_inds[run_i][fold_i][1]
    # shuf_idx = np.hstack([train_idx, test_idx])
    # n_train = len(train_idx)
    # n_test = n_tot - n_train
    # meta, X, Yc, Yr, Y = shuffle_data([meta, X, Yc, Yr, Y], indices=shuf_idx)

    # n_train = 10000
    # n_train = 70194
    # n_test = 10000

    # n_test = 17743
    # n_train = n_tot - (n_test + n_pcal)

    if trip_test:
        train_models(meta, X, Y, meta_labels, X_labels, model_strs, n_train, n_test, n_pcal,
            alpha, beta, X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins, resume, testing_pre_post=True,
            save_suffix=str(run_i) + "_" + str(fold_i) + \
                ('_nomtm' if nomtm else '') + \
                ('_rnn' if import_rnn else '') + \
                ('_nopca' if no_pca else '') + \
                ('_noravg' if noravg else ''))
        print()
        train_models(meta, X, Y, meta_labels, X_labels, model_strs, n_train, n_test, n_pcal,
            alpha, beta, X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins, resume, testing_sp=True,
            save_suffix=str(run_i) + "_" + str(fold_i) + \
                ('_nomtm' if nomtm else '') + \
                ('_rnn' if import_rnn else '') + \
                ('_nopca' if no_pca else '') + \
                ('_noravg' if noravg else '') + "_solo")
    else:
        train_models(meta, X, Y, meta_labels, X_labels, model_strs, n_train, n_test, n_pcal,
            alpha, beta, X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, scaler, X_autolog, X_mins, resume, testing_sp=solo,
            save_suffix=str(run_i) + "_" + str(fold_i) + \
                ('_nomtm' if nomtm else '') + \
                ('_rnn' if import_rnn else '') + \
                ('_nopca' if no_pca else '') + \
                ('_noravg' if noravg else '') + ('_solo' if solo else ''))


