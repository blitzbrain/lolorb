import warnings
warnings.filterwarnings("ignore")

from Server import *


# Options

earliest_patch = '9.11'
perf_earliest_patch = '9.7'
n_max_feats = 0.3
# n_min_feats = 120
n_min_feats = 0.1
cv_k_folds = 20
cv_iter = 20
svm_C = 0.94
svm_tol = 1e-5
# n_pca_threshold_nsqo = 16
n_pca_threshold_nsqo = 99999
# n_pca_threshold_sqo = 14
n_pca_threshold_sqo = 99999
# n_pca_threshold_sqpc = 10
n_pca_threshold_sqpc = 99999
# n_pca_threshold_ppc = 12
n_pca_threshold_ppc = 99999
n_max_missing = 1


model_dict, model_strs, scales, n_train, n_test, n_pcal = load_ld(pro_sq_models_fn)
_, _, full_5_centers, full_5_scales, full_5_coefs = model_dict[(5, 5, 5, 5, 5, 5, 5)][6]
f5_x_ls = model_dict[(5, 5, 5, 5, 5, 5, 5)][0]
mkey = "LogisticRegression"
f5_center_, f5_scale_, f5_coef_ = full_5_centers[mkey], full_5_scales[mkey], full_5_coefs[mkey]

data = load_ld("pro_d")
pr_fl(len(data))

_ = load_global_stats()

# # First, filter out games with too many unknown solo queue names, or no solo queue data
# data = [d for d in data if d["meta"]["n_sqnames_missing"] < 1]
# pr_fl(len(data))
# data = [d for d in data if 'X' in d]
# pr_fl(len(data))

# Filter out games that were too long ago
# data = [d for d in data if d["meta"]["timestamp"] < n_weeks_ago(5, milliseconds=False)]
patches_list = []
for i in range(len(patches_all)):
    patch = patches_all[i]
    patches_list.append(patch)
    if patch == earliest_patch:
        break
patches_list = patches_list[::-1]
patches_set = set(patches_list)
perf_patches_list = []
for i in range(len(patches_all)):
    patch = patches_all[i]
    perf_patches_list.append(patch)
    if patch == perf_earliest_patch:
        break
perf_patches_list = perf_patches_list[::-1]
perf_patches_set = set(perf_patches_list)
# pr_fl(len(data))

data = [d for d in data if d['meta']['patch'] in perf_patches_set]
pr_fl(len(data))


# Filter out game(s) with some missing features
contain_nan = [
    'visionwardbuys',
    'fbassist',
    'doubles',
    'triples',
    'csat10',
    'goldat10',
    'xpat10',
]
for l in contain_nan:
    data = [d for d in data if not any(np.isnan(pl[l]) if type(pl[l]) is np.float64 \
                            else pl[l] == ' ' or pl[l] is None for pl in d["players"])]
    pr_fl(len(data))


# Get list of unique players
pl_list = set()
for d in data:
    for p in d["players"]:
        pl_list.add(p["player"])
pl_list = list(pl_list)
pr_fl(len(pl_list))

# Create arrays to store gameid,player,role,league,patch_in_perfpatches,game_performance entries
pro_correspondence = deepcopy(pro_correspondence_ratios)
for l in pro_correspondence:
    key = pro_correspondence[l]
    pro_correspondence[l] = {
        role: (abs(f5_coef_['blue_'+role+'_'+key]) + abs(f5_coef_['red_'+role+'_'+key])) / 2
          for role in roles_all
    }
    pro_correspondence[l] = {
        role: {side: abs(f5_coef_[side + '_' + role + '_' + key]) / pro_correspondence[l][role]
          for side in cols_all} for role in roles_all
    }
pl_perfs_meta = np.zeros((len(data) * 10, 5))
n_perf_feats = len(pro_full_player_labels)
pl_perfs = np.zeros((len(data) * 10, n_perf_feats))
pl_perfs_i = 0
gid_list = [d["meta"]["gameid"] for d in data]
nans = 0
for d in data:
    gid = d['meta']['gameid']
    patch = d["meta"]["patch"]
    league = pro_leagues.index(d['meta']['league'])
    for i in range(10):
        pl = d["players"][i]
        r = pro_role_ord[i]
        role = r.split('_')[1]
        side = r.split('_')[0]
        opp_r = cols_all[1 - cols_all.index(side)] + '_' + role
        opp_pl = d["players"][pro_role_ord.index(opp_r)]
        opp_cid = opp_pl["champion"]
        name = pl["player"]
        cid = pl["champion"]
        team = d[side + "_team"]
        
        # Get global stats for the matchup
        ms = get_chgg_gms(patch_tss[patch] * 1000, cid, len(chgg_elos) - 1, role, opp_cid)
        
        pl_perfs_meta[pl_perfs_i] = [gid_list.index(gid), pl_list.index(name), roles_all.index(role), league,
                                     int(d["meta"]["patch"] in perf_patches_set)]
        pl_perfs[pl_perfs_i] = [float(pl[l]) for l in X_pro_player_labels] + \
                               [float(pl[l]) * pro_correspondence[l][role][side] for l in pro_correspondence] + \
            [float(pl[l]) / (ms['u'][pro_gms_ratios[l]] if ms['u'][pro_gms_ratios[l]] > 0 else 1.0) for l in pro_gms_ratios] + \
                               [np.nan_to_num(float(team[l])) for l in pro_team_player_labels]
#         if np.any(np.isnan(pl_perfs[pl_perfs_i])):
#             pr((pl_perfs[pl_perfs_i]))
#             if i == 0:
#                 nans += 1
        
        pl_perfs_i += 1
pr_fl(pl_perfs_meta.shape, pl_perfs.shape, nans)



# Construct final data array - encode categoricals with index
leagues_list = deepcopy(pro_leagues)
sqo_labels = []
for d in data:
    if 'X' in d:
        X_ls = d["X"]["X_labels"]
        if len(X_ls) > len(sqo_labels):
            sqo_labels = X_ls
# sqo_labels = [l for l in sqo_labels if '_' in l]
sqo_labels = deepcopy(f5_x_ls)
pr_fl(len(sqo_labels))
f5_center = np.asarray([f5_center_[l] for l in sqo_labels])
f5_scale = np.asarray([f5_scale_[l] for l in sqo_labels])
f5_coef = np.asarray([f5_coef_[l] for l in sqo_labels])
sqo_pl_labels = ['_'.join(l.split('_')[2:]) for l in sqo_labels if l[:8] == "blue_top"]
sqo_bot_labels = ['_'.join(l.split('_')[1:]) for l in sqo_labels if l[:5] == "blue_" and l.split('_')[1] not in roles_all]
role_league_perf_cache = {}
for league_i in range(len(leagues_list)):
    for ri in range_5:
        onehot = np.all(pl_perfs_meta[:, [2,3,4]] == [ri, league_i, 1], axis=1)
        if sum(onehot) == 0:
            pr_fl("Error: no pro historical stats for", leagues_list[league_i], roles_all[ri])
        role_league_perf_cache[str(ri) + '_' + str(league_i)] = np.mean(pl_perfs[np.nonzero(onehot)[0]], axis=0)
player_role_perfs = {}
D = []
patches_makeup = defaultdict(int)
leagues_makeup = defaultdict(int)
missing_makeup = defaultdict(int)
for d in data:
    x = OrderedDict()
    x['gameid'] = d["meta"]["gameid"]
    x['blue_win'] = d["meta"]["blue_win"]
    x['league'] = leagues_list.index(d["meta"]["league"])
#     x['duration'] = d["meta"]["duration"]

    x['series_count'] = d["meta"]["series_count"]
    x['series_ingame_time'] = d["meta"]["series_ingame_time"]
    x['series_blue_wins'] = d["meta"]["series_blue_wins"]
    x['series_blue_winstreak'] = d["meta"]["series_blue_winstreak"]
    x['series_red_wins'] = d["meta"]["series_red_wins"]
    x['series_red_winstreak'] = d["meta"]["series_red_winstreak"]

    # Get player-role performances averages, excluding this game
    perfs = {}
    for i in range(10):
        pl = d["players"][i]
        r = pro_role_ord[i]
        
        gid_i = gid_list.index(d["meta"]["gameid"])
        name_i = pl_list.index(pl["player"])
        ri = roles_all.index(r.split('_')[1])
        league_i = x['league']
        
        onehot = np.logical_and(
            np.all(pl_perfs_meta[:, [1,2,4]] == [name_i, ri, 1], axis=1),
            pl_perfs_meta[:, 0] != gid_i)
        res = None
        if sum(onehot) < 3:
            res = role_league_perf_cache[str(ri) + '_' + str(league_i)]
        else:
            res = np.mean(pl_perfs[np.nonzero(onehot)[0]], axis=0)
        perfs[r] = res
        
        pl_r_key = pl["player"] + '_' + str(ri)
        if pl_r_key not in player_role_perfs:
            onehot = np.all(pl_perfs_meta[:, [1,2,4]] == [name_i, ri, 1], axis=1)
            if sum(onehot) < 3:
                pass
#                 pr_fl("Less than 3 samples (", sum(onehot), ") for", pl["player"], roles_all[ri])
            else:
                player_role_perfs[pl_r_key] = np.mean(pl_perfs[np.nonzero(onehot)[0]], axis=0)
    x['performances'] = perfs
    
    if 'X' in d:
        # Get model output
        x['sq_prediction'] = d["X"]["sq_pred"]
        x_f = d['X']['X'][0]
        x_labels = d['X']['X_labels']
    #     pr_fl(len(x_f), len(x_labels))
        out = np.zeros(len(sqo_labels))
        for l_i in range(len(x_labels)):
            l = x_labels[l_i]
            if l in sqo_labels:
                sq_li = sqo_labels.index(l)
                out[sq_li] = ((x_f[l_i] - f5_center[sq_li]) / f5_scale[sq_li]) * f5_coef[sq_li]
        x["sq_output"] = out

    # Add to dataset if criteria are met
#     if 'X' in d and (("n_missing" in d["X"] and d["X"]["n_missing"] < 1) or \
#                      ("n_missing" not in d["X"] and d["meta"]["n_sqnames_missing"] < 1)) and d["meta"]["patch"] in patches_set:
    if 'X' in d and ("n_missing" in d["X"] and d["X"]["n_missing"] <= n_max_missing):
        x['patch'] = patches_list.index(d["meta"]["patch"])
        D.append(x)
        patches_makeup[d["meta"]["patch"]] += 1
        leagues_makeup[d["meta"]["league"]] += 1
        missing_makeup[d["X"]["n_missing"]] += 1
#         pr_fl(d["meta"]["patch"]) # Check the meta info of the data that makes it through the filter (ensure balance)
#         pr_fl(d["blue_team"]["team"])
#         pr_fl(d["red_team"]["team"])
pr_fl(len(D))
pr((patches_makeup))
pr((leagues_makeup))
pr((missing_makeup))





# Create data arrays for fast indexing during feature selection model training
noncat_grps = [
    'series_count',
    'series_ingame_time',
    'sq_prediction',
]
series_wins_grp = [
    'series_blue_wins',
    'series_red_wins',
]
series_winstreak_grp = [
    'series_blue_winstreak',
    'series_red_winstreak',
]
series_grps = [
    "series_wins",
    "series_winstreak",
]
sqo_pl_r_labels = sum([[r + '_' + l for l in sqo_pl_labels] for r in roles_all], [])
X_pro_player_r_labels = sum([[r + '_' + l for l in pro_full_player_labels] for r in roles_all], [])
X_pro_player_t_labels = sum([[r + '_' + l for l in pro_full_player_labels] for r in t_roles], [])
grps_config = \
    sqo_pl_r_labels + sqo_bot_labels + X_pro_player_r_labels
#     sqo_pl_labels + sqo_bot_labels + X_pro_player_r_labels
#     ['diff_' + l for l in sqo_pl_r_labels] + \
#     sqo_bot_labels + ['diff_' + l for l in sqo_bot_labels] + \
#     X_pro_player_r_labels + ['diff_' + l for l in X_pro_player_r_labels]
#     sqo_pl_r_labels + ['diff_' + l for l in sqo_pl_r_labels] + \
X_groups = [
    "league",
    "patch",
] + noncat_grps + series_grps + grps_config
leagues_arr = np.zeros((len(D), len(leagues_list)))
patches_arr = np.zeros((len(D), len(patches_list)))
for i in range(len(D)):
    d = D[i]
    leagues_arr[i, d["league"]] = 1
    patches_arr[i, d["patch"]] = 1
X_arrs = [leagues_arr, patches_arr]
X_arrs += [np.asarray([d[l] for d in D]).reshape(-1, 1) for l in noncat_grps]

# Current configuration: take difference between sides, allow selection among roles (no grouping or summing)

# X_arrs += [np.asarray([[d[l] for l in series_wins_grp] for d in D])]
# X_arrs += [np.asarray([[d[l] for l in series_winstreak_grp] for d in D])]
X_arrs += [np.asarray([d["series_blue_wins"] - d["series_red_wins"] for d in D]).reshape(-1, 1)]
X_arrs += [np.asarray([d["series_blue_winstreak"] - d["series_red_winstreak"] for d in D]).reshape(-1, 1)]

# X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index(r + '_' + l)] for r in t_roles] for d in D])
#        for l in sqo_pl_labels]                                                                  # raw, roles grouped
# X_arrs += sum([[np.asarray([[d["sq_output"][sqo_labels.index(c + '_' + r + '_' + l)] for c in cols_all] for d in D])
#        for l in sqo_pl_labels] for r in roles_all], [])                                           # raw, roles ungrouped
# X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for r in roles_all] for d in D]) - \
#            np.asarray([[d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for r in roles_all] for d in D])
#         for l in sqo_pl_labels]                                                                 # diff, roles grouped
X_arrs += sum([[np.asarray([d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for d in D]).reshape(-1, 1) - \
                np.asarray([d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for d in D]).reshape(-1, 1)
        for l in sqo_pl_labels] for r in roles_all], [])                                        # diff, roles ungrouped
# X_arrs += [np.sum([np.asarray([d["sq_output"][sqo_labels.index('blue_' + r + '_' + l)] for d in D]).reshape(-1, 1) - \
#                    np.asarray([d["sq_output"][sqo_labels.index('red_' + r + '_' + l)] for d in D]).reshape(-1, 1)
#         for r in roles_all], axis=0) for l in sqo_pl_labels]                                    # diff, roles summed

# X_arrs += [np.asarray([[d["sq_output"][sqo_labels.index(c + '_' + l)] for c in cols_all] for d in D]) for l in sqo_bot_labels]
X_arrs += [np.asarray([d["sq_output"][sqo_labels.index('blue_' + l)] for d in D]).reshape(-1, 1) - \
           np.asarray([d["sq_output"][sqo_labels.index('red_' + l)] for d in D]).reshape(-1, 1) for l in sqo_bot_labels]

# X_arrs += [np.asarray([[d["performances"][r][i] for r in t_roles] for d in D])
#         for i in range(n_perf_feats)]                                               # raw, roles grouped
# X_arrs += sum([[np.asarray([[d["performances"][c + '_' + r][i] for c in cols_all] for d in D])
#         for i in range(n_perf_feats)] for r in roles_all], [])                        # raw, roles ungrouped
# X_arrs += [np.asarray([[d["performances"]["blue_" + r][i] for r in roles_all] for d in D]) - \
#            np.asarray([[d["performances"]["red_" + r][i] for r in roles_all] for d in D])
#         for i in range(n_perf_feats)]                                               # diff, roles grouped
X_arrs += sum([[np.asarray([d["performances"]["blue_" + r][i] for d in D]).reshape(-1, 1) - \
                np.asarray([d["performances"]["red_" + r][i] for d in D]).reshape(-1, 1)
        for i in range(n_perf_feats)] for r in roles_all], [])                      # diff, roles ungrouped
# X_arrs += [np.sum([np.asarray([d["performances"]["blue_" + r][i] for d in D]).reshape(-1, 1) - \
#                    np.asarray([d["performances"]["red_" + r][i] for d in D]).reshape(-1, 1)
#         for r in roles_all], axis=0) for i in range(n_perf_feats)]                  # diff, roles summed




# Remove a random subset of data

# tried_is = [
#     138, 129,  54,   2,  66,  86,  99,  32
#     107,  59,  70, 103, 123,   0,  76,  95
#      32,  48,  42, 143,  70, 131,  34,  90
#       0,   3,  79,  26,  53, 122, 113, 110
#     118,  42,  52,  30,  16,  72,  70, 100
#       8, 107,  48,  18,  53, 102,  78, 140
# ]

n_data = X_arrs[0].shape[0]
# rem_is = np.random.choice(list(set(range(n_data)) - set(tried_is)), 8, False)
# rem_is = np.random.choice(n_data, 8, False)
# rem_is = [107, 78, 140, 138, 129]
rem_is = []
inds = [j for j in range(n_data) if j not in rem_is]
X_a = [X_arr[inds] for X_arr in X_arrs]

# Apply robust scaling for appropriate feature groups
scale_groups = noncat_grps + series_grps + grps_config
scalers = {}
for l in scale_groups:
    l_i = X_groups.index(l)
    scaler = RobustScaler()
    X_a[l_i] = scaler.fit_transform(X_a[l_i])
    scalers[l] = scaler

# Create final X index representation
X = np.tile(range(len(inds)), (len(X_groups), 1)).T.astype(float)
for j in range(len(X_groups)):
    X[:, j] += j / 10000

Y = np.asarray([d["blue_win"] for d in D])[inds]
pr_fl(X.shape, Y.shape, len(X_a), len(X_groups), sum([arr.shape[1] for arr in X_a]), X_a[0].shape)



# Test accuracy with all features
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = LinearSVC(loss='squared_hinge', penalty='l1', dual=False, C=0.78)
m = FoldingWrapper(m, X_a, pca=None)
# scores = np.hstack([cross_val_score(m, X, Y, cv=StratifiedShuffleSplit(n_splits=20)) for _ in range(10)])
scores = cross_val_score(m, X, Y, cv=LeaveOneOut())
fscore = np.mean(scores)
pr_fl("Accuracy test w/ all data: ", fscore)



# # Feature selection (with PCA above n components)
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = FoldingWrapper(m, X_a, pca=n_pca_threshold_nsqo)

# n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(X_groups))
# n_max_feats_ = min(n_max_feats, len(X_groups)) if isinstance(n_max_feats, int) else int(n_max_feats * len(X_groups))
# selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=True, floating=False,
#                cv=(cv_iter, StratifiedShuffleSplit(n_splits=cv_k_folds)),
#                estimator=m, n_jobs=n_parallel_cpu, verbose=5)
# X_selected = selector.fit_transform(X, Y)
# scores = cross_val_score(m, X_selected, Y, cv=LeaveOneOut())
# fscore = np.mean(scores)
# pr_fl("nsqo accuracy:", fscore)

# pr(([(i, selector.subsets_[i]['avg_score']) for i in range(1, len(selector.subsets_) + 1)]))

# feat_is = selector.k_feature_idx_
# pr(([X_groups[i] for i in feat_is], len(feat_is)))


# # Train and save final model and all infill data
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# if len(feat_is) > n_pca_threshold_nsqo:
#     m = Pipeline(steps=[
#         ("pca", PCA(n_components=n_pca_threshold_nsqo)),
#         ("model", m),
#     ])
# X_f = np.hstack([X_a[i] for i in feat_is])
# m.fit(X_f, Y)
# X_flabels = [X_groups[i] for i in feat_is]
# fscalers = [scalers[l] for l in X_flabels]
# save_ld(selector, "pro_model_selector")
# save_ld(((m, X_flabels, fscalers, patches_list, leagues_list, player_role_perfs, role_league_perf_cache), fscore), "pro_model")



# (m, X_flabels, fscalers, patches_list, leagues_list, player_role_perfs, role_league_perf_cache), fscore = load_ld("pro_model")
# selector = load_ld("pro_model_selector")
# feat_is = selector.subsets_[max(list(selector.subsets_.keys()))]["feature_idx"]
# X_selected = X[:, feat_is]


# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = FoldingWrapper(m, X_a, pca=n_pca_threshold_nsqo)

# n_min_feats_ = int(0.5 * X_selected.shape[1])
# n_max_feats_ = X_selected.shape[1]
# selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=False, floating=True,
#                cv=(cv_iter, StratifiedShuffleSplit(n_splits=cv_k_folds)),
#                estimator=m, n_jobs=n_parallel_cpu, verbose=5)
# X_selected = selector.fit_transform(X_selected, Y)
# scores = cross_val_score(m, X_selected, Y, cv=LeaveOneOut())
# fscore = np.mean(scores)
# pr_fl("nsqo bw accuracy:", fscore)

# # pr(([(i, selector.subsets_[i]['avg_score']) for i in range(1, len(selector.subsets_) + 1) if i in selector.subsets_]))

# feat_is = selector.k_feature_idx_
# pr(([X_groups[i] for i in feat_is], len(feat_is)))


# # Train and save final model and all infill data
# m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
# if len(feat_is) > n_pca_threshold_nsqo:
#     m = Pipeline(steps=[
#         ("pca", PCA(n_components=n_pca_threshold_nsqo)),
#         ("model", m),
#     ])
# X_f = np.hstack([X_a[i] for i in feat_is])
# m.fit(X_f, Y)
# X_flabels = [X_groups[i] for i in feat_is]
# fscalers = [scalers[l] for l in X_flabels]

# save_ld(selector, "pro_model_selector_bw")
# save_ld(((m, X_flabels, fscalers, patches_list, leagues_list, player_role_perfs, role_league_perf_cache), fscore), "pro_model_bw")



selector = load_ld("pro_model_selector")
lcs = leagues_list.index("LCS")
lcs_is = [i for i in range(len(X)) if D[i]["league"] == lcs]
X_lcs = X[lcs_is]
Y_lcs = Y[lcs_is]

m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
m = FoldingWrapper(m, X_a, pca=n_pca_threshold_nsqo)

subset_keys = list(selector.subsets_.keys())
subsets = [selector.subsets_[k] for k in subset_keys if k > 75]
X_input = [(X[:, subset["feature_idx"]], X_lcs[:, subset["feature_idx"]]) for subset in subsets]

def train_prom(m, X, Y, X_, Y_):
    m.fit(X, Y)
    return accuracy_score(Y_, m.predict(X_))

res = Parallel(n_jobs=n_parallel_cpu)(delayed(train_prom)(m, X_t, Y, X_l, Y_lcs) for (X_t, X_l)in X_input)
save_ld(res, "spec_res")

best_i = np.argmax(res)
fscore = res[best_i]
feat_is = subsets[best_i]["feature_idx"]

pr_fl("Pro specialization results:")
pr_fl("Accuracy:", fscore)
pr_fl("International accuracy:", subsets[best_i]["avg_score"])
pr_fl("Number of features:", len(subsets[best_i]["feature_idx"]))
pr_fl("Feature list:")
pr(([X_groups[i] for i in feat_is], len(feat_is)))


# Train and save final model and all infill data
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
if len(feat_is) > n_pca_threshold_nsqo:
    m = Pipeline(steps=[
        ("pca", PCA(n_components=n_pca_threshold_nsqo)),
        ("model", m),
    ])
X_f = np.hstack([X_a[i] for i in feat_is])
m.fit(X_f, Y)
X_flabels = [X_groups[i] for i in feat_is]
fscalers = [scalers[l] for l in X_flabels]

save_ld(selector, "pro_model_selector_lcs")
save_ld(((m, X_flabels, fscalers, patches_list, leagues_list, player_role_perfs, role_league_perf_cache), fscore), "pro_model_lcs")


sys.exit()




# Solo-queue data only prediction
X_sq = X[:, [i for i in range(X.shape[1]) if '_'.join(X_groups[i].split('_')[1:]) not in pro_full_player_labels]]
pr_fl(X_sq.shape)
sqlabels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels]
pr_fl(len(sqlabels))
# Solo-queue only feature selection (with PCA above n components)
# m = LinearSVC()
# m = LogisticRegression()
# m = DecisionTreeClassifier()
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = LinearSVC(loss='squared_hinge', penalty='l1', dual=False, C=100)
# m = GaussianProcessClassifier(n_restarts_optimizer=5)
m = FoldingWrapper(m, X_a, pca=n_pca_threshold_sqo)

n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(sqlabels))
n_max_feats_ = min(n_max_feats, len(sqlabels)) if isinstance(n_max_feats, int) else int(n_max_feats * len(sqlabels))
selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=True, floating=False,
               cv=(cv_iter, StratifiedShuffleSplit(n_splits=cv_k_folds)),
               estimator=m, n_jobs=n_parallel_cpu, verbose=5)
X_sq_selected = selector.fit_transform(X_sq, Y)
# X_selected = selector.transform(X_scaled)
scores = cross_val_score(m, X_sq_selected, Y, cv=LeaveOneOut())
fscore = np.mean(scores)
pr_fl("sqo accuracy:", fscore)

sqfeat_is = [X_groups.index(sqlabels[i]) for i in selector.k_feature_idx_]

# Train and save final model and all infill data
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
if len(sqfeat_is) > n_pca_threshold_sqo:
    m = Pipeline(steps=[
        ("pca", PCA(n_components=n_pca_threshold_sqo)),
        ("model", m),
    ])
X_f = np.hstack([X_a[i] for i in sqfeat_is])
m.fit(X_f, Y)
X_flabels = [X_groups[i] for i in sqfeat_is]
fscalers = [scalers[l] for l in X_flabels]
save_ld(selector, "pro_model_sqo_selector")
save_ld(((m,X_flabels,fscalers,patches_list,leagues_list,player_role_perfs,role_league_perf_cache),fscore), "pro_model_sqo")




# Solo-queue pre-champ-select data only prediction
sqpc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) not in pro_full_player_labels and \
               "_opgg_champion_recent" not in l and "_eloavg_" not in l]
X_sqpc = X[:, [i for i in range(X.shape[1]) if X_groups[i] in sqpc_labels]]
pr_fl(len(sqpc_labels), X_sqpc.shape)

# Solo-queue pre-champ-select only feature selection (with PCA above n components)
# m = LinearSVC()
# m = LogisticRegression()
# m = DecisionTreeClassifier()
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = LinearSVC(loss='squared_hinge', penalty='l1', dual=False, C=100)
# m = GaussianProcessClassifier(n_restarts_optimizer=5)
m = FoldingWrapper(m, X_a, pca=n_pca_threshold_sqpc)

n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(sqpc_labels))
n_max_feats_ = min(n_max_feats, len(sqpc_labels)) if isinstance(n_max_feats, int) else int(n_max_feats * len(sqpc_labels))
selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=True, floating=False,
               cv=(cv_iter, StratifiedShuffleSplit(n_splits=cv_k_folds)),
               estimator=m, n_jobs=n_parallel_cpu, verbose=5)
X_sqpc_selected = selector.fit_transform(X_sqpc, Y)
# X_selected = selector.transform(X_scaled)
scores = cross_val_score(m, X_sqpc_selected, Y, cv=LeaveOneOut())
fscore = np.mean(scores)
pr_fl("sqpc accuracy:", fscore)

sqpcfeat_is = [X_groups.index(sqpc_labels[i]) for i in selector.k_feature_idx_]
# Train and save final model and all infill data
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
if len(sqpcfeat_is) > n_pca_threshold_sqpc:
    m = Pipeline(steps=[
        ("pca", PCA(n_components=n_pca_threshold_sqpc)),
        ("model", m),
    ])
X_f = np.hstack([X_a[i] for i in sqpcfeat_is])
m.fit(X_f, Y)
X_flabels = [X_groups[i] for i in sqpcfeat_is]
fscalers = [scalers[l] for l in X_flabels]
save_ld(selector, "pro_model_sqpco_selector")
save_ld(((m,X_flabels,fscalers,patches_list,leagues_list,player_role_perfs,role_league_perf_cache),fscore), "pro_model_sqpco")


# Pro pre-champ-select data only prediction
ppc_labels = [l for l in X_groups if '_'.join(l.split('_')[1:]) in pro_full_player_labels or \
               ("_opgg_champion_recent" not in l and "_eloavg_" not in l)]
X_ppc = X[:, [i for i in range(X.shape[1]) if X_groups[i] in ppc_labels]]
pr_fl(len(ppc_labels), X_ppc.shape)

# Pro pre-champ-select data only feature selection (with PCA above n components)
# m = LinearSVC()
# m = LogisticRegression()
# m = DecisionTreeClassifier()
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol)
# m = LinearSVC(loss='squared_hinge', penalty='l1', dual=False, C=100)
# m = GaussianProcessClassifier(n_restarts_optimizer=5)
m = FoldingWrapper(m, X_a, pca=n_pca_threshold_ppc)

n_min_feats_ = max(1, n_min_feats) if isinstance(n_min_feats, int) else int(n_min_feats * len(ppc_labels))
n_max_feats_ = min(n_max_feats, len(ppc_labels)) if isinstance(n_max_feats, int) else int(n_max_feats * len(ppc_labels))
selector = SFS(k_features=(n_min_feats_, n_max_feats_), forward=True, floating=False,
               cv=(cv_iter, StratifiedShuffleSplit(n_splits=cv_k_folds)),
               estimator=m, n_jobs=n_parallel_cpu, verbose=5)
X_ppc_selected = selector.fit_transform(X_ppc, Y)
# X_selected = selector.transform(X_scaled)
scores = cross_val_score(m, X_ppc_selected, Y, cv=LeaveOneOut())
fscore = np.mean(scores)
pr_fl("ppc accuracy:", fscore)

ppcfeat_is = [X_groups.index(ppc_labels[i]) for i in selector.k_feature_idx_]

# Train and save final model and all infill data
m = SVC(gamma='auto', kernel='rbf', C=svm_C, tol=svm_tol, probability=True)
if len(ppcfeat_is) > n_pca_threshold_ppc:
    m = Pipeline(steps=[
        ("pca", PCA(n_components=n_pca_threshold_ppc)),
        ("model", m),
    ])
X_f = np.hstack([X_a[i] for i in ppcfeat_is])
m.fit(X_f, Y)
X_flabels = [X_groups[i] for i in ppcfeat_is]
fscalers = [scalers[l] for l in X_flabels]
save_ld(selector, "pro_model_ppco_selector")
save_ld(((m,X_flabels,fscalers,patches_list,leagues_list,player_role_perfs,role_league_perf_cache),fscore), "pro_model_ppco")


















