'''

    This file contains shared methods & constants for requesting data

'''

import glob
import requests
from threading import Condition
from concurrent.futures import *

from Utils import *


# Patches information
patches_all = None
if os.path.exists(all_patches_file):
    patches_all = json.load(open(all_patches_file, 'r'))
else:
    patches_all = json.load(open(all_patches_file_default, 'r'))

# Pro players solo queue names information
sq_dict = json.load(open(soloqueue_names_json + '.json', 'r', encoding='utf-8-sig'))
sq_dict_caps = deepcopy(sq_dict)
for k in sq_dict_caps:
    sq_dict_caps[k][1] = lowercasify(sq_dict_caps[k][1])
sq_inv_dict = {tuple(v): k for k, v in sq_dict_caps.items()}
sq_dict = {lowercasify(k): sq_dict[k] for k in sq_dict}


# HTTP request function
def req(url, decode_json=True, **kwargs):

    # Make a couple extra attempts if there's a request exception
    i = -1
    res = None
    while i < req_fail_reattempts and res is None:
        try:
            hdrs = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
            res = requests.get(url, headers=hdrs, **kwargs)
            if res is not None:
                if "status" in res:
                    if "message" in res["status"] and "Exception decrypting" in res["status"]["message"]:
                        res = None
                    elif "status_code" in res["status"] and res["status"]["status_code"] == 404:
                        res = None
        except Exception as e:
            pr_fl("Request error (retrying):", type(e), url, kwargs)
            pr_fl(e.args)
            pr_fl(e)
            res = None
            i += 1
            if i < req_fail_reattempts:
                time.sleep(req_fail_cooldown)
    if res is None:
        pr_fl("Request error (None):", url, kwargs)
        return None

    # if league_pos_by_summ_id in url:
        # pr_fl(res.headers)
    if decode_json:
        try:
            res = res.json()
        except Exception as e:
            pr_fl("Request json decode error (retrying):", type(e), url, kwargs)
            pr_fl(e.args)
            pr_fl(e)
            return None
    return res

# Check response status
def check(url, res, **kwargs):
    if not res or res["status"] != 200:
        # pr_fl("ERROR: Request to '" + url + "' failed with params:")
        # pr(kwargs)
        # pr(res)
        return False
    return True

def req_check(url, **kwargs):
    res = req(url, **kwargs)
    return [res, check(url, res, **kwargs)]

# Wait for cooldown before requesting
def cd_req(cd, last_t, rfunc, url, **kwargs):
    i = 0
    while time.time() - last_t['ts'] < cd and i < 2:
        sleep_t = cd - (time.time() - last_t['ts'])
        if sleep_t > 0:
            time.sleep(sleep_t)
        i += 1
    res = rfunc(url, **kwargs)
    last_t['ts'] = time.time()
    return res

# Cache requests
caches_loaded = False
def load_caches():
    global caches_loaded
    if not caches_loaded:
        for direc in dirs_to_create:
            create_folder(data_dir + direc)
        caches_loaded = True

# Load in most recent cached requests
def load_cache(c, ts_only, only_new=True):
    path = data_dir + r_cache[c]["path"]
    create_folder(path)
    fs = sorted(glob.glob(path + "/*.json"))
    if not not fs:
        fs[-1] = fs[-1].replace('\\', '/')
        thing = fs[-1].split('/')[-1]
        thing = thing.split('_')[0] if '_' in thing else thing.split('.json')[0]
        if len(thing) >= 14:
            thing = thing[:13]
        ts = float(thing) / 1000.0
        ts_key = str(ts)
        # print(c, ts, thing, time.time() - ts < r_cache[c]["ls"])
        if time.time() - ts < r_cache[c]["ls"] or not only_new:
            r_cache[c]["archive"][ts_key] = ts_key if ts_only else \
                load_json(fs[-1], pad=False)
            if r_cache[c]["data"] is None:
                r_cache[c]["ts"] = ts
                r_cache[c]["data"] = r_cache[c]["archive"][ts_key]
    r_cache[c]["loaded"] = True

# Perform a request, retrieving from cache if possible, otherwise updating
def cache_req(c, rfunc, ts_only, *args, patch=None, **kwargs):
    global caches_loaded, patches_all
    if not caches_loaded:
        load_caches()
    if not r_cache[c]["loaded"]:
        load_cache(c, ts_only)
    if patch is None:
        patch = patches_all[0]
    if time.time() - r_cache[c]["ts"] < r_cache[c]["ls"]:
        ts = str(int(r_cache[c]["ts"] * 1000.0)) + '_' + patch
        return ts if ts_only else (r_cache[c]["data"], ts)
    # if r_cache[c]["del_old"]:
        # create_folder(data_dir + r_cache[c]["path"], overwrite=True)
        # for f in sorted(glob.glob(data_dir + r_cache[c]["path"] + "/*.json")):
        #     os.remove(f)
    res = rfunc(*args, **kwargs)
    if res is None: # If request error'd, ignore timestamps & load old data
        sys.stderr.write("CACHEREQ FAILED: " + str(rfunc))
        sys.stderr.flush()
        load_cache(c, ts_only, only_new=False)
        r_cache[c]["ts"] = time.time()
        return cache_req(c, rfunc, ts_only, *args, patch=patch, **kwargs)
    r_cache[c]["ts"] = time.time()
    ts_key = str(r_cache[c]["ts"])
    r_cache[c]["archive"][ts_key] = ts_key if ts_only else res
    r_cache[c]["data"] = r_cache[c]["archive"][ts_key]
    ts = str(int(r_cache[c]["ts"] * 1000.0)) + '_' + patch
    save_json(res, r_cache[c]["path"] + '/' + ts)

    if c == "chgg_champions":
        chmp_pats = sum([sum([[patch_s2n(c["patch"]) for c in b] for b in res[a]], []) for a in res.keys()], [])
        # newest_patch = res[list(res.keys())[0]][0][0]["patch"]
        newest_patch = max(chmp_pats)
        if newest_patch > patch_s2n(patches_all[0]): # New patch detected
            patches_all = [patch_n2s(newest_patch)] + patches_all
            json.dump(patches_all, open(all_patches_file, 'w'))
            json.dump(patches_all, open(all_patches_file_default, 'w'))
    if c == "chgg_matchups":
        trigger_gsdb_upload()
    return ts if ts_only else (res, ts)

# Load a previously cached response (indexed by timestamp)
def load_archived(c, ts_key):
    if ts_key in r_cache[c]["archive"]:
        return r_cache[c]["archive"][ts_key]
    r_cache[c]["archive"][ts_key] = load_json(r_cache[c]["path"]+'/'+ts_key)
    return r_cache[c]["archive"][ts_key]


# Rito requests

def validate_response(res, rito_rate_limit_exceeded_count, region, url):
    if res is None: # Request error (treat like a 404)
        return -404
    if "status" in res:
        if res["status"]["status_code"] == 404: # Data not found
            return -404
        if res["status"]["status_code"] == 400 and "Exception decrypting" in res["status"]["message"]: # Encrypted id not found
            return -404
        poll_time = rito_err_cd                 # Server error
        if res["status"]["status_code"] == 429: # Rate limit exceeded
            rito_rate_limit_exceeded_count += 1
            poll_time = rito_cd * (2.0 ** rito_rate_limit_exceeded_count)
        if res["status"]["status_code"] == 403: # Forbidden
            poll_time = 10
        rtag = '[' + region + '] '
        pr_fl(rtag + "Bad Rito response: " + str(res["status"]) + \
                  " * Retrying in " + str(poll_time) + "...")
        pr_fl(url)
        return poll_time, rito_rate_limit_exceeded_count
    return -1   # -1 = valid, otherwise return the poll time before retrying

def rito_req(r_conds, r_tss, region, url, use_cond=True, **kwargs):
    reg = region.lower()
    u = https + reg + url
    if "params" in kwargs:
        kwargs["params"]["api_key"] = rito_key
    else:
        kwargs["params"] = {"api_key": rito_key}
    with r_conds['rr_' + reg] if r_conds and use_cond else Condition():
        ts_obj = r_tss['rr_' + reg] if r_tss else {'ts': 0}
        res = cd_req(rito_cd, ts_obj, req, u, **kwargs)
        poll_time = validate_response(res, 0, region, url)
        notFoundCount = 0
        while poll_time != -1:
            if poll_time == -404:
                pr_fl("404 Not found/id decrypt error for request: " + u)
                notFoundCount += 1
                if notFoundCount >= 3:
                    return None
                poll_time, rlec = 2, 0
            else:
                poll_time, rlec = poll_time
            time.sleep(poll_time)
            res = cd_req(rito_cd, ts_obj, req, u, **kwargs)
            poll_time = validate_response(res, rlec, region, url)
        return res

# Download all champion ids & names, titles etc, or load from cache
# def get_champions_():
#     return rito_req(None, None, 'euw1', champions_url)["data"]

all_champs_data = None
def get_champions():
    global all_champs_data, cdata_
    if all_champs_data is None:
        d = load_json(ddrag_dir + "champion", encoding='utf-8-sig', print_errs=True)
        all_champs_data = {}
        cdata_ = d["data"]
        for nkey in cdata_:
            c = cdata_[nkey]
            cid_str = c["key"]
            all_champs_data[cid_str] = {"id": int(cid_str), "key": nkey, "name": c["name"]}
    return all_champs_data
    # return cache_req("champions", get_champions_, False)[0]

def get_cids():
    return list(get_champions().keys())

def get_league_pos(r_conds, r_tss, region, summ_id, use_cond=True):
    summ_id = get_encrypted_summ_id(r_conds, r_tss, region, summ_id=str(summ_id), use_cond=use_cond)
    if summ_id is None:
        return None
    return rito_req(r_conds, r_tss, region, league_pos_by_summ_id + summ_id, use_cond=use_cond)

# Get league division for a summoner id (returns None if unranked)
def get_division(r_conds, r_tss, region, summ_id, queue=r_queue):
    league_positions = get_league_pos(r_conds, r_tss, region, summ_id)
    if league_positions is None:
        return None
    for pos in league_positions:
        if pos["queueType"] == queue:
            return get_division_index(pos["tier"] + ' ' + pos["rank"])

# Get summoner id for a given account id
def get_summ_id(r_conds, r_tss, region, acc_id):
    acc_id = get_encrypted_acc_id(r_conds, r_tss, region, acc_id=str(acc_id))
    if acc_id is None:
        return None
    summoner = rito_req(r_conds, r_tss, region, summ_by_acc_id + acc_id)
    return summoner['id']

# Get encrypted account id for a given (unencrypted) account id, or name
def get_encrypted_acc_id(r_conds, r_tss, region, acc_id=None, name=None, ret_summ_id=False):
    if name is not None:
        # Get encrypted account id by name
        summoner = rito_req(r_conds, r_tss, region, summ_by_name + name)
        if summoner is None:
            return None
        if ret_summ_id:
            return summoner['accountId'], summoner["id"]
        return summoner['accountId']

    # If account id given is already encrypted
    acc_id = str(acc_id)
    if is_str(acc_id):
        if ret_summ_id:
            return acc_id, summoner["id"]
        return acc_id

    return None

    # # Get v3 summoner by unencrypted account id
    # summoner = rito_req(r_conds, r_tss, region, v3_summ_by_acc_id + acc_id)

    # # Get v4 summoner by name, return encrypted account id
    # summoner = rito_req(r_conds, r_tss, region, summ_by_name + summoner["name"])
    # return summoner['accountId']

# Get encrypted summoner id for a given (unencrypted) summoner id, or name
def get_encrypted_summ_id(r_conds, r_tss, region, summ_id=None, name=None, use_cond=True):
    if name is not None:
        # Get encrypted summoner id by name
        summoner = rito_req(r_conds, r_tss, region, summ_by_name + name, use_cond=use_cond)
        return summoner['id']

    # If summoner id given is already encrypted
    summ_id = str(summ_id)
    if is_str(summ_id):
        return summ_id

    return None

    # # Get v3 summoner by unencrypted summoner id
    # summoner = rito_req(r_conds, r_tss, region, v3_summ_by_summ_id + summ_id)

    # # Get v4 summoner by name, return encrypted summoner id
    # summoner = rito_req(r_conds, r_tss, region, summ_by_name + summoner["name"])
    # return summoner['id']


def ranked_matchlist(r_conds, r_tss, region, acc_id, params={}):
    acc_id = get_encrypted_acc_id(r_conds, r_tss, region, acc_id=str(acc_id))
    if acc_id is None:
        return None
    return rito_req(r_conds, r_tss, region, matchlist_by_acc_id + \
        acc_id,
        params={**{"queue": r_queue_ids}, **params})

# ''.join(a_id.split('_')[1:])


# op.gg requests

opgg_last_req_t = {'ts': time.time()}

def opgg_req(url, wait_t=opgg_max_wait, **kwargs):
    # print(url)
    if wait_t > 0:
        time.sleep(np.random.random() * wait_t) # Wait a bit so that we're not ddosing op.gg
    res = cd_req(opgg_cd, opgg_last_req_t, req_check, opgg_serv+url, **kwargs)
    if not res[0]:
        res[0] = {"data": res[0]}
    return res[0]["data"]

def opgg_res(res):              # Remove hyperlinks
    if res is None:
        return {}
    if isinstance(res, list):
        return [opgg_res(a) for a in res]
    for key in list(res.keys()):
        if isinstance(res[key], dict) or isinstance(res[key], list):
            res[key] = opgg_res(res[key])
        elif isinstance(res[key], str) and res[key][:2] == "//":
            del res[key]
    return res

def opgg_renew_summoner(region, summoner_id, wait_t=opgg_max_wait):
    # print("RENEW: " + str(summoner_id), file=sys.stderr)
    if wait_t > 0:
        time.sleep(np.random.random() * wait_t) # Wait a bit so that we're not ddosing op.gg
    cd_req(opgg_cd, opgg_last_req_t, req,
           opgg_serv + region + "/renew", params={g_summ_id: summoner_id})

def opgg_ranked_summary(region, summoner, **kwargs):
    # This method returns nothing if no recent games (player hasn't played in ages) - use non-ranked instead
    # return opgg_res(opgg_req(region + "/summary/" + summoner + "/ranked", **kwargs))
    # if summoner == "pun1sher":
    #     summoner =  "Pun1sher"
    # if summoner == "ΜarkusRosenberg":
    #     summoner = "ΜarkusRosenberg"
    # if summoner == "ΜarkusRosenberg":
    #     summoner = "ΜarkusRosenberg"
    res = opgg_res(opgg_req(region + "/summary/" + summoner + "/ranked", **kwargs))
    if res == {} or res is None:
        res = opgg_res(opgg_req(region + "/summary/" + summoner, **kwargs))
        if "games" in res and len(res["games"]) > 0:
            res["games"] = [g for g in res["games"] if g['type'] == "Flex 5:5 Rank" or g['type'] == "Ranked Solo"]
    return res



def opgg_player_champ_stats(region, season_id, summoner="gRumbling", summoner_id=None):
    res = None
    if summoner_id is not None:
        res = opgg_req(region + "/championsSummId/" + str(summoner_id),
                              params={"season": season_id})
    else:
        res = opgg_req(region + "/champions/" + summoner,
                              params={"season": season_id})
    if res is None:
        res = {}
    # if season == 8:
    #     if res != {}:
    #         pr_fl(res)
    return res
    # res = []
    # for season in range(1, curr_season_id + 1):
    #     res += [ opgg_req(region + "/champions/" + summoner,
    #                       params={"season": season}) ]
    #     if res[-1] is None:
    #         res[-1] = {}
    #     if season == 8:
    #         if res[-1] != {}:
    #             pr_fl(res[-1])
    # return res

def opgg_global_champ_stats_():
    pr_fl("Downloading op.gg global champion stats")
    res = []
    for region in opgg_regions:
        res += [[]]
        for elo in opgg_elos:
            res[-1] += [[]]
            for period in opgg_periods:
                res[-1][-1] += [[]]
                for typ in opgg_types:
                    if typ == 'picked' or typ == 'banned':
                        elo = 'all'
                        continue # Picked/banned not needed/working
                    res[-1][-1][-1] += [ opgg_req(region + "/stats",
                                     params={"type": typ,
                                             "league": elo,
                                             "period": period,
                                             "mapId": opgg_mapId,
                                             "queue": opgg_queue}) ]
                    if res[-1][-1][-1] is None:
                        sys.exit("Failed to get opgg global champion stats (null for" + \
                            str((region, elo, period, typ)))
    return res

def opgg_global_champ_stats(r_conds):
    with r_conds['or']:
        return cache_req("opgg_stats", opgg_global_champ_stats_, True)


# Champion.gg requests

# Webpage javascript extraction

chgg_roletitles = {
    'Top': 'TOP',
    'Jungle': 'JUNGLE',
    'Middle': 'MIDDLE',
    'ADC': 'DUO_CARRY',
    'Support': 'DUO_SUPPORT',
    'synergy': 'SYNERGY',
    'adcsupport': 'ADCSUPPORT',
}
blitz_roledict = {
    "TOP": "TOP",
    "JUNGLE": "JUNGLE",
    "MIDDLE": "MID",
    "DUO_CARRY": "ADC",
    "DUO_SUPPORT": "SUPPORT",
}
chgg_roletitles_inv = invert_dict(chgg_roletitles)
chgg_roles_inv = invert_dict(chgg_roles)

def chgg_champion_js_(r_tss, elo, cid):
    role_flags = []
    chn = js_ch_name(cid)
    if '&' in chn:
        chn = "Nunu"
    ch_str = "champion/" + chn + '/'
    elo_str = ('?league=' + elo.lower().replace('platinum', 'plat')) if elo != '' else ''
    res = []

    # First, don't specify role (load roles for which there is data)
    # unless champion is Yuumi, in which case set as Support
    url = cwgg_serv + ch_str + ("Support" if chn == "Yuumi" else '') + elo_str

    n_tries = 0
    text = ""
    status_code = 200
    while "matchupData.champion" not in text and n_tries < max_n_retries:
        n_tries += 1
        if n_tries > 1:
            time.sleep(2)
        try:
            page = cd_req(cwgg_cd, r_tss['cw'], req, url, decode_json=False)
            # pr_fl(url, type(page), page.status_code, page.reason, page.encoding)
            if "Teemo, we have a problem" in text:
                return "flag"
            if 'status_code' not in page:
                return "flag"
            if page.status_code != 200:
                status_code = page.status_code
                pr_fl("Request error:", url, status_code)
                text = ''
            else:
                text = page.text
        except Exception as e:
            pr_fl("Request error:", type(e), url)
            pr_fl(e.args)
            pr_fl(e)
        # pr_fl(text)

    if n_tries == max_n_retries:
        pr_fl("Request error:", url, status_code)
        pr_fl("Subbing for", chn, elo)
        return "flag"

    if '<div class="analysis-holder">' not in text:
        pr_fl("PATCH NOT FOUND ON PAGE,", url, status_code)
        pr_fl("Subbing for", chn, elo)
        return "flag"
    else:
        patch = text.split('<div class="analysis-holder">')[1].split("Patch")[1].split("<strong>")[1].split('</strong')[0]
    d = translate_chgg_cj(text)
    d_roles = d["matchupData"]['champion']['roles']
    roles = [d_['role'] for d_ in d_roles]
    games_played = [d_['games'] for d_ in d_roles]
    perc_role_played = [d_['percentPlayed'] for d_ in d_roles]

    for i in range(len(roles)):
        r = roles[i]
        if i != 0:
            url = cwgg_serv + ch_str + chgg_roletitles_inv[r] + elo_str
            n_tries = 0
            text = ""
            status_code = 200
            skip_role = False
            while "matchupData.champion" not in text and n_tries < max_n_retries:
                n_tries += 1
                if n_tries > 1:
                    time.sleep(1)
                try:
                    page = cd_req(cwgg_cd, r_tss['cw'], req, url, decode_json=False)
                    text = page.text
                    # pr_fl(url, type(page), page.status_code, page.reason, page.encoding)
                    if "Teemo, we have a problem" in text:
                        skip_role = True
                        break
                    if 'status_code' not in page:
                        skip_role = True
                        break
                    if page.status_code != 200:
                        status_code = page.status_code
                        pr_fl("Request error:", url, status_code)
                        text = ''
                except Exception as e:
                    pr_fl("Request error:", type(e), url)
                    pr_fl(e.args)
                    pr_fl(e)
                # pr_fl(text)

            if skip_role == True:
                pr_fl("Teemo subbing role", r, "for", chn)
                role_flags.append(r)
                continue

            if n_tries == max_n_retries:
                pr_fl("Request error:", url, status_code)
                pr_fl("Subbing role", r, "for", chn)
                role_flags.append(r)
                continue

            d = translate_chgg_cj(text)

        if '<div class="analysis-holder">' not in text:
            pr_fl("PATCH NOT FOUND ON PAGE,", url)
            continue
        else:
            patch = text.split('<div class="analysis-holder">')[1].split("Patch")[1].split("<strong>")[1].split('</strong>')[0]

        # Request extra data from blitz (mainly wards placed & killed)
        blitz_url = "https://beta.iesdev.com/api/lolstats/champions/" + str(cid) + \
            "?patch=" + patches_all[0] + "&tier=" + ("PLATINUM_PLUS" if elo == '' else elo.upper()) + \
            "&region=world&queue=420&role=" + blitz_roledict[r]
        # pr_fl(blitz_url)
        blitz_d = {}
        n_tries = 0
        while ("data" not in blitz_d or len(blitz_d["data"]) == 0) and n_tries < 5:
            n_tries += 1
            if n_tries > 1:
                time.sleep(1)
            try:
                blitz_d = cd_req(blitz_cd, r_tss['bz'], req, blitz_url)
            except Exception as e:
                pr_fl("Request error (Blitz):", type(e), url)
                pr_fl(e.args)
                pr_fl(e)
            if blitz_d is None:
                blitz_d = {}

        if n_tries == 5:
            pr_fl("Request error (Blitz):", blitz_url)
            pr_fl("Subbing role", r, "for", chn)
            role_flags.append(r)
            continue

        # pr(blitz_d)
        blitz_d = blitz_d['data'][0]['stats']
        count = blitz_d["games"]
        neutralMinionsKilledTeamJungle = blitz_d["neutralMinionsKilledTeamJungle"]
        neutralMinionsKilled = blitz_d["neutralMinionsKilled"]
        neutralMinionsKilledEnemyJungle = neutralMinionsKilled - neutralMinionsKilledTeamJungle
        neutralMinionsKilledTeamJungle /= count
        neutralMinionsKilledEnemyJungle /= count
        wardsPlaced = blitz_d["wardsPlaced"] / count
        wardsKilled = blitz_d["wardsKilled"] / count
        visionWardsBoughtInGame = blitz_d["visionWardsBoughtInGame"] / count
        totalHeal = blitz_d["totalHeal"] / count

        d_ = d["matchupData"]
        c  = d_['championData']
        sts = c['stats']
        total_dealt = sts["totalDamageDealt"]
        del sts["totalDamageDealt"]

        dcomp = c["dmgComposition"]
        m, p, t = float(dcomp['magicDmg']), float(dcomp['physicalDmg']), float(dcomp['trueDmg'])
        dmg_comp = {
            'percentMagical': m,
            'percentPhysical': p,
            'percentTrue': t,
            'total': total_dealt,
            'totalMagical': total_dealt * (m / 100),
            'totalPhysical': total_dealt * (p / 100),
            'totalTrue': total_dealt * (t / 100),
        }

        gl = c["gameLength"]
        winsByMatchLength = {
            'zeroToFifteen': {'count': 0, 'winRate': 0, 'wins': 0},
            'fifteenToTwenty': {'count': 0, 'winRate': 0, 'wins': 0},
            'twentyToTwentyFive': {'count': 0, 'winRate': float(gl[0]), 'wins': 0},
            'twentyFiveToThirty': {'count': 0, 'winRate': float(gl[1]), 'wins': 0},
            'thirtyToThirtyFive': {'count': 0, 'winRate': float(gl[2]), 'wins': 0},
            'thirtyFiveToForty': {'count': 0, 'winRate': float(gl[3]), 'wins': 0},
            'fortyPlus': {'count': 0, 'winRate': float(gl[4]), 'wins': 0},
        }

        gp = c["experienceRate"]
        gpc = c["experienceSample"]
        winsByGamesPlayed = {
            "oneToFifty": {"gamesPlayed": -1, "players": gpc[0], "winRate": gp[0], "wins": -1},
            "fiftyOneToHundred": {"gamesPlayed": -1, "players": gpc[1], "winRate": gp[1], "wins": -1},
            "hundredOneToHundredFifty": {"gamesPlayed": -1, "players": gpc[2], "winRate": gp[2], "wins": -1},
            "hundredFiftyOneToTwoHundred": {"gamesPlayed": -1, "players": gpc[3], "winRate": gp[3], "wins": -1},
            "twoHundredOnePlus": {"gamesPlayed": -1, "players": gpc[4], "winRate": gp[4], "wins": -1},
        }

        x = {**sts, **{
            'id_': {'championId': int(cid), 'role': r},
            'patch': patch,
            'role': r,
            'championId': int(cid),
            'elo': elo.upper() if elo != '' else 'PLATINUM,DIAMOND,MASTER,CHALLENGER',
            'gamesPlayed': games_played[i],
            'percentRolePlayed': perc_role_played[i],
            "damageComposition": dmg_comp,
            "neutralMinionsKilledTeamJungle": neutralMinionsKilledTeamJungle,
            "neutralMinionsKilledEnemyJungle": neutralMinionsKilledEnemyJungle,
            "wardPlaced": wardsPlaced,
            "wardsKilled": wardsKilled,
            "visionWardsBoughtInGame": visionWardsBoughtInGame,
            "totalHeal": totalHeal,
            "killingSprees": 1.23,
            "largestKillingSpree": 5,
            "winsByMatchLength": winsByMatchLength,
            "winsByGamesPlayed": winsByGamesPlayed,
        }}

        res += [x]
    return res

def translate_chgg_cj(page):
    payload = page.split("matchupData.champion = ")[1].split('<div class="footer-attr">')[0].strip()
    payload = "matchupData.champion = " + payload
    res = {}
    for l in [l for l in payload.split('\n') if " = " in l]:
        k, v = l.strip().split(' = ')
        k, v = k.strip(), v.strip()
        k1, k2 = k.split('.')
        if k1 not in res:
            res[k1] = {}
        res[k1][k2] = json.loads(v)
    # pr(res)
    return res

def js_ch_name(cid):
    return get_champions()[str(cid)]["name"].replace(' ', '').replace("'", '').replace(".", '')


default_chgg_vals = {
    'neutralMinionsKilledTeamJungle': -1,
    'weighedScore': -1,

    'zeroToTen': 0,
    'tenToTwenty': 0,
    'twentyToThirty': 0,
    'thirtyToEnd': 0,
    
}
chgg_roles_list = [
    'TOP',
    'JUNGLE',
    'MIDDLE',
    'DUO_SUPPORT',
    'DUO_CARRY',
    'SYNERGY',
    'ADCSUPPORT',
]
def chgg_matchup_js_(r_tss, elo, cid, dl_cache, null_set, dlc_cond, champ_db):

    null_set_leng = len(null_set)
    chpn = get_champions()[cid]["name"]
    # fn = "chgg_cache/matchups_p/" + chpn + "_" + elo.lower()
    # if os.path.exists("data/" + fn + ".json"):
    #     return load_json(fn, print_errs=True)

    m_str = "matchup/"
    elo_str = ('?league=' + elo.lower().replace('platinum', 'plat')) if elo != '' else ''
    res = []

    # For each other champion and role, check cache for matchup request. If missing, make HTTP request
    roles = [r["role"] for r in champ_db[cid][chgg_elos.index(elo)]]
    if "DUO_CARRY" in roles or "DUO_SUPPORT" in roles:
        roles += ["SYNERGY", "ADCSUPPORT"]

    champ_dct = {get_champions()[c]["name"]:c for c in get_champions()}
    for role in roles:
        clist = list(get_champions().keys())
        np.random.shuffle(clist)
        for cid_ in clist:
            if cid == cid_:
                continue

            cid, cid_ = int(cid), int(cid_)

            h = (chgg_roles_list.index(role), *((cid, cid_) if cid < cid_ else (cid_, cid)))
            x = None
            if h in null_set:
                continue
            if h in dl_cache:
                x = dl_cache[h]
                if x is None:
                    continue
            else:

                # Download webpage
                c, c_ = h[1:]
                url = cwgg_serv + m_str + js_ch_name(c) + '/' + js_ch_name(c_) + '/' + chgg_roletitles_inv[role] + elo_str
                n_tries = 0
                text = ""
                status_code = 200
                while "matchupData.champion" not in text and n_tries < max_n_retries:
                    n_tries += 1
                    if n_tries > 1:
                        time.sleep(1)
                    try:
                        # with eventlet.Timeout(10):
                        page = cd_req(cwgg_cd, r_tss['cw'], req, url, decode_json=False)
                        text = page.text
                        # pr_fl(url, type(page), page.status_code, page.reason, page.encoding)
                        if "Teemo, we have a problem" in text:
                            # with dlc_cond:
                            # dl_cache[h] = None
                            # null_set.add(h)
                            break
                        if 'status_code' not in page:
                            break
                        if page.status_code != 200:
                            status_code = page.status_code
                            pr_fl("Request error:", h, url, status_code)
                            text = ''
                        else:
                            text = page.text
                    except Exception as e:
                        pr_fl("Request error:", type(e), url)
                        pr_fl(e.args)
                        pr_fl(e)
                    # pr_fl(text)

                if n_tries == max_n_retries:
                    pr_fl("Request error:", h, url, status_code)
                    # with dlc_cond:
                    dl_cache[h] = None
                    null_set.add(h)
                    continue

                # Check if error. If so, skip
                d = text
                if "Teemo, we have a problem" in d:
                    # with dlc_cond:
                    # pr_fl(url, "Teemo'd")
                    dl_cache[h] = None
                    null_set.add(h)
                    continue

                # pr_fl(url)
                if "Total Games Analyzed:" not in d:
                    pr_fl(url, "Missing N games analyzed...")
                    pr_fl(d)
                    pr_fl(url, "Missing N games analyzed...")
                    continue
                games = int(d.split("Total Games Analyzed:")[1].split('</strong>')[0].strip())
                meta_desc = d.split('<meta name="description" content="')[1].split('">')[0].split(' ')
                mi, chn = 0, '?'
                while chn not in champ_dct:
                    mi += 1
                    chn = ' '.join(meta_desc[:mi]).replace('&#39;', "'").replace('&#46;', '.').replace('&#32;', ' ')
                role_1, role_2, = meta_desc[mi], meta_desc[-1]
                if '<div class="analysis-holder">' not in d:
                    pr_fl("PATCH NOT FOUND ON PAGE,", url)
                    continue
                else:
                    patch = d.split('<div class="analysis-holder">')[1].split("Patch")[1].split("<strong>")[1].split('</strong>')[0]

                d = d.split('<table class="table table-striped">')[1]. \
                    split('<disqus disqus-shortname="championgg" disqus-identifier=')[0]

                x = {
                    '_id': {'champ1_id': c, 'champ2_id': c_, 'role': role},
                    'champ1_id': c,
                    'champ2_id': c_,
                    'count': games,
                    'patch': patch,
                    'role': role,
                    'champ1': {**deepcopy(default_chgg_vals), **{
                        'role': chgg_roletitles[role_1],
                    }},
                    'champ2': {**deepcopy(default_chgg_vals), **{
                        'role': chgg_roletitles[role_2],
                    }}
                }

                for name, key, typ in (
                    ["Win Rate", "winrate", float],
                    ["Gold Earned", "goldEarned", int],
                    ["Kills", "kills", float],
                    ["Deaths", "deaths", float],
                    ["Assists", "assists", float],
                    ["Damage Dealt", "totalDamageDealtToChampions", int],
                    ["Killing Spree", "killingSprees", float],
                    ["Minions Killed", "minionsKilled", int],
                ):
                    vs = d.split('<td class="matchup-title-width">' + name)[1].split('<td class="matchup-values-width">')[1:]
                    v, v_ = vs[0].split('</td>')[0], vs[2].split('</td>')[0]

                    if key == "winrate":
                        v, v_ = v[:-1], v_[:-1]

                    x["champ1"][key] = typ(float(v))
                    x["champ2"][key] = typ(float(v_))

                # with dlc_cond:
                dl_cache[h] = x
            res += [x]
    # create_folder(data_dir + "chgg_cache/matchups_p")
    # save_json(res, fn)
    # if len(null_set) > null_set_leng:
    #     save_json(list(null_set), "chgg_cache/matchups_p/null_set")
    pr_fl("Downloaded matchup stats for " + chpn + " in " + elo.lower())
    return res

# API requests

def chgg_req(r_ts, url, **kwargs):
    kwargs["params"]["api_key"] = chgg_key
    return cd_req(chgg_cd, r_ts, req, chgg_serv + url, **kwargs)

def chgg_champions_(r_tss):
    res = {}
    use_js = False
    # for cid in ["523"]:
    for cid in get_champions().keys():
        # use_js = False
        res[cid] = []
        for elo in chgg_elos[::-1]:
            # if elo == "IRON":
            #     elo = "BRONZE"
            # elif elo == "BRONZE":
            #     res[cid] += [ res[cid][-1] ]
            #     continue
            if elo == "IRON":
                res[cid] += [ res[cid][-1] ]
                continue

            r = None
            if not use_js:
                r = chgg_req(r_tss['cr'], "champions/" + cid,
                    params={
                        "elo": elo,   # All champData except matchups
                        "champData": "kda,damage,minions,wins,goldEarned," + \
                        "hashes,sprees,wards,totalHeal"#," + \
                        # "overallPerformanceScore,positions,normalized" + \
                        # "averageGames,maxMins",
                    })

                if r is None:
                    use_js = True
                    pr_fl("api.champion.gg champion request failed, using champion.gg webpage javascript...")

            if use_js:
                r = chgg_champion_js_(r_tss, elo, cid)
                if r == "flag":
                    load_cache("chgg_champions", False, only_new=False)
                    if cid not in r_cache["chgg_champions"]["data"]:
                        r = res[cid][-1]
                    else:
                        r = r_cache["chgg_champions"]["data"][cid][chgg_elos.index(elo)]

            res[cid] += [ r ]
        res[cid] = res[cid][::-1]
        pr_fl("Downloaded Champion.gg champion stats for " + \
                get_champions()[cid]["name"])
    return res

def chgg_champions(r_conds, r_tss):
    res = None 
    with r_conds['cr']:
        res = cache_req("chgg_champions", chgg_champions_, False, r_tss)
    return res

def chgg_matchups_(r_tss, champ_db):
    dl_cache = {}
    null_set = set()

    # if os.path.exists("data/chgg_cache/matchups_p/null_set.json"):
    #     null_set = load_json("chgg_cache/matchups_p/null_set", print_errs=True)
    #     null_set = set([tuple(h) for h in null_set])

    # use_js = False
    use_js = True
    # res = {}
    pr_fl("Downloading Champion.gg matchup stats...")
    cids = tuple(zip(*sorted([(get_champions()[c]["name"], c) for c in get_champions()] )))[1]
    # cids = ["235", "875", "523"]
    # cids = ["875"]

    jobs = sum([[(elo, cid) for elo in chgg_elos[1:]] for cid in cids], [])

    if champ_db is None:
        pr_fl("champ_db is None")
    for cid in get_champions():
        if champ_db[cid] is None:
            pr_fl("CID " + str(cid) + ", " + get_champions()[cid]["name"] + " is missing")
            sys.exit()
    dlc_cond = Condition()

    with ThreadPoolExecutor(max_workers=n_chgg_reqpool_workers) as executor:
        res_futs = [executor.submit(chgg_matchup_inner_,
            r_tss, elo, cid, dl_cache, null_set, dlc_cond, champ_db, use_js=use_js) for elo, cid in jobs]
        r = [fut.result() for fut in res_futs]

        res = {cids[i]: r[i * len(chgg_elos[1:]):(i + 1) * len(chgg_elos[1:])] for i in range(len(cids))}
        for key in res: # IRON = BRONZE
            res[key] = res[key][:1] + res[key]
        pr_fl("Downloaded Champion.gg matchup stats")
        return res

def chgg_matchup_inner_(r_tss, elo, cid, dl_cache, null_set, dlc_cond, champ_db, use_js=False):
    # if elo == "IRON":
    #     elo = "BRONZE"
    # elif elo == "BRONZE":
    #     res[cid] += [ res[cid][-1] ]
    #     continue

    r = None
    if not use_js:
        r = chgg_req(r_tss['cr'], "champions/" + cid + "/matchups",
                     params={"limit": 99999999999,
                             "elo": elo,
            })

        if r is None:
            use_js = True
            pr_fl("api.champion.gg matchups request failed, using champion.gg webpage javascript...")
            # return None

    if use_js:
        r = chgg_matchup_js_(r_tss, elo, cid, dl_cache, null_set, dlc_cond, champ_db)

    return r

def chgg_matchups(r_conds, r_tss, champ_db):
    res = None 
    with r_conds['cr']:
        res = cache_req("chgg_matchups", chgg_matchups_, True, r_tss, champ_db)
    return res




# Per-role version of chgg_matchups:

# def chgg_matchups_():
#     res = {}
#     for cid in get_champions().keys():
#         res[cid] = {}
#         pr_fl(get_champions()[cid]["name"])
#         for role in chgg_roles:
#             res[cid][role] = []
#             for elo in chgg_elos:
#                 res[cid][role] += [
#                     chgg_req("champions/" + cid + '/' + role + "/matchups",
#                              params={"limit": 5,
#                                      "elo": elo,
#                     }) ]
#         # pr_fl(len(res[cid][0]))
#         pr_fl("Downloaded matchups for " + \
                # get_champions()[cid]["name"])
#     return res



# Trigger the upload of the new global stats database (and last trained model), and
# trigger the training of the next model
def trigger_gsdb_upload():

    # if False:
    if True:

        # Generate role orderings
        res = subprocess.run(pyex + " gen_role_orderings.py", stdout=subprocess.PIPE, shell=True)
        res = res.stdout.decode('utf-8')

        # Generate champion list (for front end)
        res = subprocess.run(pyex + " gen_champ_list.py", stdout=subprocess.PIPE, shell=True)
        res = res.stdout.decode('utf-8')

        # Trigger new data upload and new model training
        # subprocess.Popen(pyex + " train_model.py", shell=True, close_fds=True, creationflags=DETACHED_PROCESS)
        # time.sleep(3 * 60)  # Wait 3 minutes for the global stats database to be loaded
        # subprocess.Popen(pyex + " upload_gsdb.py", shell=True, close_fds=True, creationflags=DETACHED_PROCESS)




# Test stuff
# load_caches()
# load_cache("chgg_champions", True)
# chgg_champions({'cr': Condition()}, {'cr': {'ts': 0}})


# Set champions global variables
get_champions()
# print(cdata_)


