#
#  Print failed chat log pastes
#


from Server import *


db = sqlalchemy.create_engine(pg_conn_str)
pgdb_cond = Condition()

engine, meta, clfails = connect_pg_clf(db, pgdb_cond)
query = sqlalchemy.sql.expression.select([clfails.c[k] \
    for k in ['source', 'chat_log']])
entry = list(pg_exec(engine, query))
engine.close()
db.dispose()

pr(entry)


