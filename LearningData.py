#
#  Learning data utilities
#

import scipy

from sklearn.preprocessing import *


from LearningConstants import *


_elem_0 = lambda x: x[0]

# Load all global op.gg and champion.gg champion and matchups stats
op_gcs, ch_gcs, ch_gms = None, None, None
# def load_global_stats(n_load=n_gsdb_load, by_patches=False, n_patches=np.inf, oldest_patch=-np.inf, load_max=9999):
def load_global_stats(n_load=n_gsdb_load, by_patches=True, n_patches=n_gsdb_patches,
    oldest_patch=-np.inf, load_max=9999, del_old=False):
    global op_gcs, ch_gcs, ch_gms
    if op_gcs is not None:
        return op_gcs, ch_gcs, ch_gms

    pr_fl("Loading global champion & matchup stats databases...")
    oc = sorted(glob.glob(data_dir + r_cache["opgg_stats"]["path"]  +  "/*.json"))[::-1]#[:n_load]
    cc_ = sorted(glob.glob(data_dir + r_cache["chgg_champions"]["path"] + "/*.json"))[::-1]
    cm_ = sorted(glob.glob(data_dir + r_cache["chgg_matchups"]["path"]  + "/*.json"))[::-1]
    cc_df = [deglob_fn(fn, '.json') for fn in cc_]
    cm_df = [deglob_fn(fn, '.json') for fn in cm_]

    old_oc = oc[n_load:]
    oc = oc[:n_load]
    old_cc, old_cm = [], []

    if by_patches:
        cc, cm = [], []
        cc_patches, cm_patches = [], []
        for i in range(len(cc_df)):
            fn = cc_df[i]
            patch = patch_s2n(fn.split('_')[1])
            if patch < oldest_patch:
                old_cc += cc_[i:]
                break
            if patch not in cc_patches:
                cc_patches.append(patch)
                cc.append(cc_[cc_df.index(fn)])
                if len(cc) >= min(n_patches, load_max):
                    old_cc += cc_[i + 1:]
                    break
                continue
            old_cc.append(cc_[i])

        for i in range(len(cm_df)):
            fn = cm_df[i]
            patch = patch_s2n(fn.split('_')[1])
            if patch < oldest_patch:
                old_cm += cm_[i:]
                break
            if patch not in cm_patches:
                cm_patches.append(patch)
                cm.append(cm_[cm_df.index(fn)])
                if len(cm) >= min(n_patches, load_max):
                    old_cm += cm_[i + 1:]
                    break
                continue
            old_cm.append(cm_[i])

    else:
        cc = cc_
        cm = cm_

    if del_old:  # Delete older or redundant stats
        for fn in old_oc + old_cc + old_cm:
            if os.path.exists(fn):
                os.remove(fn)

    op_gcs_libs = [load_json(path, pad=False, print_errs=True) for path in oc]
    ch_gcs_libs = [load_json(path, pad=False, print_errs=True) for path in cc]
    ch_gms_libs = [load_json(path, pad=False, print_errs=True) for path in cm]

    op_gcs_tss = [int(deglob_fn(path, '.json').split('_')[0]) for path in oc]
    ch_gcs_tss = [int(deglob_fn(path, '.json').split('_')[0]) for path in cc]
    ch_gms_tss = [int(deglob_fn(path, '.json').split('_')[0]) for path in cm]
    op_gcs, ch_gcs, ch_gms = [OrderedDict() for _ in range(3)]

    for i in range(len(oc)):  # op.gg Global champion stats
        op_gcs_lib = op_gcs_libs[i]
        # old_euw_only_data = False
        # if len(op_gcs_lib) != len(opgg_regions):
        #     old_euw_only_data = True
        op_gcs_i = {}

        if len(op_gcs_lib) > len(opgg_regions):
            id_ind = opgg_regions_old.index('id')
            op_gcs_lib = op_gcs_lib[:id_ind] + op_gcs_lib[id_ind + 1:]

        for r in range(len(op_gcs_lib)):
            # if old_euw_only_data:
            #     if r != 0:
            #         break
            #     op_gcs_r = op_gcs_lib
            #     reg = 'euw'
            # else:
            op_gcs_r = op_gcs_lib[r]
            reg = opgg_regions[r]
            op_gcs_i[reg] = []

            # Correct pre-Iron data
            if len(op_gcs_r) < len(opgg_elos):
                op_gcs_r = op_gcs_r[:1] + [op_gcs_r[1]] + op_gcs_r[1:-1] + [op_gcs_r[-2]] + op_gcs_r[-1:]

            for e in range(len(opgg_elos)):
                op_gcs_e = op_gcs_r[e]
                op_gcs_i[reg].append({})
                for p in range(len(opgg_periods)):
                    op_gcs_p = op_gcs_e[p]
                    per = opgg_periods[p]
                    op_gcs_i[reg][-1][per] = {}
                    for t in range(len(opgg_types)):
                        op_gcs_t = op_gcs_p[t]
                        typ = opgg_types[t]
                        op_gcs_i[reg][-1][per][typ] = {}
                        # if len(op_gcs_t) == 0:
                        #     pr_fl(i, oc[i], opgg_regions[r], opgg_elos[e], \
                        #             opgg_periods[p], opgg_types[t])
                        #     continue
                        # if type(op_gcs_t[0]) is list: # Weird bug?
                        #     pr_fl(i, oc[i], opgg_regions[r], opgg_elos[e], \
                        #         opgg_periods[p], opgg_types[t], c, \
                        #         len(op_gcs_t), len(op_gcs_t[0]))
                        #         # pr_fl(op_gcs_c)
                        #     op_gcs_t = op_gcs_t[0]
                        cis, counts = [], []
                        for c in range(len(op_gcs_t)):
                            op_gcs_c = op_gcs_t[c]
                            counts.append(op_gcs_c['games'])
                            op_name = op_gcs_c['name']
                            if op_name == "Nunu":
                                op_name = "Nunu & Willump"
                            # if op_name == "Yuumi":
                            #     op_name = "Ezreal"
                            cid = champ_dict[op_name]
                            cis.append(cid)
                            op_gcs_i[reg][-1][per][typ][cid] = op_gcs_c
                        s = sum(counts)
                        for c in range(len(cis)):
                            op_gcs_i[reg][-1][per][typ][cis[c]]['gamesFrac']=\
                                counts[c] / s
                            # Temporary fix for missing kda values
                            if op_gcs_i[reg][-1][per][typ][cis[c]]['kda'] is None:
                                op_gcs_i[reg][-1][per][typ][cis[c]]['kda'] = 2.5
        op_gcs[op_gcs_tss[i]] = op_gcs_i

    for i in range(len(cc)):  # Champion.gg Global champion stats
        ch_gcs_lib = ch_gcs_libs[i]
        ch_gcs_i = {}
        for str_cid in ch_gcs_lib:
            ch_gcs_c = ch_gcs_lib[str_cid]
            cid = int(str_cid)
            ch_gcs_i[cid] = []

            # Correct pre-Iron data
            if len(ch_gcs_c) < len(chgg_elos):
                ch_gcs_c = [ch_gcs_c[0]] + ch_gcs_c

            for e in range(len(chgg_elos)):
                ch_gcs_e = ch_gcs_c[e]
                ch_gcs_i[cid].append(OrderedDict())
                objs = []
                for r in range(len(ch_gcs_e)):
                    ch = ch_gcs_e[r]
                    if "totalHeal" not in ch:
                        ch["totalHeal"] = 3000
                    if "wardPlaced" not in ch:
                        ch["wardPlaced"] = ch["wardsPlaced"]
                    objs.append((ch["gamesPlayed"],chgg_roles[ch['role']],ch))
                for c, r, o in sorted(objs, key=_elem_0, reverse=True):
                    ch_gcs_i[cid][-1][r] = o
        ch_gcs[ch_gcs_tss[i]] = ch_gcs_i

    # Correct pre-Iron data
    for i in range(len(cm)):
        for str_cid in ch_gms_libs[i]:
            if len(ch_gms_libs[i][str_cid]) < len(chgg_elos):
                ch_gms_libs[i][str_cid] = [ch_gms_libs[i][str_cid][0]] + ch_gms_libs[i][str_cid]

    for i in range(len(cm)):  # Champion.gg Global matchup stats
        ch_gms_lib = ch_gms_libs[i]
        ch_gms_i = {}
        for str_cid in ch_gms_lib:
            ch_gms_c = ch_gms_lib[str_cid]
            cid = int(str_cid)
            ch_gms_i[cid] = []

            for e in range(len(chgg_elos)):
                ch_e = ch_gms_c[e]
                if ch_e is None or len(ch_e) == 0:
                    pr_fl("Subbing cache data: ", cm[i].split('/')[-1], chgg_elos[e], champion_names[int(str_cid)])
                    i_ = i
                    e_ = e if chgg_elos[e] != 'IRON' else chgg_elos.index('BRONZE')
                    rev = False
                    while ch_e is None or len(ch_e) == 0:
                        if i_ >= len(ch_gms_libs):
                            rev = True
                            break
                        ch_e = ch_gms_libs[i_][str_cid][e_]
                        i_ += 1
                    if rev:
                        i_ = i
                        while ch_e is None or len(ch_e) == 0:
                            i_ -= 1
                            if i_ < 0:
                                pr_fl("Fatal error: no matchup stats for", cm[i], chgg_elos[e_], champion_names[int(str_cid)])
                                sys.exit()
                            ch_e = ch_gms_libs[i_][str_cid][e_]

                ch_gms_i[cid].append({})
                res = {}
                for r in range(len(ch_e)):
                    ma = ch_e[r]
                    count = ma['count']
                    align,role=ma["champ1_id"]==str_cid,chgg_roles[ma['role']]
                    u_,o_=("champ1","champ2")if align else("champ2","champ1")
                    us, ot, ot_id = ma[u_], ma[o_], ma[o_ + "_id"]
                    
                    if "wins" not in us:
                        us["wins"] = count * us["winrate"]
                    if "wins" not in ot:
                        ot["wins"] = count * ot["winrate"]
                    
                    if role not in ch_gms_i[cid][-1]:
                        ch_gms_i[cid][-1][role] = OrderedDict()
                        res[role] = []
                    res[role].append((count, ot_id, {'u': us, 'o': ot,
                        "patch": ma['patch'], "count": count}))
                for role in res:
                    for _, o, r in sorted(res[role], key=_elem_0):
                        ch_gms_i[cid][-1][role][o] = r
        ch_gms[ch_gms_tss[i]] = ch_gms_i

    pr_fl("Finished loading global champion & matchup stats databases!")
    # pr_fl(op_gcs_tss, ch_gcs_tss, ch_gms_tss)
    # sys.exit()
    # input()
    return op_gcs, ch_gcs, ch_gms


def get_opgg_gcs(ts, cid, reg, leag, per, typ, gsdb=None, lock=None):
    global op_gcs
    if lock is None:
        lock = Condition()
    with lock:
        op_gcs_d = None
        if gsdb is not None:
            op_gcs_d = gsdb[0]
        if op_gcs_d is None:
            op_gcs_d, _, __ = load_global_stats()
        if ts is None: ts = list(op_gcs_d.keys())[0]

        # if cid == champ_dict["Qiyana"]:
        #     ts = list(op_gcs_d.keys())[0]
        # if cid == champ_dict["Sett"]:
        #     ts = list(op_gcs_d.keys())[0]
        # if cid == champ_dict["Aphelios"]:
        #     ts = list(op_gcs_d.keys())[0]
        # if cid == champ_dict["Senna"]:
        #     ts = list(op_gcs_d.keys())[0]

        if ts not in op_gcs_d:
            ts_list = list(op_gcs_d.keys())
            ts = get_nearest(ts_list, ts, direction='up')

        a_week_ago, a_month_ago = n_weeks_ago(3, t=ts/1000), n_months_ago(3, t=ts/1000)
        latest_ts = ts + (2.0 * 24.0 * 60.0 * 60.0 * 1000.0) # 2 days in the future

        if reg not in op_gcs_d[ts]:
            reg = 'euw'
        lib = op_gcs_d[ts][reg][leag][per][typ]
        if cid not in lib or (lib[cid]["gamesFrac"] > 0 and lib[cid][
                "games"] < n_opgg_gcs_games):

            games_frac = 0.0
            if cid in lib:
                games_frac = lib[cid]["gamesFrac"]

            n_tot = lib[cid]["games"] if cid in lib else 0
            samples = [ lib[cid] ] if cid in lib else []
            l, p = leag, per

            n_iter = -1
            while n_tot < n_opgg_gcs_games:
                n_iter += 1
                if n_iter >= 10000:
                    break

                earliest_ts = 0
                if p == "today":
                    earliest_ts = a_week_ago
                elif p == "week":
                    earliest_ts = a_month_ago

                for t in op_gcs_d:
                    if t == ts: continue
                    if t < earliest_ts or t > latest_ts: continue
                    if reg not in op_gcs_d[t]: continue
                    lib_ = op_gcs_d[t][reg][l][p][typ]
                    if cid in lib_:
                        samples.append(lib_[cid])
                        n_tot += lib_[cid]['games']
                    if n_tot >= n_opgg_gcs_games:
                        break
                if n_tot >= n_opgg_gcs_games:
                    break

                if p == "today":
                    p = "week"
                elif p == "week":
                    p = "month"
                else:
                    if l == 0:
                        reg = "euw"
                        l = leag
                    l -= 1
                    p = per

            if samples == []:
                pr_fl("\nOPGG NO SAMPLES - SKIPPING:", reg, opgg_elos[leag],
                    datetime.utcfromtimestamp(ts / 1000).strftime('%Y-%m-%d %H:%M:%S'),
                    champion_names[cid], per, typ)

            # if samples == []:
            #     samples.append(op_gcs_d[ts][reg][2]["month"][typ][champ_dict["Ezreal"]])

            res = {}
            for k in samples[0]:
                if k == 'name' or k == "gamesFrac":
                    continue
                res[k] = np.mean([s[k] for s in samples if s[k] is not None])
            res["gamesFrac"] = games_frac
            if lock != None:
                lock.acquire()
                lib[cid] = res
                lock.release()
            return res

        else:
            return lib[cid]


def get_chgg_gcs(ts, cid, leag, role, gsdb=None, lock=None):
    global ch_gcs
    if lock is None:
        lock = Condition()
    with lock:
        ch_gcs_d = None
        if gsdb is not None:
            ch_gcs_d = gsdb[1]
        if ch_gcs_d is None:
            _, ch_gcs_d, __ = load_global_stats()
        if ts is None: ts = list(ch_gcs_d.keys())[0]
        if ts not in ch_gcs_d:
            ts_list = list(ch_gcs_d.keys())
            ts = get_nearest(ts_list, ts, direction='up')

        if leag > platinum_i:
            leag = len(chgg_elos) - 1
        else:
            leag -= 1


        # if cid not in ch_gcs_d[ts] or not list(ch_gcs_d[ts][cid][leag_tier_is["silver"]].keys()):
        #     cid = champ_dict["Zoe"]

        if cid not in ch_gcs_d[ts]:
            # pr_fl("Champion 404:", champion_names[int(cid)], role, leag, ts)
            if champion_names[int(cid)] == "Qiyana" and ts < 1569276712979:
                ts = 1569276712979
            if champion_names[int(cid)] == "Aphelios" or champion_names[int(cid)] == "Sett" or champion_names[int(cid)] == "Senna":
                # pr_fl(list(ch_gcs_d.keys()))
                # ts = 1579890373737
                ts = list(ch_gcs_d.keys())[0]

        lib = lib_ = ch_gcs_d[ts][cid][leag]
        if role is None:
            roles = list(lib.keys())
            while not roles:
                # pr_fl(ts, cid, leag, roles)
                leag -= 1
                lib = lib_ = ch_gcs_d[ts][cid][leag]
                roles = list(lib.keys())
            role = roles[0]
        new_res, rol, res = False, role, None
        t, leag_ = ts, leag
        if role not in lib or lib[role] is None:
            i = 0
            t_i = 0
            while rol not in lib_ or lib_[rol] is None:
                # if role not in chgg_role_pref_transitions:
                    # break
                rol = chgg_role_pref_transitions[role][i]
                if rol in lib_ and lib_[rol] is not None:
                    break
                i += 1
                if i >= len(chgg_role_pref_transitions[role]):
                    rol, i = role, 0
                    if t_i >= len(ch_gcs_d.keys()):
                        t_i = 0
                        leag_ -= 1
                        # if leag_ < 0:
                        #     leag_ = len(chgg_elos) - 1
                    t = list(ch_gcs_d.keys())[t_i]
                    t_i += 1
                    if t == ts and leag == leag_:
                        if t_i >= len(ch_gcs_d.keys()):
                            t_i = 0
                            leag_ -= 1
                            # if leag_ < 0:
                            #     leag_ = len(chgg_elos) - 1
                        t = list(ch_gcs_d.keys())[t_i]
                        t_i += 1
                    lib_ = ch_gcs_d[t][cid][leag_]
            res = lib_[rol]
            new_res = True
        else:
            res = lib[role]

        if "ml_games" not in res:
            new_res, mp = True, None

            ml = res["winsByMatchLength"]
        #     if "winsByMatchesPlayed" not in res:
        #         pop_role = list(lib.keys())[0]
        #         pop_role_lib = lib[pop_role]
        #         t_i, leag_ = 0, leag
        #         while "winsByMatchesPlayed" not in pop_role_lib:
        #             if t_i >= len(ch_gcs_d.keys()):
        #                 t_i = 0
        #                 leag_ -= 1
        #                 if leag_ < 0:
        #                     leag_ = len(chgg_elos) - 1
        #                 r_lib = ch_gcs_d[ts][cid][leag_]
        #                 if role in r_lib:
        #                     role_lib = r_lib[role]
        #                     if "winsByMatchesPlayed" in role_lib:
        #                         pop_role_lib = role_lib
        #                         break
        #             t = list(ch_gcs_d.keys())[t_i]
        #             r_lib = ch_gcs_d[t][cid][leag_]
        #             if role in r_lib:
        #                 role_lib = r_lib[role]
        #                 if "winsByMatchesPlayed" in role_lib:
        #                     pop_role_lib = role_lib
        #                     break
        #             t_i += 1
        #             if t == ts and leag_ == leag:
        #                 continue
        #             pr_fl(t, cid, leag, leag_)
        #             pr_fl(len(list(ch_gcs_d[t][cid][leag_].keys())))
        #             pop_role = list(ch_gcs_d[t][cid][leag_].keys())[0]
        #             pop_role_lib = ch_gcs_d[t][cid][leag_][pop_role]
        #         mp = pop_role_lib["winsByMatchesPlayed"]
        #         res["winsByMatchesPlayed"] = mp
        #     else:
        #         mp = res["winsByMatchesPlayed"]
        #     res["mp_games"]=sum([mp[k]["gamesPlayed"] for k in mp_dict.values()])
        #     res["mp_players"] = sum([mp[k]["players"] for k in mp_dict.values()])

            if ml["twentyToTwentyFive"]['count'] == 0:
                t_idx = ts_list.index(t)
                if t / 1000 <= patch_tss['9.15']:  # Before patch 9.15/16, we didn't have these stats for Qiyana
                    ts_list = list(ch_gcs_d.keys())
                    t_ = get_nearest(ts_list, int(patch_tss['9.15']) * 1000, direction='up')
                    t_idx = ts_list.index(t_)
                t_ = ts_list[t_idx]
                # This is because we have never seen wbml count values for Yuumi because api.champion.gg went down before patch 9.10
                if cid == champ_dict["Yuumi"] and cid not in ch_gcs_d[t_]:
                    cid = champ_dict["Sona"]
                # This is because we have never seen wbml count values for Qiyana because api.champion.gg went down before patch 9.13
                if cid == champ_dict["Qiyana"] and cid not in ch_gcs_d[t_]:
                    cid = champ_dict["Yasuo"]
                if rol not in ch_gcs_d[t_][cid][leag_]:
                    rol = list(ch_gcs_d[t_][cid][leag_].keys())[0]
                new_ml = ch_gcs_d[t_][cid][leag_][rol]["winsByMatchLength"]
                for k in ml_dict.values():
                    count = new_ml[k]['count']
                    ml[k]['count'] = count
                    ml[k]['wins'] = ml[k]['winRate'] * count

            res["ml_games"] = sum([ml[k]["count"] for k in ml_dict.values()])

        if new_res:
            lib[role] = res

        return res


def get_chgg_gms(ts, cid, leag, role, cid_, gsdb=None, lock=None):
    global ch_gms
    if lock is None:
        lock = Condition()
    with lock:
        ch_gms_d = None
        if gsdb is not None:
            ch_gms_d = gsdb[2]
        if ch_gms_d is None:
            _, __, ch_gms_d = load_global_stats()
        if ts is None: ts = list(ch_gms_d.keys())[0]
        if ts not in ch_gms_d:
            ts_list = list(ch_gms_d.keys())
            ts = get_nearest(ts_list, ts, direction='up')

        if leag > platinum_i:
            leag = len(chgg_elos) - 1
        else:
            leag -= 1


        if cid not in ch_gms_d[ts]:
            # pr_fl("Matchup champion 404:", champion_names[int(cid)], role, leag, ts)
            if champion_names[int(cid)] == "Qiyana" and ts < 1569279970378:
                ts = 1569279970378
            if champion_names[int(cid)] == "Aphelios" or champion_names[int(cid)] == "Sett" or champion_names[int(cid)] == "Senna":
                # ts = 1579941680449
                ts = list(ch_gms_d.keys())[0]
        # if cid not in ch_gms_d[ts] or not list(ch_gms_d[ts][cid][leag_tier_is["silver"]].keys()):
        #     cid = champ_dict["Zoe"]


        lib_ = lib = ch_gms_d[ts][cid][leag]
        rol, leag_, t = role, leag, ts
        res = None
        n_iter = -1
        while rol not in lib_:
            n_iter += 1
            if n_iter >= 10000:
                break
            for t in ch_gms_d:
                if t == ts: continue
                if cid not in ch_gms_d[t]: continue 
                lib_ = ch_gms_d[t][cid][leag_]
                if rol in lib_:
                    break
            if rol in lib_:
                break
            leag_ -= 1
            if leag_ < 0:
                leag_ = len(chgg_elos) - 1
            if leag_ == leag:
                i = 0
                t_i = 0
                while rol not in lib_ or lib_[rol] is None:
                    # if role not in chgg_role_pref_transitions:
                        # break
                    rol = chgg_role_pref_transitions[role][i]
                    if rol in lib_ and lib_[rol] is not None:
                        break
                    i += 1
                    if i >= len(chgg_role_pref_transitions[role]):
                        rol, i = role, 0
                        if t_i >= len(ch_gms_d.keys()):
                            t_i = 0
                            leag_ -= 1
                            if leag_ < 0:
                                leag_ = len(chgg_elos) - 1
                        t = list(ch_gms_d.keys())[t_i]
                        t_i += 1
                        if t == ts and leag == leag_:
                            if t_i >= len(ch_gms_d.keys()):
                                t_i = 0
                                leag_ -= 1
                                if leag_ < 0:
                                    leag_ = len(chgg_elos) - 1
                            t = list(ch_gms_d.keys())[t_i]
                            t_i += 1
                        orig_t = t
                        # while cid not in ch_gms_d[t]:
                        #     t = list(ch_gms_d.keys())[t_i]
                        #     t_i += 1
                        #     if t_i >= len(ch_gms_d.keys()):
                        #         t_i = 0
                        #     if t == orig_t:
                        #         break
                        # if leag_ > len(ch_gms_d[t][cid]):
                        #     pr_fl(champion_names[cid], role, chgg_elos[leag_], t)
                        # if leag_ < 0:
                        #     pr_fl(champion_names[cid], role, leag, leag_, t)
                        if cid not in ch_gms_d[t]:
                            pr_fl(champion_names[int(cid)], role, rol, leag, leag_, ts, t)
                        if cid not in ch_gms_d[t] and champion_names[int(cid)] == "Sett" and \
                                (role == "support" or role == "synergy" or role == "adc_support"):
                            cid = champ_dict["Leona"]
                        lib_ = ch_gms_d[t][cid][leag_]
                if rol in lib_:
                    break
            lib_ = ch_gms_d[ts][cid][leag_]

        if rol not in lib_ or cid_ not in lib_[rol]:
            rol_orig = rol
            if rol == "middle":
                rol = "top"
            elif rol == "top":
                rol = "middle"
            if rol not in lib_ or cid_ not in lib_[rol]:
                rol = rol_orig

                # Take average of n_chgg_gms_cmn rarest matchups for role & cid
                res = {'u': {}, 'o': {}}
                if rol not in lib_:
                    pr_fl("Missing role", rol, "in", chgg_elos[elo], "for", champion_names[cid])
                lib_r = lib_[rol]
                cids = list(lib_r.keys())[:n_chgg_gms_cmn]
                keys = [k for k in lib_r[cids[0]]['o'].keys() if k != 'role']
                res["patch"] = lib_r[cids[0]]["patch"]
                res["count"] = 0
                for k in keys:
                    res['u'][k] = np.mean([lib_r[c]['u'][k] for c in cids])
                    res['o'][k] = np.mean([lib_r[c]['o'][k] for c in cids])

        if res is None:
            res = lib_[rol][cid_]

        if role not in lib:
            lib[role] = {}
        if cid_ not in lib[role]:
            lib[role][cid_] = res

        return res


# Add missing data infill variable (for when there is data)
def add_filler(fl, key, x, role=None, div=None, cid=None):
    if fl["pl_name"] in fl["seen_pl"] or "no_add" in fl and fl["no_add"]:
        return
    if x is None:
        pr_fl(key, role, div, cid)
        return
    fill = fl["fill"]
    if key not in fill:
        fill[key] = {}
    lib = fill[key]

    if role is not None:
        if role not in lib:
            lib[role] = {}
            lib["_name"] = "role"
        lib = lib[role]
    if div is not None:
        if div not in lib:
            lib[div] = {}
            lib["_name"] = "div"
        lib = lib[div]
    if cid is not None:
        if cid not in lib:
            lib[cid] = {}
            lib["_name"] = "cid"
        lib = lib[cid]
    if "_data" not in lib:
        lib["_data"] = 0
        lib["_count"] = 0
    # lib["_data"].append(x)
    count = lib["_count"] + 1
    lib["_data"] = (((count - 1) / count) * lib["_data"]) + (x * (1.0 / count))
    lib["_count"] = count

# Add missing data fill flag (for when data is missing)
def add_flag(fl, k, p_side, p_role,
             role=None, div=None, cid=None, recent_i=None, key=None):
    # if recent_i != None and "opgg_champion_recent_rank" in k:
    #     pr_fl(k, p_side, p_role, key, role, div, cid, recent_i)
    if key is None:
        key = k
    r, game_i = p_side + '_' + p_role, fl["game_i"]
    if recent_i is not None:
        lib = fl["rec_flags"]
        x = [game_i, role_is[r], recent_i, k, key]
    else:
        lib = fl["flags"]
        x = [game_i, role_is[r], r + '_' + k, key]

    if role is not None:
        x.append(role)
    if div is not None:
        x.append(div)
    if cid is not None:
        x.append(cid)
    lib.append(x)

# Get infill data from a prepared infill data (filler["fill"]) object
def get_infill(infill_data, k, role=None, div=None, cid=None):
    x = [k]
    if role is not None:
        x.append(role)
    if div is not None:
        x.append(str(div))
    if cid is not None:
        x.append(str(cid))
    val = infill_data
    for key in x:
        val = val[key]
    return val["_data"]


# Match a game from a player's recent games with the current data point
def match_op_rs_rec_game(gm, cid, queue, duration,
                         win, kills, deaths, assists, cs, sp1, sp2):
    if gm["result"] == "Remake": return False

    cid_ = champ_dict[gm["champion"] if gm["champion"] != "Nunu" else "Nunu & Willump"]
    # cid_ = champ_dict[(gm["champion"] if gm["champion"] != "Nunu" else "Nunu & Willump") if gm["champion"] != "Qiyana" else "Zoe"]
    if int(cid) != int(cid_): return False  # Match champion
    
    kills_ = gm["kills"]
    if not approx_eq(kills, kills_, tol=0.1): return False  # Match kda
    deaths_ = gm["deaths"]
    if not approx_eq(deaths, deaths_, tol=0.1): return False
    assists_ = gm["assists"]
    if not approx_eq(assists, assists_, tol=0.1): return False
    
    cs_ = gm["cs"]
    if not approx_eq(cs, cs_, tol=0.1): return False  # Match creep score
    
    queue_ = rito_queue_ids["RANKED_FLEX_SR"] if gm["type"][0] == 'F' \
        else rito_queue_ids["RANKED_SOLO_5x5"]
    if queue != queue_: return False  # Match queue
    
    if (gm["result"][0] == 'V') != win: return False  # Match result (probs same or remake issues)
    
    # if gm["spell1"].lower() in spell_ids and gm["spell2"].lower() in spell_ids:
    #     sp1_, sp2_=spell_ids[gm["spell1"].lower()], spell_ids[gm["spell2"].lower()]
    #     if not (sp1 == sp1_ and sp2 == sp2_) or (sp1 == sp2_ and sp2 == sp1_):
    #         return False  # Match spells
    
    duration_ = gm["length"].split('m ')
    duration_ = (int(duration_[0]) * 60) + int(duration_[1][:-1])
    if not approx_eq(duration, duration_, tol=2.0): return False  # Match duration within 2 seconds
    
#     for team_id in ["team1", "team2"]: # Names are hard to match
#         for pl in gm[team_id]:
#              if pl["name"].lower() not in names: return False # Match names
    return True


# Get all global stats for a champion
def get_champ_stats_global(cid, opgg_region, tier, role, op_ts, ch_ts):
    return dict([(per, get_opgg_gcs(op_ts, cid, opgg_region, \
        tier, per, 'win')) for per in opgg_periods]), \
        get_chgg_gcs(ch_ts, cid, tier, role)


#   Get a player's champion's stats over their last n_opgg_pcs_games games,
#   and for the current/latest season, and for the champion globally if
#   return_global=True
#
# cs = list of player champion stats for player; first dimension is season
#      (in ascending order), second dimension is champion (random order,
#      name is within champion object)
# cache = cache dictionary for a player (begins as {})
# cid = champion id to return stats for
# opgg_region = opgg_region to use when missing stats need to be filled with globals
# tier = league tier in op.gg integer index format
#        (0 = all, 1 = iron, 9 = challenjour)
# return_global = whether to return global stats for champion too
# op_ts, chc_ts, chm_ts, player_ts = timestamps for global stats
def get_champ_stats(cs, cache, pre, cid, opgg_region, tier, div, p_side, p_role,
  role, filler, return_global=False, op_ts=None, ch_ts=None, recent_i=None, player_ts=None):
    global op_gcs
    if op_gcs is None:
        op_gcs, _, __ = load_global_stats()

    if op_ts is None:
        op_ts = chc_ts = chm_ts = time.time() * 1000

    if player_ts is None:
        player_ts = op_ts
    player_ts = player_ts // (60 * 15 * 1000)

    if op_ts not in op_gcs:
        ts_list = list(op_gcs.keys())
        op_ts = get_nearest(ts_list, op_ts, direction='up')
    cs_key = str(cid) + '_' + str(player_ts)
    if cs_key in cache:
        if return_global:
            if cache[cs_key][1] is None:
                cache[cs_key][1], cache[cs_key][2] = \
                  get_champ_stats_global(cid, opgg_region, tier, role, op_ts, ch_ts)
            return cache[cs_key]
        return cache[cs_key][0]
    
    pre = pre + "opgg_champion_"
    cid_x = cid

    # n_games = number of games with champ cid
    # c_name = champion name
    # latest_season = most recent season entry (data for all champs)
    # champs = stats for other champions over the same sampling period/seasons
    n_games, c_name, latest_season = 0, champion_names[cid], None
    c_ranks, max_ranks, champs, res, no_stats = [], [], {}, {}, False
    cs_i, first_season_i, last_season_i = -1, 0, 0
    added_seasons = 0
    for cs_season in cs[::-1][:n_seasons_to_use]:  # Start from most recent season
        cs_i += 1
        if not cs_season:
            continue

        if not latest_season:   # Get latest season for later
            latest_season = cs_season

        ch = None        # Make sure target champion has stats for this season
        for ch_ in cs_season:
            if "name" in ch_ and (ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump") == c_name:
            # if "name" in ch_ and ((ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump") if ch_["name"] != "Qiyana" else "Zoe") == c_name:
                ch = ch_
                c_ranks.append(ch_["rank"])
        if not ch:
            continue

        first_season_i = cs_i
        if max_ranks == []:
            last_season_i = cs_i

        # We now have a season object with data for the champ we're looking for
        ch_ranks = []
        for ch_ in cs_season:    # Add stats for every champ for this season
            if "name" not in ch_:
                # pr_fl(ch_) # It's just this: {'rank': 22, 'wins': None, 'losses': None, 'winRatio': None, 'kills': None, 'deaths': None, 'assists': None, 'ratio': None, 'gold': None, 'cs': None, 'turrets': None, 'maxKills': None, 'maxDeaths': None, 'damageDealt': None, 'damageTaken': None, 'doubleKill': 0, 'tripleKill': 0, 'quadraKill': 0}
                continue
            cid_ = champ_dict[(ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump")]
            # cid_ = champ_dict[((ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump") if ch_["name"] != "Qiyana" else "Zoe")]
            ch_ranks.append(ch_["rank"])
            if cid_ not in champs:
                champs[cid_] = defaultdict(list)
            champs[cid_]["wins"].append(hard_int(ch_["wins"]))
            champs[cid_]["losses"].append(hard_int(ch_["losses"]))
            champs[cid_]["kills"].append(hard_float(ch_["kills"]))
            champs[cid_]["deaths"].append(hard_float(ch_["deaths"]))
            champs[cid_]["assists"].append(hard_float(ch_["assists"]))
            champs[cid_]["gold"].append(hard_int(ch_["gold"]))
            champs[cid_]["creep_score"].append(hard_float(ch_["cs"]))
            # champs[cid_]["turrets"].append(hard_int(ch_["turrets"]))
            champs[cid_]["max_kills"].append(hard_int(ch_["maxKills"]))
            champs[cid_]["max_deaths"].append(hard_int(ch_["maxDeaths"]))
            champs[cid_]["damage_dealt"].append(hard_int(ch_["damageDealt"]))
            champs[cid_]["damage_taken"].append(hard_int(ch_["damageTaken"]))
            champs[cid_]["double_kills"].append(hard_int(ch_["doubleKill"]))
            champs[cid_]["triple_kills"].append(hard_int(ch_["tripleKill"]))
            champs[cid_]["quadra_kills"].append(hard_int(ch_["quadraKill"]))
            champs[cid_]["rank"].append(hard_int(ch_["rank"]))

        if not ch_ranks:
            continue

        added_seasons += 1

        max_rank = max(ch_ranks)   # Get maximum champion rank for this season
        max_ranks.append(max_rank)

        # Add seasons until we have enough for a good target champion sample
        n_games += hard_int(ch["wins"]) + hard_int(ch["losses"])
        if n_games >= n_opgg_pcs_games or added_seasons > n_seasons_to_use:
            break

    # Add average stats for last n_opgg_pcs_avg_games if not already in cache
    if "avg" not in cache:
        cache['all'] = dict([(l, 0) for l in opgg_pcs_all_champ_labels])

        n_avg_games, avg_max_ranks, avg_champs, res = 0, [], {}, {}
        total_wins, total_losses, all_ranks = 0, 0, []
        for cs_season in cs[::-1][:n_seasons_to_use]:  # Start from most recent season
            if not cs_season or len(cs_season) == 0:
                continue

            ch_ranks = []
            for ch_ in cs_season:  # Add stats for every champ for this season
                if "name" not in ch_:
                    # pr_fl(ch_)
                    continue
                ci = champ_dict[(ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump")]
                # ci = champ_dict[((ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump") if ch_["name"] != "Qiyana" else "Zoe")]
                rank = ch_["rank"]
                all_ranks.append(rank)
                ch_ranks.append(rank)
                if ci not in avg_champs:
                    avg_champs[ci] = defaultdict(list)
                wins = hard_int(ch_["wins"])
                losses = hard_int(ch_["losses"])
                total_wins += wins
                total_losses += losses
                avg_champs[ci]["wins"].append(wins)
                avg_champs[ci]["losses"].append(losses)
                avg_champs[ci]["kills"].append(hard_float(ch_["kills"]))
                avg_champs[ci]["deaths"].append(hard_float(ch_["deaths"]))
                avg_champs[ci]["assists"].append(hard_float(ch_["assists"]))
                avg_champs[ci]["gold"].append(hard_int(ch_["gold"]))
                avg_champs[ci]["creep_score"].append(hard_float(ch_["cs"]))
                # avg_champs[ci]["turrets"].append(hard_int(ch_["turrets"]))
                avg_champs[ci]["max_kills"].append(hard_int(ch_["maxKills"]))
                avg_champs[ci]["max_deaths"].append(hard_int(ch_["maxDeaths"]))
                avg_champs[ci]["damage_dealt"].append(hard_int(ch_["damageDealt"]))
                avg_champs[ci]["damage_taken"].append(hard_int(ch_["damageTaken"]))
                avg_champs[ci]["double_kills"].append(hard_int(ch_["doubleKill"]))
                avg_champs[ci]["triple_kills"].append(hard_int(ch_["tripleKill"]))
                avg_champs[ci]["quadra_kills"].append(hard_int(ch_["quadraKill"]))
                avg_champs[ci]["rank"].append(hard_int(ch_["rank"]))
            
            if not ch_ranks:
                continue

            max_rank = max(ch_ranks)# Get maximum champion rank for this season
            avg_max_ranks.append(max_rank)

            # Add seasons until we have enough games for a good sample
            if total_wins + total_losses >= n_opgg_pcs_avg_games:
                break

        # If we have no champion stats, use average for someone at this elo
        if total_wins + total_losses == 0:
            cache["avg"] = None
            cache["seasonavg"] = None
            for ci in champion_names:
                # cache['all'][champion_names[ci] + '_wins'] += 0
                # cache['all'][champion_names[ci] + '_losses'] += 0
                for cat in champ_cats:
                    if ci in champ_cats[cat]:
                        cache['all'][cat + "_wins"] += 0
                        cache['all'][cat + "_losses"] += 0
        else:

            # Add 'all' data - stats for every champion and champion category
            for ci in champion_names:
                wins = sum(avg_champs[ci]["wins"]) if ci in avg_champs else 0
                losses = sum(avg_champs[ci]["losses"]) if ci in avg_champs else 0
                # cache['all'][champion_names[ci] + '_wins'] += wins
                # cache['all'][champion_names[ci] + '_losses'] += losses
                for cat in champ_cats:
                    if ci in champ_cats[cat]:
                        cache['all'][cat + "_wins"] += wins
                        cache['all'][cat + "_losses"] += losses

            # Get weighted means for all stats for all champions & seasons
            champs_avgs, total_games = {}, total_wins + total_losses
            for cid_ in avg_champs:
                ch_ = avg_champs[cid_]
                if len(ch_["wins"]) == 1:
                    champs_avgs[cid_] = dict([(k, ch_[k][0]) for k in ch_])
                    n_wins = ch_["wins"][0]
                    n_losses = ch_["losses"][0]
                    champs_avgs[cid_]["total_wins"] = n_wins # Total for all seasons for this champ
                    champs_avgs[cid_]["total_losses"] = n_losses
                else:
                    wins = np.asarray(ch_["wins"])
                    losses = np.asarray(ch_["losses"])
                    games = wins + losses
                    tn_games = sum(games)
                    if tn_games == 0:
                        games = np.ones(wins.shape)
                        tn_games = len(games)
                    weights = games / tn_games
                    if weights is None:
                        pr_fl("weights none")
                    for k in ch_:
                        if ch_[k] is None:
                            pr_fl("ch_[k] is None... ", k)
                            pr_fl(ch_)
                    try:
                        champs_avgs[cid_] = \
                            dict([(k, sum(weights * ch_[k])) for k in ch_])
                    except Exception as e:
                        pr_fl()
                        pr_fl(weights)
                        pr_fl(ch_)
                        pr_fl(type(e))
                        pr_fl(e)
                        pr_fl(e.args)
                    n_wins, n_losses = sum(wins), sum(losses)
                    champs_avgs[cid_]["total_wins"] = n_wins
                    champs_avgs[cid_]["total_losses"] = n_losses
            c_weights = np.asarray([champs_avgs[cid_]["total_wins"] + \
                champs_avgs[cid_]["total_losses"] for cid_ in champs_avgs]) \
                / total_games
            champ_avg = dict([(k, sum(c_weights * [champs_avgs[cid_][k] for \
                cid_ in champs_avgs])) for k in a_E(champs_avgs)])
            champ_avg["alltotal_wins"] = total_wins # Total for all champions for all seasons
            champ_avg["alltotal_losses"] = total_losses
            champ_avg["mean_rank"] = np.mean(all_ranks)
            champ_avg["mean_max_rank"] = np.mean(avg_max_ranks)
            champ_avg["max_rank"] = max(avg_max_ranks)
            cache["avg"] = champ_avg
            
            # Hyperparameter: whether to use role & champion type in filling
            for k in champ_avg:
                add_filler(filler, pre + "avg_" + k, champ_avg[k], div=div)
    
    # Add fill flags if we have no player stats (for any season or champion)
    if cache["avg"] is None:
        no_stats = True
        if recent_i is None:
            for k in opgg_pcs_avg_labels:
                add_flag(filler,pre+"avg_"+k,p_side,p_role,div=div)
            for k in opgg_pcs_seasonavg_labels:
                add_flag(filler,pre+"seasonavg_"+k,p_side,p_role,div=div)
        
    # Add total stats for latest season if not already in cache
    if "seasonavg" not in cache:
        games, ranks, cids, s_champs = [], [], [], {}
        season_total_wins, season_total_losses = 0, 0
        for ch_ in latest_season:
            if "name" not in ch_:
                # pr_fl(ch_)
                continue
            cid_ = champ_dict[(ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump")]
            # cid_ = champ_dict[((ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump") if ch_["name"] != "Qiyana" else "Zoe")]
            cids.append(cid_)
            
            wins = hard_int(ch_["wins"])
            losses = hard_int(ch_["losses"])
            season_total_wins += wins
            season_total_losses += losses
            games.append(wins + losses)
            ranks.append(ch_["rank"])
            
            s_champs[cid_] = {
                "wins": wins,
                "losses": losses,
                "kills": hard_float(ch_["kills"]),
                "deaths": hard_float(ch_["deaths"]),
                "assists": hard_float(ch_["assists"]),
                "gold": hard_int(ch_["gold"]),
                "creep_score": hard_float(ch_["cs"]),
                # "turrets": hard_int(ch_["turrets"]),
                "max_kills": hard_int(ch_["maxKills"]),
                "max_deaths": hard_int(ch_["maxDeaths"]),
                "damage_dealt": hard_int(ch_["damageDealt"]),
                "damage_taken": hard_int(ch_["damageTaken"]),
                "double_kills": hard_int(ch_["doubleKill"]),
                "triple_kills": hard_int(ch_["tripleKill"]),
                "quadra_kills": hard_int(ch_["quadraKill"]),
                "rank": hard_int(ch_["rank"]),
            }

        tn_games = sum(games)
        if tn_games == 0:
            tn_games = len(games)
            games = np.ones(tn_games)
        c_weights = np.asarray(games) / tn_games
        season_champ = dict([(k, sum(c_weights * [s_champs[cid_][k] for \
            cid_ in cids])) for k in s_champs[list(s_champs.keys())[0]]])
            
        season_champ["total_wins"] = season_total_wins
        season_champ["total_losses"] = season_total_losses
        season_champ["mean_rank"] = np.mean(ranks)
        season_champ["max_rank"] = max(ranks)
        cache["seasonavg"] = season_champ
        
        for k in season_champ:
            add_filler(filler, pre+"seasonavg_"+k, season_champ[k], div=div)
    
    # Add global stats for champion if none found
    op_ch, ch_ch = None, None
    if n_games == 0:
        res = dict(empty_ch)
        if op_ch is None:
            op_ch, ch_ch = \
                get_champ_stats_global(cid, opgg_region, tier, role, op_ts, ch_ts)
        
        res["kills"] = ch_ch["kills"]
        res["deaths"] = ch_ch["deaths"]
        res["assists"] = ch_ch["assists"]
        res["gold"] = ch_ch["goldEarned"]
        res["creep_score"] = ch_ch["minionsKilled"]
        res["damage_dealt"] = ch_ch["damageComposition"]["total"]
        res["damage_taken"] = ch_ch["totalDamageTaken"]

        res["total_wins"] = 0
        res["total_losses"] = 0
        res["alltotal_wins"] = 0
        res["alltotal_losses"] = 0

        # add_flag(filler, pre + "recent_turrets", p_side, p_role,
        #     div=div, cid=cid_x, recent_i=recent_i)
        add_flag(filler, pre + "recent_max_kills", p_side, p_role,
            div=div, cid=cid_x, recent_i=recent_i)
        add_flag(filler, pre + "recent_max_deaths", p_side, p_role,
            div=div, cid=cid_x, recent_i=recent_i)
        
        s = cache["seasonavg"]
        if no_stats:
            mr_key = pre + "seasonavg_max_rank"
            add_flag(filler, pre + "recent_rank", p_side, p_role,
                div=div, recent_i=recent_i, key=mr_key)
            add_flag(filler, pre + "recent_mean_rank", p_side, p_role,
                div=div, recent_i=recent_i, key=mr_key)
            add_flag(filler, pre + "recent_mean_max_rank", p_side, p_role,
                div=div, recent_i=recent_i, key=mr_key)
            add_flag(filler, pre + "recent_max_rank", p_side, p_role,
                div=div, recent_i=recent_i, key=mr_key)
            # if recent_i == 3 and p_side == "blue" and p_role == "top":
            #     pr_fl(pre, p_side, p_role, recent_i)
            #     sys.exit()
        else:
            res["rank"] = s["max_rank"]
            res["mean_rank"] = s["max_rank"]
            res["mean_max_rank"] = s["max_rank"]
            res["max_rank"] = s["max_rank"]
    else:
            
        # Get weighted means for target champion
        champs_avgs, total_wins, total_losses = {}, 0, 0
        for cid_ in champs:
            ch_ = champs[cid_]
            if len(ch_["wins"]) == 1:
                if cid_ == cid:
                    champs_avgs[cid_] = dict([(k, ch_[k][0]) for k in ch_])
                n_wins = ch_["wins"][0]
                n_losses = ch_["losses"][0]
                if cid_ == cid:
                    champs_avgs[cid_]["total_wins"] = n_wins
                    champs_avgs[cid_]["total_losses"] = n_losses
                total_wins += n_wins
                total_losses += n_losses
            else:
                wins, losses=np.asarray(ch_["wins"]), np.asarray(ch_["losses"])
                if cid_ == cid:
                    games = wins + losses
                    tn_games = sum(games)
                    if tn_games == 0:
                        games = np.ones(wins.shape)
                        tn_games = len(games)
                    weights = games / tn_games
                    champs_avgs[cid_] = \
                        dict([(k, sum(weights * ch_[k])) for k in ch_])
                n_wins, n_losses = sum(wins), sum(losses)
                if cid_ == cid:
                    champs_avgs[cid_]["total_wins"] = n_wins
                    champs_avgs[cid_]["total_losses"] = n_losses
                total_wins += n_wins
                total_losses += n_losses
#         c_weights = np.asarray([champs_avgs[cid_]["total_wins"] + \
#             champs_avgs[cid_]["total_losses"] for cid_ in champs_avgs]) / \
#             (total_wins + total_losses)
#         champ_avg = dict([(k, sum(c_weights * [champs_avgs[cid_][k] for \
#             cid_ in champs_avgs])) for k in champs_avgs[cid]])

#         res["avg"] = champ_avg
        res["alltotal_wins"] = total_wins
        res["alltotal_losses"] = total_losses

        res["mean_rank"] = np.mean(c_ranks)
        res["mean_max_rank"] = np.mean(max_ranks)
        res["max_rank"] = max(max_ranks)

        ch = champs_avgs[cid]
        for k in ch:
            res[k] = ch[k]

        # add_filler(filler,pre+"recent_turrets",ch["turrets"],div=div,cid=cid_x)
        add_filler(filler, pre+"recent_max_kills", ch["max_kills"], div=div, cid=cid_x)
        add_filler(filler, pre+"recent_max_deaths", ch["max_deaths"], div=div, cid=cid_x)

    # Get stats for champ for latest season
    ch = None
    if latest_season:
        for ch_ in latest_season:
            if "name" not in ch_:
                # pr_fl(ch_)
                continue
            if (ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump") == c_name:
            # if ((ch_["name"] if ch_["name"] != "Nunu" else "Nunu & Willump") if ch_["name"] != "Qiyana" else "Zoe") == c_name:
                ch = ch_
                break

    season_ch = None
    pre_ = pre + "season_"
    if ch is None:
        season_ch = dict(empty_ch)
        # Set empty fields to average values
        if op_ch is None:
            op_ch, ch_ch = \
                get_champ_stats_global(cid, opgg_region, tier, role, op_ts, ch_ts)

        season_ch["kills"] = ch_ch["kills"]
        season_ch["deaths"] = ch_ch["deaths"]
        season_ch["assists"] = ch_ch["assists"]
        season_ch["gold"] = ch_ch["goldEarned"]
        season_ch["creep_score"] = ch_ch["minionsKilled"]
        season_ch["damage_dealt"] = ch_ch["damageComposition"]["total"]
        season_ch["damage_taken"] = ch_ch["totalDamageTaken"]
        # add_flag(filler, pre_ + "turrets", p_side, p_role,
        #     div=div, cid=cid_x, recent_i=recent_i)
        add_flag(filler, pre_ + "max_kills", p_side, p_role,
            div=div, cid=cid_x, recent_i=recent_i)
        add_flag(filler, pre_ + "max_deaths", p_side, p_role,
            div=div, cid=cid_x, recent_i=recent_i)
        if no_stats:
            add_flag(filler, pre_ + "rank", p_side, p_role,
                div=div, recent_i=recent_i, key=pre + "seasonavg_max_rank")
        else:
            season_ch["rank"] = cache["seasonavg"]["max_rank"]
    else:
        season_ch = {
            "wins": hard_int(ch["wins"]),
            "losses": hard_int(ch["losses"]),
            "kills": hard_float(ch["kills"]),
            "deaths": hard_float(ch["deaths"]),
            "assists": hard_float(ch["assists"]),
            "gold": hard_int(ch["gold"]),
            "creep_score": hard_float(ch["cs"]),
            # "turrets": hard_int(ch["turrets"]),
            "max_kills": hard_int(ch["maxKills"]),
            "max_deaths": hard_int(ch["maxDeaths"]),
            "damage_dealt": hard_int(ch["damageDealt"]),
            "damage_taken": hard_int(ch["damageTaken"]),
            "double_kills": hard_int(ch["doubleKill"]),
            "triple_kills": hard_int(ch["tripleKill"]),
            "quadra_kills": hard_int(ch["quadraKill"]),
            "rank": hard_int(ch["rank"]),
        }
        # add_filler(filler, pre_ + "turrets",
        #     season_ch["turrets"], div=div, cid=cid_x)
        add_filler(filler, pre_ + "max_kills",
            season_ch["max_kills"], div=div, cid=cid_x)
        add_filler(filler, pre_ + "max_deaths",
            season_ch["max_deaths"], div=div, cid=cid_x)

    res["first_season"] = first_season_i
    res["last_season"] = last_season_i
    # pr_fl(res.keys())
    for k in list(res.keys()):
        new_keys = res.keys()
        if k not in new_keys:
            continue
        if k[:len("recent_")] != "recent_":
            res["recent_" + k] = res.pop(k)
        else:
            suff = k.split("recent_")[-1]
            res["recent_" + suff] = res.pop(k)
            if suff in new_keys:
                del res[suff]

    for k in season_ch:
        res["season_" + k] = season_ch[k]
    if return_global and op_ch is None:
        op_ch, ch_ch = \
            get_champ_stats_global(cid, opgg_region, tier, role, op_ts, ch_ts)
    cache[cs_key] = [res, op_ch, ch_ch]
    # pr_fl(res.keys())
    # pr_fl()
    # pr_fl()
    if return_global:
        return cache[cs_key]
    return cache[cs_key][0]


# Get array verion data labels depending on load_games options
def get_arr_labels(code_champs=True, code_summs=True):
    coded_x_keys_ = OrderedDict(coded_x_keys)
    if not code_champs:
        for k in X_r_player_champion_labels:
            del coded_x_keys_[k]
    if not code_summs:
        for k in X_r_player_spell_labels:
            del coded_x_keys_[k]

    X_arr_labels_ = sum([[k] if k not in coded_x_keys_ else [k + '__x' + \
        str(j) for j in range(coded_x_keys_[k])] for k in X_labels_all], [])
    return meta_arr_labels_all, X_arr_labels_, Yc_labels_all, Yr_labels_all

# Get recent match array version data labels depending on load_games options
def get_recent_match_arr_labels(code_champs_rec=False, code_summs_rec=False,
                                code_queues_rec=False):
    coded_x_c_prev_keys_ = OrderedDict(coded_x_c_prev_keys)
    if not code_champs_rec:
        for k in X_r_player_champion_labels:
            del coded_x_c_prev_keys_[k]
    if not code_summs_rec:
        for k in X_r_player_spell_labels:
            del coded_x_c_prev_keys_[k]
    if not code_queues_rec:
        del coded_x_c_prev_keys_["queue"]

    return sum([[k] if k not in coded_x_c_prev_keys_ else [k + '__x' + \
        str(j) for j in range(coded_x_c_prev_keys_[k])] for k in \
        X_c_prev_labels_all], [])


# Convert metadata dictionary to array
def metadata_to_arr(m):
    del m["names"]
    del m["roles"]
    m["region_id"] = region_is[m["region_id"]]
    m["game_version"] = patch_s2n('.'.join(m["game_version"].split('.')[:2]))


def add_new_jsons(gids, gs=None):
    pr_fl("Adding missing jsons to csv...")
    new_gs = []

    for g_id in gids:
        game_full = load_json(curr_data_jsons + g_id + '.json', pad=False)
        if game_full is None:
            continue

        game = game_full["game"]
        queue = queue_names[game["queueId"]]
        lps = game_full["lps"]

        divisions = []
        for i in N_PL_r:
            if not lps[i]:
                continue
            for lp in lps[i]:
                if lp["queueType"] == queue:
                    divisions += [get_division_index(lp["tier"]+' '+lp["rank"])]
                    break

        game_div = int(np.median([d for d in divisions if d is not None]))
        new_gs.append({
            "g_id": g_id,
            "region": game["platformId"],
            "division": game_div,
            "timestamp": int(game["gameCreation"] / 1000),
        })

    if len(new_gs) == 0:
        pr_fl("No missing games! Continuing as normal...")
        return gs

    pr_fl("Adding ", len(new_gs), " missing jsons to csv...")
    new_games = pd.DataFrame(new_gs)
    new_games = new_games[cg_cols]
    new_games = new_games.set_index('g_id')

    res = db_add(new_games, curr_games_path, None, index_col=0)
    pr_fl("Success!")
    return res


def retrospect_pcs(gm, cs):
    if gm["result"] == "Remake":
        return
    cid_ = champ_dict[(gm["champion"] if gm["champion"] != "Nunu" else "Nunu & Willump")]  # Champion played in this game

    # Find the champion stats entry for the champion
    for s_i in range(len(cs)):
        cs_season = cs[s_i]
        found_entry = False
        for chmp_i in range(len(cs_season)):
            chmp = cs_season[chmp_i]
            if "name" in chmp and champ_dict[chmp["name"]] == cid_:  # If we have an entry for the champion in this season
                found_entry = True
                wins = hard_int(chmp["wins"])
                losses = hard_int(chmp["losses"])
                n_games = wins + losses
                if n_games <= 1:          # If this is the only time they've played the champion, pop entry
                    del cs[s_i][chmp_i]
                    break

                win = int(gm["result"] == "Victory")                # Compute restrospective champion stats
                new_wins = wins - win
                factor = n_games / (n_games - 1)
                cs[s_i][chmp_i]["wins"] = new_wins
                cs[s_i][chmp_i]["losses"] = losses - (1 - win)
                cs[s_i][chmp_i]["winRatio"] = 100 * (new_wins / (n_games - 1))
                k = factor * (hard_float(chmp["kills"]) - (hard_int(gm["kills"]) / n_games))
                d = factor * (hard_float(chmp["deaths"]) - (hard_int(gm["deaths"]) / n_games))
                a = factor * (hard_float(chmp["assists"]) - (hard_int(gm["assists"]) / n_games))
                cs[s_i][chmp_i]["kills"] = k
                cs[s_i][chmp_i]["deaths"] = d
                cs[s_i][chmp_i]["assists"] = a
                cs[s_i][chmp_i]["ratio"] = (k + a) / (1 if d <= 0 else d)
                cs[s_i][chmp_i]["cs"] = factor * (hard_float(chmp["cs"]) - (hard_int(gm["cs"]) / n_games))

                # We don't yet have gold or damage dealt/taken extracted/crawled from op.gg recent games (todo)
                # cs[s_i][chmp_i]["gold"] = factor * (chmp["gold"] - (gm["gold"] / n_games))
                # cs[s_i][chmp_i]["damage_dealt"] = factor * (chmp["damage_dealt"] - (gm["damage_dealt"] / n_games))
                # cs[s_i][chmp_i]["damage_taken"] = factor * (chmp["damage_taken"] - (gm["damage_taken"] / n_games))
                # We also don't have wards purchased or kill participation from op.gg champion stats (worth finding?)

                break
        if found_entry:
            break


# Loads full game data jsons (in curr_data_jsons folder by default)
# Uniformly sample players: n_occ = 1 or None indicating all games, or 'temporal' for 1 occurence per ~20 (op.gg) recent matches
def load_games(n_occ=None, n_occ_elo=None, N=None, folders=None, code_champs=False,
  code_summs=False, code_champs_rec=False, code_summs_rec=False, code_pick_order=False,
  code_queues_rec=False, conv=True, return_labels=True, add_newest=False,
  save=True, filename="d_all_", save_dir="learning_data", save_every=12500, min_save_n=12500):


    # First thing: Compare curr_games csv with the file list, to see for which files we are
    # missing an entry. Then, add those missing entries (load json & get division + timestamp)

    cj_prefix = curr_data_jsons[:-1]
    cj_st = len(cj_prefix) + 1
    json_l = len(".json")

    filelist = glob.glob(cj_prefix + '/*')
    gid_list = [fn[cj_st:-json_l] for fn in filelist]
    gid_set = set(gid_list)





    # Get all data archives & construct total databases
    # if folders is None:
    #     archives = [x.replace('\\', '/') for x in sorted(glob.glob(data_dir + \
    #                 "/carchive*/"))]
    #     folders = [ curr_data_jsons ]
    #     folders += [x + curr_data_jsons for x in archives]
    #     gs, ps = load_current_data(make_r_conds())
    #     gs, ps = [ gs ], [ ps ]
    #     gs += [pd.read_csv(x + curr_games_csv, index_col=0) for x in archives]
    #     ps += [pd.read_csv(x+curr_players_csv, index_col=0) for x in archives]
    #     # Add archive info (0 = current dataset, >=1 = index of archive + 1 )
    #     for i in range(len(gs)):
    #         gs[i]["archive"] = i
    #     gs, ps = pd.concat(gs).sample(frac=1), pd.concat(ps).sample(frac=1)
    # else: TODO




    gs, _ = load_current_data(make_r_conds())


    csv_list = list(gs.index)
    csv_set = set(csv_list)
    new_gids = list(gid_set - csv_set)



    gs = add_new_jsons(new_gids, gs)




    # Get games list
    pr_fl("Total number of game jsons: " + str(gs.shape[0]))
    # games_list = list(gs.index if n_occ is None else \
    #     ps.groupby(ps.index).first()['g_id'])
    games_list = list(gs.index)
    n_games = len(games_list)
    if N is None:
        N = n_games

    # pr_fl("Unique players game jsons: " + str(n_games))
    target_n = target_n_ = min(N, n_games)
    numpy_fresh_padding = 1500

    # Setup array containers
    meta_labels = meta_labels_all
    X_labels, X_rec_labels = X_labels_all, X_c_prev_labels_all
    Yc_labels, Yr_labels = Yc_labels_all, Yr_labels_all
    if conv:
        meta_labels, X_labels, Yc_labels, Yr_labels = \
            get_arr_labels(code_champs, code_summs)
        X_rec_labels = get_recent_match_arr_labels(code_champs_rec,
            code_summs_rec, code_queues_rec)
        X_label_is = dict(zip(X_labels, range(len(X_labels))))
        X_rec_label_is = dict(zip(X_rec_labels, range(len(X_rec_labels))))

        pr_fl("X shape:", (target_n + numpy_fresh_padding, len(X_labels)))

        Meta = np.empty((target_n + numpy_fresh_padding, len(meta_labels)))
        X = np.empty((target_n + numpy_fresh_padding, len(X_labels)))
        Yc = np.empty((target_n + numpy_fresh_padding, len(Yc_labels)))
        Yr = np.empty((target_n + numpy_fresh_padding, len(Yr_labels)))
        X_rec = []

    # Add all games data
    game_i, n_tried, targ_str, filler = 0, 0, str(target_n), {}
    filler["fill"], filler["flags"] = {}, []
    filler["rec_flags"], filler["game_i"] = [], 0
    filler["seen_pl"] = set()
    timestamp_rli = X_rec_labels.index("timestamp")
    duration_rli = X_rec_labels.index("duration")
    start_time = time.time()
    timestamps = list(gs["timestamp"]) # Sort by file creation date
    ts_order = list(np.argsort(timestamps))[::-1] # Reverse order (most recent first)
    games_list_sorted = list(np.asarray(games_list)[ts_order])
    # np.random.shuffle(games_list_sorted)
    pl_seen = defaultdict(int)
    # adding_newest = False
    iteration_i = -1
    recent_count = 0
    skipped_count = 0
    while True:
        iteration_i += 1
        g_id = games_list_sorted[iteration_i]
        if isinstance(g_id, float) and np.isnan(g_id):
            err_pr("g_id is nan")
            continue
        # arch = gs.loc[g_id, "archive"]
        # if isinstance(arch, pd.Series):
        #     arch = arch.values[0]
        path = curr_data_jsons + g_id
        # path = folders[arch] + str(g_id)

        game_error = False

        try:

            game = load_json(path + ".json", pad=False)
            n_tried += 1
            if game is None:
                # err_pr("Game is None")
                continue

            rss, pcs = game["opgg_rs"], game["opgg_pcs"]
            # opgg_gcs_ts = int(game["opgg_gcs"].split('_')[0] if '_' in game["opgg_gcs"] else game["opgg_gcs"])
            # chgg_gcs_ts = int(game["chgg_gcs"].split('_')[0] if '_' in game["chgg_gcs"] else game["chgg_gcs"])
            # chgg_gms_ts = int(game["chgg_gms"].split('_')[0] if '_' in game["chgg_gms"] else game["chgg_gms"])
            opgg_gcs_ts = chgg_gcs_ts = chgg_gms_ts = timestamp = game["game"]["gameCreation"]

            # Get Rito league position objects for each player, if present
            queue_id = game["game"]["queueId"]
            queue = queue_names[queue_id]
            q_lps = []
            for i in N_PL_r:
                lpz = game["lps"][i]
                found = False
                for j in range(len(lpz)):
                    if lpz[j]["queueType"] == queue:
                        q_lps.append(lpz[j])
                        found = True
                        break
                if not found:
                    q_lps.append(None)

            # Get elo for each player & the game (tier, rank, league points etc.)
            tier_strs = [("unranked" if lp is None else lp["tier"].lower())
                         for lp in q_lps]
            tiers = [(leag_tier_is[tier] if tier[0] != 'u' else -1)
                     for tier in tier_strs]
            valid_tiers = [tier for tier in tiers if tier != -1]
            median_tier = int(np.ceil(np.median(valid_tiers)))
            mean_tier = np.mean(valid_tiers)
            int_tiers = [tier if tier != -1 else median_tier for tier in tiers]
            tiers = [tier if tier != -1 else mean_tier for tier in tiers]

            # Decode Rito game json, skip if fail
            data, all_unranked_hAST = get_game_data(game["game"],
                code_champs=False, code_summs=False, code_pick_order=code_pick_order, conv=False,
                int_tiers=int_tiers, op_gcs_ts=opgg_gcs_ts, ch_gcs_ts=chgg_gcs_ts,
                ch_gms_ts=chgg_gms_ts)
            if data is None:
                # err_pr("1227 data is None")
                continue

            meta, x, yc, yr = data
            region, roles = meta["region_id"], meta["roles"]
            opgg_region = opgg_region_dict[region]

            # Setup ordered dict order
            for l in X_c_labels_all:
                x[l] = None
            for l in c_meta_labels_all:
                x[l] = None
                meta[l] = None

            meta["tier"] = mean_tier
            names = meta["names"]
            rank_strs = [-1 if lp is None else lp["rank"] for lp in q_lps]
            divisions = [(-1 if q_lps[i] is None else get_division_index(
                tier_strs[i] + ' ' + rank_strs[i])) for i in N_PL_r]
            ranks = [deromanize(rank) if rank != -1 else -1 for rank in rank_strs]

            median_division = \
                int(np.ceil(np.median([d for d in divisions if d != -1])))
            meta["division"] = median_division
            divisions = [median_division if d == -1 else d for d in divisions]

            lps = [-1 if lp is None else lp["leaguePoints"] for lp in q_lps]
            elos = [(((100 * (divisions[i] if divisions[i] < 25 else 25)) + lps[i])
                        if lps[i] != -1 else -1) for i in N_PL_r]
            mean_elo = np.mean([elo for elo in elos if elo != -1])

            ts = meta["timestamp"] / 1000.0
            if ts < 1580500800 and ts >= patch_tss["9.20"] - (12 * 60 * 60):
                continue

            # Uniformly sample players/sample each player uniformly over time
            if n_occ is not None:
                if n_occ == 'temporal':
                    too_recent = False
                    for acc_id in meta["acc_ids"]:
                        id_ = str(region) + '_' + str(acc_id)
                        if id_ in pl_seen:
                            if ts > pl_seen[id_]:  # This game occured too recently to a previously seen game for this player
                                too_recent = True
                                break
                    if too_recent:
                        recent_count += 1
                        continue
                else:
                    n_occ_effective = n_occ
                    if n_occ_elo is not None:
                        if mean_elo >= n_occ_elo[0]:
                            n_occ_effective = n_occ_elo[1]
                    if any(pl_seen[str(region) + '_' + str(acc_id)] >= n_occ_effective for acc_id in meta["acc_ids"]):
                        continue


            elo_div = mean_elo / 100
            meta["elo"] = mean_elo
            mean_league_points = 100 * (elo_div - int(elo_div))
            meta["league_points"] = mean_league_points
            for i in N_PL_r:
                if elos[i] != -1:
                    filler["pl_name"] = names[i]
                    add_filler(filler, "elo", elos[i], roles[i].split('_')[1], divisions[i])
            elos = [elo if elo != -1 else mean_elo for elo in elos]
            lps = [lp if lp != -1 else mean_league_points for lp in lps]
            e_div = int(elo_div)
            game_rank = 1 if e_div >= 24 else (5 - (e_div % 5))
            ranks = [rank if rank != -1 else game_rank for rank in ranks]
            meta["rank"] = game_rank
            # if all_unranked_hAST:
                # TODO: fix the hASTs, which are otherwise assumed all bronze (very wrong)
                # err_pr("all unranked: " + str(divisions))
            # Get wins & losses for each player & avg for the game
            wins = [0 if lp is None else lp["wins"] for lp in q_lps]
            losses = [0 if lp is None else lp["losses"] for lp in q_lps]
            for i in N_PL_r:
                lp = q_lps[i]
                if lp is not None:
                    filler["pl_name"] = names[i]
                    add_filler(filler, "_wins", lp["wins"], roles[i].split('_')[1], divisions[i])
                    add_filler(filler, "_losses", lp["losses"], roles[i].split('_')[1], divisions[i])
            ns_games = [wins[i] + losses[i] for i in N_PL_r]
            wrs = [(100 * wins[i] / ns_games[i]) if ns_games[i] > 0 else 50.0 \
                   for i in N_PL_r]
            meta["season_wins"] = np.mean(wins)
            meta["season_losses"] = np.mean(losses)

            # Add x data
            x_rec = [None] * N_PL
            for i in N_PL_r:
                filler["pl_name"] = names[i]
                r, rs, cs = roles[i], rss[i], pcs[i]
                div, int_tier = divisions[i], int_tiers[i]
                cid = x[r + "_champion"]
                side, role = r.split('_')
                pre = r + '_'
                x[pre + "season_games"] = ns_games[i]
                x[pre + "season_wins"] = wins[i]
                x[pre + "season_losses"] = losses[i]
                x[pre + "season_win_rate"] = wrs[i]
                x[pre + "tier"] = tiers[i]
                x[pre + "int_tier"] = int_tier
                x[pre + "rank"] = ranks[i]
                x[pre + "league_points"] = lps[i]
                x[pre + "division"] = div
                # x[pre + "elo"] = elos[i]
                x[pre + "elo"] = elos[i]
                # x[pre + "elodeviation"] = elos[i] - mean_elo
                x[pre + "elodeviation"] = elos[i] - mean_elo

                for k in rs:
                    if k in opgg_rs_non_number_keys:
                        continue
                    rs[k] = hard_float(rs[k])
                if "recent" in rs:
                    for k in rs["recent"]:
                        rs["recent"][k] = hard_float(rs["recent"][k])
                
                op_wins = rs["wins"] if "wins" in rs else \
                    (rs["recent"]["wins"] if "recent" in rs else wins[i])
                op_losses = rs["losses"] if "losses" in rs else \
                    (rs["recent"]["losses"] if "recent" in rs else losses[i])
                op_games = op_wins + op_losses
                op_win_rate = (100 * op_wins / op_games) if op_games else 50.0
                leag = rs["league"].lower() if "league" in rs else \
                    leag_tiers[int_tier] + ' ' + str(ranks[i])
                tier, rank = None, None
                if tier == "unranked":
                    tier = int(np.round(mean_tier))
                    rank = game_rank
                else:
                    top_elo = "master" in leag or \
                        leag == "challenger" or leag == "unranked"
                    spl = leag.split(' ')
                    tier = spl[0]
                    # tier = leag if top_elo else spl[0]
                    rank = 1 if top_elo else int(spl[-1])
                tier_i = leag_tier_is[tier]
                rs_re = rs["recent"] if "recent" in rs else None

                pre_ = "opgg_rs_"
                pre = r + '_' + pre_
                x[pre + "wins"] = op_wins
                x[pre + "losses"] = op_losses
                x[pre + "games"] = op_games
                x[pre + "win_rate"] = op_win_rate
                x[pre + "tier"] = tier_i
                x[pre + "rank"] = rank
                x[pre + "division"] = get_division_index(leag)
                x[pre + "league_points"] = rs['lp'] if 'lp' in rs else lps[i]
                x[pre + "recent_games"] = rs_re["games"] if rs_re else 0
                x[pre + "recent_wins"] = rs_re["wins"] if rs_re else 0
                x[pre + "recent_losses"] = rs_re["losses"] if rs_re else 0
                x[pre + "recent_win_rate"] = rs_re["winRatio"] if \
                    rs_re else op_win_rate
                if rs_re:
                    kills = rs_re["killsAverage"]
                    deaths = rs_re["deathsAverage"]
                    assists = rs_re["assistsAverage"]
                    kda = rs_re["kdaRatio"]
                    kp = hard_int(rs_re["killParticipation"])
                    add_filler(filler, pre_ + "recent_kills", kills, role, div)
                    add_filler(filler, pre_ + "recent_deaths", deaths, role, div)
                    add_filler(filler, pre_ + "recent_assists", assists, role, div)
                    add_filler(filler, pre_ + "recent_kda_ratio", kda, role, div)
                    add_filler(filler,pre_+"recent_kill_participation",kp,role,div)
                    x[pre + "recent_kills"] = kills
                    x[pre + "recent_deaths"] = deaths
                    x[pre + "recent_assists"] = assists
                    x[pre + "recent_kda_ratio"] = kda
                    x[pre + "recent_kill_participation"] = kp
                else:
                    add_flag(filler, pre_ + "recent_kills", side, role, role, div)
                    add_flag(filler, pre_ + "recent_deaths", side, role, role, div)
                    add_flag(filler,
                        pre_ + "recent_assists", side, role, role, div)
                    add_flag(filler,
                        pre_ + "recent_kda_ratio", side, role, role, div)
                    add_flag(filler,
                        pre_ + "recent_kill_participation", side, role, role, div)

                spell_1 = x[r + "_spell_1"]
                spell_2 = x[r + "_spell_2"]
                spell_1_code = spell_codes[spell_1]
                spell_2_code = spell_codes[spell_2]
                champion_code = champ_codes[cid]
                duration = x["duration"]
                name = names[role_is[r]]
                win = bool(yc["blue_win"])
                if side[0] == 'r':
                    win = not win
                kills = yr[r + "_kills"]
                deaths = yr[r + "_deaths"]
                assists = yr[r + "_assists"]
                creeps = yr[r + "_creep_score"]

                # First, check if the current game is in the recent matches list
                rs_games = rs["games"] if "games" in rs else []
                n_recent = len(rs_games)
                this_gm_i = -1
                if n_recent:
                    last_was_skipd = False
                    for gm_i in range(len(rs_games)):
                        # if match_op_rs_rec_game(rs_games[gm_i], cid,
                        #   queue_id, duration, win, kills, deaths, assists,
                        #   creeps, spell_1, spell_2):
                        #     matched_count += 1
                        #     last_was_skipd = False
                        #     this_gm_i = gm_i
                        #     break
                        if (meta["timestamp"] / 1000) - rs_games[gm_i]["timestamp"] < -30.0:  # If game occured in the future relative to this match
                            last_was_skipd = True
                            this_gm_i = gm_i
                    if last_was_skipd:
                        skipped_count += 1
                if this_gm_i != -1:  # If we have found the current game in recent matches, retrospect past champion stats
                    for gm_i in range(this_gm_i + 1):  # For each game that isn't part of the history
                        retrospect_pcs(rs_games[gm_i], cs)
                    rs_games = rs_games[this_gm_i + 1:]
                    n_recent = len(rs_games)
                x[r + "_n_recent_matches"] = n_recent

                # Load player champion stats
                chs_cache = {}
                ch = get_champ_stats(cs, chs_cache, "", cid, opgg_region, int_tier,
                    div, side, role, role,
                    filler, False, opgg_gcs_ts, chgg_gcs_ts)
                pre = r + "_opgg_champion_"
                for k in ch:
                    x[pre + k] = ch[k]

                # Add player champ stat ratios
                for k, k_ in pcs_ratios:
                    x[r + '_' + k + '_/_' + k_] = x[r + '_' + k] / x[r + '_' + k_]
                
                # Add total champion performance stats for player
                for key in opgg_tots_keys:
                    pre_ = pre + key + '_'
                    d = chs_cache[key]
                    if d is not None:
                        for k in d:
                            x[pre_ + k] = d[k]
                                      
                # Add index of oldest and newest season we have pcs stats for
                n_cs_s = len(cs)
                oldest_season_i, latest_season_i = 0, 0
                for cs_i in range(n_cs_s):
                    if cs[cs_i]:
                        oldest_season_i = n_cs_s - cs_i
                        break
                for cs_i in range(n_cs_s)[::-1]:
                    if cs[cs_i]:
                        latest_season_i = n_cs_s - cs_i
                        break
                x[r + "_first_ranked_season"] = oldest_season_i
                x[r + "_last_ranked_season"] = latest_season_i

                # Get recent matches data from RankedSummary
                x_rec_ = []
                if n_recent:

                    gm_i = 0
                    for gm in rs_games:  # Go in reverse chronological order, so we can retro-decrement champ stats
                        if gm["result"] == "Remake":
                            continue
                        cid_ = champ_dict[(gm["champion"] if gm["champion"] != "Nunu" else "Nunu & Willump")]
                        spell_1_ = spell_ids[gm["spell1"].lower() if gm["spell1"].lower() in spell_ids else "flash"]
                        spell_2_ = spell_ids[gm["spell2"].lower() if gm["spell2"].lower() in spell_ids else "ignite"]
                        if spell_order[spell_1_] < spell_order[spell_2_]:
                            spell_1_, spell_2_ = spell_2_, spell_1_
                        duration_ = gm["length"].split('m ')
                        found_side, team_i = 0, False
                        while not found_side:
                            team_key = "team" + str(team_i + 1)
                            if team_key not in gm:
                                break
                            for pl in gm[team_key]:
                                if "name" not in pl:
                                    continue
                                if pl["name"] == name:
                                    found_side = True
                                    break
                            if team_i == 1: break
                            team_i += 1
                        if not found_side:
                            # err_pr("Failed to find side for " + name + \
                            #        " in recent game: " + str(gm))
                            # Set randomly since we can't know ground truth
                            team_i = np.random.randint(2)
                        queue_id_ = rito_queue_ids["RANKED_FLEX_SR"] if \
                            gm["type"][0] == 'F' \
                            else rito_queue_ids["RANKED_SOLO_5x5"]
                        ts = gm["timestamp"] * 1000
                        
                        gm_d = OrderedDict(zip(X_c_prev_labels_all,
                            [None] * X_c_prev_labels_all_len))

                        gm_d["queue"] = queue_codes[queue_id_] if \
                                code_queues_rec else queue_id_
                        gm_d["duration"] = (int(duration_[0]) * 60) + \
                                int(duration_[1][:-1])
                        gm_d["win"] = int(gm["result"] == "Victory")
                        gm_d["champion"] = champ_codes[cid_] if \
                                code_champs_rec else cid_
                        gm_d["team_side"] = team_i
                        gm_d["spell_1"] = spell_codes[spell_1_] if \
                                code_summs_rec else spell_1_
                        gm_d["spell_2"] = spell_codes[spell_2_] if \
                                code_summs_rec else spell_2_
                        gm_d["kills"] = gm["kills"]
                        gm_d["deaths"] = gm["deaths"]
                        gm_d["assists"] = gm["assists"]
                        gm_d["creep_score"] = gm["cs"]
                        # gm_d["creep_score_per_second"] = gm["csps"]
                        gm_d["kill_participation"] = gm["killParticipation"]
                        gm_d["level"] = gm["level"]
                        gm_d["timestamp"] = ts
                        gm_d["time_of_day"] = ((ts / 1000) + region_timezones[opgg_region]) % (60 * 60 * 24)
                        gm_d["pinks_purchased"] = gm["pinksPurchased"]

                        retrospect_pcs(gm, cs)
                        
                        role_ = "jungle" if (spell_1_ == smite_id or \
                            spell_2_ == smite_id) else None
                        ch, op_ch, ch_cs = get_champ_stats(cs, chs_cache, "",
                            cid_, opgg_region, int_tier, div, side,
                            role, role_, filler, True, ts, ts, recent_i=gm_i)
                        pre = "opgg_champion_"
                        for k in ch:
                            gm_d[pre + k] = ch[k]
                        
                        # Add op.gg global champion statistics
                        for per in opgg_periods:
                            pre = op_ch_pre + per + '_'
                            gm_d[pre + "games"] = op_ch[per]["games"]
                            gm_d[pre+"games_fraction"]=op_ch[per]["gamesFrac"]
                            gm_d[pre + "win_rate"] = op_ch[per]["winrate"]
                            gm_d[pre + "kda_ratio"] = op_ch[per]["kda"]
                            gm_d[pre + "gold"] = op_ch[per]["gold"]
                            gm_d[pre + "creep_score"] = op_ch[per]["cs"]

                        # Add Champion.gg global champion statistics
                        ch_k, ch_d = ch_cs["kills"], ch_cs["deaths"]
                        ch_a = ch_cs["assists"]
                        ch_kda = (ch_k + ch_a) / (ch_d)
                        ch_cd = ch_cs["damageComposition"]
                        pre = ch_ch_pre
                        gm_d[pre + "games"] = ch_cs["gamesPlayed"]
                        gm_d[pre + "win_rate"] = ch_cs["winRate"]
                        gm_d[pre + "play_rate"] = ch_cs["playRate"]
                        gm_d[pre + "percent_role_played"] = \
                            ch_cs["percentRolePlayed"]
                        gm_d[pre + "kills"] = ch_k
                        gm_d[pre + "deaths"] = ch_d
                        gm_d[pre + "assists"] = ch_a
                        gm_d[pre + "kda_ratio"] = ch_kda
                        gm_d[pre + "kill_sprees"] = ch_cs["killingSprees"]
                        gm_d[pre + "largest_kill_spree"] = \
                            ch_cs["largestKillingSpree"]
                        gm_d[pre + "creep_score"] = ch_cs["minionsKilled"]
                        gm_d[pre + "gold"] = ch_cs["goldEarned"]
                        gm_d[pre + "total_damage_taken"] = \
                            ch_cs["totalDamageTaken"]
                        gm_d[pre + "total_heal"] = ch_cs["totalHeal"]
                        gm_d[pre + "wards_placed"] = ch_cs["wardPlaced"]
                        gm_d[pre + "wards_killed"] = ch_cs["wardsKilled"]
                        gm_d[pre + "total_damage"] = ch_cd["total"]
                        gm_d[pre + "total_magic_damage"] =ch_cd["totalMagical"]
                        gm_d[pre + "total_physical_damage"] = \
                            ch_cd["totalPhysical"]
                        gm_d[pre + "total_true_damage"] = ch_cd["totalTrue"]
                        gm_d[pre + "percent_magic_damage"] = \
                            ch_cd["percentMagical"]
                        gm_d[pre + "percent_physical_damage"] = \
                            ch_cd["percentPhysical"]
                        gm_d[pre + "percent_true_damage"] =ch_cd["percentTrue"]
                        gm_d[pre + "jungle_creep_score_team"] = \
                            ch_cs["neutralMinionsKilledTeamJungle"]
                        gm_d[pre + "jungle_creep_score_enemy"] = \
                            ch_cs["neutralMinionsKilledEnemyJungle"]

                        # Add Champion.gg wins by match length data
                        ch_ml = ch_cs["winsByMatchLength"]
                        ml_games = ch_cs["ml_games"]
                        if ml_games == 0:
                            ml_games = 1
                        for k in ml_dict:
                            dat = ch_ml[ml_dict[k]]
                            count = dat["count"]
                            pre = ml_l + k + '_'
                            gm_d[pre + "games"] = count
                            gm_d[pre + "games_fraction"] = count / ml_games
                            gm_d[pre + "win_rate"] = dat["winRate"]

                        # Add Champion.gg wins by matches played data
                        # ch_mp = ch_cs["winsByMatchesPlayed"]
                        # mp_games = ch_cs["mp_games"]
                        # mp_players = ch_cs["mp_players"]
                        # for k in mp_dict:
                        #     dat = ch_mp[mp_dict[k]]
                        #     count = dat["gamesPlayed"]
                        #     p_count = dat["players"]
                        #     pre = roll + '_' + mp_l + k + '_'
                        #     x[pre + "games"] = count
                        #     x[pre + "games_fraction"] = count / mp_games
                        #     x[pre + "players"] = p_count
                        #     x[pre + "players_fraction"] = p_count/mp_players
                        #     x[pre + "wins"] = dat["wins"]
                        #     x[pre + "win_rate"] = dat["winRate"]
                        
                        # Debug
                        # for k in gm_d:
                        #     if gm_d[k] is None:
                        #         pr_fl(game_i, i, gm_i, "Recent None:", k)
                        
                        if conv:
                            gm_d = np.hstack(list(gm_d.values()))

                        x_rec_.append(gm_d)
                        gm_i += 1
                x_rec_ = x_rec_[::-1]

                if conv:
                    x_rec_ = np.vstack(x_rec_) if len(x_rec_) > 0 else np.array([])

                x_rec[role_is[r]] = x_rec_

                if code_champs:
                    x[r + "_champion"] = champion_code
                if code_summs:
                    x[r + "_spell_1"] = spell_1_code
                    x[r + "_spell_2"] = spell_2_code

            for key in c_meta_labels_all:
                x[key] = meta[key]
            
            # Debug
            # for k in x:
            #     if x[k] is None and "_opgg_rs_" not in k \
            #                     and "_opgg_champion_" not in k:
            #         pr_fl(game_i, "None:", k)

        except Exception as e:
            pr_fl("Error loading game " + g_id + ":")
            # pr_fl("Error loading json file [" + name + "]:")
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl(type(e))
            pr_fl(e)

            # raise Exception

            # Remove fill flags for this game
            new_len = len(filler["flags"])
            for flag_i in range(len(filler["flags"]))[::-1]:
                if filler["flags"][flag_i][0] == game_i:
                    new_len = flag_i
                else:
                    break
            del filler["flags"][new_len:]
            # filler["flags"] = filler["flags"][:new_len]

            # Recent games fill flags
            new_len = len(filler["rec_flags"])
            for flag_i in range(len(filler["rec_flags"]))[::-1]:
                if filler["rec_flags"][flag_i][0] == game_i:
                    new_len = flag_i
                else:
                    break
            del filler["rec_flags"][new_len:]
            # filler["rec_flags"] = filler["rec_flags"][:new_len]

            game_i -= 1
            game_error = True

        if not game_error:
            if n_occ is not None:
                j = -1
                for acc_id in meta["acc_ids"]:
                    j += 1
                    id_ = str(region) + '_' + str(acc_id)
                    if n_occ == 'temporal':
                        pl_seen[id_] = (((x_rec[j][-1][timestamp_rli] / 1000) - x_rec[j][-1][duration_rli]) if len(x_rec[j]) > 0 \
                            else (meta["timestamp"] / 1000.0)) - 30.0
                    else:
                        pl_seen[id_] += 1

            if conv:
                x = np.hstack(list(x.values()))
                yc = np.hstack(list(yc.values()))
                yr = np.hstack(list(yr.values()))

                metadata_to_arr(meta)
                meta = np.hstack(list(meta.values()))

                Meta[game_i] = meta
                X[game_i] = x
                Yc[game_i] = yc
                Yr[game_i] = yr
                X_rec.append(x_rec)

            else:
                xs.append((meta, x, x_rec, yc, yr))

            filler["seen_pl"] |= set(names)

        game_i += 1
        filler["game_i"] = game_i

        frac_done = n_tried / target_n_
        time_taken = time.time() - start_time
        frac_per_sec = frac_done / time_taken
        time_remaining = (1 - frac_done) / frac_per_sec
        mins_remaining = int(np.floor(time_remaining / 60))
        secs_reamining = int(time_remaining - (mins_remaining * 60))
        sys_print("\rLoaded game " + str(game_i) + '/' + targ_str + \
            " (" + str(n_tried) + " tried), " + str(mins_remaining) + "m " + \
            str(secs_reamining) + "s remaining      skipped, too_recent: " + str(skipped_count) + ', ' + str(recent_count) + '          ')

        if game_i >= target_n or iteration_i + 1 >= len(games_list_sorted):
            if not add_newest:
                break
            # Check if any new games have been added since launch, and load them
            # adding_newest = True
            filelist = glob.glob(cj_prefix + '/*')
            gid_list = [fn[cj_st:-json_l] for fn in filelist]
            c_gid_set = set(gid_list)

            fresh_gids = c_gid_set - gid_set
            if len(fresh_gids) < 1: # If no news, finalise
                break
        
            # Add csv entries if necessary
            gs, _ = load_current_data(make_r_conds())
            csv_list = list(gs.index)
            csv_set = set(csv_list)
            new_gids = list(c_gid_set - csv_set)
            gs = add_new_jsons(new_gids, gs)
            games_list_sorted = list(fresh_gids)
            gid_set |= fresh_gids
            target_n += len(games_list_sorted)
            iteration_i = -1

            # else:
            #     if iteration_i + 1 == len(games_list_sorted):
            #         break # No more fresh games; finalise

        # Save progress every save_every samples
        if save and not game_error and game_i >= min_save_n and game_i % save_every == 0:

            if conv and game_i < X.shape[0]:
                Meta_ = Meta[:game_i]
                X_ = X[:game_i]
                Yc_ = Yc[:game_i]
                Yr_ = Yr[:game_i]
            else:
                Meta_, X_, Yc_, Yr_ = Meta, X, Yc, Yr
            res = [Meta_, X_, X_rec, Yc_, Yr_] if conv else zip(*xs)
            if return_labels:
                res.append([meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels])
            if save_dir != "learning_data":
                save_ld(res, save_dir + filename + '_unf.data', pad=False)
            else:
                save_ld(res, filename + '_unf')
            # X_rec_ = deepcopy(X_rec)

            # filler_s = deepcopy(filler)
            orig_seen_pl = filler["seen_pl"]
            filler["seen_pl"] = list(orig_seen_pl)
            if save_dir != "learning_data":
                save_json(filler, save_dir + infill_data_dir + "infill_data_unf_" + str(int(time.time())) + '.json', pad=False)
            else:
                save_json(filler, infill_data_dir + "infill_data_unf_" + str(int(time.time())))
            # save_json(filler, infill_data_dir + str(time.time()))
            filler["seen_pl"] = orig_seen_pl

            # fill = deepcopy(filler["fill"])
            fill = filler["fill"]
            pr_fl("   Infilling before save...")
            # fl_i, max_fl = 0, str(len(fill))
            # for k in fill:
            #     lib = fill[k]
            #     if "_name" in lib:
            #         fkey = lib["_name"]
            #         fkey_vs1 = filler_keys[fkey]
            #         for v1 in fkey_vs1:
            #             lib1 = lib[v1]
            #             if "_name" in lib1:
            #                 fkey = lib1["_name"]
            #                 if fkey not in filler_keys.keys():
            #                     pr_fl()
            #                     pr_fl(v1, fkey, filler_keys.keys())

            #                 fkey_vs2 = filler_keys[fkey]
            #                 for v2 in fkey_vs2:
            #                     if v2 not in lib1:
            #                         pr_fl()
            #                         pr_fl(v1, fkey, v2, lib1.keys())
            #                     lib2 = lib1[v2]
            #                     if "_name" in lib2:
            #                         fkey = lib2["_name"]
            #                         fkey_vs3 = filler_keys[fkey]
            #                         for v3 in fkey_vs3:
            #                             lib3 = lib2[v3]
            #                             lib3["_data"] = np.mean(lib3["_data"])
            #                     else:
            #                         lib2["_data"] = np.mean(lib2["_data"])
            #             else:
            #                 lib1["_data"] = np.mean(lib1["_data"])
            #     else:
            #         lib["_data"] = np.mean(lib["_data"])
            #     fl_i += 1

            fl_i, max_fl = 0, str(len(filler["flags"]))
            for flag in filler["flags"]:
                val = fill
                for key in flag[3:]:

                    # if key not in val:
                    #     pr_fl(flag, key, val.keys())
                    #     if isinstance(key, int): # If we don't yet have data for master+
                    #         if key >= 25:
                    #             key = 24
                    #         if key not in val:
                    #             if key >= 24:
                    #                 key = 23


                    if isinstance(val, dict) and key not in val:

                        if str(key) in val:
                            key = str(key)
                        else:

                            if val["_name"] == "div": # If we don't yet have data for master+
                                key = int(key)
                                if key >= 25:
                                    key = 24
                                if key not in val and str(key) not in val:
                                    if key >= 24:
                                        key = 23
                                if key not in val:
                                    key = str(key)

                        # if val["_name"] == "cid": # New champion - use Ezreal's data
                        #     key = champ_dict["Ezreal"]
                        #     if key not in val:
                        #         key = str(key)

                        if key not in val:
                            if val['_name'] == 'cid':
                                if key == champ_dict["Sett"]:
                                    key = champ_dict["Jax"]
                                elif key == champ_dict["Senna"]:
                                    key = champ_dict["Pyke"]
                                elif key == champ_dict["Aphelios"]:
                                    key = champ_dict["Jhin"]
                        if key not in val and str(key) in val:
                            key = str(key)

                        if key not in val:
                            pr_fl("Infill error:", flag, key, val.keys(), val["_name"])




                    val = val[key]
                if conv:
                    X_[flag[0], X_label_is[flag[2]]] = val["_data"]
                else:
                    xs[flag[0]][1][flag[2]] = val["_data"]
                fl_i += 1
            for flag in filler["rec_flags"]:
                val = fill
                fkey_i = 4
                for key in flag[4:]:
                    fkey_i += 1

                    if isinstance(val, dict) and "_name" in val and val["_name"] == 'div' and fkey_i + 1 < len(flag) and "_name" in val[key] and \
                      val[key]["_name"] == 'cid' and flag[fkey_i + 1] not in val[key]:
                        pr_fl("Infill error, rotating elo:", flag, flag[fkey_i + 1], val[key].keys(), val[key]["_name"])
                        pr_fl("Old div:", key)
                        while flag[fkey_i + 1] not in val[key]:
                            key -= 1
                            if key < 0:
                                key = 26
                        pr_fl("New div:", key)

                    if isinstance(val, dict) and key not in val:

                        if str(key) in val:
                            key = str(key)
                        else:

                            if val["_name"] == "div": # If we don't yet have data for master+
                                key = int(key)
                                if key >= 25:
                                    key = 24
                                if key not in val and str(key) not in val:
                                    if key >= 24:
                                        key = 23
                                if key not in val:
                                    key = str(key)

                            # if val["_name"] == "cid": # New champion - use Ezreal's data
                            #     key = champ_dict["Ezreal"]
                            #     if key not in val:
                            #         key = str(key)

                            if key not in val:
                                if val['_name'] == 'cid':
                                    if key == champ_dict["Sett"]:
                                        key = champ_dict["Jax"]
                                    elif key == champ_dict["Senna"]:
                                        key = champ_dict["Pyke"]
                                    elif key == champ_dict["Aphelios"]:
                                        key = champ_dict["Jhin"]
                            if key not in val and str(key) in val:
                                key = str(key)

                            if key not in val:
                                pr_fl("Infill error:", flag, key, val.keys(), val["_name"])

                    val = val[key]
                if conv:
                    X_rec[flag[0]][flag[1]][flag[2], X_rec_label_is[flag[3]]] = \
                        val["_data"]
                else:
                    xs[flag[0]][2][flag[1]][flag[2]][flag[3]] = val["_data"]
                fl_i += 1

            res = [Meta_, X_, X_rec, Yc_, Yr_] if conv else zip(*xs)
            if return_labels:
                res.append([meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels])
            if save_dir != "learning_data":
                save_ld(res, save_dir + filename + '.data', pad=False)
            else:
                save_ld(res, filename)


    if conv and game_i < X.shape[0]:
        Meta = Meta[:game_i]
        X = X[:game_i]
        Yc = Yc[:game_i]
        Yr = Yr[:game_i]


    # Save filler data
    filler["seen_pl"] = list(filler["seen_pl"])
    if save_dir != "learning_data":
        save_json(filler, save_dir + infill_data_dir + "infill_data" + '.json', pad=False)
    else:
        save_json(filler, infill_data_dir + "infill_data")

    pr_fl("\nSaving unfilled data...")
    res = [Meta, X, X_rec, Yc, Yr] if conv else zip(*xs)
    if return_labels:
        res.append([meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels])
    if save_dir != "learning_data":
        save_ld(res, save_dir + filename + '_unf.data', pad=False)
    else:
        save_ld(res, filename + '_unf')

    # Fill missing data with avgs for all players
    fill = filler["fill"]
    # pr_fl("\nAveraging filler data...")
    # fl_i, max_fl = 0, str(len(fill))
    # for k in fill:
    #     lib = fill[k]
    #     if "_name" in lib:
    #         fkey = lib["_name"]
    #         fkey_vs1 = filler_keys[fkey]
    #         for v1 in fkey_vs1:
    #             lib1 = lib[v1]
    #             if "_name" in lib1:
    #                 fkey = lib1["_name"]
    #                 if fkey not in filler_keys.keys():
    #                     pr_fl()
    #                     pr_fl(v1, fkey, filler_keys.keys())

    #                 fkey_vs2 = filler_keys[fkey]
    #                 for v2 in fkey_vs2:
    #                     if v2 not in lib1:
    #                         pr_fl()
    #                         pr_fl(v1, fkey, v2, lib1.keys())
    #                     lib2 = lib1[v2]
    #                     if "_name" in lib2:
    #                         fkey = lib2["_name"]
    #                         fkey_vs3 = filler_keys[fkey]
    #                         for v3 in fkey_vs3:
    #                             lib3 = lib2[v3]
    #                             lib3["_data"] = np.mean(lib3["_data"])
    #                     else:
    #                         lib2["_data"] = np.mean(lib2["_data"])
    #             else:
    #                 lib1["_data"] = np.mean(lib1["_data"])
    #     else:
    #         lib["_data"] = np.mean(lib["_data"])
    #     fl_i += 1
    #     sys_print("\rFill value # " + str(fl_i) + '/' + max_fl)
    pr_fl("\nFilling missing data...")
    fl_i, max_fl = 0, str(len(filler["flags"]))
    for flag in filler["flags"]:
        val = fill
        for key in flag[3:]:

            if isinstance(val, dict) and key not in val:

                if str(key) in val:
                    key = str(key)
                else:

                    if val["_name"] == "div": # If we don't yet have data for master+
                        key = int(key)
                        if key >= 25:
                            key = 24
                        if key not in val and str(key) not in val:
                            if key >= 24:
                                key = 23
                        if key not in val:
                            key = str(key)

                    # if val["_name"] == "cid": # New champion - use Ezreal's data
                    #     key = champ_dict["Ezreal"]
                    #     if key not in val:
                    #         key = str(key)

                    if key not in val:
                        if val['_name'] == 'cid':
                            if key == champ_dict["Sett"]:
                                key = champ_dict["Jax"]
                            elif key == champ_dict["Senna"]:
                                key = champ_dict["Pyke"]
                            elif key == champ_dict["Aphelios"]:
                                key = champ_dict["Jhin"]
                    if key not in val and str(key) in val:
                        key = str(key)

                    if key not in val:
                        pr_fl("Infill error:", flag, key, val.keys(), val["_name"])

            val = val[key]
        if conv:
            X[flag[0], X_label_is[flag[2]]] = val["_data"]
        else:
            xs[flag[0]][1][flag[2]] = val["_data"]
        fl_i += 1
        if fl_i % 1000 == 0:
            sys_print("\rFill flag # " + str(fl_i) + '/' + max_fl)
    sys_print("\rFill flag # " + max_fl + '/' + max_fl)
    pr_fl("\nFilling missing data for recent matches...")
    fl_i, max_fl = 0, str(len(filler["rec_flags"]))
    for flag in filler["rec_flags"]:
        val = fill
        fkey_i = 4
        for key in flag[4:]:
            fkey_i += 1

            if isinstance(val, dict) and "_name" in val and val["_name"] == 'div' and fkey_i + 1 < len(flag) and "_name" in val[key] and \
              val[key]["_name"] == 'cid' and flag[fkey_i + 1] not in val[key]:
                pr_fl("Infill error, rotating elo:", flag, flag[fkey_i + 1], val[key].keys(), val[key]["_name"])
                pr_fl("Old div:", key)
                while flag[fkey_i + 1] not in val[key]:
                    key -= 1
                    if key < 0:
                        key = 26
                pr_fl("New div:", key)

            if isinstance(val, dict) and key not in val:

                if str(key) in val:
                    key = str(key)
                else:

                    if val["_name"] == "div": # If we don't yet have data for master+
                        key = int(key)
                        if key >= 25:
                            key = 24
                        if key not in val and str(key) not in val:
                            if key >= 24:
                                key = 23
                        if key not in val:
                            key = str(key)

                    # if val["_name"] == "cid": # New champion - use Ezreal's data
                    #     key = champ_dict["Ezreal"]
                    #     if key not in val:
                    #         key = str(key)

                    if key not in val:
                        if val['_name'] == 'cid':
                            if key == champ_dict["Sett"]:
                                key = champ_dict["Jax"]
                            elif key == champ_dict["Senna"]:
                                key = champ_dict["Pyke"]
                            elif key == champ_dict["Aphelios"]:
                                key = champ_dict["Jhin"]
                    if key not in val and str(key) in val:
                        key = str(key)

                    if key not in val:
                        pr_fl("Infill error:", flag, key, val.keys(), val["_name"])

            val = val[key]
        if conv:
            X_rec[flag[0]][flag[1]][flag[2], X_rec_label_is[flag[3]]] = \
                val["_data"]
        else:
            xs[flag[0]][2][flag[1]][flag[2]][flag[3]] = val["_data"]
        fl_i += 1
        if fl_i % 100 == 0:
            sys_print("\rFill flag # " + str(fl_i) + '/' + max_fl)
    sys_print("\rFill flag # " + max_fl + '/' + max_fl)
 
    pr_fl("\nFinished loading games!")
    res = [Meta, X, X_rec, Yc, Yr] if conv else zip(*xs)
    if return_labels:
        res.append([meta_labels,X_labels,X_rec_labels,Yc_labels,Yr_labels])
    if save_dir != "learning_data":
        save_ld(res, save_dir + filename + '.data', pad=False)
    else:
        save_ld(res, filename)
    return res


# Loads Rito games jsons from a list of folders (s stands for simple)
def load_games_s(ids_seen=None, n_occ=1, code_champs=True, code_summs=True, code_pick_order=True,
  conv=True, conv_meta=False, N=sys.maxsize, dataframe=False, folders=None):
    if dataframe:
        conv = False
    if folders is None:
        folders = [ data_dir + init_games_dir ]
        folders.append(x.replace('\\', '/') for x in sorted(glob.glob(data_dir + \
                    "/archive*/" + init_games_dir)))
    if ids_seen is not None:
        if type(ids_seen) is list:
            ids_seen = set(ids_seen)
        seen_count = defaultdict(int)

    meta, X, Yc, Yr = [None] * 4
    flat_keys = None
    one_occ = n_occ == 1
    game_jsons = sum([sorted(glob.glob(f + '/*.json')) for f in folders], [])
    n_jsons = len(game_jsons)
    pr_fl("Total number of game jsons: " + str(n_jsons))
    np.random.shuffle(game_jsons)
    target_n = min(N, n_jsons)
    xs, j, targ_str = [], 0, str(target_n)
    for i in range(n_jsons):
        game = load_json(game_jsons[i], pad=False)
        if game is None:
            continue
        x_ = get_game_data(game, ids_seen=ids_seen, code_champs=code_champs,
            code_summs=code_summs, conv=conv, code_pick_order=code_pick_order)
        if x_ is None:
            continue
        m, x, yc, yr = x_
        region = m["region_id"]
        a_ids = [region + '_' + str(acc_id) for acc_id in m["acc_ids"]]
        if ids_seen is not None:
            if one_occ:
                ids_seen |= set(a_ids)
            else:
                for id_ in a_ids:
                    seen_count[id_] += 1
                    if seen_count[id_] >= n_occ:
                        ids_seen.add(id_)

        if dataframe:
            g_id = region + '_' + str(m["game_id"])

            if flat_keys is None:
                flat_keys = flat_od_keys(m), flat_od_keys(x), \
                            flat_od_keys(yc), flat_od_keys(yr)

            meta = df_add(meta,[flat_od(m,keys=flat_keys[0])],[g_id],'g_id')
            X = df_add(X, [flat_od(x, keys=flat_keys[1])], [g_id], 'g_id')
            Yc = df_add(Yc, [flat_od(yc, keys=flat_keys[2])], [g_id], 'g_id')
            Yr = df_add(Yr, [flat_od(yr, keys=flat_keys[3])], [g_id], 'g_id')

        elif conv:
            if X is None:
                X = np.zeros((target_n, len(x)))
                Yc = np.zeros((target_n, len(yc)))
                Yr = np.zeros((target_n, len(yr)))

            if conv_meta:
                # Convert metadata to array format
                metadata_to_arr(m)
                m = np.hstack(m.values())

                if meta is None:
                    meta = np.zeros((target_n, len(m)))
                meta[j] = m
            else:
                if meta is None:
                    meta = []
                meta.append(m)

            X[j] = x
            Yc[j] = yc
            Yr[j] = yr

        else:
            xs.append(x_)

        j += 1
        if j % 10 == 0:
            sys_print("\rLoaded json " + str(j) + '/' + targ_str + \
                " (" + str(i) + " tried) ")
        if j >= N:
            break

    if conv:
        if conv_meta:
            meta = meta[:j]
        return meta, X[:j], Yc[:j], Yr[:j]
    
    if dataframe or conv:
        return meta, X, Yc, Yr
    
    return zip(*xs)


# Get game data from a Rito game json
def get_game_data(game, ids_seen=None, code_champs=True, code_summs=True, code_pick_order=False,
 conv=True, int_tiers=None,
 op_gcs_ts=None, ch_gcs_ts=None, ch_gms_ts=None):

    # Check we are on the correct map etc
    if game["queueId"] not in r_queue_ids:
        err_pr("Wrong queue: " + queue_names[game["queueId"]])
        return None, False
    if game["mapId"] != sr_map_id:
        err_pr("Wrong map id: " + str(game["mapId"]))
        return None, False
    if len(game["participants"]) < N_PL:
        err_pr("Not enough participants: " + str(len(game["participants"])))
        return None, False
    pis = game[g_pis]
    ps = [pi["player"] for pi in pis]
    acc_ids = [hash_int(p[g_acc_id]) for p in ps]
    if ids_seen is not None:
        for id_ in acc_ids:
            if id_ in ids_seen:
                err_pr("Player already seen: " + str(id_))
                return None, False

    # Setup ordered dicts
    meta = OrderedDict(zip(r_meta_labels_all, [None] * r_meta_labels_all_len))
    x = OrderedDict(zip(X_r_labels_all, [None] * X_r_labels_all_len))
    yc = OrderedDict(zip(Yc_labels_all, [None] * Yc_labels_all_len))
    yr = OrderedDict(zip(Yr_labels_all, [None] * Yr_labels_all_len))

    # Get metadata
    meta["game_id"] = game["gameId"]
    meta["region_id"] = region = game["platformId"]
    meta["queue_id"] = queue_id = game["queueId"]
    meta["map_id"] = game["mapId"]
    meta["season_id"] = season_id = game["seasonId"]
    meta["game_version"] = game_version = game["gameVersion"]
    meta["timestamp"] = timestamp = game["gameCreation"]
    meta["duration"] = duration = game["gameDuration"]

    opgg_region = opgg_region_dict[region]
    meta["time_of_day"] = ((timestamp / 1000) + region_timezones[opgg_region]) % (60 * 60 * 24)

    if timestamp < 1534636800000: # Games older than this are from an old crawler bug
        return None, False
        # pr_fl()
        # pr_fl(game_version, \
        #     datetime.utcfromtimestamp(timestamp/1000).strftime('%Y-%m-%d %H:%M:%S'))

    if duration < 60 * 10: # If game is a remake (<10 mins long) (old crawler bug)
        return None, False

    x["patch"] = patch_codes['.'.join(game_version.split('.')[:2])]
    x["season"] = season_codes[season_id]
    x["region"] = region_codes[region]
    x["queue"] = queue_codes[queue_id]

    # Get game elos
    names = [p[g_name] for p in ps]
    pts = dict([(pt["participantId"], pt) for pt in game["participants"]])
    pts = [pts[pi["participantId"]] for pi in pis]
    hASTs = [pt["highestAchievedSeasonTier"].lower() if "highestAchievedSeasonTier" in pt else "unranked" for pt in pts]
    hASTns = [leag_tier_is[n] for n in hASTs if n[0] != 'u']
    all_unranked_hAST = False
    if hASTns == []:
        # err_pr("No hASTns - all players in game unranked... assuming bronze (very wrong)")
        # return None, False
        # err_pr("All players unranked..?")
        hASTns = [leag_tier_is["bronze"]]
        all_unranked_hAST = True
    median_hAST = int(np.ceil(np.median(hASTns)))
    mean_hAST = np.mean(hASTns)
    meta["mean_hAST"] = mean_hAST

    # Get Y data
    r_teams = game["teams"]
    b = int(r_teams[0]["teamId"] != blue_team_id)
    blue_team, red_team = r_teams[b], r_teams[1 - b]
    yc["blue_win"] = int(blue_team["win"][0] == 'W')
    for i in Yc_team_labels_range:
        l, dl = Yc_team_labels[i], Yc_team_labels_deconved[i]
        yc["blue_" + l] = int(blue_team[dl])
        yc["red_" + l] = int(red_team[dl])
    # Set timestamps for stats if None
    big_timestamp = timestamp * 1000
    if op_gcs_ts is None: op_gcs_ts = big_timestamp
    if ch_gcs_ts is None: ch_gcs_ts = big_timestamp
    if ch_gms_ts is None: ch_gms_ts = big_timestamp

    # Get summoner X and Y data
    acc_ids_o, names_o = [None] * N_PL, [None] * N_PL
    cids, roles_taken, unsided_roles, sides = [], [], [], []
    hAST_ints, return_none = [], False
    adc_cids, support_cids = {}, {}

    # First, get player roles
    side_err = {s: False for s in cols_all}
    roles_taken_s = {s: [] for s in cols_all}
    roles_taken_pt = {}
    err_roles = {s: [] for s in cols_all}
    # Get roles from riot timeline labels, if they make sense, else note errors
    # First get smites status
    team_smites = {s: 0 for s in cols_all}
    has_smites = {i: False for i in N_PL_r}
    for pt_i in range(len(pts)):
        pt = pts[pt_i]
        side = "blue" if pt["teamId"] == blue_team_id else "red"
        sp1, sp2 = pt["spell1Id"], pt["spell2Id"]
        if spells_lib[sp1] == "smite" or spells_lib[sp2] == "smite":
            team_smites[side] += 1
            has_smites[pt_i] = True
    for pt_i in range(len(pts)):
        pt = pts[pt_i]
        side = "blue" if pt["teamId"] == blue_team_id else "red"
        
        r_taken = roles_taken_s[side]
        timeline = pt["timeline"]
        lane, role = timeline["lane"], timeline["role"]
        cid = pt["championId"]
        
        r = None
        if (has_smites[pt_i] and team_smites[side] < 2) and "jungle" not in r_taken:
              # (lane == "JUNGLE" and role == "NONE")) : # can be more than 1 "jungler" from this
            r = "jungle"
        elif lane == "TOP" and role == "SOLO" and "top" not in r_taken:
            r = "top"
        elif lane == "MIDDLE" and role == "SOLO" and "middle" not in r_taken:
            r = "middle"
        elif lane == "BOTTOM" and role == "DUO_SUPPORT" and "support" not in r_taken:
            r = "support"
        elif lane == "BOTTOM" and role == "DUO_CARRY" and "adc" not in r_taken:
            r = "adc"
        else:
            side_err[side] = True
            err_roles[side].append({"pt_i": pt_i,
                                    # "lane": lane,
                                    # "role": role,
                                    "cid": cid})
            continue
        
        r_taken.append(r)
        roles_taken_pt[pt_i] = r

    for side in cols_all:
        if side_err[side]:
            err_rs = err_roles[side]
            r_taken =  roles_taken_s[side]

            if len(err_rs) == 1:
                for r in roles_all:
                    if r not in r_taken:
                        r_taken.append(r)
                        roles_taken_pt[err_rs[0]["pt_i"]] = r
                        break
            
            elif len(err_rs) == 2:
                found_r = None
                for r in role_fill_order:
                    if r not in r_taken:
                        if found_r is None:
                            order = role_champ_orderings[r]
                            er_i = int(order.index(err_rs[1]['cid']) < \
                                        order.index(err_rs[0]['cid']))
                            r_taken.append(r)
                            roles_taken_pt[err_rs[er_i]['pt_i']] = r
                            found_r = 1 - er_i
                        else:
                            r_taken.append(r)
                            roles_taken_pt[err_rs[found_r]['pt_i']] = r
                        if len(r_taken) == 5:
                            break
            
            else: # TODO: implement a smarter algo here
                for r in role_fill_order:
                    if r not in r_taken:
                        er_i = 0
                        if len(r_taken) < 4:
                            order = role_champ_orderings[r]
                            er_i = sorted(map(lambda er_i: (order.index(err_rs[
                                er_i]['cid']), er_i), range(len(err_rs))))[0][1]
                        r_taken.append(r)
                        roles_taken_pt[err_rs[er_i]['pt_i']] = r
                        if len(r_taken) == 5:
                            break
                        del err_rs[er_i]

    for pt_i in range(len(pts)):
        pt = pts[pt_i]

        cid = pt["championId"]
        side = "blue" if pt["teamId"] == blue_team_id else "red"

        role = roles_taken_pt[pt_i]
        roll = side + '_' + role

        # Add role data
        # if pt["timeline"]["lane"] == "TOP":
        #     role = "top"
        # elif pt["timeline"]["lane"] == "JUNGLE":
        #     role = "jungle"
        # elif pt["timeline"]["lane"] == "MIDDLE":
        #     role = "middle"
        # elif pt["timeline"]["role"] == "DUO_SUPPORT":
        # # elif pt["timeline"]["role"] == "DUO_SUPPORT" or cid in supp_cids:
        #     role = "support"
        # else:
        #     role = "adc"

        # roll = side + '_' + role
        # if roll in roles_taken:
        #     pr_fl()
        #     pr_fl()
        #     red_rolez = []
        #     for pt_ in pts:
        #         rz = str((champion_names[pt_["championId"]], \
        #             pt_["timeline"]["lane"], pt_["timeline"]["role"]))
        #         if pt_["teamId"] == blue_team_id:
        #             pr_fl(rz)
        #         else:
        #             red_rolez += [rz]
        #     pr_fl(median_hAST)
        #     pr_fl('\n'.join(red_rolez))
        #     # err_pr("Duplicate role: " + roll + ", guessing...")
        #     # TODO: figure out the most likely roles in a smarter way, label the datapoints
        #     # try identifying the missing role(s) and randomly reassigning the duplicates
        #     if guess_roles:
        #         if role == "middle" and (side + '_' + "top") not in roles_taken:
        #             role = "top"
        #         elif role == "top" and (side + '_' + "middle") not in roles_taken:
        #             role = "middle"
        #         elif role == "adc" and (side + '_' + "support") not in roles_taken:
        #             role = "support"
        #         elif role == "support" and (side + '_' + "adc") not in roles_taken:
        #             role = "adc"
        #         elif role == "jungle" and (side + '_' + "top") not in roles_taken:
        #             role = "top"
        #         else:
        #             return_none = True
        #             # err_pr("Duplicate role: " + roll + ", ...\n" + str(roles_taken))
        #             break
        #         roll = side + '_' + role
        #     else:
        #         return_none = True
        #         break

        acc_ids_o[role_is[roll]] = acc_ids[pt_i]
        names_o[role_is[roll]] = names[pt_i]
        roles_taken.append(roll)
        unsided_roles.append(role)
        sides.append(side)

        # Add pick order data
        pick_pos = pick_order_map[pt_i + 1]
        x[roll + "_pick_position"] = pick_order_codes[pick_pos] if code_pick_order else pick_pos

        # Add champion data
        cids.append(cid)
        x[roll + "_champion"] = champ_codes[cid] if code_champs else cid

        if role == 'adc':       adc_cids[side] = cid
        elif role == "support": support_cids[side] = cid

        # Add spells data
        sp1, sp2 = pt["spell1Id"], pt["spell2Id"]
        if spell_order[sp1] < spell_order[sp2]: sp1, sp2 = sp2, sp1
        x[roll + "_spell_1"] = spell_codes[sp1] if code_summs else sp1
        x[roll + "_spell_2"] = spell_codes[sp2] if code_summs else sp2

        # Add highest achieved season tier
        hAST_s = pt["highestAchievedSeasonTier"].lower() if "highestAchievedSeasonTier" in pt else "unranked"
        hAST = mean_hAST if hAST_s[0] == 'u' else leag_tier_is[hAST_s]
        hAST_int = median_hAST if hAST_s[0] == 'u' else hAST
        if int_tiers is None: hAST_ints.append(hAST_int)
        x[roll + '_' + "highest_achieved_season_tier"] = hAST

        # Add player regression Y values (Damage dealt etc)
        for i in Yr_player_labels_range:
            l, dl = Yr_player_labels[i], Yr_player_labels_deconved[i]
            if dl[:len(timeline_label_tag)] == timeline_label_tag:
                dls = dl.split(':')
                # if dls[2] + "PerMinDeltas" not in pt["timeline"]:
                #     pr_fl( pt["timeline"])
                #     pr_fl( pt)
                #     pr_fl( region, meta["duration"])
                # if dls[1] not in pt["timeline"][dls[2] + "PerMinDeltas"]:
                #     pr_fl( pt["timeline"][dls[2] + "PerMinDeltas"])
                yr[roll + '_' + l] = pt["timeline"][dls[2] + "PerMinDeltas"][dls[1]] * 10
            else:
                yr[roll + '_' + l] = pt["stats"][dl]

        # Add op.gg global champion statistics
        for period in opgg_periods:
            stats = get_opgg_gcs(op_gcs_ts, cid, opgg_region,
                hAST_int, period, 'win')
            pre = roll + '_' + op_ch_pre + period + '_'
            x[pre + "games"] = stats["games"]
            x[pre + "games_fraction"] = stats["gamesFrac"]
            x[pre + "win_rate"] = stats["winrate"]
            x[pre + "kda_ratio"] = stats["kda"]
            x[pre + "gold"] = stats["gold"]
            x[pre + "creep_score"] = stats["cs"]

        # Add Champion.gg global champion statistics
        ch_cs = get_chgg_gcs(ch_gcs_ts, cid, hAST_int, role)
        ch_k, ch_d, ch_a = ch_cs["kills"],ch_cs["deaths"],ch_cs["assists"]
        ch_kda = (ch_k + ch_a) / (ch_d)
        ch_cd = ch_cs["damageComposition"]
        pre = roll + '_' + ch_ch_pre
        x[pre + "games"] = ch_cs["gamesPlayed"]
        x[pre + "win_rate"] = ch_cs["winRate"]
        x[pre + "play_rate"] = ch_cs["playRate"]
        x[pre + "percent_role_played"] = ch_cs["percentRolePlayed"]
        x[pre + "kills"] = ch_k
        x[pre + "deaths"] = ch_d
        x[pre + "assists"] = ch_a
        x[pre + "kda_ratio"] = ch_kda
        x[pre + "kill_sprees"] = ch_cs["killingSprees"]
        x[pre + "largest_kill_spree"] = ch_cs["largestKillingSpree"]
        x[pre + "creep_score"] = ch_cs["minionsKilled"]
        x[pre + "gold"] = ch_cs["goldEarned"]
        x[pre + "total_damage_taken"] = ch_cs["totalDamageTaken"]
        x[pre + "total_heal"] = ch_cs["totalHeal"]
        x[pre + "wards_placed"] = ch_cs["wardPlaced"]
        x[pre + "wards_killed"] = ch_cs["wardsKilled"]
        x[pre + "total_damage"] = ch_cd["total"]
        x[pre + "total_magic_damage"] = ch_cd["totalMagical"]
        x[pre + "total_physical_damage"] = ch_cd["totalPhysical"]
        x[pre + "total_true_damage"] = ch_cd["totalTrue"]
        x[pre + "percent_magic_damage"] = ch_cd["percentMagical"]
        x[pre + "percent_physical_damage"] = ch_cd["percentPhysical"]
        x[pre + "percent_true_damage"] = ch_cd["percentTrue"]
        x[pre + "jungle_creep_score_team"] = \
            ch_cs["neutralMinionsKilledTeamJungle"]
        x[pre + "jungle_creep_score_enemy"] = \
            ch_cs["neutralMinionsKilledEnemyJungle"]

        # Add Champion.gg wins by match length data
        ch_ml = ch_cs["winsByMatchLength"]
        ml_games = ch_cs["ml_games"]
        if ml_games == 0:
            ml_games = 1
        for k in ml_dict:
            dat = ch_ml[ml_dict[k]]
            count = dat["count"]
            pre = roll + '_' + ml_l + k + '_'
            x[pre + "games"] = count
            x[pre + "games_fraction"] = count / ml_games
            x[pre + "win_rate"] = dat["winRate"]

        # Add Champion.gg wins by matches played data
        # ch_mp = ch_cs["winsByMatchesPlayed"]
        # mp_games = ch_cs["mp_games"]
        # mp_players = ch_cs["mp_players"]
        # for k in mp_dict:
        #     dat = ch_mp[mp_dict[k]]
        #     count = dat["gamesPlayed"]
        #     p_count = dat["players"]
        #     pre = roll + '_' + mp_l + k + '_'
        #     x[pre + "games"] = count
        #     x[pre + "games_fraction"] = count / mp_games
        #     x[pre + "players"] = p_count
        #     x[pre + "players_fraction"] = p_count / mp_players
        #     x[pre + "wins"] = dat["wins"]
        #     x[pre + "win_rate"] = dat["winRate"]

    if return_none: return None, False
    if int_tiers is None: int_tiers = hAST_ints

    # Add matchup info
    for i in N_PL_r:
        cid = cids[i]
        role = unsided_roles[i]
        col = "blue_" if sides[i] == "red" else "red_"
        cid_ = cids[roles_taken.index(col + role)]
        m_cs = get_chgg_gms(ch_gms_ts, cid, int_tiers[i], role, cid_)
        pre = roles_taken[i] + '_' + ch_m_pre
        add_matchup_ch(x, pre, m_cs)

    # Add ADC-Support Synergy etc
    for side in cols_all:
        cid, cid_ = adc_cids[side], support_cids[side]
        m_cs = get_chgg_gms(ch_gms_ts, cid, median_hAST, "synergy", cid_)
        pre = side + '_' + ch_m_pre + "synergy_adc" + '_'
        add_matchup_ch(x, pre, m_cs)
        m_cs = get_chgg_gms(ch_gms_ts, cid_, median_hAST, "synergy", cid)
        pre = side + '_' + ch_m_pre + "synergy_support" + '_'
        add_matchup_ch(x, pre, m_cs)

    for si in N_T_r:
        cid, cid_ = adc_cids[cols_all[si]], support_cids[cols_all[1 - si]]
        m_cs = get_chgg_gms(ch_gms_ts, cid, median_hAST, "adc_support", cid_)
        pre = cols_all[si] + '_' + ch_m_pre + "adc_support" + '_'
        add_matchup_ch(x, pre, m_cs)
        m_cs = get_chgg_gms(ch_gms_ts, cid_, median_hAST, "adc_support", cid)
        pre = cols_all[1 - si] + '_' + ch_m_pre + "support_adc" + '_'
        add_matchup_ch(x, pre, m_cs)

    meta["acc_ids"] = acc_ids_o
    meta["names"] = names_o
    meta["roles"] = roles_taken

    for l in X_meta_labels_r:
        x[l] = meta[l]

    if conv:
        x = np.hstack(x.values())
        yc = np.hstack(yc.values())
        yr = np.hstack(yr.values())

    return (meta, x, yc, yr), all_unranked_hAST


def add_matchup_ch(x, pre, m_cs):
    plyr = m_cs['u']
    x[pre + "games"] = m_cs["count"]
    x[pre + "wins"] = plyr["wins"]
    x[pre + "win_rate"] = plyr["winrate"]
    x[pre + "kills"] = plyr["kills"]
    x[pre + "deaths"] = plyr["deaths"]
    x[pre + "assists"] = plyr["assists"]
    x[pre + "gold"] = plyr["goldEarned"]
    x[pre + "creep_score"] = plyr["minionsKilled"]
    x[pre + "total_damage_dealt_to_champions"] = \
        plyr["totalDamageDealtToChampions"]
    x[pre + "kill_sprees"] = plyr["killingSprees"]
    x[pre + "jungle_creep_score_team"] = \
        plyr["neutralMinionsKilledTeamJungle"]
    x[pre + "weighed_score"] = plyr["weighedScore"]



def add_recent_match_features(X, X_rec, X_labels, X_rec_labels, verbose=True, copy_over_X_rec=False):
    X_labels_is = get_list_is_dict(X_labels)
    X_rec_labels_new = X_rec_labels + new_X_rec_labels

    # Add features to recent matches data (requires array format)
    len_str = ' / ' + str(len(X)) + '     '
    for game_i in range(len(X)):
        if verbose and game_i % 100 == 0:
            sys_print('\r' + str(game_i) + len_str)
        x = X[game_i]
        ts = x[X_labels_is["timestamp"]]
        if game_i >= len(X_rec) and copy_over_X_rec:
            X_rec = [X_rec[0]] * len(X)
        for i in range(len(X_rec[game_i])):
            gms = X_rec[game_i][i]
            # pr_fl(gms.shape, len(X_rec_labels))
            dicts = []
            for recent_i in range(len(gms)):
                gm = OrderedDict(zip(X_rec_labels, gms[recent_i]))

                # Calculate KDA ratios
                gm["kda_ratio"] = calculate_kda(gm["kills"], gm["deaths"],
                    gm["assists"])
                gm["opgg_champion_recent_kda_ratio"] = calculate_kda(
                    gm["opgg_champion_recent_kills"],
                    gm["opgg_champion_recent_deaths"], gm["opgg_champion_recent_assists"])
                gm["opgg_champion_season_kda_ratio"] = calculate_kda(
                    gm["opgg_champion_season_kills"],
                    gm["opgg_champion_season_deaths"],
                    gm["opgg_champion_season_assists"])

                # Calculate champion win rate
                opgg_games = gm["opgg_champion_recent_wins"] + \
                             gm["opgg_champion_recent_losses"]
                if opgg_games < 0.01:
                    gm["opgg_champion_recent_win_rate"] = 50
                else:
                    gm["opgg_champion_recent_win_rate"] = 100 * \
                        gm["opgg_champion_recent_wins"] / opgg_games
                
                # Calculate time since data point game
                gm["time_since_match"] = (ts - gm["timestamp"]) / 1000
                dicts.append(gm)

            X_rec[game_i][i] = np.array([list(gm.values()) for gm in dicts])
    if verbose:
        sys_print('\r' + str(len(X)) + len_str)
        pr_fl()
    if copy_over_X_rec:
        return X_rec_labels_new, X_rec
    return X_rec_labels_new

def get_sample_recavgs(x_rec, game_i, ns_avgs, c_1, c_2,
                       avg_game_duration_, rd_is, infill_data,
                       X_rec_l_is, X_rec_new_keys, n_X_rec_new_labels):

    # _, X_rec_labels_al, Xr_mins, _, _, _, _, X_rec_alphas, X_rec_betas, X_rec_gammas, \
    #      _, _, _, _, _ = \
    #     load_ld("model_logs/AutoLogTransf.data", pad=False)
    # # X_alphas_dict = dict(zip(X_labels_al, X_alphas))
    # # X_betas_dict = dict(zip(X_labels_al, X_betas))
    # # X_gammas_dict = dict(zip(X_labels_al, X_gammas))
    # X_rec_alphas_dict = [dict(zip(X_rec_labels_al, X_rec_alphas[i])) for i in range(5)]
    # X_rec_betas_dict = [dict(zip(X_rec_labels_al, X_rec_betas[i])) for i in range(5)]
    # X_rec_gammas_dict = [dict(zip(X_rec_labels_al, X_rec_gammas[i])) for i in range(5)]
    # Xr_mins_dict = [dict(zip(X_rec_labels_al, Xr_mins[i])) for i in range(5)]

#     expdec_15th = (1/15) * 1 / (1 + (np.e ** c_2) + (np.e ** c_4))
    flags = []
    filler = [[] for i in range(n_X_rec_new_labels * 5)]
    x_rec_new = []
    i_ = -1
    for i in range(len(x_rec)):
        if rd_is is not None:
            if i not in rd_is:
                continue
        i_ += 1
        gms = x_rec[i]
        if len(gms) > 0:
            # Filter out future games which might have made it through the filter (30 seconds forward or more)
            gms = gms[np.nonzero(gms[:, X_rec_l_is["time_since_match"]] >= -30.0)[0]]
        if len(gms) < 1:
            flags.append((game_i, i_))
            x_rec_new_ = np.zeros(n_X_rec_new_labels)
        else:
            x_rec_new_ = []
            for n_rec_avg in ns_avgs:
                duration_norm = avg_game_duration_ / gms[-n_rec_avg:, X_rec_l_is["duration"]]
                # duration_norm = 1
                # pr_fl((0.5 * (gms[-n_rec_avg:,
                #     X_rec_l_is["time_since_match"]] / (60*60*24))) - 3)
                # expdec_norm = 1 / (1 + np.exp(((c_1 * (gms[-n_rec_avg:,
                #     X_rec_l_is["time_since_match"]] / (60*60*24))) + c_2).astype(np.float32)))
#                 expdec_norm = 1 / (1 + np.exp(((c_1 * (gms[-n_rec_avg:,
#                     X_rec_l_is["time_since_match"]] / (60*60*24))) + c_2).astype(np.float32)) + \
#                     np.exp(((c_3 * (np.arange(min(n_rec_avg, gms.shape[0]))[::-1])) + c_4).astype(np.float32)))
                # expdec_norm = 1 / (1 + \
                #     np.exp(((c_1 * (np.arange(min(n_rec_avg, gms.shape[0]))[::-1])) + c_2).astype(np.float32)))
#                 expdec_norm[np.nonzero(expdec_norm < expdec_15th)[0]] = 0
                for k in rec_avg_keys:
                    # pr_fl(k, gms[0, X_rec_l_is[k]])
                    for j in range(len(gms)):
                        if gms[j, X_rec_l_is[k]] is None:
                            # pr_fl(game_i, i, i_, j, gms.shape, k, gms[j, -2:])
                            if "_rank" in k:
                                gms[j, X_rec_l_is[k]] = 46.6
                            # elif "_turrets" in k:
                            #     gms[j, X_rec_l_is[k]] = 7.1
                            # else:
                            #     pr_fl(k, gms[j, X_rec_l_is[k]])
                    # x_rec_new_.append(np.mean(((gms[-n_rec_avg:,
                    # x_rec_new_.append(np.mean(expdec_norm * ((gms[-n_rec_avg:,
                    #                             X_rec_l_is[k]]) - game_means[k])))
                    # x_new = (gms[-n_rec_avg:, X_rec_l_is[k]] * \
                    #     (duration_norm if k in rec_avg_durnorm else 1)) - game_means[k]
                    x_new = gms[-n_rec_avg:, X_rec_l_is[k]]
                    # x_new = (X_rec_alphas_dict[i % 5][k] * x_new) + np.log((X_rec_betas_dict[i % 5][k] * \
                    #   (max(X_rec_gammas_dict[i % 5][k], 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k], 0.))).astype(np.float32))
                    # x_new = (0.02 * x_new) + np.log((0.15 * \
                    #   (max(0.12, 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k], 0.))).astype(np.float32))
                    x_new *= (duration_norm if k in rec_avg_durnorm else 1)
                    x_rec_new_.append(np.mean(x_new))
                    # x_rec_new_.append(np.mean(expdec_norm * x_new))
                for k, k_ in rec_ratios:
                    k__gms = gms[-n_rec_avg:, X_rec_l_is[k_]].flatten()
                    zero_is = np.nonzero(k__gms < 1)[0]
                    if len(zero_is) > 0:
                        k__gms[zero_is] = 1
                    k_gms = gms[-n_rec_avg:, X_rec_l_is[k]]
                    # zero_is = np.nonzero(k_gms < 1)[0]
                    # if len(zero_is) > 0:
                    #     k_gms[zero_is] = 1
                    k_key = k + '_/_' + k_
                    # x_new = (((duration_norm if k_key in rec_ratios_durnorm else 1) * \
                    #     (k_gms - k__gms)) - game_means[k_key])
                    x_new = k_gms - k__gms
                    # x_new = (X_rec_alphas_dict[i % 5][k_key] * x_new) + np.log((X_rec_betas_dict[i % 5][k_key] * \
                    #   (max(X_rec_gammas_dict[i % 5][k_key], 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k_key], 0.))).astype(np.float32))
                    # x_new = (0.02 * x_new) + np.log((0.15 * \
                    #   (max(0.12, 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k_key], 0.))).astype(np.float32))
                    x_new *= (duration_norm if k_key in rec_ratios_durnorm else 1)
                    x_rec_new_.append(np.mean(x_new))
                    # x_rec_new_.append(np.mean(expdec_norm * x_new))
                    # x_rec_new_.append(np.mean(((duration_norm * (k_gms - k__gms)) - game_means[k + '_/_' + k_])))
                #     # x_rec_new_.append(np.mean(expdec_norm * (gms[-n_rec_avg:,
                #     #     X_rec_l_is[k]]) - game_means[k]))
                #     # x_rec_new_.append(np.mean(((gms[-n_rec_avg:,
                #     # x_rec_new_.append(np.mean(expdec_norm * ((gms[-n_rec_avg:,
                #     #                         X_rec_l_is[k]]) - game_means[k])))
                #     x_new = (gms[-n_rec_avg:, X_rec_l_is[k]] * \
                #         (duration_norm if k in rec_avg_durnorm else 1)) - game_means[k]
                #     x_rec_new_.append(np.mean(x_new))
                #     if k in rec_avg_expdec:
                #         x_rec_new_.append(np.mean((expdec_norm * (gms[-n_rec_avg:, X_rec_l_is[k]] * \
                #             (duration_norm if k in rec_avg_durnorm else 1))) - game_means[k]))
                # for k, k_ in rec_ratios:
                #     k__gms = gms[-n_rec_avg:, X_rec_l_is[k_]].flatten()
                #     zero_is = np.nonzero(k__gms < 1)[0]
                #     if len(zero_is) > 0:
                #         k__gms[zero_is] = 1
                #     k_gms = gms[-n_rec_avg:, X_rec_l_is[k]]
                #     # zero_is = np.nonzero(k_gms < 1)[0]
                #     # if len(zero_is) > 0:
                #     #     k_gms[zero_is] = 1
                #     # x_rec_new_.append(np.mean(expdec_norm * ((duration_norm * (k_gms - k__gms)) - game_means[k + '_/_' + k_])))
                #     k_key = k + '_/_' + k_
                #     x_new = (((duration_norm if k_key in rec_ratios_durnorm else 1) * \
                #         (k_gms - k__gms)) - game_means[k_key])
                #     x_rec_new_.append(np.mean(x_new))
                #     if k_key in rec_ratios_expdec:
                #         x_new_ = (duration_norm if k_key in rec_ratios_durnorm else 1) * \
                #             (k_gms - k__gms)
                #         x_rec_new_.append(np.mean((expdec_norm * x_new_) - game_means[k_key]))
            x_rec_new_ = np.hstack(x_rec_new_)
            if infill_data is None:
                for j in range(len(X_rec_new_keys)):
                    filler[((i % 5) * n_X_rec_new_labels) + j].append(x_rec_new_[j])
        x_rec_new.append(x_rec_new_)
    x_rec_new = np.hstack(x_rec_new)
    return x_rec_new, filler, flags
def get_recent_match_avgs_hypopt(X, X_rec, X_rec_labels, c_1=mtm_C_1, c_2=mtm_C_2,
  ns_avgs=mtm_rec_ns, pass_1=None,
  infill_data=None, roles=t_roles, rd_is=None, verbose=False, n_jobs=1):
    X_rec_l_is = get_list_is_dict(X_rec_labels)
    X_rec_new_keys = sum([sum([[str(n) + "ravg_" + k] for k in rec_avg_keys], []) + \
                          # sum([[str(n) + "ebbiravg_" + k] for k in rec_avg_expdec], []) + \
                          sum([[str(n) + "ravg_" + k] for k in rec_ratios_keys], []) \
                          # sum([[str(n) + "ebbiravg_" + k] for k in rec_ratios_expdec], []) \
                     for n in ns_avgs], [])
    n_X_rec_new_labels = len(X_rec_new_keys)
    X_rec_avgs_labels = sum([
        [r + '_' + k for k in X_rec_new_keys] for r in roles], [])

#     filler, flags = [[] for i in range(n_X_rec_new_labels * 5)], []
    # duration_normalizer = avg_game_duration

#     game_means = None
#     game_counts = None
#     if infill_data is not None:
#         game_means = infill_data["game_means"]
#         # print(game_means["duration"])
#     else:
#         game_means = defaultdict(int)
#         game_counts = defaultdict(int)
#         if isinstance(pass_1, tuple):
# #             print("Imported first pass")
#             game_means, game_counts = pass_1
#         else:

#             # First pass: get means of raw features, and ratio features normalized by game duration
#             len_str = ' / ' + str(len(X)) + '     '
#             for game_i in range(len(X)):
#                 if verbose and game_i % 100 == 0:
#                     sys_print('\r' + str(game_i) + len_str)
#                 x = X[game_i]
#                 x_rec = X_rec[game_i]
#                 i_ = -1
#                 for i in range(len(x_rec)):
#                     if rd_is is not None:
#                         if i not in rd_is:
#                             continue
#                     i_ += 1
#                     gms = x_rec[i]
#                     if len(gms) > 0:
#                         # Filter out future games which might have made it through the filter (30 seconds forward or more)
#                         gms = gms[np.nonzero(gms[:, X_rec_l_is["time_since_match"]] >= -30.0)[0]]
#                     if len(gms) < 1:
#                         pass
#                     else:
#                         for n_rec_avg in ns_avgs:
#                             n_new = min(n_rec_avg, gms.shape[0])
#                             duration_norm = avg_game_duration / gms[-n_rec_avg:, X_rec_l_is["duration"]]
#                             for k in rec_avg_keys:
#                                 # pr_fl(k, gms[0, X_rec_l_is[k]])
#                                 for j in range(len(gms)):
#                                     if gms[j, X_rec_l_is[k]] is None:
#                                         # pr_fl(game_i, i, i_, j, gms.shape, k, gms[j, -2:])
#                                         if "_rank" in k:
#                                             gms[j, X_rec_l_is[k]] = 46.6
#                                         # elif "_turrets" in k:
#                                         #     gms[j, X_rec_l_is[k]] = 7.1
#                                         # else:
#                                         #     pr_fl(k, gms[j, X_rec_l_is[k]])
#                                 games_avg = np.mean(gms[-n_rec_avg:, X_rec_l_is[k]] * \
#                                                     (duration_norm if k in rec_avg_durnorm else 1))
#                                 count = game_counts[k] + n_new
#                                 game_means[k] = (((count - n_new) / count) * game_means[k]) + (games_avg * (n_new / count))
#                                 game_counts[k] = count

#                             for k, k_ in rec_ratios:
#                                 k__gms = gms[-n_rec_avg:, X_rec_l_is[k_]].flatten()
#                                 zero_is = np.nonzero(k__gms < 1)[0]
#                                 if len(zero_is) > 0:
#                                     k__gms[zero_is] = 1
#                                 k_gms = gms[-n_rec_avg:, X_rec_l_is[k]]
#                                 # zero_is = np.nonzero(k_gms < 1)[0]
#                                 # if len(zero_is) > 0:
#                                 #     k_gms[zero_is] = 1
#                                 k_key = k + '_/_' + k_
#                                 games_avg = np.mean((duration_norm if k_key in rec_ratios_durnorm else 1) * (k_gms - k__gms))
#                                 count = game_counts[k_key] + n_new
#                                 game_means[k_key] = (((count - n_new) / count) * game_means[k_key]) + (games_avg * (n_new / count))
#                                 game_counts[k_key] = count
#             if verbose:
#                 sys_print('\r' + str(len(X)) + len_str)
#                 pr_fl()

#             if pass_1 == "return":
#                 return game_means, game_counts

    # Second pass: use means found in first pass to standardize the same features, then exponentially time decay
    res = Parallel(n_jobs=n_jobs, verbose=0)(delayed(get_sample_recavgs)(X_rec[game_i], game_i,
                       ns_avgs, c_1, c_2, avg_game_duration, rd_is, infill_data,
                       X_rec_l_is, X_rec_new_keys, n_X_rec_new_labels) for game_i in range(len(X)))
    X_rec_avgs = np.empty((len(res), n_X_rec_new_labels * N_PL))
    filler = [[] for _ in range(n_X_rec_new_labels * 5)]
    flags = []
    for i in range(len(res)):
        r = res[0]
        X_rec_avgs[i] = r[0]
        for j in range(n_X_rec_new_labels * 5):
            filler[j] += r[1][j]
        flags += r[2]
        del res[0]

    if infill_data is None:
        for i in range(len(filler)):
            filler[i] = np.mean(filler[i])
        infill_data = {
            "filler": filler,
            # "game_means": game_means,
        }
        create_folder(data_dir)
        create_folder(data_dir + infill_data_dir)
        save_json(infill_data, data_dir + infill_data_dir + "rec_infill_data" + ".json", pad=False)
        # save_json(infill_data, data_dir + infill_data_dir + "rec_infill_data_" + str(time.time()) + ".json", pad=False)
    else:
        filler = infill_data["filler"]

    for game_i, role_i in flags:
        for i in range(n_X_rec_new_labels):
            X_rec_avgs[game_i, (n_X_rec_new_labels * role_i) + i] = filler[(n_X_rec_new_labels * (role_i % 5)) + i]

    return X_rec_avgs, X_rec_avgs_labels


def get_recent_match_avgs(X, X_rec, X_rec_labels, c_1=mtm_C_1, c_2=mtm_C_2,
  ns_avgs=mtm_rec_ns, pass_1=None,
  infill_data=None, roles=t_roles, rd_is=None, verbose=True, n_jobs=1):
    X_rec_l_is = get_list_is_dict(X_rec_labels)
    X_rec_new_keys = sum([sum([[str(n) + "ravg_" + k] for k in rec_avg_keys], []) + \
                          # sum([[str(n) + "ebbiravg_" + k] for k in rec_avg_expdec], []) + \
                          sum([[str(n) + "ravg_" + k] for k in rec_ratios_keys], []) \
                          # sum([[str(n) + "ebbiravg_" + k] for k in rec_ratios_expdec], []) \
                     for n in ns_avgs], [])
    n_X_rec_new_labels = len(X_rec_new_keys)
    X_rec_avgs_labels = sum([
        [r + '_' + k for k in X_rec_new_keys] for r in roles], [])

    filler, flags = [[] for i in range(n_X_rec_new_labels * 5)], []
    # duration_normalizer = avg_game_duration

    # _, X_rec_labels_al, Xr_mins, _, _, _, _, X_rec_alphas, X_rec_betas, X_rec_gammas, \
    #      _, _, _, _, _ = \
    #     load_ld("model_logs/AutoLogTransf.data", pad=False)
    # # X_alphas_dict = dict(zip(X_labels_al, X_alphas))
    # # X_betas_dict = dict(zip(X_labels_al, X_betas))
    # # X_gammas_dict = dict(zip(X_labels_al, X_gammas))
    # X_rec_alphas_dict = [dict(zip(X_rec_labels_al, X_rec_alphas[i])) for i in range(5)]
    # X_rec_betas_dict = [dict(zip(X_rec_labels_al, X_rec_betas[i])) for i in range(5)]
    # X_rec_gammas_dict = [dict(zip(X_rec_labels_al, X_rec_gammas[i])) for i in range(5)]
    # Xr_mins_dict = [dict(zip(X_rec_labels_al, Xr_mins[i])) for i in range(5)]

    # game_means = None
    # game_counts = None
    # if infill_data is not None:
    #     game_means = infill_data["game_means"]
    #     # print(game_means["duration"])
    # else:
    #     game_means = defaultdict(int)
    #     game_counts = defaultdict(int)
    #     if isinstance(pass_1, tuple):
    #         game_means, game_counts = pass_1
    #     else:

    #         # First pass: get means of raw features, and ratio features normalized by game duration
    #         len_str = ' / ' + str(len(X)) + '     '
    #         for game_i in range(len(X)):
    #             if verbose and game_i % 100 == 0:
    #                 sys_print('\r' + str(game_i) + len_str)
    #             x = X[game_i]
    #             x_rec = X_rec[game_i]
    #             i_ = -1
    #             for i in range(len(x_rec)):
    #                 if rd_is is not None:
    #                     if i not in rd_is:
    #                         continue
    #                 i_ += 1
    #                 gms = x_rec[i]
    #                 if len(gms) > 0:
    #                     # Filter out future games which might have made it through the filter (30 seconds forward or more)
    #                     gms = gms[np.nonzero(gms[:, X_rec_l_is["time_since_match"]] >= -30.0)[0]]
    #                 if len(gms) < 1:
    #                     pass
    #                 else:
    #                     for n_rec_avg in ns_avgs:
    #                         n_new = min(n_rec_avg, gms.shape[0])
    #                         duration_norm = avg_game_duration / gms[-n_rec_avg:, X_rec_l_is["duration"]]
    #                         for k in rec_avg_keys:
    #                             # pr_fl(k, gms[0, X_rec_l_is[k]])
    #                             for j in range(len(gms)):
    #                                 if gms[j, X_rec_l_is[k]] is None:
    #                                     # pr_fl(game_i, i, i_, j, gms.shape, k, gms[j, -2:])
    #                                     if "_rank" in k:
    #                                         gms[j, X_rec_l_is[k]] = 46.6
    #                                     # elif "_turrets" in k:
    #                                     #     gms[j, X_rec_l_is[k]] = 7.1
    #                                     # else:
    #                                     #     pr_fl(k, gms[j, X_rec_l_is[k]])
    #                             x_new = gms[-n_rec_avg:, X_rec_l_is[k]]
    #                             x_new = (X_rec_alphas_dict[i % 5][k] * x_new) + np.log(X_rec_betas_dict[i % 5][k] * \
    #                                 (max(X_rec_gammas_dict[i % 5][k], 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k], 0.)))
    #                             x_new *= (duration_norm if k in rec_avg_durnorm else 1)
    #                             games_avg = np.mean(x_new)
    #                             count = game_counts[k] + n_new
    #                             game_means[k] = (((count - n_new) / count) * game_means[k]) + (games_avg * (n_new / count))
    #                             game_counts[k] = count

    #                         for k, k_ in rec_ratios:
    #                             k__gms = gms[-n_rec_avg:, X_rec_l_is[k_]].flatten()
    #                             zero_is = np.nonzero(k__gms < 1)[0]
    #                             if len(zero_is) > 0:
    #                                 k__gms[zero_is] = 1
    #                             k_gms = gms[-n_rec_avg:, X_rec_l_is[k]]
    #                             # zero_is = np.nonzero(k_gms < 1)[0]
    #                             # if len(zero_is) > 0:
    #                             #     k_gms[zero_is] = 1
    #                             k_key = k + '_/_' + k_
    #                             x_new = k_gms - k__gms
    #                             x_new = (X_rec_alphas_dict[i % 5][k_key] * x_new) + np.log(X_rec_betas_dict[i % 5][k_key] * \
    #                                 (max(X_rec_gammas_dict[i % 5][k_key], 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k_key], 0.)))
    #                             x_new *= (duration_norm if k_key in rec_ratios_durnorm else 1)
    #                             games_avg = np.mean(x_new)
    #                             count = game_counts[k_key] + n_new
    #                             game_means[k_key] = (((count - n_new) / count) * game_means[k_key]) + (games_avg * (n_new / count))
    #                             game_counts[k_key] = count
    #         if verbose:
    #             sys_print('\r' + str(len(X)) + len_str)
    #             pr_fl()

    #         if pass_1 == "return":
    #             return game_means, game_counts

    # Second pass: use means found in first pass to standardize the same features, then exponentially time decay
    X_rec_avgs = np.empty((len(X), len(X_rec_avgs_labels)))
    len_str = ' / ' + str(len(X)) + '     '
    for game_i in range(len(X)):
        if verbose and game_i % 100 == 0:
            sys_print('\r' + str(game_i) + len_str)
        x = X[game_i]
        x_rec = X_rec[game_i]
        x_rec_new = []
        i_ = -1
        for i in range(len(x_rec)):
            if rd_is is not None:
                if i not in rd_is:
                    continue
            i_ += 1
            gms = x_rec[i]
            if len(gms) > 0:
                # Filter out future games which might have made it through the filter (30 seconds forward or more)
                gms = gms[np.nonzero(gms[:, X_rec_l_is["time_since_match"]] >= -30.0)[0]]
            if len(gms) < 1:
                flags.append((game_i, i_))
                x_rec_new_ = np.zeros(len(X_rec_new_keys))
            else:
                x_rec_new_ = []
                for n_rec_avg in ns_avgs:
                    # duration_norm = 1
                    duration_norm = avg_game_duration / gms[-n_rec_avg:, X_rec_l_is["duration"]]
                    # pr_fl((0.5 * (gms[-n_rec_avg:,
                    #     X_rec_l_is["time_since_match"]] / (60*60*24))) - 3)
                    # expdec_norm = 1 / (1 + np.exp(((c_1 * (gms[-n_rec_avg:,
                    #     X_rec_l_is["time_since_match"]] / (60*60*24))) + c_2).astype(np.float32)))
                    for k in rec_avg_keys:
                        # pr_fl(k, gms[0, X_rec_l_is[k]])
                        for j in range(len(gms)):
                            if gms[j, X_rec_l_is[k]] is None:
                                # pr_fl(game_i, i, i_, j, gms.shape, k, gms[j, -2:])
                                if "_rank" in k:
                                    gms[j, X_rec_l_is[k]] = 46.6
                                # elif "_turrets" in k:
                                #     gms[j, X_rec_l_is[k]] = 7.1
                                # else:
                                #     pr_fl(k, gms[j, X_rec_l_is[k]])
                        # x_rec_new_.append(np.mean(((gms[-n_rec_avg:,
                        # x_rec_new_.append(np.mean(expdec_norm * ((gms[-n_rec_avg:,
                        #                             X_rec_l_is[k]]) - game_means[k])))
                        # x_new = (gms[-n_rec_avg:, X_rec_l_is[k]] * \
                        #     (duration_norm if k in rec_avg_durnorm else 1)) - game_means[k]
                        x_new = gms[-n_rec_avg:, X_rec_l_is[k]]
                        # pr_fl(x_new, k)
                        if x_new is None or len(x_new) == 0:
                            pr_fl("x_new is None:", k)
                        # x_new = (X_rec_alphas_dict[i % 5][k] * x_new) + np.log((X_rec_betas_dict[i % 5][k] * \
                        #   (max(X_rec_gammas_dict[i % 5][k], 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k], 0.))).astype(np.float32))
                        # x_new = (0.02 * x_new) + np.log((0.15 * \
                        #   (max(0.12, 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k], 0.))).astype(np.float32))
                        x_new *= (duration_norm if k in rec_avg_durnorm else 1)
                        x_rec_new_.append(np.mean(x_new))
                        # x_rec_new_.append(np.mean(expdec_norm * x_new))
                    for k, k_ in rec_ratios:
                        k__gms = gms[-n_rec_avg:, X_rec_l_is[k_]].flatten()
                        zero_is = np.nonzero(k__gms < 1)[0]
                        if len(zero_is) > 0:
                            k__gms[zero_is] = 1
                        k_gms = gms[-n_rec_avg:, X_rec_l_is[k]]
                        # zero_is = np.nonzero(k_gms < 1)[0]
                        # if len(zero_is) > 0:
                        #     k_gms[zero_is] = 1
                        k_key = k + '_/_' + k_
                        # x_new = (((duration_norm if k_key in rec_ratios_durnorm else 1) * \
                        #     (k_gms - k__gms)) - game_means[k_key])
                        x_new = k_gms - k__gms
                        # x_new = (X_rec_alphas_dict[i % 5][k_key] * x_new) + np.log((X_rec_betas_dict[i % 5][k_key] * \
                        #   (max(X_rec_gammas_dict[i % 5][k_key], 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k_key], 0.))).astype(np.float32))
                        # x_new = (0.02 * x_new) + np.log((0.15 * \
                        #   (max(0.12, 1e-8) + np.maximum(x_new - Xr_mins_dict[i % 5][k_key], 0.))).astype(np.float32))
                        x_new *= (duration_norm if k_key in rec_ratios_durnorm else 1)
                        x_rec_new_.append(np.mean(x_new))
                        # x_rec_new_.append(np.mean(expdec_norm * x_new))
                        # x_rec_new_.append(np.mean(((duration_norm * (k_gms - k__gms)) - game_means[k + '_/_' + k_])))
                x_rec_new_ = np.hstack(x_rec_new_)
                if infill_data is None:
                    for j in range(len(X_rec_new_keys)):
                        filler[((i % 5) * n_X_rec_new_labels) + j].append(x_rec_new_[j])
            x_rec_new.append(x_rec_new_)
        x_rec_new = np.hstack(x_rec_new)
        X_rec_avgs[game_i] = x_rec_new
    if verbose:
        sys_print('\r' + str(len(X)) + len_str)
        pr_fl()

    if infill_data is None:
        for i in range(len(filler)):
            filler[i] = np.mean(filler[i])
        infill_data = {
            "filler": filler,
            # "game_means": game_means,
        }
        create_folder(data_dir)
        create_folder(data_dir + infill_data_dir)
        save_json(infill_data, data_dir + infill_data_dir + "rec_infill_data" + ".json", pad=False)
        # save_json(infill_data, data_dir + infill_data_dir + "rec_infill_data_" + str(time.time()) + ".json", pad=False)
    else:
        filler = infill_data["filler"]

    for game_i, role_i in flags:
        for i in range(n_X_rec_new_labels):
            X_rec_avgs[game_i, (n_X_rec_new_labels * role_i) + i] = filler[(n_X_rec_new_labels * (role_i % 5)) + i]

    return X_rec_avgs, X_rec_avgs_labels


# Scale data based on scaling groups to retain relativity
def scale_data(data, labels, n_train=None):
    labels_is = get_list_is_dict(labels)
    grps = scaling_groups['0-1']
    scales = []
    scale_dict = OrderedDict([(l, None) for l in labels])
    ks_all = set()
    for grp in grps:
        ks = []
        for k in labels:
            if k in ks_all:
                continue
            for k_sfx in grp:
                if k[-k_sfx_lens[k_sfx]:] == k_sfx:
                    ks.append(k)
                    ks_all.add(k)
                    break
        if len(ks) == 0:
            scales.append(None)
            continue
        ks_is = [labels_is[k] for k in ks]
        d = data[:n_train, ks_is].flatten()
        min_ = min(d)
        max_ = max(d - min_)
        if max_ == 0:
            pr_fl(grp, ks)
            pr_fl(max_,min_)
        data[:, ks_is] = (data[:, ks_is] - min_) / max_
        scales.append((max_, min_))
        for k in ks:
            scale_dict[k] = (max_, min_)
    return scales, scale_dict

def scale_data_with_dict(data, labels, scale_dict, make_copy=False):
    if make_copy:
        data = data.copy()
    for k in scale_dict:
        if scale_dict[k] != None and k in labels:
            i = labels.index(k)
            data[:, i] = (data[:, i] - scale_dict[k][1]) / scale_dict[k][0]
    return data

# Reverse the scaling performed by scale_data
def unscale_data(data, labels, scales, sc_ls=None):
    labels_is = get_list_is_dict(labels)
    grps = scaling_groups['0-1']
    ks_all = set()
    for i in range(len(grps)):
        scale = scales[i]
        if scale is None:
            continue
        factor, offset = scale
        grp = grps[i]
        ks = []
        for k in labels:
            if sc_ls is not None:
                if k not in sc_ls:
                    continue
            if k in ks_all:
                continue
            for k_sfx in grp:
                if k[-k_sfx_lens[k_sfx]:] == k_sfx:
                    ks.append(k)
                    ks_all.add(k)
                    break
        if len(ks) == 0:
            continue
        ks_is = [labels_is[k] for k in ks]
        data[:, ks_is] = (data[:, ks_is] * factor) + offset

# Scale data without scaling groups (for recent games data - 
# relativity not as important for neural nets)
def scale_data_simple(data, return_scaled=True):
    # data_mins = np.min(data, axis=0)
    # data_maxs = np.max(data, axis=0)
    # div_factor = data_maxs - data_mins
    # scaler_ = RobustScaler().fit(data)
    # data_mins = scaler_.center_
    # div_factor = scaler_.scale_
    data_mins = np.median(data, axis=0)
    div_factor = scipy.stats.iqr(data, axis=0)
    if return_scaled:
        return (data - data_mins) / div_factor, data_mins, div_factor
    return data_mins, div_factor

# Scale recent matches data
def scale_rec_data(X_r, X_rec_labels, max_samples=np.inf):
    # X_r = deepcopy(X_r_)
    # all_vals = np.vstack(sum([[X_r[j][k] for k in N_PL_r if len(X_r[j][k]) != 0] \
    #     for j in range(len(X_r))], []))
    # X_r_mins, X_r_div = scale_data_simple(all_vals, return_scaled=False)
    mins, facs = [], []
    len_str = ' / ' + str(len(X_rec_labels)) + '     '
    for i in range(len(X_rec_labels)):
        sys_print('\r' + str(i + 1) + len_str)
        if '__x' in X_rec_labels[i]:
            mins.append(0)
            facs.append(1)
            continue
        all_vals = np.hstack(sum([[X_r[j][k][:, i].astype(float) for k in N_PL_r if len(X_r[j][k]) > 0] \
                                  for j in range(min(max_samples, len(X_r)))], []))
        mins.append(np.median(all_vals))
        facs.append(scipy.stats.iqr(all_vals))
    X_r_mins, X_r_div = np.asarray(mins), np.asarray(facs)
#     len_str = ' / ' + str(len(X_r)) + '     '
#     for i in range(len(X_r)):
#         if i % 100 == 0:
#             sys_print('\r' + str(i) + len_str)
#         for j in range(len(X_r[i])):
#             if len(X_r[i][j]) == 0:
#                 continue
#             X_r[i][j] = (X_r[i][j].astype(float) - X_r_mins) / X_r_div
#     pr_fl()
    return X_r_mins, X_r_div

# Remove duplicate players games (so we have 1 game for 1 player)
def remove_duplicate_players(meta, X, X_rec, Yc, Yr, meta_labels, elo_limit=None,
        return_remaining=False, n_dup=1):
    regions_i = meta_labels.index("region_id")
    elo_i = meta_labels.index("elo")
    acc_ids_is = [i for i in range(
        len(meta_labels)) if "acc_ids" in meta_labels[i]]
    G_id = (100 * meta[:, acc_ids_is]) + np.atleast_2d(meta[:, regions_i]).T
    s, dup_is = np.sort(G_id, axis=None), set()
    multi_sampled = set(s[:-1][s[1:] == s[:-1]])
    for g_i in multi_sampled:
        game_is = np.nonzero(G_id == g_i)[0]
#         np.random.shuffle(game_is)
        game_versions = meta[game_is, meta_labels.index("timestamp")]
        game_is = game_is[np.argsort(game_versions)[::-1]]
        i = 0
        while i < len(game_is) and game_is[i] in dup_is:
            i += 1
        if i >= len(game_is) - 1:
            continue
        # dup_is |= set(game_is[i + n_dup:])
        other_is = set(game_is)
        other_is.remove(game_is[i])
        dup_is |= other_is
    if elo_limit is not None:
        dup_is = [i for i in dup_is if meta[i, elo_i] < elo_limit]
        dup_is_set = set(dup_is)
    else:
        dup_is_set = dup_is
        dup_is = list(dup_is)
    res = [np.delete(meta, dup_is, axis=0), \
           np.delete(X, dup_is, axis=0), \
           [X_rec[i] for i in range(len(X_rec)) if i not in dup_is_set], \
           np.delete(Yc, dup_is, axis=0), \
           np.delete(Yr, dup_is, axis=0)]
    if return_remaining:
        res.append(dup_is)
    return res


# Remove a given subset of values from a dataset by labels
def remove_data_subset(data, original_labels, labels):
    inds = OrderedDict.fromkeys(range(len(original_labels)))
    new_labels = OrderedDict.fromkeys(original_labels)
    for i in range(len(original_labels)):
        ol = original_labels[i]
        for l in labels:
            if ol == l or ol[:len(l) + 3] == l + '__x':
                inds.pop(i)
                new_labels.pop(ol)
                break
    return data[:, list(inds.keys())], list(new_labels.keys())

# Get a feature subset by removing other subsets
def get_data_subset(data, original_labels, subsets):
    new_labels = original_labels
    for subset in label_subsets_dict:
        if subset not in subsets:
            data, new_labels = remove_data_subset(
                data, new_labels, label_subsets_dict[subset])
    return data, new_labels

def remove_data_inds(meta, X, X_rec, Yc, Yr, inds):
    inds_set = set(inds)
    return np.delete(meta, inds, axis=0), \
           np.delete(X, inds, axis=0), \
           [X_rec[i] for i in range(len(X_rec)) if i not in inds_set] if X_rec is not None else None, \
           np.delete(Yc, inds, axis=0), \
           np.delete(Yr, inds, axis=0)

filter_rec_feats = [
     'kills_/_opgg_champion_recent_kills',
     'kills_/_chgg_eloavg_champion_kills',
     'deaths_/_opgg_champion_recent_deaths',
     'deaths_/_chgg_eloavg_champion_deaths',
     'assists_/_opgg_champion_recent_assists',
     'assists_/_chgg_eloavg_champion_assists',
     'kda_ratio_/_opgg_champion_recent_kda_ratio',
     'kda_ratio_/_opgg_eloavg_champion_today_kda_ratio',
     'kda_ratio_/_chgg_eloavg_champion_kda_ratio',
     'creep_score_/_opgg_champion_recent_creep_score',
     'creep_score_/_opgg_eloavg_champion_today_creep_score',
     'creep_score_/_chgg_eloavg_champion_creep_score',
     'opgg_champion_recent_damage_dealt_/_chgg_eloavg_champion_total_damage',
     'opgg_champion_recent_damage_taken_/_chgg_eloavg_champion_total_damage_taken',
     'pinks_purchased_/_chgg_eloavg_champion_wards_placed',
     'pinks_purchased_/_chgg_eloavg_champion_wards_killed',
]

def get_rec_feats(meta, X, X_rec, meta_labels, X_labels, X_rec_labels, incl_queue=True, incl_champion=False, 
           incl_spells=True, incl_team_side=False, incl_cats=True, incl_ratios=True, incl_invdur=True): # team_side bugged, always blue
    rec_ratios_use = [(k, k_) for (k, k_) in rec_ratios if k in X_rec_labels and k_ in X_rec_labels and (k + '_/_' + k_) in filter_rec_feats]
    rec_ratios_use_keys = [k + '_/_' + k_ for (k, k_) in rec_ratios_use]

    new_labels = []
    rem_feat_is = []
    return_labels = deepcopy(X_rec_labels)
    return_labels.remove("queue")
    return_labels.remove("champion")
    return_labels.remove("spell_1")
    return_labels.remove("spell_2")
    return_labels.remove("team_side")
    if incl_queue:  new_labels += ["queue__x" + str(i) for i in range(len(queue_codes))]
    if incl_cats:   new_labels += ["cats__x" + tag for tag in cat_tags]
    if incl_spells: new_labels += ["spells__x" + str(i) for i in range(len(spell_codes))]
    if incl_invdur: new_labels += ["duration_inverse"]
    if incl_ratios: new_labels += rec_ratios_use_keys
    # Todo: same for champion (?)

    queue_ind = X_rec_labels.index("queue")
    champion_ind = X_rec_labels.index("champion")
    spell_1_ind = X_rec_labels.index("spell_1")
    spell_2_ind = X_rec_labels.index("spell_2")
    team_side_ind = X_rec_labels.index("team_side")
    dur_ind = X_rec_labels.index("duration")
    ratios_inds = [(X_rec_labels.index(k), X_rec_labels.index(k_)) for (k, k_) in rec_ratios_use]

    queue_map_func = lambda z: queue_codes[z]
    champion_map_func = lambda z: champ_codes[z]
    spell_map_func = lambda z: spell_codes[z]
    team_side_map_func = lambda z: team_side_codes[z]

    rem_feat_is.append(queue_ind)
    rem_feat_is.append(champion_ind)
    rem_feat_is.append(spell_1_ind)
    rem_feat_is.append(spell_2_ind)
    rem_feat_is.append(team_side_ind)

    X_rec_return = deepcopy(X_rec)
    past_elo_estimates = np.empty((X.shape[0], len(X_rec_return[0])))

    len_str = ' / ' + str(len(X_rec_return)) + '     '
    meta_elo_ind = meta_labels.index("elo")
    rec_win_ind = X_rec_labels.index("win")
    for i in range(len(X_rec_return)):
        if i % 100 == 0:
            sys_print('\r' + str(i) + len_str)
        final_elo = meta[i, meta_elo_ind]           # Use match average while we don't have accurate per player data
        for j in range(len(X_rec_return[i])):
            if len(X_rec_return[i][j]) == 0:
                past_elo_estimates[i, j] = final_elo
                continue
            # Compute past elo estimate
            past_elo_estimates[i, j] = max(0, final_elo + (avg_lp_incr * np.sum((X_rec_return[i][j][:, rec_win_ind] * -2) + 1)))

            new_feats = []
            if incl_queue:
                new_feats.append(np.array(list(map(queue_map_func, X_rec_return[i][j][:, queue_ind]))))
            if incl_cats:
                new_feats.append(np.array(list(map(code_champ_as_cat, X_rec_return[i][j][:, champion_ind]))))
            if incl_spells:
                new_feats.append(np.array(list(map(spell_map_func, X_rec_return[i][j][:, spell_1_ind]))) + \
                                 np.array(list(map(spell_map_func, X_rec_return[i][j][:, spell_2_ind]))))
            if incl_invdur:
                new_feats.append(1 / X_rec_return[i][j][:, [dur_ind]])
            if incl_ratios:
                for k_i, k__i in ratios_inds:
                    k__gms = X_rec_return[i][j][:, [k__i]]
                    # zero_is = np.nonzero(k__gms < 1)[0]
                    # if len(zero_is) > 0:
                    #     k__gms[zero_is] = 1
                    new_feats.append(X_rec_return[i][j][:, [k_i]] - k__gms)
            # Todo: same for champion etc
            X_rec_return[i][j] = np.delete(X_rec_return[i][j], rem_feat_is, axis=1)
            X_rec_return[i][j] = np.hstack(new_feats + [X_rec_return[i][j]])
    pr_fl()
    return np.hstack([X, past_elo_estimates]), X_labels + past_elo_estimate_labels, X_rec_return, new_labels + return_labels


# Remove redundant features
def remove_redundant_features(X, X_labels, X_rec, X_rec_labels,
        r_feats_game=redundant_feats_game, r_feats_player=redundant_feats_player,
        r_feats_recent_matches=redundant_feats_recent_matches,
        r_feats_recent_matches_avgs=redundant_feats_recent_matches_avgs,
        recent_ns=range(1, 21)):
        # recent_ns=[2, 5, 12]):

    r_feats_all = r_feats_game + \
        sum([[role + '_' + l for l in r_feats_player] for role in t_roles], []) + \
        sum([sum([[role + '_' + str(n) + 'avg_' + l for l in r_feats_recent_matches_avgs] \
            for role in t_roles], []) for n in recent_ns], [])

    r_rec_feats_all = r_feats_recent_matches

    rem_feats = []
    for i in range(len(X_labels)):
        l = X_labels[i]
        for l_ in r_feats_all:
            if l_[-1] == '*':
                if l_[:-1] == l[:len(l_) - 1]:
                    rem_feats.append(i)
            else:
                if l_ == l:
                    rem_feats.append(i)

    rem_feats_rec = []
    for i in range(len(X_rec_labels)):
        l = X_rec_labels[i]
        for l_ in r_rec_feats_all:
            if l_[-1] == '*':
                if l_[:-1] == l[:len(l_) - 1]:
                    rem_feats_rec.append(i)
            else:
                if l_ == l:
                    rem_feats_rec.append(i)

    new_X_labels = [X_labels[i] for i in range(len(X_labels)) if i not in rem_feats]
    new_X_rec_labels = [X_rec_labels[i] for i in range(len(X_rec_labels)) if i not in rem_feats_rec]

    new_X = np.delete(X, rem_feats, axis=1)
    new_X_rec = deepcopy(X_rec)
    for i in range(len(new_X_rec)):
        for j in range(len(new_X_rec[i])):
            if len(new_X_rec[i][j]) == 0:
                continue
            new_X_rec[i][j] = np.delete(new_X_rec[i][j], rem_feats_rec, axis=1)

    return new_X, new_X_labels, new_X_rec, new_X_rec_labels


# Removes duplicate players, then adds back data for higher elos
def get_balanced_dataset(meta, X, X_rec, Yc, Yr, meta_labels, n_dup=1):
    meta_, X_, X_rec_, Yc_, Yr_, remaining_is = remove_duplicate_players(
        meta, X, X_rec, Yc, Yr, meta_labels, elo_limit=None, return_remaining=True, n_dup=n_dup)
    remaining_is = np.asarray(remaining_is)

    # Division at which we start to add games if we don't have enough
    m_div = 17 # Platinum 3

    # Get average amount of data per region for base division (Platinum 4 atm)
    basediv = league_ns["platinum"] + 1
    basediv_is = np.nonzero(meta_[:, meta_labels.index("division")] == basediv)[0]
    add_is = []
    for reg_i in region_is.values():
        reg_is_ = np.nonzero(meta_[:, meta_labels.index("region_id")] == reg_i)[0]
        reg_is = np.nonzero(meta[:, meta_labels.index("region_id")] == reg_i)[0]
        n_basediv = len(set(basediv_is) & set(reg_is_))
        remaining_in_reg = np.asarray(list(set(reg_is) & set(remaining_is)))
        for div in range(m_div, league_ns["challenger"] + 1):
            n_div = sum(meta_[reg_is_, meta_labels.index("division")] == div)
            if n_div < n_basediv:
                # pr_fl(type(remaining_in_reg[0]))
                # pr_fl(type(np.nonzero(
                #     meta[remaining_in_reg, meta_labels.index("division")] == div)[0]))
                remaining_in_div = remaining_in_reg[np.nonzero(
                    meta[remaining_in_reg, meta_labels.index("division")] == div)[0]]
                np.random.shuffle(remaining_in_div)
                # Sort by descending game version (patch)
                game_versions = meta[remaining_in_div, meta_labels.index("game_version")]
                remaining_in_div = remaining_in_div[np.argsort(game_versions)[::-1]]
                add_is += list(remaining_in_div[:n_basediv - n_div])

    meta_ = np.vstack([meta_, meta[add_is]])
    X_ = np.vstack([X_, X[add_is]])
    X_rec_ = X_rec_ + [X_rec[i] for i in add_is]
    Yc_ = np.vstack([Yc_, Yc[add_is]])
    Yr_ = np.vstack([Yr_, Yr[add_is]])
    return meta_, X_, X_rec_, Yc_, Yr_


# Get dataset for when we only know opponent champions (champ select)
def one_side_data(meta, X, X_rec, Yc, Yr,
  meta_labels, X_labels, X_rec_labels, Yc_labels, Yr_labels, include_X_rec=False):
    sides_is = np.random.randint(0, 2, X.shape[0])
    swap_is = np.nonzero(sides_is)[0]
    blue_is = [i for i in range(len(X_labels)) if X_labels[i][:5] == "blue_"]

    rem_lis = []
    X_ = X[:]
    for i in range(len(blue_is)):
        l = X_labels[blue_is[i]]
        red_i = X_labels.index("red_" + l[5:])
        X_[swap_is, blue_is[i]] = X[swap_is, red_i]

        rem = False
        for j in range(1, 21):
            if "_" + str(j) + "ravg_" in l:
                rem = True
                break
        if not rem:
            for l_ in one_side_rem_feats_player:
                if l_ in l:
                    rem = True
                    break
        if rem:
            rem_lis.append(red_i)
        else:
            X_[swap_is, red_i] = X[swap_is, blue_is[i]]

    for l in one_side_rem_feats:
        rem_lis.append(X_labels.index(l))

    X_ = np.delete(X_, rem_lis, axis=1)

    X_labels_ = [X_labels[i] for i in range(len(X_labels)) if i not in rem_lis]
    for i in range(len(X_labels_)):
        if X_labels_[i][:5] == "blue_":
            X_labels_[i] = "friendly_" + X_labels_[i][5:]
        elif X_labels_[i][:4] == "red_":
            X_labels_[i] = "opponent_" + X_labels_[i][4:]

    blue_win_i = Yc_labels.index("blue_win")
    Yc_ = Yc[:]
    Yc_[swap_is, blue_win_i] = 1 - Yc_[swap_is, blue_win_i]
    
    # Add side (blue or red) as feature
    side_feat = np.atleast_2d(sides_is).T
    X_ = np.hstack([1 - side_feat, side_feat, X_]) # blue side = [1, 0], red side = [0, 1]
    X_labels_ = ["friendly_side__x1", "friendly_side__x2"] + X_labels_

    return X_, Yc_, X_labels_


# Add team sum Y regression target values
def add_Yr_team_sums(Yr, Yr_labels):
    base_labels = []
    r = t_roles[0]
    r_len = len(r)
    for l in Yr_labels:
        if l[:r_len] == r:
            base_labels.append( l[r_len + 1:] )
    Yr_team = []
    for l in base_labels:
        Yr_team.append( [Yr[:, Yr_labels.index(r + '_' + l)] for r in t_roles] )
        Yr_labels.append( "team_" + l )
    Yr_team = np.sum(Yr_team, axis=1).T
    Yr = np.hstack([Yr, Yr_team])
    return Yr, Yr_labels


def add_beta_winrates(alpha, beta, X_bopt, X_labels_bopt, X_scales, incl_n_games=True):
#     n_total = n_train + n_test
    new_X, new_ls = [], []
#     X_bopt = X
#     X_labels_bopt = X_labels
#     X_bopt = X[:n_total].copy()
#     X_labels_bopt = list(X_labels)
    wins_ls = [l for l in X_labels_bopt if l[-5:] == "_wins" and "_eloavg_" not in l and "_opgg_champion_all_" not in l and "ravg_" not in l]
    ls_ = [l[:-5] for l in wins_ls]
#     len(ls_)

    if X_scales is not None:
        unscale_data(X_bopt, X_labels_bopt, X_scales, sc_ls=wins_ls + \
        [l_ + "_losses" for l_ in ls_ if l_ + "_losses" in X_labels_bopt] + \
        [l_ + "_games" for l_ in ls_ if l_ + "_games" in X_labels_bopt])
#     X_bopt.max()

    # X_bopt = RobustScaler().fit_transform(X_bopt)
    n_wls = len(wins_ls)
    wins_arr = np.asarray([X_bopt[:, X_labels_bopt.index(l)] for l in wins_ls])
    # pr_fl(wins_ls[-10:])
    # pr_fl(wins_arr[-10:])
    # sys.exit()
    # pr_fl(wins_arr.shape)
    new_ls = []
    losses_list = []
    losses_ls = []
    games_ls = []
    wr_ls = []
    new_ls = []
    new_ngls = []
    for l_i in range(n_wls):
        l_ = ls_[l_i]
        losses_l = l_ + "_losses"
        games_l = l_ + "_games"
        if losses_l in X_labels_bopt:
            losses_ls.append(losses_l)
            losses_list.append(X_bopt[:, X_labels_bopt.index(losses_l)])
#         elif games_l in X_labels_bopt:
        else:
            games = X_bopt[:, X_labels_bopt.index(games_l)] - wins_arr[l_i]
            losses_list.append(X_bopt[:, X_labels_bopt.index(games_l)] - wins_arr[l_i])
#         else:
#             pr_fl("ERROR: " + wins_ls[l_i])
#             continue
        wr_l = l_ + "_win_rate"
        if wr_l in X_labels_bopt:
            wr_ls.append(wr_l)

        new_l = l_ + "_bayes_win_rate"
        new_ls.append(new_l)
        if incl_n_games:
            new_ngl = l_ + "_games"
            new_ngls.append(new_ngl)
    wins_arr = wins_arr.T
    losses_arr = np.vstack(losses_list).T
    # pr_fl(wins_arr.shape, losses_arr.shape)
    # pr_fl(wins_arr.max(), losses_arr.max(), wins_arr.min(), losses_arr.min())
    n_games_arr = wins_arr + losses_arr
    new_wins_arr = (wins_arr + alpha)
    # pr_fl([wins_ls[i] for i in np.nonzero((new_wins_arr + (losses_arr + beta)) == 0)[1]])
    bayes_wr_arr = new_wins_arr / (new_wins_arr + (losses_arr + beta))
    # wl_data = [wins_arr, losses_arr, bayes_wr_arr] + ([n_games_arr] if incl_n_games else [])

    # del_ls = wins_ls + games_ls + losses_ls + wr_ls
    del_ls = games_ls + wr_ls
#     del_ls = games_ls
    X_bopt, X_labels_bopt = remove_data_subset(X_bopt, X_labels_bopt, del_ls)

    # Add the win rate (and n_games) features
    X_bopt = np.hstack([X_bopt] + ([n_games_arr] if incl_n_games else []) + [bayes_wr_arr])
    X_labels_bopt += new_ls + new_ngls
    
    return X_bopt, X_labels_bopt


# def load_by_patch(lfunc, fn, **kwargs):
