#
#  Refresh worker (runs on worker server)
#


. ~/lolorb/Constants.sh


cd ~/opggapi/
git pull
/home/ubuntu/.nvm/versions/node/v$NPM_VER/bin/forever restart server_cluster.js

cd ~/lolorb/
git pull
dos2unix *.sh
$PYEXEC restart_workers.py


