#
#  Server methods
#


import subprocess
import psutil
import signal

from ServerConstants import *
from Learning import *



class StaleRequestException(Exception):
    pass


def incr_counter(log_vars, key, v=1):
    with log_vars[key][0]:
        log_vars[key][1] += v

def get_wrapper(key):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(log_vars, req_d, *args, **kwargs):
            # If request has gone stale, drop
            if key in stale_detect_funcs and req_d is not None and req_d["req_i"] is not None:
                request_i = log_vars["pgdb_threadpool"].submit(get_request_index,
                    log_vars, req_d, log_vars["pgdb_cond"], log_vars["pgdb_db"]).result()
                if request_i is not None and request_i > req_d["req_i"]:
                    raise StaleRequestException

            incr_counter(log_vars, key)
            # if log_vars[key][1] > 0 and key == "pg_active":
            #     pr_fl(str(func), "OPEN", log_vars[key][1])
            res = None
            try:
                res = func(log_vars, req_d, *args, **kwargs)
            except Exception as e:
                # if not isinstance(e, StaleRequestException):
                #     pr_fl(req_d)
                #     for line in traceback.format_tb(e.__traceback__):
                #         pr_fl(line)
                #     pr_fl(type(e))
                #     pr_fl(e)
                incr_counter(log_vars, key, -1)
                # if isinstance(e, StaleRequestException):
                # if log_vars[key][1] > 0 and key == "pg_active":
                #     pr_fl(str(func), "CLOSE", log_vars[key][1])
                raise e
            incr_counter(log_vars, key, -1)
            # if log_vars[key][1] > 0 and key == "pg_active":
            #     pr_fl(str(func), "CLOSE", log_vars[key][1])

            # If request has gone stale, drop
            if key in stale_detect_funcs and req_d is not None and req_d["req_i"] is not None:
                request_i = log_vars["pgdb_threadpool"].submit(get_request_index,
                    log_vars, req_d, log_vars["pgdb_cond"], log_vars["pgdb_db"]).result()
                if request_i is not None and request_i > req_d["req_i"]:
                    raise StaleRequestException
            return res
        return wrapper
    return decorator

mp_decorator = get_wrapper("mp_active")
pl_decorator = get_wrapper("pl_active")
op_decorator = get_wrapper("op_active")
pg_decorator = get_wrapper("pg_active")
renew_decorator = get_wrapper("renew_active")
stale_detect_funcs = set(["op_active", "pl_active"])


@pl_decorator
def get_opgg_profile(log_vars, req_d, pgdb_tp, pgdb_cond, db, opgg_threadpool, region, name):

    id_ = region + '_' + name

    renew_req_d = req_d
    # renew_req_d = {**req_d, **{"req_i": None}}
    # pr_fl(name)
    # cols = players.c.keys()
    cols = cols_to_use

    cache_entry = pgdb_tp.submit(cache_db_player, log_vars, req_d, pgdb_cond, db, region, name).result()

    # Load cache result
    timestamp = None
    summoner_id = None
    rs = None
    rf, cf = None, None
    season_pcs = {season_id: None for season_id in seasons_to_use}
    if cache_entry is not None:
        # pr_fl(cache_entry[2], type(cache_entry[2]))
        ts = cache_entry[cols.index("timestamp")]
        timestamp = float(ts) if ts is not None else time.time()
        sid = cache_entry[cols.index("summoner_id")]
        summoner_id = int(sid) if sid is not None else np.random.randint(10000)
        rs = cache_entry[cols.index("ranked_summary")]
        ch = cache_entry[cols.index("intermediate_features")]
        rf = cache_entry[cols.index("ranked_features")]
        cf = cache_entry[cols.index("champion_features")]
        for season_id in seasons_to_use:
            # season_data = cache_entry[cols.index("season_" + season_id)]
            season_pcs[season_id] = cache_entry[cols.index("season_" + str(season_id))]

    # Figure out what to request from op.gg
    req_seasons = {season_id: season_pcs[season_id] == None for season_id in seasons_to_use[:-1]}
    expired = time.time() - timestamp > opgg_expiry if timestamp is not None else True
    req_rs = expired
    req_seasons[curr_season_id] = expired or season_pcs[curr_season_id] == None
    if cache_entry is not None and timestamp < season_start:  # Decide whether to request last season's champion stats
        req_seasons[curr_season_id - 1] = True

    # If everything is up-to-date, just return
    if not req_rs and all([not req_seasons[i] for i in seasons_to_use]):
        return 0, rs, [season_pcs[i] for i in seasons_to_use], ch, rf, cf, False

    # If player does not exist in database, check they exist on op.gg, then request everything else
    if cache_entry is None:
        rs = opgg_threadpool.submit(request_player_rs, log_vars, renew_req_d, pgdb_tp, pgdb_cond, db, region, name, cached=False).result()
        if rs is None:
            # pr_fl("rs req failed: " + name)
            return -1, -1, -1, None, None, None, None
        summoner_id = rs[opgg_rs_db_keys.index("summonerId")]

        # Request player renewal, and player champion stats for recent seasons
        opgg_threadpool.submit(renew_summoner, log_vars, renew_req_d, region, summoner_id, 0)
        pcs_futs = [opgg_threadpool.submit(request_player_pcs, log_vars, renew_req_d,
            pgdb_tp, pgdb_cond, db, region, season_id, name, summoner_id) for season_id in seasons_to_use]

        # Set thread to request ranked summary and recent champ stats again in ~10 seconds
        Timer(renew_sleep_time, delayed_update, (log_vars, renew_req_d, pgdb_tp, pgdb_cond, db, region, name, time.time())).start()

        # Set the last renewal timestamp
        pgdb_tp.submit(set_renewing_flag, log_vars, renew_req_d, pgdb_cond, db, region, name, time.time())

        # Update summoner id and timestamp
        pgdb_tp.submit(update_sid_ts, log_vars, renew_req_d, pgdb_cond, db, id_, summoner_id, time.time())

        # pr_fl(name + " success")
        return 1, rs, [f.result() for f in pcs_futs], None, None, None, True

    # Cache entry expired - renew and get new data
    rn = pgdb_tp.submit(get_renewing_flag, log_vars, req_d, pgdb_cond, db, region, name).result()
    renewing = not ( rn == None or time.time() - rn > renew_sleep_time + op_load_time )

    if not renewing:
        # Otherwise, if we need a new ranked summary
        opgg_threadpool.submit(renew_summoner, log_vars, renew_req_d, region, summoner_id, 0)

        # Set thread to request ranked summary and recent champ stats again in ~7 seconds
        Timer(renew_sleep_time, delayed_update, (log_vars, renew_req_d, pgdb_tp, pgdb_cond, db, region, name, timestamp)).start()

        # Set the last renewal timestamp
        pgdb_tp.submit(set_renewing_flag, log_vars, renew_req_d, pgdb_cond, db, region, name, time.time())

    rs_fut = None
    if req_rs:
        rs_fut = opgg_threadpool.submit(request_player_rs, log_vars, req_d, pgdb_tp, pgdb_cond, db, region, name)

    # If we need any player champion stats
    pcs_futs = {}
    for i in seasons_to_use:
        if req_seasons[i]:
            pcs_futs[i] = opgg_threadpool.submit(request_player_pcs, log_vars, req_d,
                pgdb_tp, pgdb_cond, db, region, i, name, summoner_id)

    # Update timestamp
    pgdb_tp.submit(update_timestamp, log_vars, req_d, pgdb_cond, db, id_, time.time())

    new_rs = rs
    if req_rs:
        # Update summoner id
        old_summoner_id = rs[opgg_rs_db_keys.index("summonerId")]
        new_rs = rs_fut.result()
        if new_rs is None:
            new_rs = rs
        summoner_id = rs[opgg_rs_db_keys.index("summonerId")]
        if old_summoner_id != summoner_id:
            # Name change - re-request remaining player champion stats
            req_seasons = {season_id: not req_seasons[season_id] for season_id in seasons_to_use}

            for i in seasons_to_use:
                if req_seasons[i]:
                    pcs_futs[i] = opgg_threadpool.submit(request_player_pcs, log_vars, req_d,
                        pgdb_tp, pgdb_cond, db, region, i, name, summoner_id)
            req_seasons = {season_id: True for season_id in seasons_to_use}

            # if new_rs is None:
            #     pr_fl("rs fut fail: " + name)
            pgdb_tp.submit(update_sid_ts, log_vars, req_d, pgdb_cond, db, id_, summoner_id, time.time())

    for i in seasons_to_use:
        if req_seasons[i]:
            season_pcs[i] = pcs_futs[i].result()
    return 2, new_rs, [season_pcs[i] for i in seasons_to_use], None, None, None, True


def connect_pg(db, pgdb_cond):
    with pgdb_cond:
        engine = db.connect()
    meta = sqlalchemy.MetaData(engine)
    return engine, meta

def connect_pg_table(db, pgdb_cond, table_name):
    engine, meta = connect_pg(db, pgdb_cond)
    table = sqlalchemy.Table(table_name, meta, autoload=True, autoload_with=engine)
    return engine, meta, table

def connect_pg_pl(db, pgdb_cond):
    return connect_pg_table(db, pgdb_cond, 'players')
def connect_pg_cl(db, pgdb_cond):
    return connect_pg_table(db, pgdb_cond, 'clients')
def connect_pg_rc(db, pgdb_cond):
    return connect_pg_table(db, pgdb_cond, 'rcache')
def connect_pg_lg(db, pgdb_cond):
    return connect_pg_table(db, pgdb_cond, 'wlogs')
def connect_pg_clf(db, pgdb_cond):
    return connect_pg_table(db, pgdb_cond, 'clfails')
def connect_pg_conf(db, pgdb_cond):
    return connect_pg_table(db, pgdb_cond, 'sysconf')

@op_decorator
def rito_req_op(log_vars, req_d, *args, **kwargs):
    return rito_req(*args, **kwargs)

@op_decorator
def ranked_matchlist_op(log_vars, req_d, *args, **kwargs):
    return ranked_matchlist(*args, **kwargs)

# Get matches for the four days previous to the target timestamp
five_days = 14 * 24 * 60 * 60
five_months = 5 * 30 * 24 * 60 * 60
max_n_games = 20
def get_riot_mh(log_vars, req_d, pgdb_tp, pgdb_cond, db, region, name, req_timestamp, r_conds, r_tss):

    id_ = region + '_' + name

    # First, check cache for match history as far back as the target timestamp - 4 days
    entries = cache_riotmh(log_vars, req_d, pgdb_cond, db, id_)

    success = False
    history = []
    oldest_ts = np.inf
    if entries is not None:
        starts, ends, timestamps, histories = list(zip(*entries))
        oldest_ts = min(ends)
        if max(timestamps) > min(req_timestamp, time.time() - (5 * 60)) and min(ends) < req_timestamp - five_days:
            success = True
            history = sum(histories, [])

    acc_id = None
    if not success:  # Get match history from Riot up to the target timestamp
        try:
            acc_id = get_encrypted_acc_id(r_conds, r_tss, region, name=name)
            if acc_id is None:
                raise ValueError
        except Exception as e:
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl("Failed to get Riot account id for " + name, type(e), e)
            return []

        end_ts = np.inf
        curr_i = 0
        histories = []
        # while end_ts > req_timestamp - (five_days + five_months): # Add a five month buffer so we save time later
        while end_ts > req_timestamp - five_days:
            new_mh = None
            begin_i = curr_i * 100
            end_i = begin_i + 100
            curr_i += 1
            try:
                pr_fl("Downloading match history [" + str(begin_i) + "] for " + name)
                new_mh = ranked_matchlist_op(log_vars, req_d, r_conds, r_tss, region, acc_id,
                    params={"queue": [rito_queue_ids["RANKED_SOLO_5x5"]], "beginIndex": begin_i, "endIndex": end_i})['matches']
            except Exception as e:
                if isinstance(e, StaleRequestException):
                    raise e
                for line in traceback.format_tb(e.__traceback__):
                    pr_fl(line)
                pr_fl(type(e))
                pr_fl(e)
                break

            if len(new_mh) == 0:
                break

            end_ts = new_mh[-1]["timestamp"] / 1000
            start_ts = new_mh[0]["timestamp"] / 1000
            insert_new_riotmh(log_vars, req_d, pgdb_cond, db, id_, new_mh, end_ts, start_ts)
            histories.append(new_mh)

        if histories == []:
            return []  # Could not download match history
        
        history = sum(histories, [])

    pr_fl("MH size for " + name + ": " + str(len(history)))

    # Get the set of game ids which we need to request
    less_than_test = False
    ts_ = np.inf
    game_ids = set()
    for match in history:
        game_id = match["gameId"]
        ts = match["timestamp"] / 1000
        if ts < ts_:
            less_than_test = True
        else:
            less_than_test = False
        ts_ = ts
        if game_id in game_ids:
            continue

        if ts < req_timestamp and ts > req_timestamp - five_days:
            game_ids.add(game_id)

    pr_fl("#recent for " + name + ": " + str(len(game_ids)))

    # For each of these games, check the cache, then, if not found, request from Riot
    games = []
    for game_id in game_ids:
        # pr_fl("CHECKING CACHE " + region + " " + str(game_id))
        game = cache_riotgame(log_vars, req_d, pgdb_cond, db, region, game_id)
        # pr_fl(type(game))
        if game is None:
            try:
                pr_fl("Downloading game for " + name, less_than_test)
                game = rito_req_op(log_vars, req_d, r_conds, r_tss, region, game_by_game_id + str(game_id))
                if game is not None and game["queueId"] != rito_queue_ids["RANKED_SOLO_5x5"]:
                    pr_fl("Wrong queue for game: " + str(game["queueId"]))
                    raise ValueError
            except Exception as e:
                if isinstance(e, StaleRequestException):
                    raise e
                for line in traceback.format_tb(e.__traceback__):
                    pr_fl(line)
                pr_fl(type(e))
                pr_fl(e)
                continue
            if game is None:
                continue
            ts = game["gameCreation"]
            # pr_fl(type(game))
            # game_copy = deepcopy(game)
            insert_new_riotgame(log_vars, req_d, pgdb_cond, db, region, game_id, ts, game)
            # g = cache_riotgame(log_vars, req_d, pgdb_cond, db, region, game_id)
            # if g is None:
            #     pr_fl("FAILED TO INSERT GAME " + region + " " + str(game_id))
            # pr_fl(type(game_copy))
        else:
            game = game[1]  # Tuple of game_creation timstamp and game json
        ts = game["gameCreation"]
        games.append((ts, game))
        if len(games) > max_n_games + 10:
            break

    if games == []:
        pr_fl("Match history empty for " + name)
        return []  # Either no games played or request(s) failed

    # Sort games reverse-chronologically
    games.sort()
    games = games[::-1]

    # Get most recent max_n_games matches
    games = games[:max_n_games]

    summ_id = None
    if acc_id is None:
        try:
            acc_id, summ_id = get_encrypted_acc_id(r_conds, r_tss, region, name=name, ret_summ_id=True)
        except Exception as e:
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl("Failed to get Riot account id for " + name, type(e), e)
    # pr_fl(name, acc_id)
    rs_games = [get_opgg_rec_feats(r_conds, r_tss, game, name, acc_id, summ_id) for _, game in games]       # Convert to op.gg player summary format
    rs_games = [g for g in rs_games if g is not None]
    rs_games = [[rsg[k] for k in opgg_rs_game_keys] for rsg in rs_games]   # Convert to slim list format
    return rs_games


def get_opgg_rec_feats(r_conds, r_tss, game, name, acc_id, summ_id):
    pl_i, pl = 0, None
    for pi in game["participantIdentities"]:
        # if pi["player"]["currentAccountId"] == acc_id:
        if "player" in pi and (lowercasify(pi["player"]["summonerName"]) == lowercasify(name) or \
          pi["player"]["currentAccountId"] == acc_id or pi["player"]["accountId"] == acc_id or \
          ("summonerId" in pi["player"] and pi["player"]["summonerId"] == summ_id)):
            pl_i = pi["participantId"]
            break
    for pl_ in game["participants"]:
        if pl_["participantId"] == pl_i:
            pl = pl_
            break

    if pl is None:
        pr_fl("FAILED TO TO FIND PLAYER IN GAME: " + name + " in " + str(game["gameId"]))
        return None
    if game["queueId"] not in r_queue_ids:
        pr_fl("503 Wrong queue type " + str(game["queueId"]))
        return None
    if game["mapId"] != sr_map_id:
        pr_fl("504 Wrong map " + game["mapId"])
        return None
    if game["gameDuration"] < 10 * 60:
        pr_fl("505 Remake")
        return None

    blue_team = pl["teamId"] == 100
    team_id = 100 if blue_team else 200
    win = (game["teams"][0]["win"] == "Win" and blue_team) or (game["teams"][1]["win"] == "Win" and not blue_team)
    cid = pl["championId"]
    res = {}
    res["type"] = "Ranked Solo"
    dur = int(game["gameDuration"])
    res["timestamp"] = int(game["gameCreation"] / 1000) + dur
    dur_mins = dur // 60
    dur_m = 60 * (dur_mins)
    dur_secs = dur - dur_m
    res["length"] = str(dur_mins) + 'm ' + str(dur_secs) + 's'
    res["result"] = "Victory" if win else "Defeat"
    res["champion"] = champion_names[cid]
    res["spell1"] = spells_lib[pl["spell1Id"]]
    res["spell2"] = spells_lib[pl["spell2Id"]]
    kills = pl["stats"]["kills"]
    deaths = pl["stats"]["deaths"]
    assists = pl["stats"]["assists"]
    res["kills"] = kills
    res["deaths"] = deaths
    res["assists"] = assists
    res["ratio"] = ((kills + assists) / deaths) if deaths > 0 else (kills + assists)
    res["level"] = pl["stats"]["champLevel"]
    res["cs"] = pl["stats"]["totalMinionsKilled"]
    res["csps"] = pl["stats"]["totalMinionsKilled"] / dur
    res["pinksPurchased"] = pl["stats"]["visionWardsBoughtInGame"]
    team_kills_total = sum([pl_["stats"]["kills"] \
                                    for pl_ in game["participants"] if pl_["teamId"] == team_id])
    res["killParticipation"] = int(100 * kills / team_kills_total) if team_kills_total > 0 else 0
    return res


@pg_decorator
def cache_riotmh(log_vars, req_d, pgdb_cond, db, id_):
    engine, meta, table = connect_pg_table(db, pgdb_cond, 'mhistories')
    # query = table.select().where(table.c.id == region + '_' + name)
    query = sqlalchemy.sql.expression.select([table.c[k] \
        for k in ["mh_start", "mh_end", "timestamp", "mhistory"]]).where(table.c.player == id_)
    # query = sqlalchemy.sql.expression.select([table.c[k] \
    #     for k in cols_to_use]).get(region + '_' + name)
    entry = list(pg_exec(engine, query))
    # pr_fl(entry)
    engine.close()
    if len(entry) == 0:
        return None
    return entry

@pg_decorator
def insert_new_riotmh(log_vars, req_d, pgdb_cond, db, id_, res, end_ts, start_ts):
    engine, meta, table = connect_pg_table(db, pgdb_cond, 'mhistories')
    ins = table.insert().values(id=make_shortlink(16), player=id_, timestamp=int(time.time()),
        mh_start=start_ts, mh_end=end_ts, mhistory=res)
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def cache_riotgame(log_vars, req_d, pgdb_cond, db, region, game_id):
    engine, meta, table = connect_pg_table(db, pgdb_cond, 'rgames')
    # query = table.select().where(table.c.id == region + '_' + name)
    query = sqlalchemy.sql.expression.select([table.c[k] \
        for k in ["game_creation", "game"]]).where(table.c.id == region + '_' + str(game_id))
    # query = sqlalchemy.sql.expression.select([table.c[k] \
    #     for k in cols_to_use]).get(region + '_' + name)
    entry = list(pg_exec(engine, query))
    # pr_fl(entry)
    engine.close()
    # pr_fl("CACHE CHECK RESULT: " + region + " " + str(game_id)+' ' + str(len(entry)))
    if len(entry) == 0:
        return None
    # else:
    #     pr_fl([e[0] for e in entry])
    return entry[0]

@pg_decorator
def insert_new_riotgame(log_vars, req_d, pgdb_cond, db, region, game_id, ts, game):
    engine, meta, table = connect_pg_table(db, pgdb_cond, 'rgames')
    ins = table.insert().values(id=region + '_' + str(game_id), timestamp=int(time.time()), game_creation=ts,
        game=game)
    pg_exec(engine, ins)
    engine.close()


@op_decorator
def get_opgg_pastelo(log_vars, req_d, pgdb_tp, pgdb_cond, db, region, name, summ_id, req_timestamp):

    id_ = region + '_' + name

    cache_entries = pgdb_tp.submit(cache_pastelo, log_vars, req_d, pgdb_cond, db, id_).result()
    
    # We want the oldest entry that is still from after the target timestamp
    res = None
    curr_ts = np.inf
    if cache_entries is not None:
        for _, timestamp, elo in cache_entries:
            if timestamp < curr_ts and timestamp >= req_timestamp:
                res = elo
                curr_ts = timestamp
    
    if res is None:  # Create new entry
        res = [req("https://" + region + ".op.gg/summoner/ajax/lpHistory.json/summonerId=" + str(summ_id) + "&period=" + period) \
            for period in ["week", "day"]]
        if res[1] is None or "categories" not in res[1] or "data" not in res[1]:
            res = [res[0]]
        if res[0] is None or "categories" not in res[0] or "data" not in res[0]:
            res = None

        if res is not None:
            pgdb_tp.submit(insert_new_pastelo, log_vars, req_d, pgdb_cond, db, id_, res)

    if res is None:
        return None, False

    dates, values = [], []
    if len(res) > 1:
        dates += res[1]["categories"]
        values += res[1]["data"]
    for i in range(len(res[0]["categories"])):
        d = res[0]["categories"][i]
        if d not in dates:
            dates.append(d)
            values.append(res[0]["data"][i])

    dates = [[int(d_) for d_ in d.split('.')] if '.' in d else [None, "now"] for d in dates]
    now = time.time()
    dates = [(datetime(2019, int(month), 1) + timedelta(days=max(day - 1, 0))).timestamp() if day != "now" else now for month, day in dates]
    values = [(100 * min(24, opgg_shortleag[v["tierRank"]])) + v["lp"] for v in values]

    data = list(zip(dates, values))
    data.sort()
    data = np.asarray(data).T
    res = np.interp(req_timestamp, data[0], data[1])

    return res, data[0][-1] < now # and data[0][0] > now


@pg_decorator
def cache_pastelo(log_vars, req_d, pgdb_cond, db, id_):
    engine, meta, table = connect_pg_table(db, pgdb_cond, 'pastelo')
    # query = table.select().where(table.c.id == region + '_' + name)
    query = sqlalchemy.sql.expression.select([table.c[k] \
        for k in ["id", "timestamp", "elo"]]).where(table.c.player == id_)
    # query = sqlalchemy.sql.expression.select([table.c[k] \
    #     for k in cols_to_use]).get(region + '_' + name)
    entry = list(pg_exec(engine, query))
    # pr_fl(entry)
    engine.close()
    if len(entry) == 0:
        return None
    return entry

@pg_decorator
def insert_new_pastelo(log_vars, req_d, pgdb_cond, db, id_, res):
    engine, meta, table = connect_pg_table(db, pgdb_cond, 'pastelo')
    ins = table.insert().values(id=make_shortlink(16), player=id_, timestamp=int(time.time()), elo=res)
    pg_exec(engine, ins)
    engine.close()



@pg_decorator
def cache_db_player(log_vars, req_d, pgdb_cond, db, region, name):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    # query = players.select().where(players.c.id == region + '_' + name)
    query = sqlalchemy.sql.expression.select([players.c[k] \
        for k in cols_to_use]).where(players.c.id == region + '_' + name)
    # query = sqlalchemy.sql.expression.select([players.c[k] \
    #     for k in cols_to_use]).get(region + '_' + name)
    entry = list(pg_exec(engine, query))
    # pr_fl(entry)
    engine.close()
    if len(entry) == 0:
        return None
    return entry[0]

@pg_decorator
def update_ch_cache(log_vars, req_d, pgdb_cond, db, regions, names, chs):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    inss = [players.update().where(players.c.id == region + '_' + name)
        .values(intermediate_features=parsePyJ(ch)) for region, name, ch in zip(regions, names, chs)]
    for ins in inss:
        pg_exec(engine, ins)
    engine.close()

@pg_decorator
def update_rf_cache(log_vars, req_d, pgdb_cond, db, regions, names, rfs):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    inss = [players.update().where(players.c.id == region + '_' + name)
        .values(ranked_features=parsePyJ(rf)) for region, name, rf in zip(regions, names, rfs)]
    for ins in inss:
        pg_exec(engine, ins)
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def update_cf_cache(log_vars, req_d, pgdb_cond, db, regions, names, cfs):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    inss = [players.update().where(players.c.id == region + '_' + name)
        .values(champion_features=parsePyJ(cf)) for region, name, cf in zip(regions, names, cfs)]
    for ins in inss:
        pg_exec(engine, ins)
    pg_exec(engine, ins)
    engine.close()

@renew_decorator
def delayed_update(log_vars, req_d, pgdb_threadpool, pgdb_cond, db, region, name, timestamp):
    rs = request_player_rs(log_vars, {**req_d, **{"req_i": None}}, pgdb_threadpool, pgdb_cond, db, region, name)
    if rs is None:
        return
    summoner_id = rs[opgg_rs_db_keys.index("summonerId")]
    request_player_pcs(log_vars, {**req_d, **{"req_i": None}}, pgdb_threadpool, pgdb_cond, db, region, curr_season_id, name, summoner_id)
    if timestamp < season_start:
        request_player_pcs(log_vars, {**req_d, **{"req_i": None}}, pgdb_threadpool, pgdb_cond, db, region, curr_season_id - 1, name, summoner_id)
    id_ = region + '_' + name
    pgdb_threadpool.submit(update_sid_ts, log_vars, {**req_d, **{"req_i": None}}, pgdb_cond, db, id_, summoner_id, time.time())

    # Delete (expired) cache feature data for this player
    pgdb_threadpool.submit(update_ch_cache, log_vars, req_d, pgdb_cond, db, [region], [name], [{}])
    pgdb_threadpool.submit(update_rf_cache, log_vars, req_d, pgdb_cond, db, [region], [name], [{}])
    pgdb_threadpool.submit(update_cf_cache, log_vars, req_d, pgdb_cond, db, [region], [name], [{}])


@pg_decorator
def update_sid_ts(log_vars, req_d, pgdb_cond, db, id_, sid, ts):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    ins = players.update().where(players.c.id == id_).values(summoner_id=sid)
    pg_exec(engine, ins)
    ins = players.update().where(players.c.id == id_).values(timestamp=int(ts))
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def update_timestamp(log_vars, req_d, pgdb_cond, db, id_, ts):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    ins = players.update().where(players.c.id == id_).values(timestamp=int(ts))
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def insert_new_rs(log_vars, req_d, pgdb_cond, db, id_, rs, cached):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    ins = None
    if not cached:
        ins = players.insert().values(id=id_, ranked_summary=rs)
    else:
        ins = players.update().where(players.c.id == id_).values(ranked_summary=rs)
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def insert_new_pcs(log_vars, req_d, pgdb_cond, db, id_, kwargs):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    ins = players.update().where(players.c.id == id_).values(**kwargs)
    pg_exec(engine, ins)
    engine.close()

@op_decorator
def renew_summoner(log_vars, req_d, *args, **kwargs):
    return opgg_renew_summoner(*args, **kwargs)

rs_default = {
    'lp': 50,
    'wins': 0,
    'losses': 0,
    'winRatio': 50.0,
}
recent_default = {
    'winRatio': 50.0,
    'wins': 0,
    'losses': 0,
    'games': 0,
    'killsAverage': 5,
    'deathsAverage': 5,
    'assistsAverage': 7,
    'kdaRatio': 2.5,
    'killParticipation': 50.0,
}
rs_game_default = {
    "wins": 0,
    "losses": 0,
    "games": 0,
    "winRatio": 50.0,
    'killsAverage': 5,
    'deathsAverage': 5,
    'assistsAverage': 7,
    'kdaRatio': 2.5,
    'killParticipation': 50.0,
    # 'turrets': 0,
}
def format_rs(rs):
    rs_x = [rs[k] if k in rs else rs_default[k] for k in opgg_rs_keys[:-2]]
    recent = rs["recent"] if "recent" in rs else {}
    for k in opgg_rs_recent_keys:
        rs_x.append(recent[k] if k in recent else recent_default[k])
    games = rs["games"] if "games" in rs else []
    rs_x.append([[games[i][k] if k in games[i] else rs_game_default[k] for k in opgg_rs_game_keys] for i in range(len(games))])
    return rs_x

@op_decorator
def request_player_rs(log_vars, req_d, pgdb_threadpool, pgdb_cond, db, region, name, cached=True, **kwargs):
    rs = opgg_ranked_summary(region, name, wait_t=0)
    if rs == {}:
        return None
        # return deepcopy(rs_default);
    rs = format_rs(rs)
    id_ = region + '_' + name
    pgdb_threadpool.submit(insert_new_rs, log_vars, req_d, pgdb_cond, db, id_, rs, cached)
    return rs

pcs_default = {
    'rank': 46.6,
    'name': "Ezreal",
    'wins': 0,
    'losses': 0,
    'winRatio': 50.0,
    'kills': 6,
    'deaths': 6,
    'assists': 8,
    'ratio': 2.5,
    'gold': 12686,
    'cs': 175,
    # 'turrets': ,
    'maxKills': 18,
    'maxDeaths': 18,
    'damageDealt': 23835,
    'damageTaken': 17652,
    'doubleKill': 10,
    'tripleKill': 1,
    'quadraKill': 0,
    'pentaKill': 0,

}
def format_pcs(pcs):
    return [[c[k] if k in c else pcs_default[k] for k in opgg_pcs_keys] for c in pcs]

@op_decorator
def request_player_pcs(log_vars, req_d, pgdb_threadpool, pgdb_cond, db, region, season_id, name, summoner_id, **kwargs):
    pcs = opgg_player_champ_stats(region, season_id, summoner_id=summoner_id)
    if pcs == {}:
        pcs = -1
    else:
        pcs = format_pcs(pcs)
    id_ = region + '_' + name
    kwargs = { "season_" + str(season_id): pcs }
    pgdb_threadpool.submit(insert_new_pcs, log_vars, req_d, pgdb_cond, db, id_, kwargs)
    return pcs

@pg_decorator
def get_renewing_flag(log_vars, req_d, pgdb_cond, db, region, name):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    query = sqlalchemy.sql.expression.select([players.c['renewal_timestamp'] \
            ]).where(players.c.id == region + '_' + name)
    entry = list(pg_exec(engine, query))
    engine.close()
    if len(entry) == 0:
        return None
    return entry[0][0]

@pg_decorator
def set_renewing_flag(log_vars, req_d, pgdb_cond, db, region, name, ts):
    engine, meta, players = connect_pg_pl(db, pgdb_cond)
    ins = players.update().where(players.c.id == region + '_' + name).values(renewal_timestamp=int(ts))
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def get_request_index(log_vars, req_d, pgdb_cond, db):
    engine, meta, clients = connect_pg_cl(db, pgdb_cond)
    query = sqlalchemy.sql.expression.select([clients.c['request_index'] \
            ]).where(clients.c.session_id == req_d['session_id'])
    entry = list(pg_exec(engine, query))
    engine.close()
    if len(entry) == 0:
        return None
    return entry[0][0]

@pg_decorator
def set_request_index(log_vars, req_d, pgdb_cond, db, cached=False):
    engine, meta, clients = connect_pg_cl(db, pgdb_cond)
    if not cached:
        ins = clients.insert().values(session_id=req_d['session_id'],
            request_index=req_d["req_i"],
            expiry_timestamp=int(time.time() + session_lifespan))
    else:
        ins = clients.update().where(clients.c.session_id == req_d['session_id']).values(
            request_index=req_d["req_i"],
            expiry_timestamp=int(time.time() + session_lifespan))
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def cache_db_request(log_vars, req_d, pgdb_cond, db, request_hash):
    engine, meta, rcache = connect_pg_rc(db, pgdb_cond)
    query = sqlalchemy.sql.expression.select([rcache.c[k] \
        for k in ['result', 'rorder']]).where(
        rcache.c.request_hash == request_hash and rcache.c.expiry_timestamp > int(time.time()))
    entry = list(pg_exec(engine, query))
    engine.close()
    if len(entry) == 0:
        return None
    return entry[0]

@pg_decorator
def cache_db_request_by_shortlink_wrapped(log_vars, req_d, pgdb_cond, db, sl):
    return cache_db_request_by_shortlink(log_vars, req_d, pgdb_cond, db, sl)

def cache_db_request_by_shortlink(log_vars, req_d, pgdb_cond, db, sl):
    engine, meta, rcache = connect_pg_rc(db, pgdb_cond)
    query = sqlalchemy.sql.expression.select([rcache.c[k] \
        for k in ['request_hash', 'result', 'rorder']]).where(rcache.c.shortlink == sl)
    entry = list(pg_exec(engine, query))
    engine.close()
    if len(entry) == 0:
        return None
    return entry[0]

@pg_decorator
def get_new_shortlink(log_vars, req_d, pgdb_cond, db):
    sl = None
    used = True
    while used:
        sl = make_shortlink()
        res = cache_db_request_by_shortlink(log_vars, req_d, pgdb_cond, db, sl)
        if res is None:
            used = False
        else:
            pr_fl("Shortlink exists: " + res)
    return sl

@pg_decorator
def set_client_reqhash(log_vars, req_d, pgdb_cond, db, request_hash):
    engine, meta, clients = connect_pg_cl(db, pgdb_cond)
    rcache = sqlalchemy.Table('rcache', meta, autoload=True, autoload_with=engine)
    prev_hash = None
    # Try to get previous request hash for client
    query = sqlalchemy.sql.expression.select([clients.c[k] \
        for k in ['request_hash']]).where(clients.c.session_id == req_d['session_id'])
    entry = list(pg_exec(engine, query))
    if len(entry) != 0:
        prev_hash = entry[0][0]
    # Update client request hash
    ins = clients.update().where(clients.c.session_id == req_d['session_id']).values(request_hash=request_hash)
    # If we had a previous request hash, try to find it in the clients database
    if prev_hash is not None:
        query = sqlalchemy.sql.expression.select([clients.c['session_id'] \
                ]).where(clients.c.request_hash == prev_hash)
        entry = list(pg_exec(engine, query))
        if len(entry) == 0:  # If there are no clients with this request hash, set the usual cache lifespan
            query = sqlalchemy.sql.expression.select([rcache.c[k] \
                for k in ['timestamp']]).where(rcache.c.request_hash == prev_hash and rcache.c.typ == "normal")
            entry = list(pg_exec(engine, query))
            if len(entry) > 0:
                t = int(entry[0][0]) + request_cache_lifespan
                update_cache_info(log_vars, req_d, pgdb_cond, db, prev_hash, expiry=t, eng=(engine, meta, rcache))
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def insert_new_result(log_vars, req_d, pgdb_cond, db, request_hash, result, typ, comp, rorder, lifespan=request_cache_lifespan):
    engine, meta, rcache = connect_pg_rc(db, pgdb_cond)
    ins = None

    if "normal" in typ:
        result = deepcopy(result)
        result[9] = comp

    # pr_fl([type(r) for r in result])
    # pr_fl([type(r) for r in result[6]])
    # pr_fl([type(r) for r in result[7]])
    # pr_fl([((type(r) if not isinstance(r, list) else [type(ri) for ri in r]) if not isinstance(r, dict) else [type(ri) for ri in r.values()]) for r in result[9].values()])

    t = int(time.time())
    submission = {
        'shortlink': result[8],
        'type': typ,
        'timestamp': t,
        'expiry_timestamp': t + lifespan,
        'rorder': list([int(i) for i in rorder]),
        'result': result,
    }

    # If an entry exists already, update the result
    query = sqlalchemy.sql.expression.select([rcache.c[k] \
            for k in ['timestamp']]).where(rcache.c.request_hash == request_hash)
    entry = list(pg_exec(engine, query))
    if entry is None or len(entry) == 0 or entry[0] is None:
        ins = rcache.insert().values(**{**submission, **{'request_hash': request_hash}})
    else:
        ins = rcache.update().where(rcache.c.request_hash == request_hash).values(**submission)

    pg_exec(engine, ins)
    engine.close()

def pg_exec(engine, query):
    # return engine.execute(query)
    try:
        return engine.execute(query)
    except Exception as e:
        if isinstance(e, sqlalchemy.exc.IntegrityError):
            pr_fl("SQL ERROR: " + str(e).split('\n')[0])
            pass
        else:
            pr_fl("\n### UNEXPECTED POSTGRES ERROR ### " + str(query) + "\n")
            pr_fl(type(e))
            pr_fl(e)
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl(str(query))

@pg_decorator
def update_cache_info_wrapped(*args, **kwargs):
    return update_cache_info(*args, **kwargs)

def update_cache_info(log_vars, req_d, pgdb_cond, db, request_hash, typ=None, ts=None, expiry=None, eng=None):
    upd = {}
    if typ is not None: upd['type'] = typ
    if ts is not None: upd['timestamp'] = int(ts)
    if expiry is not None: upd['expiry_timestamp'] = int(expiry)
    if not upd:
        return
    engine, meta, rcache = None, None, None
    if eng is None:
        engine, meta, rcache = connect_pg_rc(db, pgdb_cond)
    else:
        engine, meta, rcache = eng
    ins = rcache.update().where(rcache.c.request_hash == request_hash).values(**upd)
    pg_exec(engine, ins)
    if eng is None:
        engine.close()

@pg_decorator
def clean_postgres(log_vars, req_d, pgdb_cond, db):
    # try:
    engine, meta, clients = connect_pg_cl(db, pgdb_cond)
    rcache = sqlalchemy.Table('rcache', meta, autoload=True, autoload_with=engine)
    ins = clients.delete().where(clients.c.expiry_timestamp < int(time.time()))
    pg_exec(engine, ins)
    ins = rcache.delete().where(rcache.c.expiry_timestamp < int(time.time()))
    pg_exec(engine, ins)
    engine.close()
    # except:
    #     for line in traceback.format_tb(e.__traceback__):
    #         pr_fl(line)
    #     pr_fl(type(e))
    #     pr_fl(e)

@pg_decorator
def new_wlog_entry(log_vars, req_d, pgdb_cond, db, wkey, log_str):
    engine, meta, wlogs = connect_pg_lg(db, pgdb_cond)
    ins = wlogs.insert().values(worker_key=wkey, timestamp=int(time.time() * 1000), log=log_str)
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def update_wlog(log_vars, req_d, pgdb_cond, db, wkey, log_str):
    engine, meta, wlogs = connect_pg_lg(db, pgdb_cond)
    ins = wlogs.update().where(wlogs.c.worker_key == wkey).values(timestamp=int(time.time() * 1000), log=log_str)
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def new_clfail_entry(log_vars, req_d, pgdb_cond, db, source, chat_log):
    engine, meta, clfails = connect_pg_clf(db, pgdb_cond)
    ins = clfails.insert().values(id=make_shortlink(), source=source, chat_log=chat_log)
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def update_op_status(log_vars, req_d, pgdb_cond, db, region, code, reason):
    engine, meta, sysconf = connect_pg_conf(db, pgdb_cond)
    submission = {
        "status_code": code, 
        "status_text": reason, 
    }
    ins = sysconf.update().where(sysconf.c.region == region).values(**submission)
    pg_exec(engine, ins)
    engine.close()

@pg_decorator
def get_op_status(log_vars, req_d, pgdb_cond, db, region):
    engine, meta, sysconf = connect_pg_conf(db, pgdb_cond)
    query = sqlalchemy.sql.expression.select([sysconf.c[k] \
        for k in ['status_code', "status_text", 'enabled', 'message']]).where(sysconf.c.region == region)
    entry = list(pg_exec(engine, query))
    engine.close()
    if len(entry) == 0:
        return None
    return entry[0]



def get_request_hash(req_data, region_i, avg_elo_i, ts, patch, regions, flags, t):
    # Remove ptr_cached data from request, as this is too much data for a single hash/postgres row
    flags = [f for f in flags if f[:11] != "ptr_cached:"]
    rd = [req_data, region_i, avg_elo_i, ts, patch, regions, flags, int(t / request_cache_lifespan)]
    return json.dumps(rd)


def prediction_request_handler(log_vars, req_d, full_args, pl_threadpool):
    pgdb_threadpool = full_args[1]
    pgdb_cond = full_args[2]
    db = full_args[3]

    # If chat-log-fail message, log the failure and send empty response
    if req_d["comp"] == -9000:
        pgdb_threadpool.submit(new_clfail_entry, log_vars, req_d, pgdb_cond, db, req_d["source"], req_d["shortlink"])
        return [50.0, -1, -1, -1000, -1, mp_err_codes["Chat log fail"], -1, -1, -1, -1, -1]

    # Update the client's most recent request index
    req_ind = pgdb_threadpool.submit(get_request_index, log_vars, req_d, pgdb_cond, db).result()

    if req_ind is not None and req_ind > req_d["req_i"]:
        return stale_request()
    pgdb_threadpool.submit(set_request_index, log_vars, req_d, pgdb_cond, db, cached=req_ind is not None)

    # If shortlink request, return the cached response and composition
    if req_d["shortlink"] != -1:
        res = pgdb_threadpool.submit(cache_db_request_by_shortlink_wrapped, log_vars, req_d, pgdb_cond, db, req_d["shortlink"]).result()
        if res is None:
            return [50.0, -1, -1, req_d["req_i"], -1, mp_err_codes["Shortlink not found"], -1, -1, -1, -1, -1]

        request_hash, res, _ = res
        # res = json.loads(res)
        pgdb_threadpool.submit(update_cache_info_wrapped, log_vars, req_d, pgdb_cond, db, request_hash,
            typ='solid', expiry=time.time() + shortlink_lifespan)

        res[3] = req_d["req_i"];
        # opgg_rcodes = [opgg_rcodes[j] for j in rorder]
        # opgg_rcodes = [opgg_rcodes[j] for j in restore_order]
        # renewals = [renewals[j] for j in rorder]
        # renewals = [renewals[j] for j in restore_order]
        # if pl_percs != -1:
        #     pl_percs = [pl_percs[j] for j in rorder]
        #     pl_percs = [pl_percs[j] for j in restore_order]
        # res[4] = opgg_rcodes
        # res[6] = renewals
        # res[7] = pl_percs

        return res

    try:
        res = process_prediction_request(log_vars, req_d, full_args, pl_threadpool)
    except Exception as e:
        if not isinstance(e, StaleRequestException):
            raise e
        return stale_request()
    return res


def stale_request():
    pr_fl("STALE REQUEST DROPPED")
    return [50.0, -1, -1, -1000, -1, mp_err_codes["Stale request"], -1, -1, -1, -1, -1]



@mp_decorator
def process_prediction_request(log_vars, req_d, full_args, pl_threadpool):
    # req_i = req_d[3]
    # time.sleep(0.5); # Simulate processing time
    # response = [req_i * 10, -1, -1, req_i]
    # return response


    opgg_threadpool, pgdb_threadpool, pgdb_cond, \
        db, gsdb, gsdb_cond, r_conds, r_tss, infill_data, rec_infill_data, model_dict, \
        model_strs, scales, n_train, n_test, n_pcal, X_scales, X_scalesdict, X_scaler, X_autolog, X_mins, \
        Yr_scales, Yr_scalesdict, pro_model, pro_sq_models = full_args

    # pr_fl("Request from", req_d["session_id"], str(log_vars["pg_active"][1]))

    req_data, region_i, avg_elo_i, req_i = req_d["data"], req_d["region_i"], req_d["avg_elo_i"], req_d["req_i"]
    req_timestamp, req_patch = req_d["timestamp"], req_d["patch"]
    req_regions = req_d["regions"]
    flags = req_d["flags"]

    pro_match = 'pro' in flags
    ptr_match = 'ptr_' in flags
    no_player_spec = "no_player_spec" in flags
    side_unknown = "side_unknown" in flags
    momentum = "momentum" in flags

    # Load cached and precomputed features for pro training past match prediction
    preloaded_features = False
    if ptr_match:
        cached_d = [d for d in flags if d[:11] == 'ptr_cached:']
        if cached_d != []:
            pr_fl("Pro training prediction: Loading cached X data for", req_patch, "match")
            cached_d = json.loads(cached_d[0].split('ptr_cached:')[1])
            X = np.asarray(cached_d['X'])
            X_labels = cached_d["X_labels"]
            preloaded_features = True

    # pr_fl("BEGIN REQueST")
    # pr([champion_names[rq[2]] for rq in req_data])

    # Reorder by team/role for compatibility with previous code (not much overhead)
    rorder = np.argsort([role_is[cols_all[rq[team_li]] + '_' + roles_all[rq[role_li]]] for rq in req_data])
    restore_order = np.argsort(rorder)
    req_data = [req_data[i] for i in rorder]
    req_regions = [req_regions[i] for i in rorder]

    # pr([champion_names[rq[2]] for rq in req_data])

    pro_players = {r: "?unknown~" for r in t_roles}
    for i in range(len(req_data)):
        rd = req_data[i]
        side = cols_all[rd[team_li]]
        role = roles_all[rd[role_li]]
        name = rd[name_li]
        if type(name) is str:
            name = lowercasify(name)
        reg = opgg_regions[req_regions[i]].upper()
        if name == -1:
            pro_players[side + '_' + role] = "?~unknown"
            pr_fl("unknown pro (no solo queue name given)")
        else:
            if (reg, name) in sq_inv_dict:
                pro_players[side + '_' + role] = sq_inv_dict[(reg, name)]
            else:
                pr_fl("unknown pro:", name)
    pro_league = "Worlds"
    for l in league_namedict:
        if "pro_l:" + l in flags:
            pro_league = l
    pro_league =  league_namedict[pro_league]
    pro_ = pro_model, pro_sq_models, pro_players, pro_league

    pr_fl([rq[name_li] for rq in req_data])
    pr_fl([champion_names[rq[cid_li]] if rq[cid_li] in champion_names else '?' for rq in req_data])
    pr_fl()
    pr_fl(pro_league, '/', req_patch)
    pr_fl([opgg_regions[regi] for regi in req_regions])

    # Get hash of request
    request_hash = get_request_hash(req_data, region_i, avg_elo_i, req_timestamp, req_patch, req_regions, flags, time.time())

    # Set current client request hash
    pgdb_threadpool.submit(set_client_reqhash, log_vars, req_d, pgdb_cond, db, request_hash)

    # Check cache for request
    # pr_fl("Checking cache for request", req_d["session_id"], str(log_vars["pg_active"][1]))
    res = pgdb_threadpool.submit(cache_db_request, log_vars, req_d, pgdb_cond, db, request_hash).result()
    if res is not None:
        res, rorder = res
        # res = json.loads(res)
        opgg_rcodes, renewals, pl_percs = res[4], res[6], res[7]
        res[3] = req_i

        # Reorder into team/tole (t_roles) order, then reorder into current request's format
        opgg_rcodes = [opgg_rcodes[j] for j in rorder]
        opgg_rcodes = [opgg_rcodes[j] for j in restore_order]
        renewals = [renewals[j] for j in rorder]
        renewals = [renewals[j] for j in restore_order]
        if pl_percs != -1:
            pl_percs = [pl_percs[j] for j in rorder]
            pl_percs = [pl_percs[j] for j in restore_order]
        res[4] = opgg_rcodes
        res[6] = renewals
        res[7] = pl_percs
        res[9] = -1

        pr_fl("RESPONDED FROM CACHE")
        # pr_fl(res)
        if pro_match:
            print_optimal_bets(res[0])
        return res

    # Get renewing flag for each player in the request
    # pr_fl("Checking renewals for request", req_d["session_id"], str(log_vars["pg_active"][1]))
    req_opgg_regions = [opgg_regions[reg_i] for reg_i in req_regions]
    req_rito_regions = [(rito_regions[reg_i if reg_i < len(rito_regions) else (len(rito_regions) - 1)]) for reg_i in req_regions]
    renewals = []
    for i in range(len(req_data)):
        name = req_data[i][name_li]
        if name == -1:
            renewals.append(None)
        else:
            renewals.append(pgdb_threadpool.submit(get_renewing_flag, log_vars, req_d, pgdb_cond, db, req_opgg_regions[i], name))
    renewals = [rf.result() if rf != None else None for rf in renewals]
    t = time.time()
    renewals = [False if rn is None or t - rn > renew_sleep_time + op_load_time else True for rn in renewals]

    # Get op.gg profiles
    region = rito_regions[region_i]
    blue_opgg_n, red_opgg_n = 0, 0
    # opgg_profiles = {i: -1 for i in range(len(req_data))}
    # opgg_responses = {}
    pl_elos = {}
    names = []
    valid_names = []
    valid_names_is = []
    valid_names_regs = []
    # names_is = OrderedDict()
    for i in range(len(req_data)):
        name = req_data[i][name_li]
        if name == "-1":
            name = -1
        names.append(name)
        if name != -1:
            name = lowercasify(name.strip())
            req_data[i][name_li] = name
            valid_names.append(name)
            valid_names_regs.append(req_opgg_regions[i])
            valid_names_is.append(i)
            # names_is[name + ''.join([' '] * i)] = i

    opgg_results = [[-2, -1, -1, -1, -1, -1, -1] for _ in valid_names]
    # pr_fl("Sending op.gg requests etc for", req_d["session_id"])

    p_flags = {}
    if avg_elo_i >= 1:
        p_flags['sqo'] = True
        if pro_match:
            avg_elo_i = 0

    ts = time.time()
    opgg_rcodes = [-1] * len(req_data)
    if not preloaded_features:
        # TODO: perform op.gg status checks for each of the players' regions in the request (not just the navbar selection)
        op_scode, op_stext, op_enabled, op_msg = pgdb_threadpool.submit(get_op_status, log_vars, req_d, pgdb_cond, db, req_opgg_regions[i]).result()
        if op_scode >= 200 and op_scode < 300 and op_enabled:
            opgg_futs = [pl_threadpool.submit(get_opgg_profile, log_vars, req_d, pgdb_threadpool, pgdb_cond, 
                db, opgg_threadpool, valid_names_regs[i], valid_names[i]) for i in range(len(valid_names))]
            opgg_results = [fut.result() for fut in opgg_futs]
        rss = [-1] * len(req_data)
        pcs = [-1] * len(req_data)
        chs = [{} for _ in range(len(req_data))]
        rfs = [{} for _ in range(len(req_data))]
        cfs = [{} for _ in range(len(req_data))]
        del_names = []
        opgg_successes = 0
        # for j in range(len(names_is)):
        for j in range(len(valid_names)):
            # pr_fl(names_is.keys())
            # name = list(names_is.keys())[j]
            name = valid_names[j]
            # i = list(names_is.values())[j]
            i = valid_names_is[j]
            rcode, rs, cs, ch, rf, cf, renewing = opgg_results[j]
            # opgg_responses[i] = rcode
            opgg_rcodes[i] = opgg_err_code_map[rcode]
            # opgg_profiles[i] = (rs, cs)

            # r = req_data[i][team_li] + '_' + req_data[i][role_li]
            # ch = ch[r] if r in ch else {}
            # if ch == {}:
            #     new_ch[i] = True

            rss[i] = rs
            chs[i] = ch if ch is not None and not isinstance(ch, int) else {}
            rfs[i] = rf if rf is not None and not isinstance(rf, int) else {}
            cfs[i] = cf if cf is not None and not isinstance(cf, int) else {}
            cs_dicts = []
            renewals[i] = renewing
            if rs != -1:
                if cs == -1:
                    cs_dicts = -1
                else:
                    for i_ in range(len(cs)):
                        # pr_fl(opgg_pcs_keys)
                        # pr_fl(len(opgg_pcs_keys))
                        if cs[i_] == -1:
                            cs_dicts.append([])
                            continue
                        # cs_dicts.append({champ_dict[v[opgg_pcs_keys.index('name')]]:
                        #     {opgg_pcs_keys[k_i]: v[k_i] for k_i in range(len(opgg_pcs_keys)) if opgg_pcs_keys[k_i] != 'name'} for v in cs[i_]})
                        cs_dicts.append([{opgg_pcs_keys[k_i]: v[k_i] for k_i in range(len(opgg_pcs_keys))} for v in cs[i_]])
                        # cs_dicts.append([{opgg_pcs_keys[k_i]: v[k_i] for k_i in range(len(opgg_pcs_keys)) if opgg_pcs_keys[k_i] != 'name'} for v in cs[i_]])
                pcs[i] = cs_dicts

                opgg_successes += 1
                leag = rs[opgg_rs_db_keys.index("league")].lower() # Get approximate elo
                if leag == "unranked" or league == "unknown":
                    continue
                lp = int(rs[opgg_rs_db_keys.index("lp")])
                div = get_division_index(leag)
                elo = (100 * (div if div < 25 else 25)) + lp
                pl_elos[i] = elo
            else:
                # del_names.append(name)
                del_names.append(j)
                # opgg_profiles[i] = -1
        orig_chs = deepcopy(chs)
        orig_rfs = deepcopy(rfs)
        orig_cfs = deepcopy(cfs)
        # pr_fl("Finished loading op.gg requests etc for", req_d["session_id"])

        opgg_rcodes = [opgg_rcodes[j] for j in restore_order]
        renewals = [renewals[j] for j in restore_order]
        # for name in del_names:
        for j in del_names:
            req_data[valid_names_is[j]][name_li] = -1
            # del names_is[name]
            # del valid_names[j]
            # del valid_names_is[j]
        valid_names = [valid_names[j] for j in range(len(valid_names)) if j not in del_names]
        valid_names_is = [valid_names_is[j] for j in range(len(valid_names_is)) if j not in del_names]

        # Add op.gg profiles
        # for i in range(len(req_data)):
        #     req_data[i][name_li] = opgg_profiles[i]

        now_ts = False
        if req_timestamp == -1:
            req_timestamp = ts
            now_ts = True
        ts_act = req_timestamp * 1000
        if req_patch not in patch_tss:
            req_patch = list(patch_tss.keys())[0]
        ts_b = (patch_tss[req_patch] * 1000) if req_patch != patches_all[0] else ts_act

        # Add past elo and match history info if we're predicting for a game in the past
        # if True:
        if (((not now_ts) and spec_use_riot) or ts - req_timestamp > (15 * 60)):  # 15 minutes ago
            for i in range(len(req_data)):
                name = req_data[i][name_li]
                if name != -1:
                    # if i in pl_elos:
                    #     del pl_elos[i]
                    past_elo, out_of_range = get_opgg_pastelo(log_vars, req_d, pgdb_threadpool, pgdb_cond, db,
                        req_opgg_regions[i], name, rss[i][opgg_rs_db_keys.index("summonerId")], req_timestamp)
                    if past_elo is not None and (i not in pl_elos or pl_elos[i] == -1 or not out_of_range):
                        pl_elos[i] = past_elo

                    # Add match history
                    rsg = get_riot_mh(log_vars, req_d, pgdb_threadpool, pgdb_cond, db,
                        req_rito_regions[i], name, req_timestamp, r_conds, r_tss)
                    if rsg is not None and len(rsg) > 0:
                        rss[i][opgg_rs_db_keys.index("games")] = rsg

        # Add player elos
        mean_elo = np.mean(list(pl_elos.values())) if len(pl_elos) > 0 else league_elo_avgs[0]
        for i in range(len(req_data)):
            req_data[i].append(pl_elos[i] if i in pl_elos else \
                (mean_elo if avg_elo_i == 0 else league_elo_avgs[avg_elo_i - 1]))

    # Construct model key
    blue_team = [d for d in req_data if d[team_li] == 0]
    red_team = [d for d in req_data if d[team_li] == 1]
    n_blue = sum([d[cid_li] != -1 for d in blue_team])
    n_red = sum([d[cid_li] != -1 for d in red_team])

    # If we have no data
    if n_blue == 0 and n_red == 0 and opgg_successes == 0 and not preloaded_features:
        return [50.0, -1, -1, req_i, opgg_rcodes, mp_err_codes["Need more data (player(s) not found)"], renewals, -1, -1, -1, -1, -1]

    n_blue_m = round_count(n_blue)
    n_red_m = round_count(n_red)
    opgg_blue = sum([d[name_li] != -1 for d in blue_team])
    opgg_red = sum([d[name_li] != -1 for d in red_team])
    opgg_blue_m = round_count(opgg_blue)
    opgg_red_m = round_count(opgg_red)
    profic_overlap_blue = [d[role_li] for d in blue_team if d[cid_li] != -1 and d[name_li] != -1]
    profic_overlap_red = [d[role_li] for d in red_team if d[cid_li] != -1 and d[name_li] != -1]
    n_profic_overlap_blue = len(profic_overlap_blue)
    n_profic_overlap_red = len(profic_overlap_red)
    n_profic_overlap_blue_m = round_count(n_profic_overlap_blue)
    n_profic_overlap_red_m = round_count(n_profic_overlap_red)
    n_matchups, matchups_ris = 0, []
    if n_blue == 5 and n_red == 5:
        n_matchups = 5
        matchups_ris = range(5)
    else:
        if n_blue > 0 and n_red > 0:
            blue_cid_ris = [d[role_li] for d in blue_team if d[cid_li] != -1]
            red_cid_ris = [d[role_li] for d in red_team if d[cid_li] != -1]
            matchups_ris = list(set(blue_cid_ris) & set(red_cid_ris))
            n_matchups = len(matchups_ris)
    n_matchups_m = round_count(n_matchups)
    t_role_is = {role_is[cols_all[req_data[i][team_li]] + '_' + roles_all[req_data[i][role_li]]]: i for i in range(len(req_data))}

    n_champs = n_blue + n_red
    n_opgg = opgg_blue + opgg_red

    # if opgg_blue_m == 5:
    #     opgg_blue_m = 1
    #     n_profic_overlap_blue_m = 1
    # if opgg_red_m == 5:
    #     opgg_red_m = 1
    #     n_profic_overlap_red_m = 1

    model_key = n_blue_m, n_red_m, opgg_blue_m, opgg_red_m, \
        n_profic_overlap_blue_m, n_profic_overlap_red_m, n_matchups_m

    if model_key not in model_dict:
        pr_fl("model key missing: " + str(model_key))
        return [50.0, -1, -1, req_i, opgg_rcodes, mp_err_codes["Server error (model key missing)"], renewals, -1, -1, -1, -1, -1]

    # Construct data point
    # blue_team_dict = {d[role_li]: d[2:] for d in blue_team}
    # red_team_dict = {d[role_li]: d[2:] for d in red_team}
    
    alpha, beta, _, _, _ = model_dict[(5, 5, 5, 5, 5, 5, 5)][6]
    X_rec_labels = get_recent_match_arr_labels(False, False, False)
    filler = {}
    filler["no_add"] = True
    filler["fill"], filler["flags"] = {}, []
    filler["rec_flags"], filler["game_i"] = [], 0
    filler["pl_name"] = ""
    filler["seen_pl"] = set()
    rf_flags = [{} for _ in range(len(req_data))]
    cf_flags = [{} for _ in range(len(req_data))]

    # pr_fl("Performing feature extraction for " + req_d["session_id"])

    infer_full_both = False
    if not preloaded_features:

        # pr(names)
        # pr([rq[3] for rq in req_data])
        # pr([champion_names[rq[2]] if rq[2] in champion_names else '?' for rq in req_data])
        # camille_wins = []
        # camille_losses = []
        # for pc in pcs:
        #     found = False
        #     for chmp in pc[-1]:
        #         if chmp["name"] == "Cassiopeia":
        #             found = True
        #             camille_wins += [chmp["wins"]]
        #             camille_losses += [chmp["losses"]]
        #     if not found:
        #         camille_wins += [None]
        #         camille_losses += [None]
        # pr(camille_wins)
        # pr(camille_losses)

        X, X_rec, X_labels = extract_X(ts_act, ts_b, gsdb, gsdb_cond, infill_data, filler, rf_flags, cf_flags, #0,
            req_opgg_regions, mean_elo, names, req_data, rss, pcs, chs, rfs, cfs,
            n_matchups, matchups_ris, X_rec_labels, profic_overlap_blue, profic_overlap_red,
            t_role_is, X_scales, X_scalesdict, X_scaler, X_autolog, X_mins, rec_infill_data, n_opgg, alpha, beta)

        # Figure out whether to compute side-swapped features too
        recc_req = False
        for i in range(len(req_data)):
            if req_data[i][cid_li] == -2: # This is a pick-recommendation request (side-specific by default)
                recc_req = True
                break
        compute_player_spec = True
        if no_player_spec:
            compute_player_spec = False
        compute_both_sides = False
        # compute_both_sides = (compute_player_spec and not recc_req) or (side_unknown) # "side_unknown" works for recc_reqs
        infer_full_both = side_unknown  # Whether to compute full feature prediction for both sides

        # Compute input vars for the opposite side
        X_oc, X_rec_oc, X_labels_oc = None, None, None
        if compute_both_sides:
            req_data_oc = deepcopy(req_data)  # Invert colours in the req_data
            for i in range(len(req_data_oc)):
                req_data_oc[i][team_li] = 1 - req_data_oc[i][team_li]

            # Compute the new ordering (re-arrangement indices) for the side-inverted participants
            oc_order = [role_is[cols_all[rd[team_li]] + '_' + roles_all[rd[role_li]]] for rd in req_data_oc]
            oc_rorder = np.argsort(oc_order)
            req_data_oc = [req_data_oc[i] for i in oc_rorder]
            oc_rev_order = np.argsort(oc_rorder)

            inv_locals = ["req_data_oc", "rf_flags", "cf_flags", "req_opgg_regions", "names", "rss", "pcs", "chs", "rfs", "cfs"]
            for ll in inv_locals:  # Temporarily invert input lists
                val = locals()[ll]
                locals()[ll] = [val[i] for i in oc_rorder]
            profic_overlap_blue, profic_overlap_red = profic_overlap_red, profic_overlap_blue

            X_oc, X_rec_oc, X_labels_oc = extract_X(ts_act, ts_b, gsdb, gsdb_cond, infill_data, filler, rf_flags, cf_flags, #0,
                req_opgg_regions, mean_elo, names, req_data_oc, rss, pcs, chs, rfs, cfs,
                n_matchups, matchups_ris, X_rec_labels, profic_overlap_blue, profic_overlap_red,
                t_role_is, X_scales, X_scalesdict, X_scaler, X_autolog, X_mins, rec_infill_data, n_opgg, alpha, beta)

            for ll in inv_locals:  # Restore order
                val = locals()[ll]
                locals()[ll] = [val[i] for i in oc_rev_order]
            profic_overlap_blue, profic_overlap_red = profic_overlap_red, profic_overlap_blue

        # Define conditions for using full-5 models with singly-imputed (zero'd in) missing features
        model_key = list(model_key)
        if n_blue == 1 or (n_blue == 2 and n_red <= 1) or (n_blue == 4 and n_red == 5):
            model_key[0] = 5
        if n_red == 1 or (n_red == 2 and n_blue <= 1) or (n_red == 4 and n_blue == 5):
            model_key[1] = 5
        if opgg_blue == 1 or (opgg_blue == 2 and opgg_red <= 1) or (opgg_blue == 4 and opgg_red == 5):
            model_key[2] = 5
        if opgg_red == 1 or (opgg_red == 2 and opgg_blue <= 1) or (opgg_red == 4 and opgg_blue == 5):
            model_key[3] = 5
        if model_key[0] == 5 and model_key[1] == 5:
            model_key[6] = 5
        if model_key[0] == 5 and model_key[2] == 5:
            model_key[4] = 5
        if model_key[1] == 5 and model_key[3] == 5:
            model_key[5] = 5
        model_key = tuple(model_key)
        pr_fl("Model key:", model_key)

    # If this is a pick recommendation request
    for i in range(len(req_data)):
        if req_data[i][cid_li] == -2:

            return [50.0, -1, -1, req_i, opgg_rcodes, mp_err_codes["Server error (feature missing)"], renewals, -1, -1, -1, -1, -1]

            rq = req_data[i]
            rs = rss[i]
            pc = pcs[i]
            ch = chs[i]
            rf = rfs[i]
            cf = cfs[i]
            name = names[i]
            has_opgg = rq[name_li] != -1
            pl_opgg_region = req_opgg_regions[i]

            side = cols_all[rq[team_li]]
            role = roles_all[rq[role_li]]
            elo = rq[-1]
            tier, _, _ = get_league_info(elo)
            cids_taken = set([req_data[j][cid_li] for j in range(len(req_data)) if req_data[j][cid_li] >= 0])
            cids = []
            leftover_cids = []
            filler = {}
            filler["fill"], filler["flags"] = {}, []
            filler["rec_flags"], filler["game_i"] = [], 0
            filler["pl_name"] = ""
            filler["seen_pl"] = set()

            X_new, X_rec_new = [], []
            # X_labels_ = X_labels
            for cid in champion_names.keys():
                if cid not in cids_taken:
                    cids.append(cid)
                    # req_data[i][cid_li] = cid
                    # pr_fl(req_data)
                    x = get_x_for_champ(X[0], X_labels, req_data, ts_b, gsdb, gsdb_cond, filler, rf_flags, cf_flags, pl_opgg_region,
                        side, role, cid, elo, tier, name, rs, pc, ch, rf, cf, matchups_ris, n_matchups, profic_overlap_blue, profic_overlap_red)
                    # if X_labels_ is None:
                    #     X_labels_ = list(x.keys())
                    # else:
                    #     if X_labels != X_labels_:
                    #         x = x[[X_labels_.index(l) for l in X_labels]]
                    #     for l in X_labels_:
                    #         if l not in X_labels:
                    #             pr_fl(l)
                    # pr_fl(x.shape)
                    X_new.append(x)
                    # X_rec_new.append(x_rec)
                    filler["game_i"] += 1
                else:
                    leftover_cids.append(cid)
            X_new = np.asarray(X_new)
            X, X_rec, X_labels = infill_missing_data(X_new, X_rec, X_labels, X_rec_labels, infill_data, filler, t_role_is, verbose=False)

            X, X_labels = add_engineered_features(X, X_rec, X_labels, X_rec_labels,
                X_scales, X_scalesdict, X_scaler, X_autolog, X_mins, req_data, rec_infill_data, n_opgg, alpha, beta, add_recent_match_feats=True)

            perc, perc_ch, _, _ = get_prediction(model_key, model_dict, model_strs,
                X, X_labels, n_champs, n_opgg, flags, pro_, req_patch,
                verbose=False, return_perc_pl=False, return_perc_ch=has_opgg)

            # perc = []
            # perc_ch = []
            # for j in range(len(X)):
            #     perc_, perc_ch_, _, _ = get_prediction(model_key, model_dict, model_strs,
            #         X[j:j+1], X_rec[j:j+1], X_labels, X_rec_labels, X_scales, X_scalesdict, X_scaler, X_autolog, X_mins,
            #         req_data, filler, infill_data, rec_infill_data, n_champs, n_opgg,
            #         verbose=True, return_perc_pl=False)
            #     perc.append(perc_[0])
            #     if has_opgg:
            #         perc_ch.append(perc_ch_[0])

            cids += leftover_cids
            perc = list(perc) + ([-1] * len(leftover_cids))
            recc_res = sorted(zip(perc, cids))
            recc_res_ch = -1
            if has_opgg:
                perc_ch = list(perc_ch) + ([-1] * len(leftover_cids))
                recc_res_ch = sorted(zip(perc_ch, cids))
            res = [recc_res, recc_res_ch, -1, req_i, opgg_rcodes, mp_err_codes["Success"], renewals, -1, -1, -1, -1, -1]
            if not any(renewals):
                pgdb_threadpool.submit(insert_new_result, log_vars, req_d, pgdb_cond, db, request_hash, res, "recc", -1, rorder)

            # # Store new intermediate features in cache
            pgdb_threadpool.submit(update_ch_cache, log_vars, req_d, pgdb_cond, db, [pl_opgg_region],
                *zip(*((req_data[i][name_li], chs[i]) for i in range(len(chs)) if req_data[i][name_li] != -1 and chs[i] != orig_chs[i])))

            # Store new final features in cache
            pgdb_threadpool.submit(update_rf_cache, log_vars, req_d, pgdb_cond, db, [pl_opgg_region],
                *zip(*((req_data[i][name_li], rfs[i]) for i in range(len(rfs)) if req_data[i][name_li] != -1 and rfs[i] != orig_rfs[i])))
            pgdb_threadpool.submit(update_cf_cache, log_vars, req_d, pgdb_cond, db, [pl_opgg_region],
                *zip(*((req_data[i][name_li], cfs[i]) for i in range(len(cfs)) if req_data[i][name_li] != -1 and cfs[i] != orig_cfs[i])))

            return res

    # Get full feature prediction
    pr_fl("\n\n########## Performing model inference ##########", "*Pro match*" if pro_match else '')
    res__ = get_prediction(model_key, model_dict, model_strs,
        X, X_labels, n_champs, n_opgg, flags, pro_, req_patch, verbose=False, return_output=ptr_match, p_flags=p_flags)
    if ptr_match:
        perc, perc_ch, perc_pl, err_code, X_final, x_ls, pre_feats, output = res__
        pr_fl("Output shapes:", X_final.shape, pre_feats.shape, output.shape)
    else:
        perc, perc_ch, perc_pl, err_code = res__
    pr_fl("Prediction:", perc)

    if infer_full_both:
        model_key_inverted = tuple([model_key[i] for i in [1, 0, 3, 2, 5, 4, 6]])
        res__ = get_prediction(model_key_inverted, model_dict, model_strs,
            X_oc, X_labels_oc, n_champs, n_opgg, flags, pro_, req_patch, verbose=False, return_output=ptr_match)
        perc_oc, perc_ch_oc, perc_pl_oc, err_code_oc = res__
        perc = np.mean([perc, perc_oc], axis=0)
        perc_ch = np.mean([perc_ch, perc_ch_oc], axis=0)
        perc_pl = np.mean([perc_pl, perc_pl_oc], axis=0)

    # Get individual-player-only predictions
    pl_percs = [-1] * len(req_data)
    if not no_player_spec:
        for i in range(len(req_data)):
            if req_data[i][name_li] != -1:

                sided_percs = []
                for rq_d, X_, X_labels_ in [[req_data[i], X, X_labels]]:
                # for rq_d, X_, X_labels_ in [[req_data[i], X, X_labels], [req_data_oc[i], X_oc, X_labels_oc]]:
                    team_i = rq_d[team_li]
                    key = [0, 0, 0, 0, 0, 0, 0]
                    key[2 + team_i] = 5
                    if rq_d[cid_li] > -1:
                        key[0 + team_i] = 5
                        key[4 + team_i] = 5

                    r = cols_all[team_i] + '_' + roles_all[rq_d[role_li]]
                    new_ls = [l for l in X_labels_ if '_'.join(l.split('_')[:2]) == r or \
                                                     # l.split('_')[0] == cols_all[team_i] or \
                                                     l.split('_')[0] not in cols_all]
                    new_X = X_[:, [X_labels_.index(l) for l in new_ls]]
                    # pr_fl(len(X_labels_), len(new_ls), X_.shape, new_X.shape)
                    # pr_fl(rq_d[name_li], r)
                    
                    pl_perc, _, _, _ = get_prediction(key, model_dict, model_strs,
                        new_X, new_ls, int(rq_d[cid_li] > -1), 1, [f for f in flags if f != "pro"], pro_, req_patch,
                        verbose=False, return_perc_ch=False, return_perc_pl=False)
                    sided_percs.append(pl_perc)

                pl_percs[i] = np.mean(sided_percs)
                # pl_percs[i] = pl_perc[0]

    pl_percs = [pl_percs[j] for j in restore_order]
    out = parsePyJ((X, X_labels, X_final, x_ls, pre_feats, output)) if ptr_match else -1
    res = [perc[0], perc_ch[0], perc_pl[0], req_i, opgg_rcodes, err_code, renewals, pl_percs, -1, -1, ts, out]
    # shortlink and shortlink match composition
    no_renewals = not any(renewals)
    # if not any(renewals):
    res[8] = pgdb_threadpool.submit(get_new_shortlink, log_vars, req_d, pgdb_cond, db).result()
    # pr_fl("LIFESPAN: " + str(current_request_lifespan if not any(renewals) else renew_sleep_time))
    # pr_fl("renewals: " + str(renewals))

    lifespan = ptr_lifespan if ptr_match else (current_request_lifespan if no_renewals else renew_sleep_time)

    pgdb_threadpool.submit(insert_new_result, log_vars, req_d, pgdb_cond, db, request_hash, res,
        "normal" + ("" if no_renewals else "_new"), req_d["comp"], rorder, lifespan=lifespan)

    if not preloaded_features:
        # Store new intermediate features in cache
        pgdb_threadpool.submit(update_ch_cache, log_vars, req_d, pgdb_cond, db,
            *zip(*((req_opgg_regions[i], req_data[i][name_li], chs[i]) for i in range(len(chs)) if req_data[i][name_li] != -1 and chs[i] != orig_chs[i])))

        # Store new final features in cache
        pgdb_threadpool.submit(update_rf_cache, log_vars, req_d, pgdb_cond, db,
            *zip(*((req_opgg_regions[i], req_data[i][name_li],
                rfs[i]) for i in range(len(rfs)) if req_data[i][name_li] != -1 and rfs[i] != orig_rfs[i])))
        pgdb_threadpool.submit(update_cf_cache, log_vars, req_d, pgdb_cond, db,
            *zip(*((req_opgg_regions[i], req_data[i][name_li],
                cfs[i]) for i in range(len(cfs)) if req_data[i][name_li] != -1 and cfs[i] != orig_cfs[i])))

    # pr_fl("Responded with new result")
    # pr_fl(res)

    # if pro_match:
    print_optimal_bets(perc[0])

    return res

def extract_X(ts_act, ts_b, gsdb, gsdb_cond, infill_data, filler, rf_flags, cf_flags, #0,
        req_opgg_regions, mean_elo, names, req_data, rss, pcs, chs, rfs, cfs,
        n_matchups, matchups_ris, X_rec_labels, profic_overlap_blue, profic_overlap_red,
        t_role_is, X_scales, X_scalesdict, X_scaler, X_autolog, X_mins, rec_infill_data, n_opgg, alpha, beta):

    # Get full feature x object
    # pr_fl(req_data)

    x, x_rec = get_x_vector(ts_act, ts_b, gsdb, gsdb_cond, infill_data, filler, rf_flags, cf_flags, #0,
        req_opgg_regions, mean_elo, names, req_data, rss, pcs, chs, rfs, cfs,
        n_matchups, matchups_ris, X_rec_labels, profic_overlap_blue, profic_overlap_red)
    # n_rec = sum()
    X = np.asarray([np.asarray(list(x.values()))])
    X_labels = list(x.keys())
    X_rec = [x_rec]

    # pr(names)
    # pr([rq[3] for rq in req_data])
    # pr([champion_names[rq[2]] for rq in req_data])
    # camille_wins = []
    # camille_losses = []
    # for pc in pcs:
    #     found = False
    #     for chmp in pc[-1]:
    #         if chmp["name"] == "Cassiopeia":
    #             found = True
    #             camille_wins += [chmp["wins"]]
    #             camille_losses += [chmp["losses"]]
    #     if not found:
    #         camille_wins += [None]
    #         camille_losses += [None]
    # pr(camille_wins)
    # pr(camille_losses)

    # winsls = [r + "_opgg_champion_recent_wins" for r in t_roles]
    # pr_fl(X[:, [X_labels.index(l) for l in winsls]])
    # lossesls = [r + "_opgg_champion_recent_losses" for r in t_roles]
    # pr_fl(X[:, [X_labels.index(l) for l in lossesls]])

    # for flag in filler["rec_flags"]:
    #     if t_role_is[flag[1]] == 0:
    #         if "_recent_rank" in flag[3]:
    #             pr_fl("FALG  ", flag)

    # Lookup model infill of missing values
    X, X_rec, X_labels = infill_missing_data(X, X_rec, X_labels, X_rec_labels, infill_data, filler, t_role_is, verbose=False)

    # for l in X_labels:
    #     pr_fl(l, X[:, X_labels.index(l)])
    # pr_fl()
    # pr_fl(X_rec)
    # sys.exit()

    # Get final x feature vector from current x 'vector'
    X, X_labels = add_engineered_features(X, X_rec, X_labels, X_rec_labels,
        X_scales, X_scalesdict, X_scaler, X_autolog, X_mins, req_data, rec_infill_data, n_opgg, alpha, beta)

    # Cache computed features and add pre-computed from cache
    X, X_labels = cache_features(X, X_labels, req_data, rfs, cfs, rf_flags, cf_flags)

    return X, X_rec, X_labels


def print_optimal_bets(win_perc):
    pr_fl("\n\n")
    pr_fl("##################################")
    pr_fl("########## Optimal bets ##########")
    pr_fl("##################################")
    n_sites = int((sum(1 for line in open('bet_params.txt')) - 2) / 3)
    win_prob = win_perc / 100.0
    bankroll_total = 0
    with open("bet_params.txt", 'r') as f:
        base_conservatism = float(f.readline().strip())
        conservatism = float(f.readline().strip())
        bos = int(f.readline().strip())
        pkey = f.readline().strip()
        pr_fl("Conservatism (base, favourite):", base_conservatism, conservatism)
        pr_fl("")
        for site_i in range(n_sites):
            brol = f.readline().strip().split(' : ')
            bankroll, site = float(brol[0]), brol[1]
            b_odds = f.readline().strip()
            if '/' in b_odds:
                b_odds = b_odds.split('/')
            else:
                b_odds = b_odds, '1'
            r_odds = f.readline().strip()
            if '/' in r_odds:
                r_odds = r_odds.split('/')
            else:
                r_odds = r_odds, '1'
            bankroll_total += bankroll

            b_odds = float(b_odds[0]) / float(b_odds[1])
            r_odds = float(r_odds[0]) / float(r_odds[1])

            odds_prob = ((1 / (b_odds + 1)) + 1 - (1 / (r_odds + 1))) / 2
            post_cons = conservatism * ((abs(odds_prob - 0.5) * 5.7)**1.1)

            # corr_ratio = 0.5
            # p = win_prob
            # p = (win_prob * corr_ratio) + (corrected * (1 - corr_ratio))

            p = win_prob
            p = (p * (1 - base_conservatism)) + (odds_prob * base_conservatism)
            p = (p * (1 - post_cons)) + (odds_prob * post_cons)


            blue_fr = max(0, ((b_odds * p) - (1 - p)) / b_odds)
            red_fr = max(0, ((r_odds * (1 - p)) - p) / r_odds)
            underdog = int(blue_fr > 0 and b_odds > 1)
            blue_fr *= bet_mults[pkey][bos][underdog]
            blue_fr = min(blue_fr, bet_caps[pkey][bos][underdog])
            underdog = int(red_fr > 0 and r_odds > 1)
            red_fr *= bet_mults[pkey][bos][underdog]
            red_fr = min(red_fr, bet_caps[pkey][bos][underdog])

            pr_fl(bankroll, site, "B", round(bankroll * blue_fr, 2), '(', str(round(blue_fr * 100, 1)) + '% )',
                                  "R", round(bankroll * red_fr, 2), '(', str(round(red_fr * 100, 1)) + '% )',
                                  "    Effective P (odds P):", round(p * 100, 2), '(', round(odds_prob * 100, 2), '% )')
    pr_fl('')
    pr_fl("Bankroll:", bankroll_total)
    pr_fl("\n\n")


def pro_prediction(model_dict, pro_, X, X_labels, sq_perc, patch, p_flags={}, alt_labels=None):
    pkey = 'nsqo'
    pro_model, pro_sq_models, pro_players, pro_league = pro_
    if len(pro_model[pkey]) != 6:
        pr_fl(len(pro_model[pkey]))
        pr_fl([type(o) for o in pro_model[pkey]])
    _, _, _, _, perfs, _ = pro_model[pkey]
    pro_stats_count = 0
    for ri in range(5):
        role = roles_all[ri]
        blue_key = pro_players["blue_" + role] + '_' + str(ri)
        red_key = pro_players["red_" + role] + '_' + str(ri)
        pro_stats_count += int(blue_key in perfs) + int(red_key in perfs)
        if blue_key not in perfs:
            pr_fl(("Missing pro error: ", blue_key))
        if red_key not in perfs:
            pr_fl(("Missing pro error: ", red_key))
    if pro_stats_count < pro_sqo_missing or 'sqo' in p_flags:
        reason_sqo = "(p_flag)" if 'sqo' in p_flags else "(missing pros; only " + str(pro_stats_count) + ")" 
        pr_fl("**Using sqo (solo-queue-data-only) model** " + reason_sqo)
        pkey = 'sqo'
    p_flags['key'] = pkey

    pro_model = pro_model[pkey]
    if len(pro_model) != 6:
        print(len(pro_model))
    pro_models, fscalers, patches, leagues, perfs, lperfs = pro_model
    pro_model, X_flabels = pro_models[pro_league]
    x_ls, models = pro_sq_models[0][(5,5,5,5,5,5,5)][:2]
    use_ls = x_ls
    if alt_labels is not None:
        use_ls = model_dict[alt_labels][0]
    m = models[0]
    if isinstance(m, CalibratedClassifierCV):
        m = m.base_estimator

    # Assuming Pipeline, get scaler center
    scaler = m.steps[0][1]
    center = scaler.center_ if isinstance(scaler, RobustScaler) else scaler.mean_

    # Construct X for solo queue model phase
    X_ = np.zeros((X.shape[0], len(x_ls)))
    count = 0
    for i in range(len(x_ls)):
        l = x_ls[i]
        if l in X_labels and l in use_ls:
            X_[:, i] = X[:, X_labels.index(l)]
            count += 1
        else:
            X_[:, i] = center[i]

    # Compute scaled features and model output
    X_ = scaler.transform(X_)
    X_ = X_ * m.steps[1][1].coef_
    # X_ = X_.reshape(X_.shape[1], X_.shape[2])

    # Fold
    # # if "fold" in pro_flags:
    # #     if pro_flags["fold"] == "mean_diff":
    # # u_ls = list(set(['_'.join(l.split('_')[2:]) for l in pro_labels if l.split('_')[1] in roles_all]))
    # # n_ls = [l for l in pro_labels if l.split('_')[1] not in roles_all]
    # u_ls, n_ls = pro_labels
    # X_folded = \
    #     np.asarray([np.mean(X_[:, [x_ls.index("blue_" + r + '_' + l) for r in roles_all]], axis=1) for l in u_ls]).T - \
    #     np.asarray([np.mean(X_[:, [x_ls.index("red_" + r + '_' + l) for r in roles_all]], axis=1) for l in u_ls]).T
    # #   np.hstack([
    # #     np.asarray([np.mean(X_[:, [x_ls.index("blue_" + r + '_' + l) for r in roles_all]], axis=1) for l in u_ls]).T,
    # #     np.asarray([np.mean(X_[:, [x_ls.index("red_" + r + '_' + l) for r in roles_all]], axis=1) for l in u_ls]).T,
    # #   ])
    # X_ = np.hstack([X_folded, np.asarray([X_[:, x_ls.index(l)] for l in n_ls]).T])

    final_X = []
    for feat_i in range(len(X_flabels)):
        l = X_flabels[feat_i]

        # odds_prob = 0.5
        # odds_margin = 0.03

        # for pf in p_flags:
        #     if pf.split(':')[0] == 'odds':

        # b_odds = 4 / 1.0
        # r_odds = 1 / 7.0
        
        # b_odds = float(odds['bd']) / float(odds['bn'])
        # r_odds = float(odds['rd']) / float(odds['rn'])
        # odds_prob = ((1 / (b_odds + 1)) + 1 - (1 / (r_odds + 1))) / 2
        # odds_margin = np.abs((1 / (b_odds + 1)) - (1 - (1 / (r_odds + 1))))


        x = None
        if l == "sq_prediction":
            x = np.tile(sq_perc[0] / 100.0, (X.shape[0], 1))
        elif l == "league":
            x = np.zeros(len(leagues))
            x[leagues.index(pro_league)] = 1
            x = np.tile(x, (X_.shape[0], 1))
        elif l == "patch":
            x = np.zeros(len(patches))
            x[patches.index(patch)] = 1
            x = np.tile(x, (X_.shape[0], 1))
        # elif l == "odds_prob":

        # elif l == "odds_margin":
        elif l == "series_count":
            x = np.tile(0, (X_.shape[0], 1))
        elif l == "series_ingame_time":
            # x = np.tile(2 * 32.7 * 60, (X_.shape[0], 1))
            x = np.tile((32 * 0) * 60, (X_.shape[0], 1))
        elif l == "series_wins": # blue - red
            x = np.tile(0, (X_.shape[0], 1))
        elif l == "series_winstreak": # blue - red
            x = np.tile(0, (X_.shape[0], 1))
        else:
            l_list = l.split('_')
            role = l_list[0]
            if role not in roles_all: # Bot synergy features
                x = X_[:, [x_ls.index("blue_" + l)]] - X_[:, [x_ls.index("red_" + l)]]
            else:
                ri = roles_all.index(role)
                l_suff = '_'.join(l_list[1:])
                if l_suff in pro_full_player_labels:
                    l_i = pro_full_player_labels.index(l_suff)
                    blue_key = pro_players["blue_" + role] + '_' + str(ri)
                    red_key = pro_players["red_" + role] + '_' + str(ri)
                    pro_stats_count += int(blue_key in perfs) + int(red_key in perfs)
                    blue_x = np.asarray([[perfs[blue_key][l_i] if blue_key in perfs else \
                                          lperfs[str(ri) + '_' + str(pro_leagues.index(pro_league))][l_i]] for _ in range(len(X))])
                    red_x = np.asarray([[perfs[red_key][l_i] if red_key in perfs else \
                                         lperfs[str(ri) + '_' + str(pro_leagues.index(pro_league))][l_i]] for _ in range(len(X))])
                    x = blue_x - red_x
                else:
                    x = X_[:, [x_ls.index("blue_" + l)]] - X_[:, [x_ls.index("red_" + l)]]

        if l in fscalers:
            x = fscalers[l].transform(x)
        final_X.append(x)

    final_X = np.hstack(final_X)



    preds = pro_model.predict_proba(final_X)[:, 1]
    perc = preds * 100.0
    perc = np.minimum(np.maximum(1.0, perc), 99.0)

    # Adjust for inflated probabilities due to lack of data. This will be less accurate when predicting the favorite to win, but 
    # for our use case, this actually results in a more profitable model, as it favours low-risk betting on underdogs (high odds)
    # perc -= 50
    # perc *= 0.75
    # perc += 50

    # pr_fl(perc)
    return perc

# Get prediction(s) for given list of x objects
def get_prediction(
        model_key, model_dict, model_strs,
        X, X_labels,
        n_champs, n_opgg, flags, pro_, patch,
        verbose=False, return_perc_pl=True, return_perc_ch=True,
        return_output=False, p_flags={}):

    err_code = mp_err_codes["Success"]
    perc, scaled_feats, output, x_ls, X_ = None, None, None, None, None
    model_key = tuple(model_key)

    # Todo: fix alpha beta belonging to individual models (should be same for all)
    x_ls, models, scores, high_elo_scores, pscores_before, pscores, _ = model_dict[model_key]

    # Get solo queue prediction
    X_ = get_final_X(X, X_labels, x_ls, model_dict, model_strs)
    perc = get_final_perc(models[0], X_, verbose=verbose, return_output=return_output)
    # pr_fl("SQO PERC:", perc)

    if 'pro' in flags:

        # Get pro prediction
        perc = pro_prediction(model_dict, pro_, X, X_labels, perc, patch, p_flags=p_flags)

    else:

        scaled_feats, output = None, None
        if return_output:
            perc, scaled_feats, output = perc
        # if verbose: pr_fl(perc, model_key, X.shape, X_.shape, len(x_ls))

        # Get whether we need more data
        score = scores[0]
        if score < 78.0:
            err_code = mp_err_codes["Need more data (low model accuracy)"]

    # Get champions-only/players-only predictions
    perc_ch, perc_pl = [-1], [-1]
    if n_champs > 0 and n_opgg > 0:
        if return_perc_ch:
            # pr_fl("Champions only")
            perc_ch = get_champions_only_perc(X, X_labels, model_key, model_dict, model_strs, flags, pro_, patch)
        if return_perc_pl:
            # pr_fl("Players only")
            perc_pl = get_players_only_perc(X, X_labels, model_key, model_dict, model_strs, flags, pro_, patch)

    res = (perc, perc_ch, perc_pl, err_code)
    if return_output:
        res += (X_, x_ls, scaled_feats, output)
    return res


# Predict & transform into final win %
def get_final_perc(model, X, verbose=True, return_output=False):
    # Y = model.predict_proba(X)
    # y = Y[:, 1]
    # pr_fl(X.shape, Y.shape)

    scaled_X = X
    output = None
    if return_output:
        m = model
        if isinstance(m, ApproximalWrapper):
            m = m.base_estimator
        if isinstance(m, CalibratedClassifierCV):
            m = m.base_estimator
        if isinstance(m, Pipeline):
            scaler = m.steps[0][1]
            scaled_X = scaler.transform(X)
            m = m.steps[1][1]
        # coefs = m.coef_
        output = scaled_X #* coefs

    y = model.predict_proba(X)[:, 1]
    y = correct_percentage(y, model, verbose=verbose)
    if return_output:
        return y, scaled_X, output
    else:
        return y

# Transform prediction into final percentage (scale conservatively & for optimism)
def correct_percentage(y, model, verbose=True):
    if y == 0.0: # Divide by zero workaround
        y += 0.001
    if y == 1.0:
        y -= 0.001

    perc = y * 100
    if verbose: pr_fl(perc, y.shape)
    orig_y = y
    orig_perc = perc

    # Scale result
    # if (not isinstance(model, ApproximalWrapper)) and (not \
    #   (isinstance(model, Pipeline) and isinstance(model.steps[1][1], ApproximalWrapper))):
    #     # y = (np.log10((1 * (y / (1 - y)))) + (np.e)) / (2 * np.e)
    #     # y = (y + orig_y) / 2
    #     perc = y * 100
    #     # perc = 20 + (y * 60)
    # else:
    #     perc = 30 + ((((y * 100) - 35) / 30) * 40)
    perc = y * 100

    # Finally cap the percent between 1 and 99
    perc = np.minimum(np.maximum(1.0, perc), 99.0)
    if verbose:
        pr_fl("original: " + str(orig_perc) + ", corrected: " + str(perc))
    return perc

# Get the prediction when using only champion data (ignore the specific players)
def get_champions_only_perc(X, X_labels, key, model_dict, model_strs, flags, pro_, patch):
    new_key = (key[0], key[1], 0, 0, 0, 0, key[6])
    x_ls, models, _, _, _, _, _ = model_dict[new_key]
    X_ = get_final_X(X, X_labels, x_ls, model_dict, model_strs)
    perc = get_final_perc(models[0], X_, verbose=False)
    if 'pro' in flags:
        perc = pro_prediction(model_dict, pro_, X, X_labels, perc, patch, alt_labels=new_key)
    return perc

# Get the prediction when using only player data (as if champion selection is unknown)
def get_players_only_perc(X, X_labels, key, model_dict, model_strs, flags, pro_, patch):
    new_key = (0, 0, key[2], key[3], 0, 0, 0)
    x_ls, models, _, _, _, _, _ = model_dict[new_key]
    X_ = get_final_X(X, X_labels, x_ls, model_dict, model_strs)
    perc = get_final_perc(models[0], X_, verbose=False)
    if 'pro' in flags:
        perc = pro_prediction(model_dict, pro_, X, X_labels, perc, patch, alt_labels=new_key)
    return perc

# Infill missing data
def infill_missing_data(X, X_rec, X_labels, X_rec_labels, infill_data, filler, t_role_is, verbose=False):
    X_label_is = dict(zip(X_labels, range(len(X_labels))))
    X_rec_label_is = dict(zip(X_rec_labels, range(len(X_rec_labels))))
    fill = infill_data["fill"]
    # pr_fl("\nFilling missing data...")
    fl_i, max_fl = 0, str(len(filler["flags"]))
    for flag in filler["flags"]:
        val = fill
        for key in flag[3:]:


            if isinstance(val, dict) and key not in val:

                if str(key) in val:
                    key = str(key)
                else:

                    if val["_name"] == "div": # If we don't yet have data for master+
                        key = int(key)
                        if key >= 25:
                            key = 24
                        if key not in val and str(key) not in val:
                            if key >= 24:
                                key = 23
                        if key not in val:
                            key = str(key)

                    # if val["_name"] == "cid": # New champion - use Ezreal's data
                    #     key = champ_dict["Ezreal"]
                    #     if key not in val:
                    #         key = str(key)
                    if key not in val:
                        if val['_name'] == 'cid':
                            if key == champ_dict["Sett"]:
                                key = champ_dict["Jax"]
                            elif key == champ_dict["Senna"]:
                                key = champ_dict["Pyke"]
                            elif key == champ_dict["Aphelios"]:
                                key = champ_dict["Jhin"]
                    if key not in val and str(key) in val:
                        key = str(key)

                    if key not in val:
                        pr_fl(flag, key, val.keys(), val["_name"])


            val = val[key]
        # if key not in X_labels, make new value
        l = flag[2]
        # if "_rank" in l or "_turrets" in l:
        #     continue
        if l not in X_labels:
            X_new = np.zeros((len(X), X.shape[1] + 1))
            X_new[:, :-1] = X
            X = X_new
            X_label_is[l] = len(X_labels)
            X_labels.append(l)
        X[flag[0], X_label_is[l]] = val["_data"]
        fl_i += 1
        if fl_i % 1000 == 0 and verbose:
            sys_print("\rFill flag # " + str(fl_i) + '/' + max_fl)
    # sys_print("\rFill flag # " + max_fl + '/' + max_fl)
    # pr_fl("\nFilling missing data for recent matches...")
    # if X_rec[0] != -1:
    fl_i, max_fl = 0, str(len(filler["rec_flags"]))
    for flag in filler["rec_flags"]:
        val = fill
        for key in flag[4:]:


            if isinstance(val, dict) and key not in val:

                if str(key) in val:
                    key = str(key)
                else:

                    if val["_name"] == "div": # If we don't yet have data for master+
                        key = int(key)
                        if key >= 25:
                            key = 24
                        if key not in val and str(key) not in val:
                            if key >= 24:
                                key = 23
                        if key not in val:
                            key = str(key)

                    # if val["_name"] == "cid": # New champion - use Ezreal's data
                    #     key = champ_dict["Ezreal"]
                    #     if key not in val:
                    #         key = str(key)
                    if key not in val:
                        if val['_name'] == 'cid':
                            if key == champ_dict["Sett"]:
                                key = champ_dict["Jax"]
                            elif key == champ_dict["Senna"]:
                                key = champ_dict["Pyke"]
                            elif key == champ_dict["Aphelios"]:
                                key = champ_dict["Jhin"]
                    if key not in val and str(key) in val:
                        key = str(key)
                    if key not in val:
                        pr_fl(flag, key, val.keys(), val["_name"])


            val = val[key]
        player_i = t_role_is[flag[1]]
        # if flag[2] >= len(X_rec[flag[0]][flag[1]]):
        #     continue
        # if "_turrets" in flag[4]:
        #     pr_fl(flag)
        #     pr_fl(val["_data"])
        l = flag[3]
        # if "_rank" in l or "_turrets" in l:
        #     continue
        # if "opgg_champion_recent_rank" in l:
        #     pr_fl(player_i, flag, val["_data"])
        try:
            X_rec[flag[0]][player_i][flag[2], X_rec_label_is[flag[3]]] = \
                val["_data"]
        except Exception as e:
            pr_fl(e)
            pr_fl(flag)
            pr_fl(len(X_rec))
            pr_fl(len(X_rec[flag[0]]))
            pr_fl(len(X_rec[flag[0]][flag[1]][flag[2]]))
            pr_fl(len(X_rec[flag[0]][flag[1]][flag[2]][X_rec_label_is[flag[3]]]))
            sys.exit()
        fl_i += 1
        if fl_i % 100 == 0 and verbose:
            sys_print("\rFill flag # " + str(fl_i) + '/' + max_fl)
        # sys_print("\rFill flag # " + max_fl + '/' + max_fl)

    return X, X_rec, X_labels


# Filter final feature labels for a specific caching group
def get_featcache_labels(k, k_, X_ls):
    return {
        "ranked": {
            "summary": lambda ls: [l for l in ls if \
                l in X_c_player_labels_ranked_final_full_set or \
                "opgg_rs_" in l or \
                "ravg_" in l],
        },
        "champion": {
            "ranked": lambda ls: [l for l in ls if \
                "opgg_champion_recent_" in l or \
                "opgg_champion_season_" in l],
            "total": lambda ls: [l for l in ls if \
                "opgg_champion_avg_" in l or \
                "opgg_champion_seasonavg_" in l or \
                "opgg_champion_all_" in l],
            "global": lambda ls: [l for l in ls if \
                "_eloavg_" in l and "_/_" not in l and \
                "_matchup_" not in l],
            "matchups": lambda ls: [l for l in ls if \
                "chgg_eloavg_matchup" in l and "_/_" not in l and \
                "_synergy_" not in l and "_adc_support_" not in l and "_support_adc_" not in l],
            "synergies": lambda ls: [l for l in ls if \
                "chgg_eloavg_matchup" in l and "_/_" not in l and \
                ("_synergy_" in l or "_adc_support_" in l or "_support_adc_" in l)],
        }
    }[k][k_](X_ls)



# Cache computed features and add pre-computed from cache
def cache_features(X, X_labels, req_data, rfs, cfs, rf_flags, cf_flags):
    
    # First, add new computed features to cache - for each player
    for i in range(len(req_data)):
        rd = req_data[i]
        if rd[name_li] == -1:
            continue

        # Get player specific feature labels
        side, role = cols_all[rd[team_li]], roles_all[rd[role_li]]
        pl_ls = [l for l in X_labels if l.split('_')[:2] == [side, role]]
        if len(pl_ls) == 0:
            continue
        rf, cf = rfs[i], cfs[i]

        # Check for ranked summary features
        k = "ranked"
        k_ = "summary"
        feat_labels = get_featcache_labels(k, k_, pl_ls)
        if len(feat_labels) > 0: # If present, store features in cache
            rf["summary"] = {l: X[0, X_labels.index(l)] for l in feat_labels}

        # Check for player champion features
        k = "champion"
        for k_ in ["ranked", "total", "global", "matchups", "synergies"]:
            feat_labels = get_featcache_labels(k, k_, pl_ls)
            if len(feat_labels) > 0: # If present, store features in cache
                cf[k_] = {l: X[0, X_labels.index(l)] for l in feat_labels}

    # Second, append flagged pre-computed features to the x vector from the cache
    new_X, new_ls = [], []
    for i in range(len(req_data)):
        rd = req_data[i]
        if rd[name_li] == -1:
            continue
        rf, cf = rfs[i], cfs[i]

        rf_fs = rf_flags[i]
        cf_fs = cf_flags[i]

        if "summary" in rf_fs:
            for elo_str in rf_fs["summary"]:
                # game_i = rf_fs["summary"][elo_str] # todo
                rf_summary = rf["summary"]
                new_X += list(rf_summary.values())
                new_ls += list(rf_summary.keys())

        for k_ in ["ranked", "total", "global", "matchups", "synergies"]:
            if k_ in cf_fs:
                for hash_ in cf_fs[k_]:
                    # game_i = cf_fs[k_][elo_str] # todo
                    cf_k = cf[k_]
                    new_X += list(cf_k.values())
                    new_ls += list(cf_k.keys())

    return np.hstack([X, np.atleast_2d(new_X)]), X_labels + new_ls




# Add engineered x features to datapoint, and scale
def add_engineered_features(X, X_rec, X_labels, X_rec_labels,
  X_scales, X_scalesdict, X_scaler, X_autolog, X_mins, req_data, rec_infill_data, n_opgg, alpha, beta, add_recent_match_feats=True):

    # Compute recent games n-averages and features (global statistics, in-game
    # performance normalized by the global average for the role/champion/elo rating)
    if n_opgg > 0 and add_recent_match_feats:
        X_rec = deepcopy(X_rec)
        rd_is = [i for i in range(len(req_data)) if req_data[i][name_li] != -1]
        roles_taken = [cols_all[req_data[i][team_li]] + '_' + roles_all[req_data[i][role_li]] for i in rd_is]

        # opgg_rds = [d for d in req_data if d[name_li] != -1]
        # pr_fl(len(opgg_rds))
        # pr_fl(roles_taken)
        # pr_fl(X_rec_labels)
        # X_rec_labels = [l for l in X_rec_labels if get_label_role(l) in roles_taken]
        # pr_fl(X_rec_labels)
        X_rec_labels, X_rec = add_recent_match_features(X, X_rec, X_labels, X_rec_labels, verbose=False, copy_over_X_rec=True)
        X_rec_avgs, X_rec_avgs_labels = get_recent_match_avgs(
            X, X_rec, X_rec_labels,
            infill_data=rec_infill_data, roles=roles_taken, rd_is=rd_is, verbose=False)
        # for l in X_rec_avgs_labels:
        #     pr_fl(l, X_rec_avgs[:, X_rec_avgs_labels.index(l)])
        # pr_fl(roles_taken)
        X, X_labels = np.hstack((X, X_rec_avgs)), X_labels + X_rec_avgs_labels

    # Scale data
    if X_scalesdict is not None:
        X_full_labels = list(X_scalesdict.keys())
        X_mins_dict = dict(zip(X_full_labels, X_mins))
        X_center_dict = dict(zip(X_full_labels, X_scaler.center_ if isinstance(X_scaler, RobustScaler) else X_scaler.mean_))
        X_scales_dict = dict(zip(X_full_labels, X_scaler.scale_))

        X = scale_data_with_dict(X, X_labels, X_scalesdict, make_copy=True)  # 0-1 Min-max

        a, b, g = X_autolog
        for i in range(X.shape[1]):
            k = X_labels[i]
            if k not in X_center_dict:
                continue
            X[:, i] = (X[:, i] - X_center_dict[k]) / X_scales_dict[k]   # RobustScaler
            X[:, i] = ( a * X[:, i]) + np.log((b * \
                      (max(g, 1e-8) + np.maximum(X[:, i] - X_mins_dict[k], 0.))).astype(np.float32))

    # Bayesian win rates
    # if n_opgg > 0:
    #     X, X_labels = add_beta_winrates(alpha, beta, X, X_labels, X_scales)

    # Add cached final features and cache new features


    return X, X_labels


# Get final x vector for the given model feature labels
def get_final_X(X, X_labels, x_ls, model_dict, model_strs):
    mkey = model_strs[0]
    _, _, full_5_centers, full_5_scales, full_5_coefs = model_dict[(5, 5, 5, 5, 5, 5, 5)][6]
    full_5_params = lambda ls, X__: np.atleast_2d(np.hstack([((X__[:, li] - full_5_centers[mkey][ls[li]]) / \
            full_5_scales[mkey][ls[li]]) for li in range(len(ls))]))
            # full_5_scales[mkey][ls[li]]) * full_5_coefs[mkey][ls[li]] for li in range(len(ls))]))
    X_ = []
    for k in x_ls:
        k_list = k.split('_')
        first_two = '_'.join(k_list[:2])
        if first_two not in t_roles and "synergy" not in k_list \
          and "_adc_support_" not in k and "_support_adc_" not in k:
            k_ = '_'.join(k_list[1:])
            t = k_list[0]
            # if k_ in non_first_two_added:
            #     continue
            # non_first_two_added.append(k_)

            if "_champion__x" in k:
                # x_.append(np.sum(x[[X_labels.index(t + '_' + r + '_' + k_) \
                #     for r in roles_all if t + '_' + r + '_' + k_ in X_labels]]))

                ls = [t + '_' + r + '_' + k_ for r in roles_all if t + '_' + r + '_' + k_ in X_labels]

                # if len(ls) > 0:
                res = np.sum(full_5_params(ls, X[:, [X_labels.index(l) for l in ls]]), axis=1)

                # res = np.sum(X[:, [X_labels.index(t + '_' + r + '_' + k_) \
                #     for r in roles_all if t + '_' + r + '_' + k_ in X_labels]], axis=1)

                # if res.ndim < 2:
                #     res = np.atleast_2d(res).T
                X_.append(res)
                # pr_fl(X_[-1].shape)
            else:
                # pr_fl([l for l in X_labels if "chgg_eloavg_matchup_synergy_adc_games" in l])

                # x_.append(np.mean(x[[X_labels.index(t + '_' + r + '_' + k_) \
                #     for r in roles_all if t + '_' + r + '_' + k_ in X_labels]]))

                # thing = X[:, [X_labels.index(t + '_' + r + '_' + k_) \
                #     for r in roles_all if t + '_' + r + '_' + k_ in X_labels]]
                # if np.isnan(thing).any() or len(thing) == 0 or np.isinf(thing).any() or thing.shape[1] == 0:
                #     pr_fl(t, k_, thing)

                ls = [t + '_' + r + '_' + k_ for r in roles_all if t + '_' + r + '_' + k_ in X_labels]

                if len(ls) == 0:
                    pr_fl(k, t)
                # if len(ls) > 0:
                res = np.mean(full_5_params(ls, X[:, [X_labels.index(l) for l in ls]]), axis=1)

                # res = np.mean(X[:, [X_labels.index(t + '_' + r + '_' + k_) \
                #     for r in roles_all if t + '_' + r + '_' + k_ in X_labels]], axis=1)

                # if np.isnan(res).any():
                #     pr_fl(t, k_, thing)

                # if res.ndim < 2:
                #     res = np.atleast_2d(res)
                X_.append(res)
                # pr_fl(X_[-1].shape)

                # pr_fl(x_[-1], k, x[[X_labels.index(r + '_' + k_) for r in t_roles if r + '_' + k_ in X_labels]])
        else:
            x_ = X[:, X_labels.index(k)].copy() if k in X_labels else np.tile(full_5_centers[mkey][k], X.shape[0])

            # if x_.ndim < 2:
            #     x_ = np.atleast_2d(x_)
            X_.append(x_)
            # pr_fl(X_[-1].shape)
    # pr_fl(np.hstack(X_).shape)
    return np.vstack(X_).T

# Convert cid/name count for use as model key (0, 1 or 5)
def round_count(c):
    return 1 if c >= 2 and c <= 4 else c


# Quickly parse postgres json (Postjson) value (decodes numbers encoded as strings)
# (assumes no string values start with a number character)
def parsePgJ(v):
    if not isinstance(v, str):
        return v
    if v[0].isdigit():
        if 'm ' in v and v[-1] == 's': # game duration does start with a number character (eg 20m 12s)
            return v
        if '.' in v:
            return float(v)
        return int(v)
    return v



def get_x_champion(filler, ts_b, gsdb, gsdb_cond, opgg_region, rf_flags, cf_flags,
  side, role, cid, elo, name, rs, pc, ch, rf, cf, profic_overlap_blue, profic_overlap_red):
    x = OrderedDict()
    tier, div, lp = get_league_info(elo)

    # Get global stats
    if cid != -1:
        # Check champion feature cache for this data
        global_stats_hash = '_'.join((str(h) for h in (tier, side, role, cid)))
        cached = False
        if cf is not None:
            if "global" in cf:
                if global_stats_hash in cf["global"]:
                    cached = True
                    if "global" not in cf_flags:
                        cf_flags["global"] = {}
                    cf_flags["global"][global_stats_hash] = filler["game_i"]

        # If not found in cache, extract features
        if not cached:
            x_new = add_global_champ_stats(ts_b, ts_b, opgg_region, tier, side, role, cid, gsdb, gsdb_cond)

            # Add result to x dict
            x = {**x, **x_new}

    # If we have an op.gg profile, get the champion stats
    if rs != -1:
        # pr_fl(type(pc))
        # if role == "jungle":
        #     pr_fl(len(pc), len(pc[0]))

        # x_new =    # add_opgg_champion_features() now adds to existing dict
        x = add_opgg_champion_features(ts_b, filler, pc, ch, opgg_region, rf, cf, rf_flags, cf_flags,
            tier, div, elo, side, role, cid, name, profic_overlap_blue, profic_overlap_red, x=x)

        # Add result to x dict
        # x = {**x, **x_new}

    return x




# Get x dictionary for a particular player
def get_x_player(ts_act, filler, ts_b, gsdb, gsdb_cond, infill_data, opgg_region, mean_elo, rf_flags, cf_flags,
                 side, role, cid, elo, name, rs, pc, ch, rf, cf, profic_overlap_blue, profic_overlap_red):
    x = OrderedDict()

    # Get tier etc
    tier, div, lp = get_league_info(elo)

    # Get champion features (call a function which can be run separately)
    # if cid != -1:
    x_new = get_x_champion(filler, ts_b, gsdb, gsdb_cond, opgg_region, rf_flags, cf_flags,
        side, role, cid, elo, name, rs, pc, ch, rf, cf, profic_overlap_blue, profic_overlap_red)
    x = {**x, **x_new}

    # Get player features (call a function which can be run separately)
    x_rec = np.array([])
    if rs != -1:

        # Check ranked feature cache for this data
        ranked_hash = str(elo)
        cached = False
        if rf is not None:
            if "summary" in rf:
                if ranked_hash in rf["summary"]:
                    cached = True
                    if "summary" not in rf_flags:
                        rf_flags["summary"] = {}
                    rf_flags["summary"][ranked_hash] = filler["game_i"]

        # If not found in cache, extract features
        if not cached:
            x_new, x_rec = add_opgg_ranked_features(ts_act, filler, opgg_region, mean_elo, rs, pc, ch, tier, elo, side, role, name)
            x = {**x, **x_new}

    return x, x_rec


def get_x_matchup(ts_b, gsdb, gsdb_cond, side, role, tier, cid, cid_):
    x = OrderedDict()
    m_cs = get_chgg_gms(ts_b, cid, tier, role, cid_, gsdb=gsdb, lock=gsdb_cond)
    pre = side + '_' + role + '_' + ch_m_pre
    add_matchup_ch(x, pre, m_cs)
    return x

def get_x_botsynergies(ts_b, gsdb, gsdb_cond, tier, cids):
    x = OrderedDict()
    for side in cols_all:
        cid, cid_ = cids[side] # adc, support
        m_cs = get_chgg_gms(ts_b, cid, tier, "synergy", cid_, gsdb=gsdb, lock=gsdb_cond)
        pre = side + '_' + ch_m_pre + "synergy_adc" + '_'
        add_matchup_ch(x, pre, m_cs)
        m_cs = get_chgg_gms(ts_b, cid_, tier, "synergy", cid, gsdb=gsdb, lock=gsdb_cond)
        pre = side + '_' + ch_m_pre + "synergy_support" + '_'
        add_matchup_ch(x, pre, m_cs)

    for si in N_T_r:
        cid, cid_ = cids[cols_all[si]][0], cids[cols_all[1 - si]][1]
        m_cs = get_chgg_gms(ts_b, cid, tier, "adc_support", cid_, gsdb=gsdb, lock=gsdb_cond)
        pre = cols_all[si] + '_' + ch_m_pre + "adc_support" + '_'
        add_matchup_ch(x, pre, m_cs)
        m_cs = get_chgg_gms(ts_b, cid_, tier, "adc_support", cid, gsdb=gsdb, lock=gsdb_cond)
        pre = cols_all[1 - si] + '_' + ch_m_pre + "support_adc" + '_'
        add_matchup_ch(x, pre, m_cs)
    return x


def add_opgg_champion_features(ts_b, filler, pc, ch, opgg_region, rf, cf, rf_flags, cf_flags,
  tier, div, elo, side, role, cid, name, profic_overlap_blue, profic_overlap_red, x=OrderedDict()):

    # Check champion feature cache for the player champion stats
    ranked_champ_hash = '_'.join((str(h) for h in (elo, side, role, cid)))
    cached = False
    if cf is not None:
        if "ranked" in cf:
            if ranked_champ_hash in cf["ranked"]:
                cached = True
                if "ranked" not in cf_flags:
                    cf_flags["ranked"] = {}
                if "total" not in cf_flags:
                    cf_flags["total"] = {}
                cf_flags["ranked"][ranked_champ_hash] = filler["game_i"]
                cf_flags["total"][str(elo)] = filler["game_i"]

    # If not found in cache, extract features
    # ch_ = deepcopy(ch)
    ch_ = ch
    if not cached:

        # Load player champion stats from op.gg
        cid_f = 13 if cid == -1 else cid
        ch_data = get_champ_stats(pc, ch_, "", cid_f, opgg_region, tier,
            div, side, role, role,
            filler, False, ts_b, ts_b)
        r = side + '_' + role
        pre = r + "_opgg_champion_"
        # pr_fl(pre)
        if cid != -1 and roles_all_is[role] in (profic_overlap_blue if side == "blue" else profic_overlap_red):
            for k in ch_data:
                x[pre + k] = ch_data[k]
                # if "_rank" in k:
                #     pr_fl(pre + k)

            # Add player champ stat ratios
            for k, k_ in pcs_ratios:
                x[r + '_' + k + '_/_' + k_] = x[r + '_' + k] / x[r + '_' + k_]

            # pr_fl(x[pre + "recent_rank"])

        # Check champion feature cache for the total player champion stats
        total_hash = str(elo)
        cached = False
        if cf is not None:
            if "total" in cf:
                if total_hash in cf["total"]:
                    cached = True
                    if "total" not in cf_flags:
                        cf_flags["total"] = {}
                    cf_flags["total"][str(elo)] = filler["game_i"]

        # If not found in cache, extract features
        if not cached:

            # Add total champion performance stats for player
            for key in opgg_tots_keys:
                pre_ = pre + key + '_'
                d = ch_[key]
                if d is not None:
                    for k in d:
                        # pr_fl(k, side, role)
                        # if pre_ + k == "blue_jungle_opgg_champion_all_Fighter_wins":
                        #     pr_fl(x[pre_ + k])
                        #     pr_fl(d[k])
                        x[pre_ + k] = d[k]

            # Add to cache only if we have all data
            if all(ch_[key] is not None for key in opgg_tots_keys):
                for key in ch_:
                    ch[key] = ch_[key]
    
    return x

    # for l in x:
    #     if "_all_" in l: 
    #         pr_fl(l)

    # return x
                          
    # # Add index of oldest and newest season we have pcs stats for
    # n_cs_s = len(cs)
    # oldest_season_i, latest_season_i = 0, 0
    # for cs_i in range(n_cs_s):
    #     if cs[cs_i]:
    #         oldest_season_i = curr_season_id - cs_i
    #         break
    # for cs_i in range(n_cs_s)[::-1]:
    #     if cs[cs_i]:
    #         latest_season_i = curr_season_id - cs_i
    #         break
    # # pr_fl(oldest_season_i, latest_season_i)
    # x[r + "_first_ranked_season"] = oldest_season_i
    # x[r + "_last_ranked_season"] = latest_season_i


def add_opgg_ranked_features(ts_act, filler, opgg_region, mean_elo, rs, pc, ch, tier, elo, side, role, name):
    x = OrderedDict()

    tier, div, lp = get_league_info(elo)
    r = side + '_' + role

    rank = 1 if div >= 25 else 5 - (div % 5)
    wins = rs[opgg_rs_db_keys.index("wins")]
    losses = rs[opgg_rs_db_keys.index("losses")]
    n_games = wins + losses
    wr = (100 * wins / n_games) if n_games > 0 else 50.0

    pre = r + '_'
    x[pre + "season_games"] = n_games
    x[pre + "season_wins"] = wins
    x[pre + "season_losses"] = losses
    x[pre + "season_win_rate"] = wr
    # x[pre + "tier"] = tier
    # x[pre + "int_tier"] = tier
    x[pre + "rank"] = rank
    # x[pre + "league_points"] = lp
    x[pre + "division"] = div
    x[pre + "elo"] = elo
    x[pre + "elodeviation"] = elo - mean_elo

    for k in opgg_rs_db_keys:
        if k in opgg_rs_non_number_keys:
            continue
        rs[opgg_rs_db_keys.index(k)] = hard_float(rs[opgg_rs_db_keys.index(k)])
    for k in opgg_rs_recent_keys:
        rs[opgg_rs_db_keys.index("recent_" + k)] = hard_float(rs[opgg_rs_db_keys.index("recent_" + k)])

    pre_ = "opgg_rs_"
    pre = r + '_' + pre_
    # x[pre + "wins"] = ws
    # x[pre + "losses"] = ls
    x[pre + "games"] = n_games
    # x[pre + "win_rate"] = wr
    x[pre + "tier"] = tier
    x[pre + "rank"] = rank
    x[pre + "division"] = div
    x[pre + "league_points"] = lp

    recent_wr = rs[opgg_rs_db_keys.index("recent_winRatio")]
    if recent_wr == -1:
        recent_wr = wr
    x[pre + "recent_win_rate"] = recent_wr
    x[pre + "recent_games"] = rs[opgg_rs_db_keys.index("recent_games")]
    x[pre + "recent_wins"] = rs[opgg_rs_db_keys.index("recent_wins")]
    x[pre + "recent_losses"] = rs[opgg_rs_db_keys.index("recent_losses")]

    for k in ["kills", "deaths", "assists", "kda_ratio", "kill_participation"]:
        v = rs[opgg_rs_db_keys.index("recent_" + recent_keys_dict_thing[k])]
        if v == -1:
            v = get_infill(infill_data, "recent_" + k, role, div)
        x[pre + "recent_" + k] = v

    # Get recent matches data from RankedSummary
    n_recent = len(rs[opgg_rs_db_keys.index("games")])
    x[r + "_n_recent_matches"] = n_recent
    x_rec_ = []

    if n_recent:
        # name = names[i]

        rs_games = rs[opgg_rs_db_keys.index("games")]
        rs_games = [{opgg_rs_game_keys[k_i]: parsePgJ(rs_games[i_][k_i]) for k_i in \
            range(len(opgg_rs_game_keys))} for i_ in range(len(rs_games))]

        # Todo: see if the prediction changes much before and after the match is actually played
        # for gm_i in range(len(rs_games)):
        #     if match_op_rs_rec_game(rs_games[gm_i], cid,
        #       queue_id, duration, win, kills, deaths, assists,
        #       creeps, spell_1, spell_2):
        #         rs_games = rs_games[gm_i + 1:]
        #         break

        gm_i = 0
        for gm in rs_games:  # Go in reverse chronological order, so we can retro-decrement champ stats

            time_since = (ts_act / 1000) - gm["timestamp"]
            # Don't add if game occurred in the future
            # if time_since < 0:
            #     continue
            # if time_since > (4 * 24 * 60 * 60):
            #     continue
            if gm["result"] == "Remake":
                continue

            # pr_fl(gm_i,)
            cid_ = champ_dict[gm["champion"] if gm["champion"] != "Nunu" else "Nunu & Willump"]
            spell_1_ = spell_ids[gm["spell1"].lower() if gm["spell1"].lower() in spell_ids else "flash"]
            spell_2_ = spell_ids[gm["spell2"].lower() if gm["spell2"].lower() in spell_ids else "ignite"]
            if spell_order[spell_1_] < spell_order[spell_2_]:
                spell_1_, spell_2_ = spell_2_, spell_1_
            duration_ = gm["length"].split('m ')
            found_side, team_i = 0, False
            while not found_side:
                team_key = "team" + str(team_i + 1)
                if team_key not in gm:
                    break
                for pl in gm[team_key]:
                    if "name" not in pl:
                        continue
                    if pl["name"] == name:
                        found_side = True
                        break
                if team_i == 1: break
                team_i += 1
            if not found_side:
                # err_pr("Failed to find side for " + name + \
                #        " in recent game: " + str(gm))
                # Set randomly since we can't know ground truth
                team_i = np.random.randint(2)
            queue_id_ = rito_queue_ids["RANKED_FLEX_SR"] if \
                gm["type"][0] == 'F' \
                else rito_queue_ids["RANKED_SOLO_5x5"]
            dur = (int(duration_[0]) * 60) + \
                int(duration_[1][:-1])
            ts = (gm["timestamp"]) * 1000
            
            gm_d = OrderedDict(zip(X_c_prev_labels_all,
                [None] * X_c_prev_labels_all_len))

            gm_d["queue"] = queue_id_
            gm_d["duration"] = dur
            gm_d["win"] = int(gm["result"] == "Victory")
            gm_d["champion"] = cid_
            gm_d["team_side"] = team_i
            gm_d["spell_1"] = spell_1_
            gm_d["spell_2"] = spell_2_
            gm_d["kills"] = gm["kills"]
            gm_d["deaths"] = gm["deaths"]
            gm_d["assists"] = gm["assists"]
            gm_d["creep_score"] = gm["cs"]
            # gm_d["creep_score_per_second"] = gm["csps"]
            gm_d["kill_participation"] = gm["killParticipation"]
            gm_d["level"] = gm["level"]
            gm_d["timestamp"] = ts
            gm_d["time_of_day"] = ((ts / 1000) + region_timezones[opgg_region]) % (60 * 60 * 24)
            gm_d["pinks_purchased"] = gm["pinksPurchased"]

            retrospect_pcs(gm, pc)
            
            role_ = "jungle" if (spell_1_ == smite_id or \
                spell_2_ == smite_id) else None
            ch_data, op_ch, ch_cs = get_champ_stats(pc, ch, "",
                cid_, opgg_region, tier, div, side,
                role, role_, filler, True, ts, ts, recent_i=gm_i)
            pre = "opgg_champion_"
            for k in ch_data:
                gm_d[pre + k] = ch_data[k]
            # if "recent_rank" not in ch_data and gm_i == 3:
            #     pr(filler["rec_flags"])
            #     sys.exit()

            # Add op.gg global champion statistics
            for per in opgg_periods:
                pre = op_ch_pre + per + '_'
                gm_d[pre + "games"] = op_ch[per]["games"]
                gm_d[pre+"games_fraction"]=op_ch[per]["gamesFrac"]
                gm_d[pre + "win_rate"] = op_ch[per]["winrate"]
                gm_d[pre + "kda_ratio"] = op_ch[per]["kda"]
                gm_d[pre + "gold"] = op_ch[per]["gold"]
                gm_d[pre + "creep_score"] = op_ch[per]["cs"]

            # Add Champion.gg global champion statistics
            ch_k, ch_d = ch_cs["kills"], ch_cs["deaths"]
            ch_a = ch_cs["assists"]
            ch_kda = (ch_k + ch_a) / (ch_d)
            ch_cd = ch_cs["damageComposition"]
            pre = ch_ch_pre
            gm_d[pre + "games"] = ch_cs["gamesPlayed"]
            gm_d[pre + "win_rate"] = ch_cs["winRate"]
            gm_d[pre + "play_rate"] = ch_cs["playRate"]
            gm_d[pre + "percent_role_played"] = \
                ch_cs["percentRolePlayed"]
            gm_d[pre + "kills"] = ch_k
            gm_d[pre + "deaths"] = ch_d
            gm_d[pre + "assists"] = ch_a
            gm_d[pre + "kda_ratio"] = ch_kda
            gm_d[pre + "kill_sprees"] = ch_cs["killingSprees"]
            gm_d[pre + "largest_kill_spree"] = \
                ch_cs["largestKillingSpree"]
            gm_d[pre + "creep_score"] = ch_cs["minionsKilled"]
            gm_d[pre + "gold"] = ch_cs["goldEarned"]
            gm_d[pre + "total_damage_taken"] = \
                ch_cs["totalDamageTaken"]
            gm_d[pre + "total_heal"] = ch_cs["totalHeal"]
            gm_d[pre + "wards_placed"] = ch_cs["wardPlaced"]
            gm_d[pre + "wards_killed"] = ch_cs["wardsKilled"]
            gm_d[pre + "total_damage"] = ch_cd["total"]
            gm_d[pre + "total_magic_damage"] =ch_cd["totalMagical"]
            gm_d[pre + "total_physical_damage"] = \
                ch_cd["totalPhysical"]
            gm_d[pre + "total_true_damage"] = ch_cd["totalTrue"]
            gm_d[pre + "percent_magic_damage"] = \
                ch_cd["percentMagical"]
            gm_d[pre + "percent_physical_damage"] = \
                ch_cd["percentPhysical"]
            gm_d[pre + "percent_true_damage"] =ch_cd["percentTrue"]
            gm_d[pre + "jungle_creep_score_team"] = \
                ch_cs["neutralMinionsKilledTeamJungle"]
            gm_d[pre + "jungle_creep_score_enemy"] = \
                ch_cs["neutralMinionsKilledEnemyJungle"]

            # Add Champion.gg wins by match length data
            ch_ml = ch_cs["winsByMatchLength"]
            ml_games = ch_cs["ml_games"]
            if ml_games == 0:
                ml_games = 1
            for k in ml_dict:
                dat = ch_ml[ml_dict[k]]
                count = dat["count"]
                pre = ml_l + k + '_'
                gm_d[pre + "games"] = count
                gm_d[pre + "games_fraction"] = count / ml_games
                gm_d[pre + "win_rate"] = dat["winRate"]

            # Add Champion.gg wins by matches played data
            # ch_mp = ch_cs["winsByMatchesPlayed"]
            # mp_games = ch_cs["mp_games"]
            # mp_players = ch_cs["mp_players"]
            # for k in mp_dict:
            #     dat = ch_mp[mp_dict[k]]
            #     count = dat["gamesPlayed"]
            #     p_count = dat["players"]
            #     pre = roll + '_' + mp_l + k + '_'
            #     x[pre + "games"] = count
            #     x[pre + "games_fraction"] = count / mp_games
            #     x[pre + "players"] = p_count
            #     x[pre + "players_fraction"] = p_count/mp_players
            #     x[pre + "wins"] = dat["wins"]
            #     x[pre + "win_rate"] = dat["winRate"]
            
            # Debug
            # for k in gm_d:
            #     if gm_d[k] is None:
            #         pr_fl(game_i, i, gm_i, "Recent None:", k)
            # 
            gm_d = np.hstack(list(gm_d.values()))

            x_rec_.append(gm_d)
            gm_i += 1
        x_rec_ = x_rec_[::-1]

    x_rec_ = np.vstack(x_rec_) if len(x_rec_) > 0 else np.array([])

    return x, x_rec_

def add_global_champ_stats(op_gcs_ts, ch_gcs_ts, opgg_region, tier, side, role, cid, gsdb, gsdb_cond):
    x = OrderedDict()
    roll = side + '_' + role

    # Add op.gg global champion statistics
    for period in opgg_periods:
        stats = get_opgg_gcs(op_gcs_ts, cid, opgg_region,
            tier, period, 'win', gsdb=gsdb, lock=gsdb_cond)
        pre = roll + '_' + op_ch_pre + period + '_'
        x[pre + "games"] = stats["games"]
        x[pre + "games_fraction"] = stats["gamesFrac"]
        x[pre + "win_rate"] = stats["winrate"]
        x[pre + "kda_ratio"] = stats["kda"]
        x[pre + "gold"] = stats["gold"]
        x[pre + "creep_score"] = stats["cs"]

    # Add Champion.gg global champion statistics
    ch_cs = get_chgg_gcs(ch_gcs_ts, cid, tier, role, gsdb=gsdb, lock=gsdb_cond)
    ch_k, ch_d, ch_a = ch_cs["kills"],ch_cs["deaths"],ch_cs["assists"]
    ch_kda = calculate_kda(ch_k, ch_d, ch_a)
    ch_cd = ch_cs["damageComposition"]
    pre = roll + '_' + ch_ch_pre
    x[pre + "games"] = ch_cs["gamesPlayed"]
    x[pre + "win_rate"] = ch_cs["winRate"]
    x[pre + "play_rate"] = ch_cs["playRate"]
    x[pre + "percent_role_played"] = ch_cs["percentRolePlayed"]
    x[pre + "kills"] = ch_k
    x[pre + "deaths"] = ch_d
    x[pre + "assists"] = ch_a
    x[pre + "kda_ratio"] = ch_kda
    x[pre + "kill_sprees"] = ch_cs["killingSprees"]
    x[pre + "largest_kill_spree"] = ch_cs["largestKillingSpree"]
    x[pre + "creep_score"] = ch_cs["minionsKilled"]
    x[pre + "gold"] = ch_cs["goldEarned"]
    x[pre + "total_damage_taken"] = ch_cs["totalDamageTaken"]
    x[pre + "total_heal"] = ch_cs["totalHeal"]
    x[pre + "wards_placed"] = ch_cs["wardPlaced"]
    x[pre + "wards_killed"] = ch_cs["wardsKilled"]
    x[pre + "total_damage"] = ch_cd["total"]
    x[pre + "total_magic_damage"] = ch_cd["totalMagical"]
    x[pre + "total_physical_damage"] = ch_cd["totalPhysical"]
    x[pre + "total_true_damage"] = ch_cd["totalTrue"]
    x[pre + "percent_magic_damage"] = ch_cd["percentMagical"]
    x[pre + "percent_physical_damage"] = ch_cd["percentPhysical"]
    x[pre + "percent_true_damage"] = ch_cd["percentTrue"]
    x[pre + "jungle_creep_score_team"] = \
        ch_cs["neutralMinionsKilledTeamJungle"]
    x[pre + "jungle_creep_score_enemy"] = \
        ch_cs["neutralMinionsKilledEnemyJungle"]

    # Add Champion.gg wins by match length data
    ch_ml = ch_cs["winsByMatchLength"]
    ml_games = ch_cs["ml_games"]
    if ml_games == 0:
        ml_games = 1
    for k in ml_dict:
        dat = ch_ml[ml_dict[k]]
        count = dat["count"]
        pre = roll + '_' + ml_l + k + '_'
        x[pre + "games"] = count
        x[pre + "games_fraction"] = count / ml_games
        x[pre + "win_rate"] = dat["winRate"]

    # Add Champion.gg wins by matches played data
    # ch_mp = ch_cs["winsByMatchesPlayed"]
    # mp_games = ch_cs["mp_games"]
    # mp_players = ch_cs["mp_players"]
    # for k in mp_dict:
    #     dat = ch_mp[mp_dict[k]]
    #     count = dat["gamesPlayed"]
    #     p_count = dat["players"]
    #     pre = roll + '_' + mp_l + k + '_'
    #     x[pre + "games"] = count
    #     x[pre + "games_fraction"] = count / mp_games
    #     x[pre + "players"] = p_count
    #     x[pre + "players_fraction"] = p_count / mp_players
    #     x[pre + "wins"] = dat["wins"]
    #     x[pre + "win_rate"] = dat["winRate"]

    return x


# Get modified x datapoint for a particular role/champion pair
role_adc_i = roles_all_is["adc"]
role_support_i = roles_all_is["support"]
def get_x_for_champ(x, X_labels, req_data, ts_b, gsdb, gsdb_cond, filler, rf_flags, cf_flags, #, game_i,
  opgg_region, side, role, cid, elo, tier, name, rs, pc, ch, rf, cf, matchups_ris, n_matchups, profic_overlap_blue, profic_overlap_red):

    x_new = get_x_champion(filler, ts_b, gsdb, gsdb_cond, opgg_region, rf_flags, cf_flags,
        side, role, cid, elo, name, rs, pc, ch, rf, cf, profic_overlap_blue, profic_overlap_red)
    x = x.copy()
    for l in x_new.keys():
        x[X_labels.index(l)] = x_new[l]
    if n_matchups > 0 and roles_all_is[role] in matchups_ris and side[0] == 'b':
        # Get matchup opponent cid
        side_ = "red" if side[0] == "b" else "blue"
        cid_ = None
        elo_ = None
        for d in req_data:
            if d[team_li] == col_is[side_] and d[role_li] == roles_all_is[role]:
                cid_ = d[cid_li]
                elo_ = d[-1]
        tier_, _, _ = get_league_info(elo_)

        # Check champion feature cache for this data
        matchup_hash = '_'.join((str(h) for h in (side, role, tier, cid, cid_)))
        cached = False
        if cf is not None:
            if "matchups" in cf:
                if matchup_hash in cf["matchups"]:
                    cached = True
                    if "matchups" not in cf_flags:
                        cf_flags["matchups"] = {}
                    cf_flags["matchups"][matchup_hash] = filler["game_i"]

        # If not found in cache, extract features
        if not cached:
            x_new = get_x_matchup(ts_b, gsdb, gsdb_cond, side, role, tier, cid, cid_)
            for l in x_new.keys():
                x[X_labels.index(l)] = x_new[l]
            x_new = get_x_matchup(ts_b, gsdb, gsdb_cond, side_, role, tier_, cid_, cid)
            for l in x_new.keys():
                x[X_labels.index(l)] = x_new[l]

        if n_matchups == 5 and role in ["adc", "support"]:
            cids = {"blue": [-1, -1], "red": [-1, -1]}
            elos = []
            for d in req_data:
                if d[role_li] == role_adc_i:
                    cids[cols_all[d[team_li]]][0] = proc_cid(d[cid_li], replacement=cid)
                    elos.append(d[-1])
                if d[role_li] == role_support_i:
                    cids[cols_all[d[team_li]]][1] = proc_cid(d[cid_li], replacement=cid)
                    elos.append(d[-1])
            tier_bot, _, _ = get_league_info(int(np.median(elos)))
                # if d[team_li] == col_is[side_] and d[role_li] == roles_all_is[role]:
                #     cid_ = d[cid_li]

            # Check champion feature cache for this data
            synergies_hash = '_'.join((str(tier_bot), str(cids)))
            cached = False
            if cf is not None:
                if "synergies" in cf:
                    if synergies_hash in cf["synergies"]:
                        cached = True
                        if "synergies" not in cf_flags:
                            cf_flags["synergies"] = {}
                        cf_flags["synergies"][synergies_hash] = filler["game_i"]

            # If not found in cache, extract features
            if not cached:
                x_new = get_x_botsynergies(ts_b, gsdb, gsdb_cond, tier_bot, cids)
                for l in x_new.keys():
                    x[X_labels.index(l)] = x_new[l]

    return x


# Get x datapoint for a req_data match composition
def get_x_vector(ts_act, ts_b, gsdb, gsdb_cond, infill_data, filler, rf_flags, cf_flags, #, game_i,
  opgg_regs, mean_elo, names, req_data, rss, pcs, chs, rfs, cfs, n_matchups, matchups_ris, X_rec_labels, profic_overlap_blue, profic_overlap_red):
    x = OrderedDict()
    x_rec = []
    # infill_data = deepcopy(infill_data)
    # filler = deepcopy(filler)
    # req_data = deepcopy(req_data)
    # rss = deepcopy(rss)
    # pcs = deepcopy(pcs)

    x["timestamp"] = ts_act

    # x["patch"] = patch_codes['8.20']
    # x["season"] = season_codes[curr_season_id]
    # x["region"] = region_codes[region]
    # x["queue"] = queue_codes[queue_id]

    # Get features for each player

    # create role ordered list

    x_new = None
    for i in range(len(req_data)):
        rq = req_data[i]
        side = cols_all[rq[team_li]]
        role = roles_all[rq[role_li]]
        cid = proc_cid(rq[cid_li])
        name = names[i]
        elo = rq[-1]
        cf = cfs[i]
        x_new, new_x_rec = get_x_player(
            ts_act, filler, ts_b, gsdb, gsdb_cond, infill_data, opgg_regs[i], mean_elo, rf_flags[i], cf_flags[i],
            side, role, cid, elo, name, rss[i], pcs[i], chs[i], rfs[i], cf, profic_overlap_blue, profic_overlap_red)
        x = {**x, **x_new}
        x_rec.append(new_x_rec)

        if n_matchups > 0 and roles_all_is[role] in matchups_ris and side[0] == 'b':
            tier, _, _ = get_league_info(elo)
            # Get matchup opponent cid
            side_ = "red"
            cid_ = None
            elo_ = None
            for d in req_data:
                if d[team_li] == col_is[side_] and d[role_li] == roles_all_is[role]:
                    cid_ = proc_cid(d[cid_li])
                    elo_ = d[-1]
            tier_, _, _ = get_league_info(elo_)

            # Check champion feature cache for this data
            matchup_hash = '_'.join((str(h) for h in (side, role, tier, cid, cid_)))
            cached = False
            if cf is not None:
                if "matchups" in cf:
                    if matchup_hash in cf["matchups"]:
                        cached = True
                        if "matchups" not in cf_flags:
                            cf_flags["matchups"] = {}
                        cf_flags["matchups"][matchup_hash] = filler["game_i"]

            # If not found in cache, extract features
            if not cached:
                x_new = get_x_matchup(ts_b, gsdb, gsdb_cond, side, role, tier, cid, cid_)
                x_new_ = get_x_matchup(ts_b, gsdb, gsdb_cond, side_, role, tier_, cid_, cid)

                # Add result to x dict
                x = {**x, **x_new, **x_new_}

    if n_matchups == 5:
        cids = {"blue": [-1, -1], "red": [-1, -1]}
        elos = []
        for d in req_data:
            if d[role_li] == role_adc_i:
                cids[cols_all[d[team_li]]][0] = proc_cid(d[cid_li])
                elos.append(d[-1])
            if d[role_li] == role_support_i:
                cids[cols_all[d[team_li]]][1] = proc_cid(d[cid_li])
                elos.append(d[-1])
        tier_bot, _, _ = get_league_info(int(np.median(elos)))
            # if d[team_li] == col_is[side_] and d[role_li] == roles_all_is[role]:
            #     cid_ = d[cid_li]

        # Check champion feature cache for this data
        synergies_hash = '_'.join((str(tier_bot), str(cids)))
        cached = False
        if cf is not None:
            if "synergies" in cf:
                if synergies_hash in cf["synergies"]:
                    cached = True
                    if "synergies" not in cf_flags:
                        cf_flags["synergies"] = {}
                    cf_flags["synergies"][synergies_hash] = filler["game_i"]


        # If not found in cache, extract features
        if not cached:
            x_new = get_x_botsynergies(ts_b, gsdb, gsdb_cond, tier_bot, cids)

            # Add result to x vector
            x = {**x, **x_new}

    return x, x_rec


def proc_cid(cid, replacement=None):
    if cid == -2:
        if replacement != None:
            return replacement
        return champ_dict["Ezreal"] # If pick recommendation request, just use ezreal here to make the labels
    return cid


