#
#  Monitoring library & constants
#


from Server import *


# Options
hosts = [
    {
        "addr": HOST_URL_NA,                                     # Lorb root host server address
        "pem": PEM_FILE_NA,                                      # SSH permissions key .pem file path
        "regions": 'rest',                                       # Riot/op.gg regions served by this root
        "aws_region": "us-west-1",                               # AWS server region id
        "aws_spot": "sfr-fd610b1f-1a4f-4645-bb43-4039becbe315",  # Minion worker spot fleet request id
    },
    # {
    #     "addr": HOST_URL_KR,
    #     "pem": PEM_FILE_KR,
    #     "regions": 'kr',
    #     "aws_region": "ap-northeast-2",
    #     "aws_spot": "sfr-e12d2399-929f-4577-827d-7935347efc63",
    # }
]

undercap = 90                       # CPU % autoscaling capacity thresholds
overcap = 50
autoscale = True                    # Whether to add or remove spot fleet instances according to demand
enable_logging = False              # Whether monitor.py should start logger.py (and restart it when autoscaling)
worker_log_expiry = 3               # Seconds before a worker is considered dead and monitoring on it stops
logger_restart_cooldown = 2 * 60    # Minimum number of seconds between logger restarts when n_workers changes
monitoring_interval = 1.5           # Seconds between total log updates and new worker checks
aws_cooldown = 7 * 60               # Seconds between AWS CLI actions (allow system to rebalance)


# Constants
host_n_workers = 2                  # Number of worker processes on the host server (CPU count of the chosen instance type)
lorb_log_file = 'lorb.log'
traffic_history_csv = logs_dir + 'traffic_history'

# Traffic metrics
tr_cols = [
    'cpu%',             # CPU % usage (average for system)
    'ram%',             # RAM % usage (average for system)
    'MbitUp',           # Network traffic (totals for system)
    'MbitDown',
    'mp_a',             # Match prediction active threads (totals for system)
    'pl_a',             # Player profile loads
    'op_a',             # op.gg API requests
    'pg_a',             # postgres connections
    'mp_qsize',         # Thread queue sizes (totals for system)
    'op_qsize',
    'pg_qsize',
    'mp_threads',       # Number of threads (total for system)
    'op_threads',
    'pg_threads',
    'max_mp_threads',   # Maximum number of threads for the entire system (for all processes serving a root server)
    'max_pl_threads',
    'max_op_threads',
    'max_pg_threads'
]
traffic_columns = ["timestamp", "n_workers"] + tr_cols


# Get public IPs for the worker minions of a root server
def get_public_ips(addr, pem):
    priv_ips = set()
    subprocess.Popen("ssh -i " + pem + " -o StrictHostKeyChecking=no ubuntu@" + addr + \
        " \"sudo rabbitmqctl list_connections > /home/ubuntu/lorb/logs/connections.log\"", shell=True)
    time.sleep(3)

    res = subprocess.run("ssh -i " + pem + " -o StrictHostKeyChecking=no ubuntu@" + addr + \
                        " \"sudo " + worker_pyex + " /home/ubuntu/lolorb/unique_connections.py\"", stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    priv_ips = json.loads(res)

    pub_ips = [addr]
    for ip in priv_ips:
        res = subprocess.run("ssh -i " + pem + " -o StrictHostKeyChecking=no ubuntu@" + addr + \
                           " \"ssh -i " + pem + " -o StrictHostKeyChecking=no ubuntu@" + ip + " curl -s " + meta_url + "/public-ipv4\"",
               stdout=subprocess.PIPE, shell=True)
        res = res.stdout.decode('utf-8').split('\n')[-1]
        pub_ips.append(res)
    # pr_fl(pub_ips)
    return pub_ips


