#
#  Match prediction worker
#


import pika

from Server import *


# Get process metadata
worker_index = int(sys.argv[-1]) if len(sys.argv) > 1 else 0
worker_key = socket.gethostname() + ':' + str(worker_index)
create_folder(logs_dir) # Create logs directory


# Load globals
model_dict, model_strs, scales, n_train, n_test, n_pcal = load_ld(models_fn)     # Models
pro_sq_models = load_ld(pro_sq_models_fn)          # Regular models used in the first stage of pro model
pro_model_nsqo, pro_scores_nsqo = load_ld(pro_model_fn + '_nsqo')       # Pro model
pro_model_sqo, pro_scores_sqo = load_ld(pro_model_fn + '_sqo')          # Pro model when we don't have pro stats
pr(("Pro model accuracy (nsqo):", pro_scores_nsqo))
pr(("Pro model accuracy  (sqo):", pro_scores_sqo))
pro_model = {'nsqo': pro_model_nsqo, 'sqo': pro_model_sqo}

infill_data = load_json(infill_data_dir + infill_data_fn, print_errs=True)           # Infill data
rec_infill_data = load_json(infill_data_dir + rec_infill_data_fn, print_errs=True)
gsdb = load_global_stats(del_old=worker_index == 0 and not crawl_server)             # Global champion stats

# Connect to postgres
db = sqlalchemy.create_engine(pg_conn_str, pool_size=cauldron_size, max_overflow=cauldron_overflow)

# Define threadpools and access conditions for bandwidth-limited concurrent IO operations
opgg_threadpool = ThreadPoolExecutor(max_workers=n_opgg_threads)  # op.gg HTTP request thread pool
pgdb_threadpool = ThreadPoolExecutor(max_workers=n_pgcn_threads)  # postgres connection thread pool
gsdb_cond = Condition()              # global champion statistics database cache write access condition
pgdb_cond = Condition()              # postgres new connection database object access condition
r_conds = make_r_conds()             # For rito requests
r_tss = make_r_timestamps()

log_vars = OrderedDict([
    ("mp_active", [Condition(), 0]),  # Active match prediction threads counter & write condition
    ("pl_active", [Condition(), 0]),  # Active player profile requests counter & write condition
    ("op_active", [Condition(), 0]),  # Active op.gg HTTP request threads counter & write condition
    ("pg_active", [Condition(), 0]),  # Active postgres connection threads counter & write condition
    ("renew_active", [Condition(), 0]),  # Active profile op.gg renewal threads counter & write condition
    ("pgdb_threadpool", pgdb_threadpool),  # postgres connection thread pool
    ("pgdb_db", db),                  # postgres database object (SQLAlchemy)
    ("pgdb_cond", pgdb_cond),         # postgres new connection database object access condition
])

# Define global variables shared by all threads
X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, X_scaler, X_autolog, X_mins = scales
full_args = (opgg_threadpool, pgdb_threadpool, pgdb_cond,
    db, gsdb, gsdb_cond, r_conds, r_tss, infill_data, rec_infill_data, model_dict,
    model_strs, scales, n_train, n_test, n_pcal, X_scales, X_scalesdict, X_scaler, X_autolog, X_mins,
    Yr_scales, Yr_scalesdict, pro_model, pro_sq_models)

# RabbitMQ user/pass credentials object
rmq_creds = pika.credentials.PlainCredentials(rmq_username, rmq_password)


# Define consumer for the prediction request message queue (blocking server worker thread)
def sub_worker(worker_i, subworker_i):
    global log_vars, full_args

    # Create player profile request thread pool (one for each possible player in a request)
    pl_threadpool = ThreadPoolExecutor(max_workers=N_PL)

    # Connect to RabbitMQ
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbit_host, credentials=rmq_creds))
    channel = connection.channel()
    channel.queue_declare(queue=mp_qname,
        arguments={'x-max-length': mp_rpc_queue_maxsize, 'overflow': 'reject-publish'})

    # Define the actual remote procedure function
    def on_request(ch, method, props, body):
        data = json.loads(body.decode('utf-8'))
        if len(data) < 10:
            data.append("unknown")

        # Extract data from minified format
        req_d = {
            "data": data[0],
            "region_i": data[1],
            "avg_elo_i": data[2],
            "req_i": data[3],
            "session_id": data[4],
            "shortlink": data[5],
            "comp": data[6],
            "timestamp": data[7],
            "patch": data[8],
            "regions": data[9],
            "flags": data[10],
            "source": data[11],
        }

        response = "null"
        try:  # Compute prediction
            response = prediction_request_handler(log_vars, req_d, full_args, pl_threadpool)
        except Exception as e:
            pr_fl("Error handling request (worker " + str(worker_i) + '-' + str(subworker_i) + '):')
            pr_fl(req_d)
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl(type(e))
            pr_fl(e)

        # Return response through reply queue
        ch.basic_publish(
            exchange='',
            routing_key=props.reply_to,
            properties=pika.BasicProperties(correlation_id=props.correlation_id),
            body=json.dumps(response),
        )
        ch.basic_ack(delivery_tag=method.delivery_tag)

        # pr_fl("Finished responding to request", req_d["session_id"], str(log_vars["pg_active"][1]))

    channel.basic_qos(prefetch_count=1)  # Pop the request from the queue while we attempt to compute it's result
    channel.basic_consume(on_request, queue=mp_qname)  # Define remote procedure callee

    # If testing this script (on crawl server), print ready status
    if crawl_server and subworker_i == n_mp_threads - 1:
        pr_fl("MP worker " + str(worker_i) + '-' + str(subworker_i) + ' (pid ' + \
            str(os.getpid()) + ") awaiting match prediction RPC requests")

    # Start listening for prediction requests (block until process is terminated from the outside)
    channel.start_consuming()

    return 200


# Get the current system workload and usage statistics and return the log string
def get_log_str():
    traffic_in, traffic_out = -1, -1
    if not crawl_server:
        try:
            ifs = psutil.net_if_addrs()
            interface = None
            for key in ifs:
                if ifs[key][0][1] != "127.0.0.1":
                    interface = key
                    break
            if interface is not None:
                with subprocess.Popen(("./get_traffic.sh " + interface).split(' '),
                       stdout=subprocess.PIPE,
                       stderr=subprocess.STDOUT,
                       universal_newlines=True) as proc:    
                    for line in proc.stdout:
                        if traffic_in == -1:
                            traffic_in = float(line)
                        else:
                            traffic_out = float(line)
        except Exception as e:
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl(type(e))
            pr_fl(e)
            
    log_str = worker_key + "@cpu%:" + str(psutil.cpu_percent()) + '|ram%:' + str(psutil.virtual_memory().percent) + \
        '|MbitUp:' + str(traffic_out) + '|MbitDown:' + str(traffic_in) + '|'
    for key in ["mp_active", "pl_active", "op_active", "pg_active"]:
        count = log_vars[key][1]
        with log_vars[key][0]:
            log_str += key.replace("_active", '_a') + ':' + str(count) + '|'
    log_str = log_str[:-1]
    log_str += "|mp_qsize:" + str(mp_threadpool._work_queue.qsize())
    log_str += "|mp_threads:" + str(len(mp_threadpool._threads))
    log_str += "|op_qsize:" + str(opgg_threadpool._work_queue.qsize())
    log_str += "|op_threads:" + str(len(opgg_threadpool._threads))
    log_str += "|pg_qsize:" + str(pgdb_threadpool._work_queue.qsize())
    log_str += "|pg_threads:" + str(len(pgdb_threadpool._threads))
    log_str += '\n'
    return log_str

# Check op.gg server status for a region
def check_op(log_vars, pgdb_tp, pgdb_cond, db, region):
    url = "http://" + (region if region != 'kr' else 'www') + ".op.gg"
    try:
        page = requests.get(url)
        code, reason = page.status_code, page.reason
    except Exception as e:
        code = -1
        reason = "request failed: " + str(e)
    pgdb_tp.submit(update_op_status, log_vars, None, pgdb_cond, db, region, code, reason)


###
###  Prediction server main thread
###  In background: cleans expired database entries, checks op.gg server status, logs usage
###
excep = False
run_t, run_t_actual = 0, 0
try:
    # Create thread pool for individual match prediction requests
    with ThreadPoolExecutor(max_workers=n_mp_threads) as mp_threadpool:

        # Submit blocking RPC function calls to thread pool
        exit_futs = [mp_threadpool.submit(sub_worker,
            worker_index, i) for i in range(n_mp_threads)]

        # exit_codes = [fut.result() for fut in exit_futs]
        # pr_fl(exit_codes)

        # Create log entry for this worker process
        pgdb_threadpool.submit(new_wlog_entry, log_vars, None, pgdb_cond, db, worker_key, get_log_str())

        k = 0
        while True:

            for j in range(n_mp_threads):
                fut = exit_futs[j]
                try:
                    e = fut.exception(0)
                    if e is not None:  # If call exited with an error
                        for line in traceback.format_tb(e.__traceback__):
                            pr_fl(line)
                        pr_fl(type(e))  # Print error
                        pr_fl(e)
                        time.sleep(3)  # Wait 3 seconds before trying to restart the sub worker
                        exit_futs[j] = mp_threadpool.submit(sub_worker, worker_index, j)
                        excep = True
                    else:
                        pr_fl("Future completed without exception...?")
                    # Restart sub worker
                    # exit_futs[j] = mp_threadpool.submit(sub_worker, worker_index, i)
                except Exception as e: # Call hasn't yet completed/errored
                    if isinstance(e, KeyboardInterrupt):
                        pr_fl("Keyboard interrupt...")
                        raise KeyboardInterrupt

            if excep:
                continue

            run_t_actual += time.time() - run_t_actual
            time.sleep(mp_log_interval)
            # run_t += mp_log_interval
            run_t = mp_log_interval * (run_t_actual // mp_log_interval)

            if is_root and worker_index == 0: # If we're the LorbRoot

                # Clean postgres database (delete expired entries)
                if run_t % pg_clean_interval == 0 and run_t > 0:
                    pgdb_threadpool.submit(clean_postgres, log_vars, None, pgdb_cond, db)

                # Also, check availability of op.gg servers, and modify sysconf pg table accordingly
                if run_t % op_check_interval == 0:
                    serve_regs = ['kr']
                    if serve_regions == 'rest':
                        serve_regs = [r for r in opgg_regions if r != 'kr']
                    for reg in serve_regs:
                        time.sleep(op_check_interval / len(serve_regs))
                        Thread(target=check_op, args=(log_vars, pgdb_threadpool, pgdb_cond, db, reg), daemon=True).start()

            # Output theadpool usage to postgres log
            pgdb_threadpool.submit(update_wlog, log_vars, None, pgdb_cond, db, worker_key, get_log_str())

            # Clear output logs
            if run_t % log_clean_interval == 0 and run_t > 0 and worker_index == 0:
                if os.path.exists(combined_log_path):
                    with open(combined_log_path, 'w') as f:
                        f.write('')
                        f.close()

            k += 1


except Exception as e:  # Print any errors in full
    for line in traceback.format_tb(e.__traceback__):
        pr_fl(line)
    pr_fl(type(e))
    pr_fl(e)

finally:
    db.dispose()  # Cleanup postgres connections


