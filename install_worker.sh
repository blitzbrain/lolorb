#
#  AWS Ubuntu 16.x Lorb.gg worker install script
#

#
#  Once this script has been run for an AWS instance, a reusable AMI and launch template can be created,
#  at which point this script is no longer required.
#
#  This script should be run from within the lolorb repo folder.
#  This script should work from start to finish but has not been tested.
#  Running each subsection separately is recommended. Remember to set the constants using ./Constants.sh
#  First command (copying over pem key file) runs locally, then we ssh into new server machine
#


. ./Constants.sh


# First argument = worker machine's public IP
WORKER_URL=$1


scp -i $PEM_FILE -o StrictHostKeyChecking=no $PEM_FILE ubuntu@$WORKER_URL:~/
scp -i $PEM_FILE -o StrictHostKeyChecking=no worker_config_default.json ubuntu@$WORKER_URL:/home/ubuntu/lolorb/worker_config.json

# NOTE: If installing for Korean server, manually set "regions": "kr" in worker_config.json
# NOTE: Manually set "is_root": false and "root_addr": "[the local IP of the root Lorb server host]" in worker_config.json

ssh -i $PEM_FILE -o StrictHostKeyChecking=no ubuntu@$HOST_URL -c "git clone https://$GIT_USER:$GIT_PASS@$REPO_HOST/$REPO_NAMESPACE/lolorb.git -b $REPO_BRANCH"
ssh -i $PEM_FILE -o StrictHostKeyChecking=no ubuntu@$HOST_URL

# Install dos2unix
sudo apt-get install dos2unix

# Configure git for lolorb
cd lolorb
git config credential.helper store
git config core.filemode true
git pull
dos2unix *.sh
cp Constants_default.sh Constants.sh
cp worker_config_default.json worker_config.json
cd ~

echo "Please open the file on the server at"
echo
echo "~/lolorb/Constants.sh"
echo 
echo "and set the \$GIT_USER and \$GIT_PASS credential variables."
echo "You may also wish to edit the server role configuration in worker_config.json"
echo "Press enter once this is complete..."
echo
read -p ""

. ./lolorb/Constants.sh


# Copy startup script
sudo cp lolorb/rc.local /etc/rc.local


# Set pem key permissions
chmod 600 $PEM_FILE

sudo apt-get update
# sudo apt-get upgrade # Not sure if we want to try this on AWS ubuntu


###
###  Download Lorb respositories
###
cd ~
git config --global user.name $GIT_USER
echo "https://$GIT_USER:$GIT_PASS@$REPO_HOST" | sudo tee -a .git-credentials >/dev/null
# git clone https://$GIT_USER:$GIT_PASS@$REPO_HOST/$REPO_NAMESPACE/lolorb.git -b $REPO_BRANCH
git clone https://$GIT_USER:$GIT_PASS@$REPO_HOST/$REPO_NAMESPACE/opggapi.git -b $REPO_BRANCH


###
### Install Node.js Version Manager
###
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
. ~/.nvm/nvm.sh
export NVM_DIR="/home/ubuntu/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"


###
### Install Python and required packages
###
sudo apt-get -y install python3 python3-pip
cd lolorb
pip3 install -r requirements.txt


# Configure git for opggapi
cd ~/opggapi
git config credential.helper store
git config core.filemode true
git pull


###
### Install Node.js and opggapi required packages
###
nvm install $NPM_VER
nvm alias default node
sudo ln -s "$(which node)" /usr/local/bin/node
npm install forever -g
npm install


###
### Create swapfile for python subprocess
###
sudo dd if=/dev/zero of=/swapfile bs=1024 count=1024k
sudo mkswap /swapfile
sudo swapon /swapfile
sudo echo "/swapfile	none	swap	sw	0	0" | sudo tee -a /etc/fstab >/dev/null


# setup folders to copy over data
cd ~/lolorb/
mkdir data
cd data
mkdir chgg_cache
mkdir opgg_cache
mkdir learning_data
mkdir infill_data
cd chgg_cache
mkdir champions
mkdir matchups


