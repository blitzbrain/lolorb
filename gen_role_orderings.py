#
#  Generate role champion orderings
#


from LearningData import *


# Get player roles for making role champion orderings
def get_game_roles(game):
    pis = game[g_pis]
    pts = dict([(pt["participantId"], pt) for pt in game["participants"]])
    pts = [pts[pi["participantId"]] for pi in pis]
    side_err = {"blue": False, "red": False}
    roles_taken = {"blue": {}, "red": {}}
    
    for pt_i in range(len(pts)):
        pt = pts[pt_i]
        side = "blue" if pt["teamId"] == blue_team_id else "red"
        if side_err[side]:
            continue
        
        r_taken = roles_taken[side]
        timeline = pt["timeline"]
        lane, role = timeline["lane"], timeline["role"]
        cid = pt["championId"]
        
        if lane == "TOP" and role == "SOLO" and "top" not in r_taken:
            r = "top"
        elif lane == "JUNGLE" and role == "NONE" and "jungle" not in r_taken:
            r = "jungle"
        elif lane == "MIDDLE" and role == "SOLO" and "middle" not in r_taken:
            r = "middle"
        elif lane == "BOTTOM" and role == "DUO_SUPPORT" and "support" not in r_taken:
            r = "support"
        elif lane == "BOTTOM" and role == "DUO_CARRY" and "adc" not in r_taken:
            r = "adc"
        else:
            other_side = "blue" if side == "red" else "red"
            if side_err[other_side]:
                return []
            side_err[side] = True
            continue
        
        r_taken[r] = cid
    res = []
    for side in cols_all:
        if not side_err[side]:
            res.append(roles_taken[side])
    return res


# Generate role champion orderings
def generate_orderings(n_games=5000): # Number of recent games to use to generate the orderings
    global role_champ_orderings
    team_roles = []
    games_seen = set()
    count = 0
    two_weeks_ago = time.time() - (60 * 60 * 24 * 7 * 2)
    flist = glob.glob(curr_data_jsons + '*')
    flist.sort(key=os.path.getmtime)
    flist = flist[::-1]
    # inds = list(range(len(flist)))
    # np.random.shuffle(inds)
    # for i in range(len(flist)):
    #     file = flist[inds[i]]

    for file in flist[:n_games]:

    #     ts = file.split('.json')[0].split('_')[-1]
    #     if int(ts) < two_weeks_ago:
    #         continue
        with open(file, 'r') as f:
            try:
                g = json.load(f)["game"]
            except:
                continue
    #         if g["gameCreation"] / 1000 < two_weeks_ago:
    # #             print("too old a game")
    #             break
            games_seen.add(g["platformId"] + '_' + str(g["gameId"]))
            team_roles += get_game_roles(g)
            count += 1
            sys_print("\r" + str(count) + "        ")

    final_res = {}
    for r in roles_all:
        final_res[r] = defaultdict(int)
    for tr in team_roles:
        for r in roles_all:
            final_res[r][tr[r]] += 1

    new_role_champ_orderings = {}
    for role in roles_all:
        ordering = list(list(zip(*sorted(final_res[role].items(), key=lambda x: x[1])[::-1]))[0])
        for cid in champion_names.keys():
            if cid not in ordering:
                ordering.append(cid)
        new_role_champ_orderings[role] = ordering

    # Save
    save_json(new_role_champ_orderings, "role_champion_orderings")
    role_champ_orderings = new_role_champ_orderings


if __name__ == "__main__":
    pr_fl("Generating role orderings...")
    generate_orderings()


