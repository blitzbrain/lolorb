# Create postgres tables, first argument is python executable

PYEX=$1

$PYEX create_players_table.py
$PYEX create_clients_table.py
$PYEX create_request_table.py
$PYEX create_wlogs_table.py
$PYEX create_clfails_table.py
$PYEX create_sysconf_table.py


