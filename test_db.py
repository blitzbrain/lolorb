#
#  Test connection to postgres
#


# from psycopg2 import connect
# from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from sqlalchemy import Column, Integer, Text, DateTime, Table
from sqlalchemy.dialects.postgresql import JSON, JSONB
import sqlalchemy

from ServerConstants import *


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()  
meta = sqlalchemy.MetaData(engine)

players = Table('players', meta, autoload=True, autoload_with=engine)
# print(players.c)
# ins = players.insert().values(id='EUW_77s7', timestamp=str(time.time()))
# print(engine.execute(ins))

# id_col = players.column('id')
# s = players.select(True)
# res = engine.execute("SELECT * FROM players")

# q = sqlalchemy.sql.expression.select([players.c.timestamp]).where(players.c['id'] == "EUW_77s7")
# res = engine.execute(q)
# print(q, list(res))

# ins = players.insert().values(id=region + '_' + name, ranked_summary=rs)
# res = engine.execute(ins)
# print(ins, res)

# ins = players.update().where(players.c.id == "EUW_888").values(ranked_summary={"key": "2"})
# res = engine.execute(ins)
# print(ins, res)

# print(type(players.c.keys()))



# result = engine.execute("SELECT 1")
# print(result.rowcount)

# Create table
# season_columns = [Column('season_' + str(i), JSON) for i in range(curr_season_id + 1)]
# sqlalchemy.Table("players", meta,  
#     Column('id', Text),
#     Column('summoner_id', Integer),
#     Column('timestamp', DateTime),
#     Column('ranked_summary', JSON),
#     *season_columns,
# )
# meta.create_all()


# con = connect(dbname=pg_name, user=pg_user, host=pg_host, password=pg_password)
# cur = con.cursor()

# cur.execute("CREATE DATABASE lorb2")
# cur.execute("CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar);")

# query = \
#     "SELECT EXISTS (" + \
#     "   SELECT 1 " + \
#     "   FROM   pg_tables" + \
#     "   WHERE  schemaname = 'schema_name'" + \
#     "   AND    tablename = 'table_name'" + \
#     "   );"

# query = \
#     " CREATE TABLE tags" + \
#     "      (" + \
#     "               region VARCHAR(4) NOT NULL," + \
#     "               name VARCHAR(16) NOT NULL," + \
#     "               summonerId INTEGER," + \
#     "               timestamp TIMESTAMP," + \
#     "               rankedSummary VARCHAR(20)," + \
#     "               PRIMARY KEY(question_id, tag_id)" + \
#     "      );"

# query = "CREATE TABLE word( word CHARACTER VARYING NOT NULL, word CHARACTER VARYING NOT NULL);"

# print(cur.execute(query))
