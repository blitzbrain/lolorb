'''

    add_games.py [ Riot, OP.GG, Champion.GG ] League of Legends Data Crawler

    Adds games to the database (curr_games.csv). Samples each league division
    equally, each game comes with at least min_opgg_recent_games previous 
    recent matches & info from each of the participants' OP.GG profiles. dThe
    entirety of the Champion.GG champion & matchup analytics database (~250MB)
    is also included (downloaded, updated automatically, cached & referenced).

'''

from Data import *


# Match history for a player min timestamp (for clean data only mode)
def get_min_birthday():
    return (time.time() - (2.0 * 60.0 * 60.0)) * 1000


# Main function to continuously add new games
# Find new players in division's current players' match histories, using
# their op.gg RankedSummarys to check they have played recently.
# Go through the new players and try to add their most recent game,
# if that game contains previously seen players, loop and use this
# player's match history as the source for new players, until we find
# a clean game in the not-highest sampled division
def add_games(r_conds, r_tss, region, being):

    # Check we have some initial data
    curr_games, curr_players = load_current_data(r_conds, region)
    if curr_games is None or curr_players is None or \
      len(curr_games) < 1 or len(curr_players) < 1:
        sys.exit("Error: No curr_games.csv data present - please run script "+\
                 "'get_games.py' until it adds a few games " +\
                 "for each league & division (~10 each), then run this script")

    # Script options

    rtag = '[' + region + '] '
    challenger_ts, challenger_count, div = [0] * 3
    seen_pl = set()
    found, on_cd, just_timed = True, False, False
    while True:

        # Find division with the least samples and try to get a game for it
        curr_games, curr_players = load_current_data(r_conds, region)
        div_c = np.bincount(curr_games['division'])
        max_div_sampled = len(div_c)
        if max_div_sampled < N_divs:
            div_c = np.append(div_c,
                np.zeros(N_divs - max_div_sampled, dtype=np.int))
        # pr_fl(rtag + str(div_c))
        pr_fl(rtag + '[' + str(' '.join([str(x) for x in div_c])) + ']')

        # Create index of which divisions not to add a game for
        # games_added = (div_c >= (np.mean(div_c) + ( 1.0 * np.std(div_c)
        #               ))).astype(np.int)
        games_added = (div_c == max(div_c)).astype(np.int)
        if sum(games_added) > 1:
            games_added = np.zeros(games_added.shape)

        div_c_ = div_c[:]
        ts = time.time()
        if ts - challenger_ts < challenger_cd:
            for div in rare_divs:
                div_c_[div] = max(div_c_)
            if not on_cd:
                on_cd = True
                just_timed = True
        elif on_cd:
            on_cd = False

        if found or just_timed:
            just_timed = False
            div = np.argmin(div_c_)

            # Find a player with this division (choose randomly)
            p_in_div = curr_players[curr_players["division"] == div].index
            p_in_div_ab = curr_players[curr_players["division"] == div+1].index
            p_in_div_be = curr_players[curr_players["division"] == div-1].index
            # acc_id=p_in_div[np.random.randint(p_in_div.shape[0])]
            in_d,ab_d,be_d=list(p_in_div),list(p_in_div_ab),list(p_in_div_be)
            np.random.shuffle(in_d)
            np.random.shuffle(ab_d)
            np.random.shuffle(be_d)
            players_list = in_d + ab_d + be_d
            players = set(players_list)
        else:
            players_list = list(players)
            np.random.shuffle(players_list)
        pr_fl(rtag + "Adding game for division " + str(div) + '...')

        # Add game for this division starting from players in the division
        # Get match history for a player
        mh, a_id = None, None
        for i in range(len(players_list)):
            a_id = players_list[i]
            acc_id = ''.join(a_id.split('_')[1:])
            mh = ranked_matchlist(r_conds, r_tss, region, acc_id)
            if mh is None:
                # pr_fl(rtag + "Failed to get match history for " + str(acc_id))
                pass
            else:
                break

        if mh is None:
            continue

        new_players = []
        found = False
        for i in range(min(len(mh["matches"]), n_mh_games)):
            if mh["matches"][i]["timestamp"] < one_week_ago():
                continue
            game_id = mh["matches"][i]["gameId"]
            game = rito_req(r_conds, r_tss, region, game_by_game_id+str(game_id))
            if game is None:
                continue

            queue = queue_names[game["queueId"]]

            for j in range(len(game[g_pis])):
                if "player" not in game[g_pis][j]:
                    pr_fl(rtag+"|no 'player' dict: "+str(j)+", "+str(game_id))
                    continue
                j_p = game[g_pis][j]["player"]
                j_acc_id = j_p[g_acc_id]
                j_a_id = region + '_' + str(j_acc_id)

                if g_summ_id not in j_p:
                    pr_fl(rtag+"*|732 summonerId not in game for "+j_name)
                    continue

                if j_a_id in players or j_a_id in seen_pl:
                    # pr_fl(rtag + "|770 seen player " + str(j_acc_id))
                    continue

                seen_pl |= set([ j_a_id ])
                new_players += [ j_a_id ]

                j_name, j_summ_id = j_p[g_name], j_p[g_summ_id]
                # if j_acc_id in get_no_recent_games():
                #     pr_fl(rtag+"|202 Less than "+str(min_opgg_recent_games)+\
                #            " recent games for " + j_name)
                #     continue
                j_mh = ranked_matchlist(r_conds, r_tss, region, j_acc_id)
                if j_mh is None:
                    pr_fl(rtag + "*|208 Match history is None for " + j_name)
                    continue
                if len(j_mh["matches"]) < 1:
                    pr_fl(rtag + "|210 Match history empty for " + j_name)
                    continue

                game_1_id = j_mh["matches"][0]["gameId"]
                game_2_id = j_mh["matches"][1]["gameId"] if \
                    len(j_mh["matches"]) > 1 else None
                if game_1_id in curr_games.index or \
                        game_2_id in curr_games.index:
                    pr_fl(rtag + "|501 Game already added")
                    continue
                if region + '_' + str(game_1_id) in being:
                    continue
                
                if j_mh["matches"][0]["timestamp"] < one_hour_ago():
                    # pr_fl(rtag + "*|209 MH most recent too old for "+j_name)
                    if div in rare_divs:
                        if challenger_count >= challenger_old_count:
                            challenger_count = 0
                            challenger_ts = time.time()
                        else:
                            challenger_count += 1
                        # found = True
                        # break
                    continue

                # Get league positions (and wins/losses) for player
                # lps = get_league_pos(r_conds, r_tss, region, j_summ_id)
                # j_div = None
                # for lp in lps:
                #     if lp["queueType"] == queue:
                #         j_div = get_division_index(lp["tier"]+' '+lp["rank"])

                # if new_players_only:
                #     # If rare division allow duplicate players
                #     if j_div not in rare_divs and \
                #           j_acc_id in curr_players.index:
                #         pr_fl(rtag + "|Previously observed player " + j_name)
                #         continue

                # If looking for rare_div and found a master player, consider
                # it a failure and start over (only later in season)
                # if div in rare_divs and j_div==league_ns["master"]:
                #     if challenger_count >= challenger_old_count:
                #         challenger_count = 0
                #         challenger_ts = time.time()
                #     else:
                #         challenger_count += 1
                #     found = True
                #     break

                # if j_div in np.nonzero(games_added)[0]:
                #     pr_fl(rtag + "|207 seen player for division [" + \
                #           str(j_div) + "]: " + j_name)
                #     continue

                # opgg_renew_summoner(j_summ_id)
                # rs = opgg_ranked_summary(j_name)

                # Old "clean-data-only" code
                # if not rs:
                #     pr_fl(rtag + "|404 RankedSummary not found for " +j_name)
                #     continue
                # if "league" not in rs:
                #     pr_fl(rtag + "|401 League not in RankedSummary "+j_name)
                #     continue
                # if rs["league"].lower() == "unranked":
                #     pr_fl(rtag + "|203 Unranked player " + j_name)
                #     continue
                # if len(rs["games"]) < min_opgg_recent_games:
                #     pr_fl(rtag+"|204 Less than "+str(min_opgg_recent_games)+\
                #            " recent games for " + j_name)
                #     continue
                # if rs["games"][0]["timestamp"] * 1000.0 < get_min_birthday():
                #     pr_fl(rtag+"*|206 op.gg most recent too old for "+j_name)
                #     continue

                res = add_game(r_conds, r_tss, game_1_id, games_added, 1,
                               region, curr_games, curr_players, being)
                if type(res) == tuple:
                    res, curr_games, curr_players = res
                pr_fl(rtag + res)
                if res[:len(succ_msg)] == succ_msg:
                    g_div = int(res.split('[')[-1].split(']')[0])
                    if g_div in rare_divs:
                        challenger_count = 0
                        challenger_ts = 0
                    found = True
                    if g_div != league_ns["challenger"]:
                        break
                if game_2_id is not None and region + '_' + str(game_2_id) not in being and \
                  j_mh["matches"][1]["timestamp"] >= one_hour_ago():
                    res = add_game(r_conds, r_tss, game_2_id, games_added, 1,
                                   region, curr_games, curr_players, being)

                    if type(res) == tuple:
                        res, curr_games, curr_players = res
                    pr_fl(rtag + res)

                if res[:len(succ_msg)] == succ_msg:
                    if int(res.split('[')[-1].split(']')[0]) in rare_divs:
                        challenger_count = 0
                        challenger_ts = 0
                    found = True
                    break
                div_str = res.split('[')[-1].split(']')[0]
                if div in rare_divs and \
                        res[:len(seen_code)] == seen_code and \
                        div_str == str(league_ns["master"]):
                    if challenger_count >= challenger_old_count:
                        challenger_count = 0
                        challenger_ts = time.time()
                    else:
                        challenger_count += 1
                    # found = True
                    # break

            if found or len(new_players) >= n_new_pl:
                break
        new_players = set(new_players)
        players |= new_players


# Add games until interrupted!
if __name__ == "__main__":
    add_games(region, make_r_conds(), make_timestamps())


