#
#  Upload global stats database to worker servers (from a root host server)
#


import os

# print(os.getcwd())
os.chdir("lolorb/")
# print(os.getcwd())


from Monitoring import *


n_pats = 1


worker_ips = []
if len(sys.argv) >= 2:  # If uploading to a particular machine IP (running on host)
    worker_ips = [ sys.argv[1] ]
    if len(sys.argv) >= 3:
        n_pats = int(sys.argv[2])
else:
    # git pull & restart workers on host
    res = subprocess.run("./refresh_worker.sh", stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')


    # Create new AMI for this wizard king (?)


    # Get all worker server ips
    res = subprocess.run("sudo rabbitmqctl list_connections > /home/ubuntu/lorb/logs/connections.log", stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    res = subprocess.run(worker_pyex + " unique_connections.py", stdout=subprocess.PIPE, shell=True)
    worker_ips = json.loads(res.stdout.decode('utf-8'))


print(worker_ips)

if len(worker_ips) == 0:
    sys.exit()

# For each worker server
os.chdir("../")
for ip in worker_ips[::-1]:

    # Transfer models & infill data
    res = subprocess.run("./lolorb/transfer_data.sh " + ip + ' ' + host_pem, stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')

    # Transfer global stats database
    res = subprocess.run(worker_pyex + " ./lolorb/transfer_data.py " + ip + ' ' + host_pem + ' ' + str(n_pats), stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')

    # Run worker refresh script to restart worker processes
    kwargs = {
        "close_fds": True,
        "shell": True,
    }
    if os.name == 'nt':
        kwargs["creationflags"] = DETACHED_PROCESS
    subprocess.Popen("ssh -i " + host_pem + " -o StrictHostKeyChecking=no ubuntu@" + ip + " ./lolorb/refresh_worker.sh", **kwargs)


# Create new AMI for this minion (?)
ip = worker_ips[0]

# Create new launch template (?)


