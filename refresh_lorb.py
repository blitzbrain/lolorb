#
#  Refresh front end (git pull and copy to /www/public)
#


import os

# os.chdir("lolorb/")


from Monitoring import *


os.chdir("../")
for host in hosts:

    # Refresh the front end site after having refreshed the host worker processes so that they can handle new requests (ie, for new champion)
    res = subprocess.run("ssh -i " + host["pem"] + " -o StrictHostKeyChecking=no ubuntu@" + host["addr"] + \
        " ./lolorb/update_lorb.sh",
        stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    pr_fl(res)


