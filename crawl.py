#
#
#  Downloads as many game jsons as possible while simultaneously downloading
#  as many full data jsons as it can (multithreading with shared riot
#  request timestamp condition)
#
#

from Utils import *
from get_matches import get_matches
from get_games import get_games
from add_games import add_games


# Solution for outputting & piping unicode characters on windows
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())


### Global variables ###
r_conds = None # Request conditions (need acquiring before a request)
r_tss = make_r_timestamps() # Rito/Chgg request timestamps
being = {}


# Function wrappers for passing conditions (data access permission variables)
def r_get_ms(conds, reg):
    global r_tss
    get_matches(conds, r_tss, reg)
def r_add_gs(conds, reg, gf):
    global r_tss, being
    g_func = get_games if gf else add_games
    g_func(conds, r_tss, reg, being)

# Start the crawler for all regions
if __name__ == "__main__":

    # Create folders to contain gathered data
    create_folder(data_dir)
    create_folder(data_dir + init_games_dir)
    create_folder(data_dir + matchlists_dir)
    create_folder(curr_data_jsons)
    create_folder(curr_games_jsons)
    for direc in dirs_to_create:
        create_folder(data_dir + direc)
    for c in r_cache:
        create_folder(data_dir + r_cache[c]["path"])

    r_conds = make_r_conds()

    even_out = dict([(reg, 0) for reg in even_out_regions])
    seed_regs = dict([(reg, 0) for reg in rito_regions])

    for reg in crawl_regions + even_out_regions:

        # Space out initialisation to smooth network spikes
        time.sleep(10)

        # Define and start get_matches thread
        if also_get_seed_games or only_get_seed_games:
            if seed_regs[reg] == 0:
                get_matches_t=Thread(target=r_get_ms,args=(r_conds,reg),daemon=True)
                get_matches_t.start()
                seed_regs[reg] += 1
            if only_get_seed_games:
                continue

        # Define and start add_games thread
        gf = use_get_games
        if reg in even_out and even_out[reg] == 0:
            gf = False
        add_games_t=Thread(target=r_add_gs,args=(r_conds,reg,gf),daemon=True)
        add_games_t.start()

        if reg in even_out:
            even_out[reg] += 1

    while(True):
        time.sleep(5)


