#
#  Stop monitor
#


from Monitoring import *


start_command = pyex + " monitor.py 37420"

# Kill existing logger process if running
stopped = False
for process in psutil.process_iter():
    cmd = None
    try:
        cmd = process.cmdline()
    except Exception as e:
        # print(e) # Access denied errors
        continue

    if type(cmd) is list and len(cmd) > 0 and isinstance(cmd[0], str):
        joined_cmd = ' '.join(cmd)
        if joined_cmd == start_command:
            # print(cmd)
            process_is_running = True
            pid = process.pid
            # print(cmd, pid, type(pid))
            os.kill(pid, signal.SIGTERM)
            pr_fl("Monitor stopped.")
            stopped = True
            break

if not stopped:
    pr_fl("Monitor process not found.")


