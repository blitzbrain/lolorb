#
#  Updates Lorb front end site
#


. ~/lolorb/Constants.sh


cd /home/ubuntu/lorb
git pull
/home/ubuntu/.nvm/versions/node/v$NPM_VER/bin/forever restart server.js
sudo rm -rdf /home/ubuntu/staging
sudo cp -rd /home/ubuntu/lorb/site/public /home/ubuntu/staging

cd /home/ubuntu/lolorb
git pull
$PYEXEC kr_modify.py
sudo rm -rdf /www/public
sudo cp -rd /home/ubuntu/staging /www/public


