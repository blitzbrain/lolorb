#
#  Learning constants
#


from Data import *


# Hyperparameters

# load_new_players_only = False # If true, ignore games where we've seen a player before

### no longer used; data-based method used instead
# If true, when there are duplicate rito role labels, try to guess "correct" ones (meta)
# i.e., two mids -> one randomly assigned top. need to make smarter algo for this
# If false, simply ignore games with duplicate roles (gives better accuracy model)
# guess_roles = False 

n_opgg_pcs_games = 100          # Min recent games used for a player's champions stats
n_opgg_pcs_avg_games = 1000     # Min games for avg player stats over all champions
n_opgg_gcs_games = 40           # Min recent games used for global stats for a champion
n_chgg_gms_cmn = 10             # Number of rarest common matchups to get avg for when 0 data (infill)
avg_lp_incr = 19                # Average amount of LP gained/lossed per match

n_gsdb_load = 120               # How many days of past op.gg global stats data to use
n_gsdb_patches = 18             # How many past patches of champion.gg global stats data to use
n_main_train = 60000            # How many initial recent training samples to extract from the raw data (target)
n_main_training = 45000         # How many of the extracted samples to train the production model with
n_main_patches = 16             # How many recent patches to use in model training

# Handcrafted momentum hyperparameters
mtm_C_1 = 0.5
mtm_C_2 = 3.0
# mtm_C_1 = 10**1.2
# mtm_C_2 = -2.0
mtm_C_3 = 10**0.2
mtm_C_4 = 0
avg_game_duration = 32.0 * 60
# mtm_rec_ns = [1, 2, 3]
mtm_rec_ns = [1, 2, 4, 8, 20]
# mtm_rec_ns = [20]

# Pro model hyperparameters
earliest_patch = '10.1'         # Earliest patch for dataset inclusion
pro_retrospect_months = 6.0     # Number of recent months
n_pro_patches = 10              # Number of recent patches
n_max_missing = 2               # Max. number of missing solo queue names for final dataset inclusion
pro_missing_max = 7             # Max. number of players (solo queue ids) missing when crawling pro samples
pro_sqo_missing = 6             # Max. number of players missing to use with nsqo instead of sqo model
avg_pro_duration = 1989.0       # Average game length used for series in-game time estimation
svm_C = 0.5                     # SVM
svm_tol = 1e-3
pca_frac = 0.5                  # PCA components as a fraction of the input dimensionality
pca_frac_sqo = 0.5              # For the solo-queue-data-only model
pca_n_max = 25                  # Max. number of PCA components (using more may cause overfitting & no improvement)

run_ps = {
    # (1, 0): (0.05951284395788444 * 10, 0.5 * 3, 0.007000000000000001 * 10),
    # (1, 1): (0.007000000000000001 * 10, 0.5 * 3, 0.007000000000000001 * 10),
    # (1, 2): (0.0567980386704668 * 10, 0.5 * 3, 0.007000000000000001 * 10),
    # (1, 3): (0.15 * 10, 0.20000000000000004 * 3, 0.007000000000000001 * 10),
    # (1, 4): (0.10547214420113397 * 10, 0.5 * 3, 0.007000000000000001 * 10),
    (2, 0): (0.07312838249906625 * 10, 0.20000000000000004 * 3, 0.007000000000000001 * 10),
    (2, 1): (0.007000000000000001 * 10, 0.5 * 3, 0.007000000000000001 * 10),
    (2, 2): (0.08770650058086565 * 10, 0.39220143150999065 * 3, 0.007000000000000001 * 10),
    (2, 3): (0.15 * 10, 0.5 * 3, 0.007000000000000001 * 10),
    (2, 4): (0.11516367038826665 * 10, 0.40460592500867626 * 3, 0.007000000000000001 * 10),
    # (2, 4): (0.01278 * 10 ,  0.3234  * 3 ,  0.01031 * 10),
    (3, 0): (0.04813636164248709 * 10,  0.3362872662629863 * 3 , 0.007000000000000001 * 10),
    (3, 1): (0.5096   ,  0.7147   ,  0.04555),
    (3, 2): (0.6274   ,  0.6109   ,  0.04156),
    # (3, 3): (0.01     ,  1.5      ,  0.01),
    # (3, 4): (0.8393   ,  1.051    ,  0.02617),
    # (4, 0): (1.311    ,  1.5      ,  0.04049),
    # (4, 1): (0.1502   ,  1.294    ,  0.01),
    # (4, 2): (0.4048   ,  1.5      ,  0.2232),
    # (4, 3): (0.4482   ,  0.7475   ,  0.08361),
    # # (4, 3): (0.8062   ,  0.07437  ,  1.443),
    # # (4, 4): (1.196     ,  0.6094   ,  0.09103),
    # (4, 4): (0.11465920112854736 * 10, 0.20000000000000004 * 3, 0.007000000000000001 * 10),
}

# Patch timestamps
patch_tss = OrderedDict([
    ('10.4', 1582002000),
    ('10.3', 1580792400),
    ('10.2', 1579582800),
    ('10.1', 1578373200),
    ('9.24', 1576213200),
    ('9.23', 1574226000),
    ('9.22', 1573102800),
    ('9.20', 1570683600),
    ('9.19', 1569564000),
    ('9.18', 1568203200),
    ('9.17', 1566907200),
    ('9.16', 1565870400),
    ('9.15', 1564660800),
    ('9.14', 1563451200),
    ('9.13', 1561723200),
    ('9.12', 1560427200),
    ('9.11', 1559217600),
    ('9.10', 1557748800),
    ('9.8', 1555459200),
    ('9.7', 1555052148),
    ('9.6', 1554160948),
    ('9.5', 1553104996),
    ('9.4', 1552024294),
    ('9.3', 1549324800),
    ('9.2', 1548892800),
    ('9.1', 1546905600),
])

# Pro matches constants
pro_role_ord = [
    "blue_top",
    "blue_jungle",
    "blue_middle",
    "blue_adc",
    "blue_support",
    "red_top",
    "red_jungle",
    "red_middle",
    "red_adc",
    "red_support",
]
pro_metalabels_ = [
    "gameid",
    "league",
    "split",
    "week",
    "game",
    "fbtime",
    "fttime",
    "fdtime",
    "heraldtime",
    "fbarontime",
]
pro_metafeatures = [
    "n_sqnames_missing",
    "series_count",
    "series_ingame_time",
    "series_blue_wins",
    "series_blue_winstreak",
    "series_red_wins",
    "series_red_winstreak",
]
pro_metalabels = [
    "timestamp",
    "time_of_day",
    "patch",
    "duration",
    "blue_win",
] + pro_metafeatures
# pro_leagues = ['LEC', 'LCK', 'CBLoL', 'LMS', 'LCS'] # 'LPL', 'MSI'
# pro_leagues = ['LEC', 'LCK', 'LCS', 'MSI'] # 'LPL', 'MSI'
pro_leagues = ['LEC', 'LCK', 'NAAc', 'LPL', 'LCS', 'MSI'] # 'LPL', 'MSI'
# pro_leagues = ['LEC', 'LCK', 'NAAc', 'LCS', 'MSI'] # 'LPL', 'MSI'
# pro_leagues = ['LEC']#, 'LCK', 'CBLoL', 'LMS', 'LCS', 'MSI'] # 'LPL', 'MSI'
league_namedict = { # Assign names to leagues if they are different from the id
    "Worlds": "MSI"
}
for l in pro_leagues:
    league_namedict[l] = l
pro_player_labels_ = [
    'player',
    'k', 'd', 'a',
    "totalgold", "goldspent",
    "minionkills", "monsterkills", "monsterkillsownjungle", "monsterkillsenemyjungle",
    "dmgtochamps",
    "wards", "wardkills", "visionwards", "visionwardbuys",
    "fb", "fbassist", "fbvictim",
    "doubles", "triples", "quadras", "pentas",
    "csat10", "goldat10", "xpat10", 
]
X_pro_player_labels = [
    'k', 'd', 'a',
    "totalgold",# "goldspent",
    "minionkills", "monsterkills", "monsterkillsownjungle", "monsterkillsenemyjungle",
    "dmgtochamps",
    "wards", "wardkills", "visionwards", "visionwardbuys",
    "fbassist",
    # "fb", "fbassist", "fbvictim",
    "doubles", "triples",# "quadras", "pentas",
    "csat10", "goldat10", "xpat10",
    #"fd",
    # 'firedrakes', 'waterdrakes', 'earthdrakes', 'airdrakes', 'elders', #
]
pro_correspondence_ratios = OrderedDict([ # ratios of pro stats to solo/duo queue stats
    ("k", "opgg_champion_recent_kills"),
    ("d", "opgg_champion_recent_assists"),
    ("a", "opgg_champion_recent_deaths"),
    # ("totalgold", "opgg_champion_recent_gold"),
    # ("goldat10", "opgg_champion_recent_gold"),
    ("minionkills", "opgg_champion_recent_creep_score"),
    ("monsterkills", "opgg_champion_recent_creep_score"),
    ("csat10", "opgg_champion_recent_creep_score"),
    ("dmgtochamps", "opgg_champion_avg_damage_dealt"),
])
pro_corr_labels = [k + '_/_' + v for k, v in pro_correspondence_ratios.items()]
pro_gms_ratios = OrderedDict([
    ("k", "kills"),
    ("d", "deaths"),
    ("a", "assists"),
    ("totalgold", "goldEarned"),
    ("goldat10", "goldEarned"),
    ("minionkills", "minionsKilled"),
    ("monsterkills", "minionsKilled"),
    ("csat10", "minionsKilled"),
    ("dmgtochamps", "totalDamageDealtToChampions"),
    ("monsterkillsownjungle", "neutralMinionsKilledTeamJungle"),
])
pro_gmsnormd_labels = [k + '_/_' + v for k, v in pro_gms_ratios.items()]
pro_player_labels = [
    'sq_name', 'sq_region', 'champion'
]
pro_team_labels = [
    "team",
    "ban1", "ban2", "ban3", "ban4", "ban5",
    "ft", "firstmidouter", "firsttothreetowers", "teamtowerkills",
    "teamdragkills",
    "herald",
    "fbaron", "teambaronkills",
]
pro_team_player_labels = [ # Use these (add to player stats, so we don't have to encode teams)
    "ft", "teamtowerkills", "firsttothreetowers",
    "teamdragkills",
    "herald",
    "fbaron",
    "teambaronkills",
]
pro_full_player_labels = X_pro_player_labels + pro_corr_labels + pro_gmsnormd_labels + pro_team_player_labels

# Constants

champions_all = get_champions()
champ_dict = dict([(v["name"], v['id']) for v in champions_all.values()])
ordered_cids = sorted([int(c) for c in get_cids()])
champion_names = invert_dict(champ_dict)
n_champs = len(ordered_cids)
n_patches = len(patches_all)
n_regions = len(rito_regions)
n_seasons = len(seasons_all)
n_queues = len(queues)
new_cids = OrderedDict([(cid,ordered_cids.index(cid)) for cid in ordered_cids])
champ_codes = OrderedDict(zip(ordered_cids, np.identity(n_champs)))
spell_list = list(spells_lib.keys())
n_spells = len(spell_list)
spell_order = dict(zip(spell_list, [spell_list.index(s) for s in spell_list]))
spell_codes = OrderedDict(zip(spell_list, np.identity(n_spells)))
season_codes = OrderedDict(zip(seasons_all, np.identity(n_seasons)))
patch_codes = OrderedDict(zip(patches_all, np.identity(n_patches)))
region_codes = OrderedDict(zip(rito_regions, np.identity(n_regions)))
queue_codes = OrderedDict(zip(r_queue_ids, np.identity(n_queues)))
team_side_codes = OrderedDict(zip(range(len(cols_all)), np.identity(N_T)))
t_role_codes = OrderedDict(zip(t_roles, np.identity(N_PL)))
pick_order_codes = OrderedDict(zip(pick_order_map.values(), np.identity(N_PL)))
role_is = dict(zip(t_roles, [t_roles.index(t) for t in t_roles]))
supp_cids = set([champ_dict[name] for name in support_champs])
region_is = dict(zip(rito_regions,[rito_regions.index(t) for t in rito_regions]))
region_names = invert_dict(region_is)
col_is = {cols_all[i]: i for i in N_T_r}
roles_all_is = {roles_all[i]: i for i in range(len(roles_all))}

pro_cd = {k.lower(): champ_dict[k] for k in champ_dict}
pro_cd["nunu"] = pro_cd["nunu & willump"]

# Role champion frequency orderings constants
role_fill_order = ['jungle', 'adc', 'top', 'middle', 'support'] # Try swapping around last 3
role_champ_orderings = load_json("role_champion_orderings")

platinum_i = leag_tier_is["platinum"]
opgg_tots_keys = ['avg', 'seasonavg', 'all']
opgg_rs_non_number_keys = \
    set(["recent", "games", "summonerId", "league", "leagueName"])

# Info for filling missing data with averages for all data in one pass
empty_ch = {
    "wins": 0,
    "losses": 0,
    "double_kills": 0,
    "triple_kills": 0,
    "quadra_kills": 0,
}
filler_keys = OrderedDict([
    ('role', roles_all),
    ('div', list(range(N_divs))),
    ('cid', ordered_cids),
])

# Dummy coded categorical variable keys for organising labels
coded_x_keys = OrderedDict([
    ('patch', len(patch_codes.values())),
    ('season', len(season_codes.values())),
    ('region', len(region_codes.values())),
    ('queue', len(queue_codes.values())),
])
coded_x_c_prev_keys = OrderedDict([
    ('queue', len(queue_codes.values())),
])
X_r_player_champion_labels = sum([[role + '_' + l for role in t_roles] for l in [
    "champion",
]], [])
X_r_player_pick_position_labels = sum([[role + '_' + l for role in t_roles] for l in [
    "pick_position",
]], [])
X_r_player_spell_labels = sum([[role + '_' + l for role in t_roles] for l in [
    "spell_1", 
    "spell_2",
]], [])
for k in X_r_player_champion_labels:
    coded_x_keys[k] = len(champ_codes.values())
    coded_x_c_prev_keys[k] = len(champ_codes.values())
for k in X_r_player_spell_labels:
    coded_x_keys[k] = len(spell_codes.values())
    coded_x_c_prev_keys[k] = len(spell_codes.values())

# Champion categories (todo: optimise for win prediction accuracy?)
datadragon_champs = load_json(ddrag_dir + "champion", encoding='utf-8-sig')["data"]
champ_cats = defaultdict(set)
for c_ident in datadragon_champs:
    for tag in datadragon_champs[c_ident]['tags']:
        champ_cats[tag].add(champ_dict[datadragon_champs[c_ident]['name']])
cat_tags = list(champ_cats.keys())

def code_champ_as_cat(cid):
    return np.asarray([int(cid) in champ_cats[tag] for tag in cat_tags]).astype(int)


# Labels

op_ch_pre = "opgg_eloavg_champion_"
ch_ch_pre = "chgg_eloavg_champion_"
ch_m_pre = "chgg_eloavg_matchup_"
ml_l = ch_ch_pre + "duration_"
mp_l = ch_ch_pre + "games_"

ml_dict = {  # Champion.gg champion stats by match length ranges
    "0_to_15": "zeroToFifteen",
    "15_to_20": "fifteenToTwenty",
    "20_to_25": "twentyToTwentyFive",
    "25_to_30": "twentyFiveToThirty",
    "30_to_35": "thirtyToThirtyFive",
    "35_to_40": "thirtyFiveToForty",
    "40_plus": "fortyPlus",
}
mp_dict = {  # Champion.gg champion stats by summoner matches played ranges
    "1_to_50": "oneToFifty",
    "51_to_100": "fiftyOneToHundred",
    "101_to_150": "hundredOneToHundredFifty",
    "151_to_200": "hundredFiftyOneToTwoHundred",
    "201_plus": "twoHundredOnePlus",
}

r_meta_labels_all = [ # Rito game json metadata labels
    "game_id",
    "region_id",
    "queue_id",
    "map_id",
    "season_id",
    "game_version",
    "timestamp",
    "time_of_day",
    "duration",
    "mean_hAST", # Mean highest achieved season tier of ranked players in game
    "acc_ids",
    "names",
    "roles",     # Roles of players in original order
]
r_meta_labels_all_len = len(r_meta_labels_all)

c_meta_labels_all = [ # Full data point (Riot+OP.GG+Champion.GG) meta_labels
    "division",
    "tier",
    "rank",
    "league_points",
    "elo",
    "season_wins",   # Average season wins over all players in game
    "season_losses", # Average season losses for over players in game
]

meta_labels_all = r_meta_labels_all + c_meta_labels_all

X_meta_labels_r = [   # Metadata to include in X
    "timestamp",
    "time_of_day",
    "duration",
    "mean_hAST",
]
X_meta_labels = X_meta_labels_r + c_meta_labels_all

X_game_labels_all = [ # X data for whole game that isn't metadata
    "patch",     # Dummy coded version of game_version
    "season",    # Dummy coded version of season_id
    "region",    # Dummy coded version of region_id
    "queue",     # Dummy coded version of queue_id
]

opgg_rs_labels = [      # op.gg player ranked summary
    "games",
    "wins",
    "losses",
    "win_rate",
    "tier",
    "rank",
    "division",
    "league_points",
    "recent_games",
    "recent_wins",
    "recent_losses",
    "recent_win_rate",
    "recent_kda_ratio",
    "recent_kills",
    "recent_deaths",
    "recent_assists",
    "recent_kill_participation",
]

opgg_pcs_labels = [     # Player champion stats
    "wins",
    "losses",
    "total_wins",
    "total_losses",
    "alltotal_wins",
    "alltotal_losses",
    "rank",
    "mean_rank",
    "mean_max_rank",
    "max_rank",
    "kills",
    "deaths",
    "assists",
    "gold",
    "creep_score",
    "damage_dealt",
    "damage_taken",
    "double_kills",
    "triple_kills",
    "quadra_kills",
    "max_kills",
    "max_deaths",
    # "turrets",
    "first_season",
    "last_season",
]
opgg_pcs_all_champ_category_labels = \
    [k + "_wins" for k in champ_cats] + \
    [k + "_losses" for k in champ_cats]
opgg_pcs_all_champ_labels = opgg_pcs_all_champ_category_labels #+ sum([[
#     c_name + "_wins",
#     c_name + "_losses",
#     # c_name + "_kda_ratio", # just wins & losses for now
#     # c_name + "_gold",
# ] for c_name in champ_dict], [])
opgg_pcs_season_labels = [l for l in opgg_pcs_labels if l not in
    ["mean_rank", "mean_max_rank", "max_rank", "first_season", "last_season",
     "total_wins", "total_losses", "alltotal_wins", "alltotal_losses"]]
opgg_pcs_seasonavg_labels = [l for l in opgg_pcs_labels if l not in
    ["first_season", "last_season",
     "mean_max_rank", "alltotal_wins", "alltotal_losses"]]
opgg_pcs_avg_labels = [l for l in opgg_pcs_labels if l not in
    ["first_season", "last_season"]]
opgg_pcs_player_labels = \
    ["opgg_champion_recent_" + l for l in opgg_pcs_labels] + \
    ["opgg_champion_season_" + l for l in opgg_pcs_season_labels]
opgg_pcs_labels_full = opgg_pcs_player_labels + \
    ["opgg_champion_all_" + l for l in opgg_pcs_all_champ_labels] + \
    ["opgg_champion_avg_" + l for l in opgg_pcs_avg_labels] + \
    ["opgg_champion_seasonavg_" + l for l in opgg_pcs_seasonavg_labels]

opgg_gcs_labels = [    # Global champion stats
    "games",
    "games_fraction",
    "win_rate",
    "kda_ratio",
    "gold",
    "creep_score",
]

opgg_gcs_labels_full = sum([[op_ch_pre + period + '_' + l for l in
    opgg_gcs_labels] for period in opgg_periods], [])

chgg_gcs_labels = [    # Champion.gg global champion stats
    "games",
    "win_rate",
    "play_rate",
    "percent_role_played",
    "kills",
    "deaths",
    "assists",
    "kda_ratio",
    "kill_sprees",
    "largest_kill_spree",
    "creep_score",
    "gold",
    "total_damage_taken",
    "total_heal",
    "wards_placed",
    "wards_killed",
    "total_damage",
    "total_magic_damage",
    "total_physical_damage",
    "total_true_damage",
    "percent_magic_damage",
    "percent_physical_damage",
    "percent_true_damage",
    "jungle_creep_score_team",
    "jungle_creep_score_enemy",
]

chgg_gms_labels = [     # Global matchup stats
    "games",
    "wins",
    "win_rate",
    "kills",
    "deaths",
    "assists",
    "gold",
    "creep_score",
    "total_damage_dealt_to_champions",
    "kill_sprees",
    "jungle_creep_score_team",
    "weighed_score",
]

chgg_gms_labels_full = [ch_m_pre + l for l in chgg_gms_labels]
chgg_gcs_labels_full = [ch_ch_pre + l for l in chgg_gcs_labels] + \
    [ml_l + l + "_games" for l in ml_dict] + \
    [ml_l + l + "_games_fraction" for l in ml_dict] + \
    [ml_l + l + "_win_rate" for l in ml_dict]# + \
    # [mp_l + l + "_games" for l in mp_dict] + \
    # [mp_l + l + "_games_fraction" for l in mp_dict] + \
    # [mp_l + l + "_players" for l in mp_dict] + \
    # [mp_l + l + "_players_fraction" for l in mp_dict] + \
    # [mp_l + l + "_wins" for l in mp_dict] + \
    # [mp_l + l + "_win_rate" for l in mp_dict]

opgg_rs_labels_full = ["opgg_rs_" + l for l in opgg_rs_labels]


X_c_player_labels_ranked = [
    "season_games",
    "season_wins",
    "season_losses",
    "season_win_rate",
    "tier",
    "int_tier",
    "rank",
    "league_points",
    "division",
    "elo",
    "elodeviation",
    "first_ranked_season",
    "last_ranked_season",
    "n_recent_matches",
]
X_c_player_labels_ranked_final = X_c_player_labels_ranked + [
    "season_bayes_win_rate"    
]
X_c_player_labels_ranked_final_full = \
    sum([sum([[t + '_' + r + '_' + l for r in roles_all] \
                                     for t in cols_all], []) \
                                     for l in X_c_player_labels_ranked_final], [])
X_c_player_labels_ranked_final_full_set = set(X_c_player_labels_ranked_final_full)

# Labels for each player
X_c_player_labels_all = \
    X_c_player_labels_ranked + \
    opgg_rs_labels_full + \
    opgg_pcs_labels_full

chgg_gms_bot_labels = sum(sum([[[c + '_' + ch_m_pre + r + '_' + l for r in [
    "synergy_adc",
    "synergy_support",
    "adc_support",
    "support_adc",
]] for l in chgg_gms_labels] for c in cols_all], []), [])

X_c_prev_labels_all = [  # Player previous games/recent matches data
    "queue",
    "duration",
    "win",
    "champion",
    "team_side",
    "spell_1",
    "spell_2",
    "kills",
    "deaths",
    "assists",
    "creep_score",
    # "creep_score_per_second",
    "kill_participation",
    "level",
    "timestamp",
    "time_of_day",
    "pinks_purchased",
] + opgg_pcs_player_labels + opgg_gcs_labels_full + chgg_gcs_labels_full
X_c_prev_labels_all_len = len(X_c_prev_labels_all)

past_elo_estimate_labels = [r + "_pastelo" for r in t_roles]

pcs_ratios = [
    ("opgg_champion_recent_kills", "chgg_eloavg_champion_kills"),
    ("opgg_champion_recent_deaths", "chgg_eloavg_champion_deaths"),
    ("opgg_champion_recent_assists", "chgg_eloavg_champion_assists"),
    ("opgg_champion_recent_gold", "opgg_eloavg_champion_today_gold"),
    ("opgg_champion_recent_creep_score", "opgg_eloavg_champion_today_creep_score"),
    ("opgg_champion_recent_damage_taken", "chgg_eloavg_champion_total_damage_taken"),
]
pcs_ratios_keys = [k + '_/_' + k_ for k, k_ in pcs_ratios]

X_c_labels_all = sum([ [role + '_' + l for l in X_c_player_labels_all]
    for role in t_roles], []) + c_meta_labels_all

X_r_player_labels = sum([[role + '_' + l for role in t_roles] for l in [
    convert_label("highestAchievedSeasonTier"),
]], []) + X_r_player_spell_labels

X_r_labels_all = X_meta_labels_r + X_game_labels_all + \
                 X_r_player_champion_labels + X_r_player_pick_position_labels + X_r_player_labels + \
    sum([[role+'_'+l for l in opgg_gcs_labels_full] for role in t_roles], [])+\
    sum([[role+'_'+l for l in chgg_gcs_labels_full] for role in t_roles], [])+\
    sum([[role+'_'+l for l in chgg_gms_labels_full] for role in t_roles], [])+\
    sum([[role+'_'+l for l in pcs_ratios_keys] for role in t_roles], [])+\
    chgg_gms_bot_labels
X_r_labels_all_len = len(X_r_labels_all)

X_labels_all = X_r_labels_all + X_c_labels_all

Yc_team_labels = [   # Team objectives/Y values
    "first_blood",
    "first_tower",
    "rift_herald",
    "first_dragon",
    "first_baron",
    "first_inhib",
]
Yc_team_labels_range = range(len(Yc_team_labels))
Yc_team_labels_deconved = [deconvert_label(l) for l in Yc_team_labels]

Yr_player_labels = [   # Player objectives/ Y values
    convert_label("totalDamageDealtToChampions"),
    convert_label("totalDamageDealt"),
    convert_label("totalDamageTaken"),
    convert_label("totalTimeCrowdControlDealt"),
    "time_ccing_others",
    "vision_score",
    "creep_score",
    "level",
    "gold",
    "kills",
    "deaths",
    "assists",
    "damage_taken_10min",
    "creep_score_10min",
    "gold_10min",
    "xp_10min",
    # "damage_taken_10-20min", # Not all games last 20 mins
    # "creep_score_10-20min",
    # "gold_10-20min",
    # "xp_10-20min",
]
Yr_player_labels_range = range(len(Yr_player_labels))
Yr_player_labels_deconved = [
    "totalDamageDealtToChampions",
    "totalDamageDealt",
    "totalDamageTaken",
    "totalTimeCrowdControlDealt",
    "timeCCingOthers",
    "visionScore",
    "totalMinionsKilled",
    "champLevel",
    "goldEarned",
    "kills",
    "deaths",
    "assists",
    "TIMELINE:0-10:damageTaken",
    "TIMELINE:0-10:creeps",
    "TIMELINE:0-10:gold",
    "TIMELINE:0-10:xp",
    # "TIMELINE:10-20:damageTaken",
    # "TIMELINE:10-20:creeps",
    # "TIMELINE:10-20:gold",
    # "TIMELINE:10-20:xp",
]
timeline_label_tag = "TIMELINE:"

Yc_labels_all = [  # All Rito game json Y data
    "blue_win",
]
Yc_labels_all += \
    sum([[c + '_' + l for l in Yc_team_labels] for c in cols_all], [])
Yc_labels_all_len = len(Yc_labels_all)

Yr_labels_all = sum([[role + '_' + l
    for role in t_roles] for l in Yr_player_labels], [])
Yr_labels_all_len = len(Yr_labels_all)

# Default array labels (all categorical x variables dummy coded)
meta_arr_labels_all = sum([[k] if k != 'acc_ids' else \
    ['acc_ids__x' + str(j) for j in N_PL_r] for k in meta_labels_all if \
    k not in ['names', 'roles']], [])
X_arr_labels_all = sum([[k] if k not in coded_x_keys else [k + '__x' + \
    str(j) for j in range(coded_x_keys[k])] for k in X_labels_all], [])
X_c_prev_arr_labels_all = sum([[k] if k not in coded_x_c_prev_keys else \
    [k + '__x' + str(j) for j in range(coded_x_c_prev_keys[k])] for k in \
    X_c_prev_labels_all], [])


# Recent matches values to add (derive)
new_X_rec_labels = [
    "kda_ratio", "opgg_champion_recent_kda_ratio",
    "opgg_champion_season_kda_ratio", "opgg_champion_recent_win_rate",
    "time_since_match",
]

# Recent matches values to average
rec_avg_keys = [
    "duration", "time_since_match", "time_of_day",
    "win", "kills", "deaths", "assists", "creep_score",
    # "creep_score_per_second",
    "kill_participation", "level", "pinks_purchased",
    
    "opgg_champion_recent_wins",
    "opgg_champion_recent_losses",
    # "opgg_champion_recent_total_wins",
    # "opgg_champion_recent_total_losses",
    # "opgg_champion_recent_rank",
    # "opgg_champion_recent_mean_rank",
    "opgg_champion_recent_kda_ratio",
    "opgg_champion_recent_win_rate",
    # "opgg_champion_recent_turrets",
    
    # "opgg_champion_season_wins",
    # "opgg_champion_season_losses",
    # "opgg_champion_season_rank",
    # "opgg_champion_season_kda_ratio", "opgg_champion_season_turrets",
    
    "opgg_eloavg_champion_month_win_rate",
    "opgg_eloavg_champion_month_games_fraction",
    "opgg_eloavg_champion_week_win_rate",
    "opgg_eloavg_champion_week_games_fraction",
    "opgg_eloavg_champion_today_win_rate",
    "opgg_eloavg_champion_today_games_fraction",
    
    "chgg_eloavg_champion_win_rate",
    "chgg_eloavg_champion_play_rate",
]

# Recent matches pairs of values to get ratios for
rec_ratios = [
    ("kills", "opgg_champion_recent_kills"),
    # ("kills", "opgg_champion_season_kills"),
    ("kills", "chgg_eloavg_champion_kills"),
    ("deaths", "opgg_champion_recent_deaths"),
    # ("deaths", "opgg_champion_season_deaths"),
    ("deaths", "chgg_eloavg_champion_deaths"),
    ("assists", "opgg_champion_recent_assists"),
    # ("assists", "opgg_champion_season_assists"),
    ("assists", "chgg_eloavg_champion_assists"),
    ("kda_ratio", "opgg_champion_recent_kda_ratio"),
    # ("kda_ratio", "opgg_champion_season_kda_ratio"),
    # ("kda_ratio", "opgg_eloavg_champion_month_kda_ratio"),
    # ("kda_ratio", "opgg_eloavg_champion_week_kda_ratio"),
    ("kda_ratio", "opgg_eloavg_champion_today_kda_ratio"),
    ("kda_ratio", "chgg_eloavg_champion_kda_ratio"),
    ("creep_score", "opgg_champion_recent_creep_score"),
    # ("creep_score", "opgg_champion_season_creep_score"),
    # ("creep_score", "opgg_eloavg_champion_month_creep_score"),
    # ("creep_score", "opgg_eloavg_champion_week_creep_score"),
    ("creep_score", "opgg_eloavg_champion_today_creep_score"),
    ("creep_score", "chgg_eloavg_champion_creep_score"),
    # ("opgg_champion_recent_gold", "opgg_eloavg_champion_month_gold"),
    # ("opgg_champion_recent_gold", "opgg_eloavg_champion_week_gold"),
    # ("opgg_champion_recent_gold", "opgg_eloavg_champion_today_gold"),
    # ("opgg_champion_recent_gold", "chgg_eloavg_champion_gold"),
    # ("opgg_champion_season_gold", "opgg_eloavg_champion_month_gold"),
    # ("opgg_champion_season_gold", "opgg_eloavg_champion_week_gold"),
    # ("opgg_champion_season_gold", "opgg_eloavg_champion_today_gold"),
    # ("opgg_champion_season_gold", "chgg_eloavg_champion_gold"),
    ("opgg_champion_recent_damage_dealt", "chgg_eloavg_champion_total_damage"),
    ("opgg_champion_recent_damage_taken", "chgg_eloavg_champion_total_damage_taken"),
    ("pinks_purchased", "chgg_eloavg_champion_wards_placed"),
    ("pinks_purchased", "chgg_eloavg_champion_wards_killed"),
]
rec_ratios_keys = [k + '_/_' + k_ for k, k_ in rec_ratios]


# Specify which of these recent match features should be duration normalised
rec_avg_durnorm = {
    "kills", "deaths", "assists", "creep_score",
    "level", "pinks_purchased",
}
rec_ratios_durnorm = {
    ("kills", "opgg_champion_recent_kills"),
    ("kills", "opgg_champion_season_kills"),
    ("kills", "chgg_eloavg_champion_kills"),
    ("deaths", "opgg_champion_recent_deaths"),
    ("deaths", "opgg_champion_season_deaths"),
    ("deaths", "chgg_eloavg_champion_deaths"),
    ("assists", "opgg_champion_recent_assists"),
    ("assists", "opgg_champion_season_assists"),
    ("assists", "chgg_eloavg_champion_assists"),
    ("creep_score", "opgg_champion_recent_creep_score"),
    ("creep_score", "opgg_champion_season_creep_score"),
    ("creep_score", "opgg_eloavg_champion_month_creep_score"),
    ("creep_score", "opgg_eloavg_champion_week_creep_score"),
    ("creep_score", "opgg_eloavg_champion_today_creep_score"),
    ("creep_score", "chgg_eloavg_champion_creep_score"),
    ("pinks_purchased", "chgg_eloavg_champion_wards_placed"),
    ("pinks_purchased", "chgg_eloavg_champion_wards_killed"),
}
rec_ratios_durnorm = [k + '_/_' + k_ for k, k_ in rec_ratios_durnorm]


# Specify which should be exponentially decayed (Ebbinghaus)
rec_avg_expdec = [
    "win",
    # "deaths",
    # "creep_score",
    "kill_participation",
    # "level",
    # "pinks_purchased",
]
rec_ratios_expdec = [
    # ("kills", "opgg_champion_recent_kills"),
    # ("kills", "opgg_champion_season_kills"),
    ("kills", "chgg_eloavg_champion_kills"),
    # ("deaths", "opgg_champion_recent_deaths"),
    # ("deaths", "opgg_champion_season_deaths"),
    ("deaths", "chgg_eloavg_champion_deaths"),
    # ("assists", "opgg_champion_recent_assists"),
    # ("assists", "opgg_champion_season_assists"),
    ("assists", "chgg_eloavg_champion_assists"),
    # ("kda_ratio", "opgg_champion_recent_kda_ratio"),
    # ("kda_ratio", "opgg_champion_season_kda_ratio"),
    # ("kda_ratio", "opgg_eloavg_champion_month_kda_ratio"),
    # ("kda_ratio", "opgg_eloavg_champion_week_kda_ratio"),
    # ("kda_ratio", "opgg_eloavg_champion_today_kda_ratio"),
    ("kda_ratio", "chgg_eloavg_champion_kda_ratio"),
    # ("creep_score", "opgg_champion_recent_creep_score"),
    # ("creep_score", "opgg_champion_season_creep_score"),
    # ("creep_score", "opgg_eloavg_champion_month_creep_score"),
    # ("creep_score", "opgg_eloavg_champion_week_creep_score"),
    # ("creep_score", "opgg_eloavg_champion_today_creep_score"),
    ("creep_score", "chgg_eloavg_champion_creep_score"),
    ("pinks_purchased", "chgg_eloavg_champion_wards_placed"),
    ("pinks_purchased", "chgg_eloavg_champion_wards_killed"),
]
rec_ratios_expdec = [k + '_/_' + k_ for k, k_ in rec_ratios_expdec]


# Label suffix groups for standardization (scaling)
scaling_groups = {
    "0-1": [[k] for k in rec_ratios_keys] + [
        ["month_games"],
        ["week_games"],
        ["today_games"],
        ["season_games"],
        [op_ch_pre + "_games"],
        [ch_ch_pre + "_games"],
        [ch_m_pre + "_games"],
        ["champion_recent_wins", "champion_recent_losses"],
        ["avg_wins", "avg_losses"],
        ["seasonavg_wins", "seasonavg_losses"],
        ["games"],
        ["wins", "losses"],
        ["n_recent_matches"],
        ["win_rate","play_rate","kill_participation","percent_role_played"],
        ["games_fraction"],
        ["champion_recent_rank","mean_rank","max_rank","avg_rank","seasonavg_rank"],
        ["tier", "hAST"],
        ["rank"],
        ["division"],
        ["elo"],
        ["elodeviation"],
        ["league_points"],
        ["season"],
        ["double_kills", "triple_kills", "quadra_kills"],
        ["kills", "deaths", "assists", "largest_kill_spree"],
        ["kda_ratio"],
        ["kill_sprees"],
        ["creep_score", "creep_score_team", "creep_score_enemy", "creep_score_10min"],
        # ["creep_score_per_second"],
        ["gold", "gold_10min"],
        ["turrets"],
        ["level"],
        ["xp_10min"],
        ["weighed_score"],
        ["damage_dealt", "damage_taken", "damage_taken_10min", "damage",
         "damage_dealt_to_champions", "total_heal"],
        ["total_time_crowd_control_dealt"],
        ["time_ccing_others"],
        ["pinks_purchased", "wards_placed", "wards_killed", "vision_score"],
        ["pick_position"],
        ["duration"],
        ["time_since_match"],
        ["time_of_day"],
        ["timestamp"],
    # ],
    ] + [[k] for k in pcs_ratios_keys],
    "robust": [

    ]
}

all_sgrps_keys = sum([sum(scaling_groups[k], []) for k in scaling_groups], [])
k_sfx_lens = dict(zip(all_sgrps_keys, [len(k) for k in all_sgrps_keys]))


# Labels groups for training models for multiple use cases (e.g., for win
# probability vs. game duration) and for testing feature importance

# Game duration
duration_labels = [
    "duration",
]

# Match metadata (queue, patch etc) + team compositions (champions & spells)
match_composition_labels = [
    "queue",
    "season",
    "patch",
    "timestamp",
    "time_of_day",
    "region",
] + sum([[r + '_' + l for l in [
    "champion",
    "pick_position",
    "spell_1",
    "spell_2"
]] for r in t_roles], [])

# Player skill elo ratings etc (skill summary statistics) + averages for match
elo_ratings_labels = [
    "division",
    "tier",
    "rank",
    "league_points",
    "elo",
    "mean_hAST",
] + sum([[r + '_' + l for l in [
    "division",
    "tier",
    "int_tier",
    "rank",
    "league_points",
    "elo",
    "highest_achieved_season_tier",
    "opgg_rs_tier",
    "opgg_rs_rank",
    "opgg_rs_division",
    "opgg_rs_league_points",
]] for r in t_roles], [])

# Player recent matches (up to ~20) statistics (score averages etc)
ns_avgs_all = range(21)
recent_matches_labels = \
    sum([ sum([[r + '_' + str(n) + 'ravg_' + l for l in 
    rec_avg_keys + rec_ratios_keys
    ] for n in ns_avgs_all], []) + \
    [r + '_' +  "n_recent_matches"] for r in t_roles], [])

# Player ranked statistics (for various recent periods, up to ~1000 games ago)
ranked_statistics_labels = [
    "season_wins",
    "season_losses",
] + sum([[r + '_' + l for l in pcs_ratios_keys + [
    "season_games",
    "season_wins",
    "season_losses",
    "season_win_rate",
    "first_ranked_season",
    "last_ranked_season",
    "opgg_rs_games",
    "opgg_rs_wins",
    "opgg_rs_losses",
    "opgg_rs_win_rate",
    "opgg_rs_recent_games",
    "opgg_rs_recent_wins",
    "opgg_rs_recent_losses",
    "opgg_rs_recent_win_rate",
    "opgg_rs_recent_kda_ratio",
    "opgg_rs_recent_kills",
    "opgg_rs_recent_deaths",
    "opgg_rs_recent_assists",
    "opgg_rs_recent_kill_participation",
    "opgg_champion_recent_wins",
    "opgg_champion_recent_losses",
    "opgg_champion_recent_total_wins",
    "opgg_champion_recent_total_losses",
    "opgg_champion_recent_alltotal_wins",
    "opgg_champion_recent_alltotal_losses",
    "opgg_champion_recent_rank",
    "opgg_champion_recent_mean_rank",
    "opgg_champion_recent_mean_max_rank",
    "opgg_champion_recent_max_rank",
    "opgg_champion_recent_kills",
    "opgg_champion_recent_deaths",
    "opgg_champion_recent_assists",
    "opgg_champion_recent_gold",
    "opgg_champion_recent_creep_score",
    "opgg_champion_recent_damage_dealt",
    "opgg_champion_recent_damage_taken",
    "opgg_champion_recent_double_kills",
    "opgg_champion_recent_triple_kills",
    "opgg_champion_recent_quadra_kills",
    "opgg_champion_recent_max_kills",
    "opgg_champion_recent_max_deaths",
    # "opgg_champion_recent_turrets",
    "opgg_champion_recent_first_season",
    "opgg_champion_recent_last_season",
    "opgg_champion_season_wins",
    "opgg_champion_season_losses",
    "opgg_champion_season_rank",
    "opgg_champion_season_kills",
    "opgg_champion_season_deaths",
    "opgg_champion_season_assists",
    "opgg_champion_season_gold",
    "opgg_champion_season_creep_score",
    "opgg_champion_season_damage_dealt",
    "opgg_champion_season_damage_taken",
    "opgg_champion_season_double_kills",
    "opgg_champion_season_triple_kills",
    "opgg_champion_season_quadra_kills",
    "opgg_champion_season_max_kills",
    "opgg_champion_season_max_deaths",
    # "opgg_champion_season_turrets",
    "opgg_champion_avg_wins",
    "opgg_champion_avg_losses",
    "opgg_champion_avg_total_wins",
    "opgg_champion_avg_total_losses",
    "opgg_champion_avg_alltotal_wins",
    "opgg_champion_avg_alltotal_losses",
    "opgg_champion_avg_rank",
    "opgg_champion_avg_mean_rank",
    "opgg_champion_avg_mean_max_rank",
    "opgg_champion_avg_max_rank",
    "opgg_champion_avg_kills",
    "opgg_champion_avg_deaths",
    "opgg_champion_avg_assists",
    "opgg_champion_avg_gold",
    "opgg_champion_avg_creep_score",
    "opgg_champion_avg_damage_dealt",
    "opgg_champion_avg_damage_taken",
    "opgg_champion_avg_double_kills",
    "opgg_champion_avg_triple_kills",
    "opgg_champion_avg_quadra_kills",
    "opgg_champion_avg_max_kills",
    "opgg_champion_avg_max_deaths",
    # "opgg_champion_avg_turrets",
    "opgg_champion_seasonavg_wins",
    "opgg_champion_seasonavg_losses",
    "opgg_champion_seasonavg_total_wins",
    "opgg_champion_seasonavg_total_losses",
    "opgg_champion_seasonavg_rank",
    "opgg_champion_seasonavg_mean_rank",
    "opgg_champion_seasonavg_max_rank",
    "opgg_champion_seasonavg_kills",
    "opgg_champion_seasonavg_deaths",
    "opgg_champion_seasonavg_assists",
    "opgg_champion_seasonavg_gold",
    "opgg_champion_seasonavg_creep_score",
    "opgg_champion_seasonavg_damage_dealt",
    "opgg_champion_seasonavg_damage_taken",
    "opgg_champion_seasonavg_double_kills",
    "opgg_champion_seasonavg_triple_kills",
    "opgg_champion_seasonavg_quadra_kills",
    "opgg_champion_seasonavg_max_kills",
    "opgg_champion_seasonavg_max_deaths",
    # "opgg_champion_seasonavg_turrets",
] + sum([["opgg_champion_all_" + k + '_' + k_ for k in \
  list(champ_dict.keys()) + list(champ_cats.keys())] for k_ in [
    "wins",
    "losses",
]], [])
] for r in t_roles], [])

# Global champion statistics for the champions in a match
global_statistics_labels = sum([[r + '_' + l for l in [
    "opgg_eloavg_champion_month_games",
    "opgg_eloavg_champion_month_games_fraction",
    "opgg_eloavg_champion_month_win_rate",
    "opgg_eloavg_champion_month_kda_ratio",
    "opgg_eloavg_champion_month_gold",
    "opgg_eloavg_champion_month_creep_score",
    "opgg_eloavg_champion_week_games",
    "opgg_eloavg_champion_week_games_fraction",
    "opgg_eloavg_champion_week_win_rate",
    "opgg_eloavg_champion_week_kda_ratio",
    "opgg_eloavg_champion_week_gold",
    "opgg_eloavg_champion_week_creep_score",
    "opgg_eloavg_champion_today_games",
    "opgg_eloavg_champion_today_games_fraction",
    "opgg_eloavg_champion_today_win_rate",
    "opgg_eloavg_champion_today_kda_ratio",
    "opgg_eloavg_champion_today_gold",
    "opgg_eloavg_champion_today_creep_score",
    "chgg_eloavg_champion_games",
    "chgg_eloavg_champion_win_rate",
    "chgg_eloavg_champion_play_rate",
    "chgg_eloavg_champion_percent_role_played",
    "chgg_eloavg_champion_kills",
    "chgg_eloavg_champion_deaths",
    "chgg_eloavg_champion_assists",
    "chgg_eloavg_champion_kda_ratio",
    "chgg_eloavg_champion_kill_sprees",
    "chgg_eloavg_champion_largest_kill_spree",
    "chgg_eloavg_champion_creep_score",
    "chgg_eloavg_champion_gold",
    "chgg_eloavg_champion_total_damage_taken",
    "chgg_eloavg_champion_total_heal",
    "chgg_eloavg_champion_wards_placed",
    "chgg_eloavg_champion_wards_killed",
    "chgg_eloavg_champion_total_damage",
    "chgg_eloavg_champion_total_magic_damage",
    "chgg_eloavg_champion_total_physical_damage",
    "chgg_eloavg_champion_total_true_damage",
    "chgg_eloavg_champion_percent_magic_damage",
    "chgg_eloavg_champion_percent_physical_damage",
    "chgg_eloavg_champion_percent_true_damage",
    "chgg_eloavg_champion_jungle_creep_score_team",
    "chgg_eloavg_champion_jungle_creep_score_enemy",
    "chgg_eloavg_champion_duration_0_to_15_games",
    "chgg_eloavg_champion_duration_15_to_20_games",
    "chgg_eloavg_champion_duration_20_to_25_games",
    "chgg_eloavg_champion_duration_25_to_30_games",
    "chgg_eloavg_champion_duration_30_to_35_games",
    "chgg_eloavg_champion_duration_35_to_40_games",
    "chgg_eloavg_champion_duration_40_plus_games",
    "chgg_eloavg_champion_duration_0_to_15_games_fraction",
    "chgg_eloavg_champion_duration_15_to_20_games_fraction",
    "chgg_eloavg_champion_duration_20_to_25_games_fraction",
    "chgg_eloavg_champion_duration_25_to_30_games_fraction",
    "chgg_eloavg_champion_duration_30_to_35_games_fraction",
    "chgg_eloavg_champion_duration_35_to_40_games_fraction",
    "chgg_eloavg_champion_duration_40_plus_games_fraction",
    "chgg_eloavg_champion_duration_0_to_15_win_rate",
    "chgg_eloavg_champion_duration_15_to_20_win_rate",
    "chgg_eloavg_champion_duration_20_to_25_win_rate",
    "chgg_eloavg_champion_duration_25_to_30_win_rate",
    "chgg_eloavg_champion_duration_30_to_35_win_rate",
    "chgg_eloavg_champion_duration_35_to_40_win_rate",
    "chgg_eloavg_champion_duration_40_plus_win_rate",
    "chgg_eloavg_matchup_games",
    "chgg_eloavg_matchup_wins",
    "chgg_eloavg_matchup_win_rate",
    "chgg_eloavg_matchup_kills",
    "chgg_eloavg_matchup_deaths",
    "chgg_eloavg_matchup_assists",
    "chgg_eloavg_matchup_gold",
    "chgg_eloavg_matchup_creep_score",
    "chgg_eloavg_matchup_total_damage_dealt_to_champions",
    "chgg_eloavg_matchup_kill_sprees",
    "chgg_eloavg_matchup_jungle_creep_score_team",
    "chgg_eloavg_matchup_weighed_score",
]] for r in t_roles], []) + sum([[c + '_' + l for l in [
    "chgg_eloavg_matchup_synergy_adc_games",
    "chgg_eloavg_matchup_synergy_support_games",
    "chgg_eloavg_matchup_adc_support_games",
    "chgg_eloavg_matchup_support_adc_games",
    "chgg_eloavg_matchup_synergy_adc_wins",
    "chgg_eloavg_matchup_synergy_support_wins",
    "chgg_eloavg_matchup_adc_support_wins",
    "chgg_eloavg_matchup_support_adc_wins",
    "chgg_eloavg_matchup_synergy_adc_win_rate",
    "chgg_eloavg_matchup_synergy_support_win_rate",
    "chgg_eloavg_matchup_adc_support_win_rate",
    "chgg_eloavg_matchup_support_adc_win_rate",
    "chgg_eloavg_matchup_synergy_adc_kills",
    "chgg_eloavg_matchup_synergy_support_kills",
    "chgg_eloavg_matchup_adc_support_kills",
    "chgg_eloavg_matchup_support_adc_kills",
    "chgg_eloavg_matchup_synergy_adc_deaths",
    "chgg_eloavg_matchup_synergy_support_deaths",
    "chgg_eloavg_matchup_adc_support_deaths",
    "chgg_eloavg_matchup_support_adc_deaths",
    "chgg_eloavg_matchup_synergy_adc_assists",
    "chgg_eloavg_matchup_synergy_support_assists",
    "chgg_eloavg_matchup_adc_support_assists",
    "chgg_eloavg_matchup_support_adc_assists",
    "chgg_eloavg_matchup_synergy_adc_gold",
    "chgg_eloavg_matchup_synergy_support_gold",
    "chgg_eloavg_matchup_adc_support_gold",
    "chgg_eloavg_matchup_support_adc_gold",
    "chgg_eloavg_matchup_synergy_adc_creep_score",
    "chgg_eloavg_matchup_synergy_support_creep_score",
    "chgg_eloavg_matchup_adc_support_creep_score",
    "chgg_eloavg_matchup_support_adc_creep_score",
    "chgg_eloavg_matchup_synergy_adc_total_damage_dealt_to_champions",
    "chgg_eloavg_matchup_synergy_support_total_damage_dealt_to_champions",
    "chgg_eloavg_matchup_adc_support_total_damage_dealt_to_champions",
    "chgg_eloavg_matchup_support_adc_total_damage_dealt_to_champions",
    "chgg_eloavg_matchup_synergy_adc_kill_sprees",
    "chgg_eloavg_matchup_synergy_support_kill_sprees",
    "chgg_eloavg_matchup_adc_support_kill_sprees",
    "chgg_eloavg_matchup_support_adc_kill_sprees",
    "chgg_eloavg_matchup_synergy_adc_jungle_creep_score_team",
    "chgg_eloavg_matchup_synergy_support_jungle_creep_score_team",
    "chgg_eloavg_matchup_adc_support_jungle_creep_score_team",
    "chgg_eloavg_matchup_support_adc_jungle_creep_score_team",
    "chgg_eloavg_matchup_synergy_adc_weighed_score",
    "chgg_eloavg_matchup_synergy_support_weighed_score",
    "chgg_eloavg_matchup_adc_support_weighed_score",
    "chgg_eloavg_matchup_support_adc_weighed_score",
]] for c in cols_all], [])

# Dictionary for key indexing (excludes duration_labels)
# label_subsets_dict = {
#     "match_composition": match_composition_labels,
#     "elo_ratings": elo_ratings_labels,
#     "recent_matches": recent_matches_labels,
#     "ranked_statistics": ranked_statistics_labels,
#     "global_statistics": global_statistics_labels,
# }
label_subsets_dict = {
    "M": match_composition_labels,
    "E": elo_ratings_labels,
    "R": recent_matches_labels,
    "P": ranked_statistics_labels,
    "G": global_statistics_labels,
}


# Manually (Expert knowledge)-chosen redundant features not likely
# to add information compared to non-redundant
redundant_feats_game = [
    # "mean_hAST",
    "season",
]
redundant_feats_player = [
    # "spell_1", # Not useful in linear models
    # "spell_2",
    # "highest_achieved_season_tier",  # All versions of elo
    "tier",
    "int_tier",
    "rank",
    "league_points",
    "division",
    "opgg_rs_tier",
    "opgg_rs_rank",
    "opgg_rs_division",
    "opgg_rs_league_points",
    # "opgg_rs_recent_*",
    # "opgg_champion_season_*",
    # "opgg_champion_seasonavg_*",
    # "opgg_champion_avg_*",
    # "opgg_champion_all_*",
    "opgg_champion_recent_total_*",
    "opgg_champion_recent_rank",
    "opgg_champion_recent_mean_rank",
    "opgg_champion_recent_mean_max_rank",
    "opgg_champion_recent_max_rank",
    # "opgg_champion_recent_max_kills",
    # "opgg_champion_recent_max_deaths",
    # "opgg_champion_recent_first_season",
    # "opgg_champion_recent_last_season",
    # "opgg_eloavg_champion_*",
    # "chgg_eloavg_champion_duration_0_to_15_games*",
    # "chgg_eloavg_champion_duration_15_to_20_games*",
    # "chgg_eloavg_champion_duration_20_to_25_games*",
    # "chgg_eloavg_champion_duration_25_to_30_games*",
    # "chgg_eloavg_champion_duration_30_to_35_games*",
    # "chgg_eloavg_champion_duration_35_to_40_games*",
    # "chgg_eloavg_champion_duration_40_plus_games*",
    "chgg_eloavg_champion_percent_*",
]
redundant_feats_recent_matches = [
    "timestamp", #                       # Would could overfitting (use time_since_match and time_of_day)
    
    # "opgg_champion_recent_first_season",
    "opgg_champion_recent_last_season",
    "chgg_eloavg_champion_kill_sprees",
    "chgg_eloavg_champion_largest_kill_spree",
    "chgg_eloavg_champion_duration_0_to_15*",
    "chgg_eloavg_champion_duration_15_to_20*",

    "opgg_champion_season_*",
    "opgg_champion_seasonavg_*",
    "opgg_champion_avg_*",
    "opgg_champion_all_*",
    "opgg_champion_recent_total_*",
    "opgg_champion_recent_rank",
    "opgg_champion_recent_gold*", # leakage
    "opgg_champion_recent_mean_rank",
    "opgg_champion_recent_mean_max_rank",
    "opgg_champion_recent_max_rank",
    "opgg_champion_recent_max_kills",
    "opgg_champion_recent_max_deaths",
    # "opgg_champion_recent_first_season",
    # "opgg_champion_recent_last_season",
    # "opgg_eloavg_champion_*",
    "chgg_eloavg_champion_duration_0_to_15_games*",
    "chgg_eloavg_champion_duration_15_to_20_games*",
    "chgg_eloavg_champion_duration_20_to_25_games*",
    "chgg_eloavg_champion_duration_25_to_30_games*",
    "chgg_eloavg_champion_duration_30_to_35_games*",
    "chgg_eloavg_champion_duration_35_to_40_games*",
    "chgg_eloavg_champion_duration_40_plus_games*",
    # "chgg_eloavg_champion_percent_*",
]
redundant_feats_recent_matches_avgs = [
    # "opgg_champion_recent_total_*",
    "opgg_champion_recent_rank",
    "opgg_champion_recent_mean_rank",
    # "opgg_champion_season_*",
    # "opgg_eloavg_champion_*",
    # "kills_/_opgg_*",
    # "deaths_/_opgg_*",
    # "assists_/_opgg_*",
    # "kda_ratio_/_opgg_*",
    # "creep_score_/_opgg_*",
    # "opgg_champion_recent_gold_/_opgg_*",
    # "opgg_champion_season_gold_/_opgg_*",
]


one_side_rem_feats = [
    "mean_hAST",
    "division",
    "tier",
    "rank",
    "league_points",
    "elo",
    "season_wins",
    "season_losses",
]

one_side_rem_feats_player = [
    "highest_achieved_season_tier",
    "spell_1",
    "spell_2",
    "season_games",
    "season_wins",
    "season_losses",
    "season_win_rate",
    "tier",
    "int_tier",
    "rank",
    "league_points",
    "division",
    "elo",
    "elodeviation",
    "first_ranked_season",
    "last_ranked_season",
    "n_recent_matches",
] + opgg_rs_labels_full + opgg_pcs_labels_full

pre_champ_select_rem_feats_player_ = [
    "champion",
    "spell_1",
    "spell_2",
    "opgg_champion_recent_wins",
    "opgg_champion_recent_losses",
    "opgg_champion_recent_total_wins",
    "opgg_champion_recent_total_losses",
    "opgg_champion_recent_alltotal_wins",
    "opgg_champion_recent_alltotal_losses",
    "opgg_champion_recent_rank",
    "opgg_champion_recent_mean_rank",
    "opgg_champion_recent_mean_max_rank",
    "opgg_champion_recent_max_rank",
    "opgg_champion_recent_kills",
    "opgg_champion_recent_deaths",
    "opgg_champion_recent_assists",
    "opgg_champion_recent_gold",
    "opgg_champion_recent_creep_score",
    "opgg_champion_recent_damage_dealt",
    "opgg_champion_recent_damage_taken",
    "opgg_champion_recent_double_kills",
    "opgg_champion_recent_triple_kills",
    "opgg_champion_recent_quadra_kills",
    "opgg_champion_recent_max_kills",
    "opgg_champion_recent_max_deaths",
    # "opgg_champion_recent_turrets",
    "opgg_champion_recent_first_season",
    "opgg_champion_recent_last_season",
    "opgg_champion_season_wins",
    "opgg_champion_season_losses",
    "opgg_champion_season_rank",
    "opgg_champion_season_kills",
    "opgg_champion_season_deaths",
    "opgg_champion_season_assists",
    "opgg_champion_season_gold",
    "opgg_champion_season_creep_score",
    "opgg_champion_season_damage_dealt",
    "opgg_champion_season_damage_taken",
    "opgg_champion_season_double_kills",
    "opgg_champion_season_triple_kills",
    "opgg_champion_season_quadra_kills",
    "opgg_champion_season_max_kills",
    "opgg_champion_season_max_deaths",
    # "opgg_champion_season_turrets",
]

pre_champ_select_rem_feats = [
    "duration"
] + sum([[r + '_' + l for l in pre_champ_select_rem_feats_player_] for r in t_roles], []) + \
    global_statistics_labels





# possible_bad_feat_subsets = [
#     ["opgg_champion_all_" + c_name + "_wins" for c_name in champ_dict]
#     ["opgg_champion_all_" + cat + "_wins" for cat in champ_cats]
#     ["opgg_champion_all_" + c_name + "_losses" for c_name in champ_dict]
#     ["opgg_champion_all_" + cat + "_losses" for cat in champ_cats]
#     ["opgg_champion_all_*_losses"]
#     ["opgg_champion_all_*_"]

# ]