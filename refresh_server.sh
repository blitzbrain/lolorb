#
#  Refresh server (runs on root server)
#


. ~/lolorb/Constants.sh


cd ~/opggapi
git pull
/home/ubuntu/.nvm/versions/node/v$NPM_VER/bin/forever restart server_cluster.js

cd ~/lorb
git pull
/home/ubuntu/.nvm/versions/node/v$NPM_VER/bin/forever restart server.js
rm -rdf /home/ubuntu/staging
cp -rd /home/ubuntu/lorb/site/public /home/ubuntu/staging

cd ~/lolorb/
git pull
dos2unix *.sh
$PYEXEC kr_modify.py
sudo rm -rdf /www/public
sudo cp -rd /home/ubuntu/staging /www/public
$PYEXEC restart_workers.py


