#
#  Send global stats database to minion
#


from Monitoring import *


# Get new minion private ip
ip = socket.gethostname().split('ip-')[-1].replace('-', '.')


res = subprocess.run("ssh -i " + host_pem + " -o StrictHostKeyChecking=no ubuntu@" + root_addr + \
        ' "' + worker_pyex + " ~/lolorb/upload_gsdb_minions.py " + ip + ' 2"',
        stdout=subprocess.PIPE, shell=True)
res = res.stdout.decode('utf-8')


