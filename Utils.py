'''
    
    Utility methods

'''

import gc
import joblib
import traceback
import itertools
import functools
from datetime import datetime, timedelta
from joblib import Parallel, delayed
from copy import deepcopy
from collections import defaultdict, deque
from threading import Thread, Condition, Timer

from Constants import *


range_5 = range(5)



pr_prefix = ''

# If we're in the right directory, get worker key
if "worker.py" in sys.argv and os.path.exists(conf_fn):
    with open(conf_fn, 'r') as f:
        worker_conf = json.load(f)
    worker_key = socket.gethostname() + ':?'
    if len(sys.argv) > 1:
        worker_key = worker_key[:-1] + sys.argv[-1]
    pr_prefix = '[' + str(worker_conf["regions"]) + '$' + worker_key + ']'

def get_datetime():
    return get_a_datetime(time.time())

def get_a_datetime(ts):
    return datetime.utcfromtimestamp(int(ts)).strftime('%Y-%m-%d %H:%M:%S')

# print() and flush() output
def pr_fl(*objs):
    global pr_prefix
    print(pr_prefix, get_datetime(), *objs)
    if sys.stdout is not None:
        sys.stdout.flush()




def make_r_timestamps():
    return dict([(k, {'ts': 0}) for k in r_ids])

def make_r_conds():
    return dict([(k, Condition()) for k in r_ids])

def calculate_kda(k, d, a):
    takedowns = k + a
    if takedowns < 1:
        return 0
    if d < 1:
        return takedowns
    return takedowns / d

# Converts a text ranking (eg, Diamond III) to a division index (21) from 0 to 26
def get_division_index(rank):
    d = rank.split(' ')
    i = get_league_n(d[0])
    if i >= 0 and i < 24 and len(d) > 1:
        div = d[1]
        i += 4 - (int(div) if ord(div[0]) >= ord('0') and ord(div[0]) <= ord('9')
                           else deromanize(div))
    return i

def get_league_n(league):
    return league_ns[league.lower()]

def get_league(div):
    leag_vals = list(league_ns.values())
    for i in range(len(leag_vals)):
        if div >= leag_vals[i]:
            return list(league_ns.keys())[i]

def get_league_info(elo):
    div = min(int(elo / 100), 26)
    lp = elo - (div * 100 if div != 26 else 24 * 100)
    if div == 26 and lp < 500:
        div = 25
    if div > 24 and lp < 200:
        div = 24
    tier = None
    if div >= 24:
        tier = leag_tier_is["master"] + (div - 24)
    else:
        tier = int((div // 4) + 1)
    return tier, div, lp

def get_kda(kills, deaths, assists):
    if deaths == 0:
        deaths = 1
    return (kills + assists) / deaths


def patch_n2s(p):
    p = float(p)
    return str(int(p)) + '.' + str(round((p - int(p)) * 100))
def patch_s2n(p):
    return int(p.split('.')[0]) + (int(p.split('.')[1]) / 100.0)


def convert_label(l): # Underscorify a Rito-style (Java variable style) label
    conv = []
    for i in range(len(l)):
        o = ord(l[i])
        if o >= 65 and o <= 90:
            conv += [i]
    convd = 0
    for i in conv:
        letter = chr(ord(l[i + convd]) + 32)
        l = l[:i + convd] + '_' + letter + l[-(len(l) - i - 1 - convd):]
        convd += 1
    return l

def deconvert_label(l):
    if l == "rift_herald":
        l = "first_" + l
    if l == "first_inhib":
        l += "itor"
    conv = []
    for i in range(len(l)):
        c = l[i]
        if c == '_':
            conv += [i]
    convd = 0
    for i in conv:
        letter = chr(ord(l[i - convd + 1]) - 32)
        l = l[:i - convd] + letter + l[-(len(l) - i - 2 + convd):]
        convd += 1
    return l

def get_label_role(l):
    l_list = l.split('_')
    first_two = '_'.join(l_list[:2])
    if first_two in t_roles:
        return first_two
    return "None"

f2 = lambda l: '_'.join(l.split('_')[:2])

def save_ld(data, name, pad=True, compress=False):
    if pad:
        create_folder(data_dir + learning_data_dir)
        name = data_dir + learning_data_dir + name + ".data"
    joblib.dump(data, name, compress=compress)

def load_ld(name, pad=True):
    if pad:
        name = data_dir + learning_data_dir + name + ".data"
    return joblib.load(name)


def save_json(obj, name, pad=True):
    if pad:
        name = data_dir + name + '.json'
    with open(name, 'w') as outfile:
        json.dump(obj, outfile)

def load_json(name, pad=True, encoding=None, print_errs=False):
    if pad:
        name = data_dir + name + '.json'
    try:
        with open(name, 'r', encoding=encoding) as infile:
            return json.load(infile)
    except Exception as e:
        if print_errs:
            pr_fl("Error loading json file [" + name + "]:")
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl(type(e))
            pr_fl(e)
        return None



#
#  db_add
#
#  Adds (concatenates) a dataframe to a csv database & returns updated database
#
#  df               = new data to add
#  path             = path of database
#  return_region    = if not None, region for which to return data for
#  **kwargs         = keyword args for database-loading pd.read_csv call
#
def db_add(df, path, return_region, **kwargs):
    new_df = None
    if os.path.exists(path):
        with open(path, 'a') as db:
            df.to_csv(db, header=False)
        new_df = pd.read_csv(path, **kwargs)
    else:
        df.to_csv(path)
        new_df = df
    if return_region is not None:
        new_df = new_df[new_df["region"] == return_region]
    return new_df

#
#  db_upd
#
#  Updates (merges) a dataframe into a csv database & returns updated database
#  Only difference to db_add is that df can contain new data for existing keys
#
def db_upd(df, path, return_region, **kwargs):
    new_df = None
    if os.path.exists(path):
        new_df = pd.read_csv(path, **kwargs)
        for key in list(df.index):
            new_df.loc[key] = list(df.loc[key])
        new_df.to_csv(path)
    else:
        df.to_csv(path)
        new_df = df
    if return_region is not None:
        new_df = new_df[new_df["region"] == return_region]
    return new_df


