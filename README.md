The dataset for this project can be found at https://drive.google.com/drive/folders/11UwUg5S70ZTYktgdr2Qa-wkHvIanecW5?usp=sharings


This package is one of three required for the full web app. The other two, `lorb` and `opggapi` should be cloned into the same directory, i.e.:
```
project_root
     ├ lolorb
     ├ lorb
     └ opggapi
```
Note: `lorb` is only required for the website/HTTP prediction request interface; the RabbitMQ prediction API server can run without it.


# LoLorb


This package (`lolorb`) has currently only been tested in a specific environment (Windows 10 for the data crawler, Ubuntu 16 for the prediction API server), but should work on any OS that can run Python 3, postgres and RabbitMQ.

There are 4 main components:
1. Data crawler `crawl.py`
2. Feature extraction and model training `new_model.py`
3. Lorb prediction API server `worker.py`
4. Server traffic monitor and auto scaler `monitor.py`



### Package import structure

```
├ UniversalUtils.py                         Universal utility functions
│ └ Constants.py                            Global constants
│   └ Utils.py                              Global utility functions
│     └ DataRequests.py                     Third-party request library (uses Riot, Champion.gg and op.gg APIs)
│       ├ [get_matches.py] ┐                    Get initial Riot matches jsons (any), to initiate crawling
│       └ Data.py          │                Match dataset library
│         ├ [get_games.py] ┤                    Get initial matches for dataset (indiscriminately)
│         ├ [add_games.py] ┴ [crawl.py]         Add matches to dataset (prioritize less common skill brackets etc.)
│         └ LearningConstants.py            Machine learned & engineered feature constants
│           └ LearningData.py               Data preprocessing and feature engineering functions
│  Models.py  ├ [gen_role_orderings.py]         Generate champion-role orderings for auto-assigning meta roles
│     └───────┴ Learning.py                 Model training and testing functions
│               ├ [new_model.py]                Train new models for Lorb probability estimation
│               └ Server.py                 Lorb server library
│                 ├ [worker.py]                 Lorb prediction API server
│                 ├ [pro_crawl.py]              Gather data for professional matches
│                 ├ [pro_model.py]              Train new model for professional prediction
│                 └ Monitoring.py           Server monitoring library
│                   ├ [monitor.py]              Traffic monitor and AWS capacity auto scaler background process
│    Graphing.py    ├ [logger.py]               Log and error output streaming background process
│         └─────────┴ [Lorb System HUD.ipynb]   Graphical activity monitor
```



## Requirements
- Python 3 and packages in `requirements.txt`
- For data crawling and Lorb API server, Node.js and the opggapi git package need to be installed and the local op.gg API server running
- For the Lorb API server, RabbitMQ and PostgreSQL (installation instructions/commands can be found in `install_server.sh`. This sets up the remote web API server (i.e., an AWS instance), but the installation commands can also be run locally to create a test environment)



## 1. Data crawler `crawl.py`

This script crawls the Riot API match database and adds new games which have recently occurred and, optionally, contain only players which have only been observed less than `n` times previously, to avoid oversampling. Other techniques are also used to sample the player population across skill brackets uniformly.

Additionally, as the main "loop" script of the Lorb backend server monitor, `crawl.py` will, every 24 hours (when new global stats & possibly game patch info are available and have been downloaded), upload new global stats to the (remote) prediction API server, train and upload new models based on the freshest data, and generate and upload champion-role orderings for auto-assigning roles based on recent trends in the meta.

- `Constants.py` contains the options for the data crawler - including bandwidth and third-party request limits, API keys, search strategy balance

1. If the op.gg API server isn't running already, start it by going to the opggapi package directory and running `node server_cluster.js`
2. If this is the first time running the script (so there is no intial data), set `only_get_seed_games = True` in `Constants.py`. Then run `python crawl.py`. After a few thousand seed games have been downloaded to the `data/init_matches` folder, turn this option off.
3. In this package's root directory, run `python crawl.py`. Note: The crawler runs a separate thread for each of the 10 geographical game server regions, so if one errors out, the exception will be lost in the others' standard output. Instead, run `python crawl.py > crawl.log` and monitor `crawl.log` separately (eg, using `tail` or `Get-Content`), so that errors will be printed separately to the console.



## 2. Feature extraction and model training `new_model.py`

This script trains new models for the Lorb prediction server, executing the full pipeline from feature extraction to feature selection and probability calibration. Multiple models are trained for all possible combinations of input data (team compositions).

- Options for the data preprocessing and feature engineering can be found in `LearningConstants.py`, `LearningData.py` and `new_model.py`.
- Options for the model parameters and output postprocessing can be found in `Models.py`. Additional postprocessing is performed by the `Server.py` before returning the response.
- Experimentation with models can be found in the various Jupyter notebooks (`.ipynb` files)

1. Since this file is run automatically by `crawl.py`, nothing is required by the user.



## 3. Lorb prediction API server `worker.py`

This script runs a RabbitMQ RPC server, that returns predictions for given probability estimate requests (from the Lorb front end). It uses multiple layers of caching to minimise unnecessary processing, especially when many very similar requests are being made as players choose champions in champion select.

- Options can be found in `ServerConstants.py`

1. If the op.gg API server isn't running already, start it by going to the opggapi package directory and running `node server_cluster.js`
2. If running in a new environment and we need to install things, use the commands in `install_worker.sh`
3. If running as the "host" (not "minion") server in a new environment, run the commands from `install_server.sh`, especially the `create_[x]_table.py` scripts which create the initial postgres tables.
4. Use `spawn_workers.py`, `restart_workers.py` and `stop_workers.py` to start and stop the parallelised server processes.
5. If using the front end, use `install_local.sh` to setup the local node packages for generating the website static files.



## 4. Server traffic monitor and auto scaler `monitor.py`

These scripts monitor server traffic by saving postgres-stored activity into a combined log file, and also combine log/error output into another log file. It also auto scales the number of "minion" server machines to accomodate the level of server traffic, currently by thresholding the CPU % usage.

- Options can be found in `Monitoring.py`

1. Start and stop the monitor and logger using `start_monitor.py` and `stop_monitor.py`
2. The logger can be started/stopped separately, if desired, using `start_logger.py` and `stop_logger.py`


