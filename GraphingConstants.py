#
#  Graphing/printing/display constants
#


# Label formatting for graphs etc

Y_labels_format = {
    "blue_win": "Blue side Victory",
    "blue_first_dragon": "Blue side First Dragon",
    "blue_first_tower": "Blue side First Tower",
    "blue_first_inhib": "Blue side First Inhibitor",
}

scorer_label_format = {
    "multitask_acc": "% Accuracy"
}

model_label_format = {
    "AdaBoostClassifier": "AdaBoost Forest",
    "LogisticRegression": "Logistic Regression",
    "MLPClassifier": "Neural Network",
    "SVC": "Support Vector Classification",
    "LinearSVC": "Linear Support Vector Classification",
    "QuadraticDiscriminantAnalysis": "Quadratic Discriminant Analysis",
    "LinearDiscriminantAnalysis": "Linear Discriminant Analysis",
    "MultiTaskElasticNet": "MultiTask ElasticNet",
    "LinearSVC": "MultiTask Lasso",
}


