'''

    This file contains shared methods & constants for data processing

'''

from DataRequests import *


def load_current_data(r_conds, region=None):
    curr_games, curr_players, exists = None, None, False
    with r_conds['cg']:
        if os.path.exists(curr_games_path):
            curr_games = pd.read_csv(curr_games_path, index_col=0)
            exists = True
    if exists:
        with r_conds['cp']:
            curr_players=pd.read_csv(data_dir + curr_players_csv, index_col=0)

    if region is None or curr_games is None:
        return curr_games, curr_players
    return curr_games[curr_games["region"] == region], \
           curr_players[curr_players["region"] == region]


# Add game to dataset
def add_game(r_conds, r_tss, game_id, games_added, n_per_div, region,
             curr_games, curr_players, being):

    if region + '_' + str(game_id) in being:
        return "507 Game being added already"

    with r_conds['ga']:
        being[region + '_' + str(game_id)] = 1

    g_id = region + '_' + str(game_id)
    opgg_reg = opgg_region_dict[region]

    # Get most recent match for root player & add it if it's for a required
    # division and includes no players already in games added
    curr_games, curr_players = load_current_data(r_conds, region)
    if curr_games is not None and g_id in curr_games.index:
        return "501 Game already added"

    with r_conds['rr_' + region.lower()]:
        game = rito_req(r_conds, r_tss, region, game_by_game_id + str(game_id), use_cond=False)
        if game is None:
            return "502 Rito request for game failed"
        if game["queueId"] not in r_queue_ids:
            return "503 Wrong queue type " + str(game["queueId"])
        if game["mapId"] != sr_map_id:
            return "504 Wrong map " + game["mapId"]
        if game["gameDuration"] < 10 * 60:
            return "505 Remake"

        # Get players' info
        pis = game[g_pis]
        ps = [pi["player"] for pi in pis]
        acc_ids, summ_ids, names = \
            zip(*[(p[g_acc_id], p[g_summ_id] if g_summ_id in p else -1, p[g_name])
                for p in ps])
        a_ids = [region + '_' + str(acc_id) for acc_id in acc_ids]
        for i in range(len(summ_ids)):
            if summ_ids[i] == -1:
                pr_fl(game_id)
                pr_fl(acc_ids)
                pr_fl(summ_ids)
                pr_fl(names)
                if not acc_ids[i]:
                    return "777 Player data missing for game " + str(game_id)
                summ_ids[i] = get_summ_id(r_conds, r_tss, region, acc_ids[i])
        pts = dict([(pt["participantId"], pt) for pt in game["participants"]])
        pts = [pts[pi["participantId"]] for pi in pis]

        # Check no participant is in no_rec (no recent games)
        # for i in range(len(acc_ids)):
        #     if acc_ids[i] in no_rec:
        #         return "202 Less than " + str(min_opgg_recent_games) + \
        #                " recent games for " + names[i]

        if len(summ_ids) < N_PL:
            return "737 Not enough summoner ids in Rito response " + str(game_id)
        if len(acc_ids) < N_PL:
            return "757 Not enough account ids in Rito response " + str(game_id)

        # if "highestAchievedSeasonTier" not in pts[0]:
        # pr_fl(str(pts[0].keys()))
        # sys.exit()
        hASTs = [pt["highestAchievedSeasonTier"].lower() if "highestAchievedSeasonTier" in pt else "unranked" for pt in pts]
        hASTlns = [league_ns_avgs[n] for n in hASTs if n[0] != 'u']
        if hASTlns == []:
            # err_pr("No hASTlns - all players in game unranked... assuming challenger (very wrong)")
            # return None, False
            # err_pr("All players unranked..?")
            hASTlns = [league_ns_avgs["challenger"]]
        median_hASTl = int(np.ceil(np.median(hASTlns)))

        if new_players_only:
            # Skip game if not in a rare division and a player has been seen before
            if median_hASTl not in rare_divs and curr_players is not None:
                for i in N_PL_r:
                    if a_ids[i] in curr_players.index:
                        divs = curr_players.loc[a_ids[i]]["division"]
                        mean_div = divs
                        n_prev = 1
                        if type(divs) is pd.Series:
                            divs = list(divs)
                            mean_div = int(round(np.mean(divs)))
                            n_prev = len(divs)
                        if mean_div not in rare_divs and n_prev >= new_players_only:
                            return "201 Previously observed player " + names[i] + \
                                ", division(s): " + str(divs) + \
                                ", apprx game division: " + str(median_hASTl)

        # Get league positions (and wins/losses) for all players
        queue = queue_names[game["queueId"]]
        lps = [get_league_pos(r_conds, r_tss, region, summ, use_cond=False) for summ in summ_ids]
    divisions, not_found = [], []
    for i in N_PL_r:
        found = False
        if lps[i] is None:
            return "506 League position request failed"
        for lp in lps[i]:
            if lp["queueType"] == queue:
                divisions += [get_division_index(lp["tier"]+' '+lp["rank"])]
                found = True
                break
        if not found:
            divisions += [ None ]
            not_found += [ i ]

    curr_games, curr_players = load_current_data(r_conds, region)
    if curr_games is not None and g_id in curr_games.index:
        return "501 Game already added"

    # Get game division (median of players), skip game if division game seen
    game_div = int(np.median([d for d in divisions if d is not None]))
    if games_added[game_div] >= n_per_div and game_div not in rare_divs:
        return seen_code+" Enough games for division ["+str(game_div)+"] seen"
    for i in not_found:
        divisions[i] = game_div

    # Get updated op.gg RankedSummaries for all players (return if a player
    # is unranked or has < min_opgg_recent_games recent games played)
    # Parallelisation not really needed since there is very little bandwidth
    # Parallel(n_jobs=n_parallel_cpu)(delayed(opgg_renew_summoner)(
    #     opgg_reg, summ_ids[i]) for i in N_PL_r)
    # time.sleep(10) # Wait for renewals to happen

    # opgg_rs = Parallel(n_jobs=n_parallel_cpu)(delayed(opgg_ranked_summary)(
    #     opgg_reg, names[i]) for i in N_PL_r)

    opgg_rs, opgg_pcs = [], []
    with ThreadPoolExecutor(max_workers=opgg_thread_pool) as executor:

        r_conds['or'].acquire()
        r_conds['or'].release()

        # Get unencrypted summoner ids
        rs_futures = [executor.submit(opgg_ranked_summary,
                                      opgg_reg, names[i]) for i in N_PL_r]
        rs_results = [rs_fut.result() for rs_fut in rs_futures]

        curr_games, curr_players = load_current_data(r_conds, region)
        if curr_games is not None and g_id in curr_games.index:
            return "501 Game already added"

        # Submit profile renewal requests
        renew_futures = [executor.submit(opgg_renew_summoner,
                                         opgg_reg, rs_results[i]["summonerId"]) for i in N_PL_r if not not rs_results[i] and "summonerId" in rs_results[i]]

        renew_results = [renew_fut.result() for renew_fut in renew_futures]
        time.sleep(10) # Wait for renewals to happen on op.gg server(s)
        # (usually takes a max of ~5 seconds on EUW, add extra 5 in case of lag)

        curr_games, curr_players = load_current_data(r_conds, region)
        if curr_games is not None and g_id in curr_games.index:
            return "501 Game already added"

        r_conds['or'].acquire()
        r_conds['or'].release()
        rs_futures = [executor.submit(opgg_ranked_summary,
                                      opgg_reg, names[i]) for i in N_PL_r]

    # Old "clean-data-only" code
    # for i in N_PL_r:
    #     if not opgg_rs[i]:
    #         return "404 RankedSummary not found for " + names[i]
    #     if "league" not in opgg_rs[i]:
    #         return "401 League not found in RankedSummary for " + names[i]
    #     if opgg_rs[i]["league"].lower() == "unranked":
    #         return "203 Unranked player " + names[i]
    #     if len(opgg_rs[i]["games"]) < min_opgg_recent_games:
    #         no_rec += [ acc_ids[i] ]
    #         return "204 Less than " + str(min_opgg_recent_games) + \
    #                " recent games for " + names[i]

    # Green light -> make all remaining requests & assemble final data json
    # Also save to file previously requested data

    # Get op.gg player champion stats
    # opgg_pcs =Parallel(n_jobs=n_parallel_cpu)(delayed(opgg_player_champ_stats)(
    #     opgg_reg, names[i]) for i in N_PL_r)
            
        # pcs_futures = [executor.submit(opgg_player_champ_stats,
        #                                opgg_reg, names[i]) for i in N_PL_r]

        pcs_futures = []
        for i in N_PL_r:
            r_conds['or'].acquire()
            r_conds['or'].release()
            pcs_futures += [ [executor.submit(opgg_player_champ_stats, opgg_reg,
                season, names[i]) for season in range(curr_season_id + 1 - n_recent_pcs_seasons, curr_season_id + 1)] ]
                # season, names[i]) for season in range(1, curr_season_id + 1)] ]

        # Wait for & collect results
        for i in N_PL_r:
            opgg_pcs += [ [{} for _ in range(curr_season_id - n_recent_pcs_seasons)] + [pcs_fut.result() for pcs_fut in pcs_futures[i]] ]

        curr_games, curr_players = load_current_data(r_conds, region)
        if curr_games is not None and g_id in curr_games.index:
            return "501 Game already added"

        opgg_rs = [rs_fut.result() for rs_fut in rs_futures]

        # opgg_pcs = [pcs_fut.result() for pcs_fut in pcs_futures]

    # Get champions, lanes & roles for each player in the game
    # champions = [pt["championId"] for pt in pts]
    # lanes = [pt["timeline"]["lane"] for pt in pts]
    # roles = [pt["timeline"]["role"] for pt in pts]
    # summs = [(pt["spell1Id"], pt["spell2Id"]) for pt in pts]

    # TODO: Get game (op/ch.gg) league, matchups, complete this section
    # league = get_league(game_div)

    # Get op.gg global champion stats (wr,pr,br,m,w,d for correct league)
    opgg_gcs = opgg_global_champ_stats(r_conds)
    opgg_gcs = game["gameCreation"]

    # Get Champion.gg global champion stats (wr etc for correct league, role)
    # chgg_gcs, gcs_ts = chgg_champions(r_conds, r_tss)
    gcs_ts = game["gameCreation"]

    # Get op.gg plat+ korea champion analytics (role, wr by summs/r&m/items/gl)
    # opgg_ca = [opgg_champion_analytics(champions[i], lanes[i], roles[i])
    #            for i in N_PL_r]   # op.gg-api needs fixing

    # Get op.gg plat+ korea matchup analytics (wr, lkr, kda, dmg etc) (TODO)
    # opgg_ma = [opgg_matchup_analytics(matchups[i])
    #            for i in range(N_PL // 2)]

    # Get Champion.gg global matchup stats (wr etc for correct league, role)
    # chgg_gms = chgg_matchups(r_conds, r_tss, chgg_gcs)
    chgg_gms = game["gameCreation"]

    # Assemble final game datapoint dictionary
    data = {
        "game": game,
        "lps": lps,
        "opgg_rs": opgg_rs,
        "opgg_pcs": opgg_pcs,
        "opgg_gcs": opgg_gcs,
        "chgg_gcs": gcs_ts,
        # "opgg_ca": opgg_ca,
        # "opgg_ma": opgg_ma,
        "chgg_gms": chgg_gms,
    }

    # Save data to file
    create_folder(curr_data_jsons)
    create_folder(curr_games_jsons)
    save_json(data, curr_data_jsons + str(g_id) + ".json", pad=False)
    # save_json(game, curr_games_jsons + str(g_id)) # Don't really need this...

    # Add game and players to current data dataframes
    timestamp = game["gameCreation"]

    new_games = pd.DataFrame([{
        "g_id": g_id,
        "region": region,
        "division": game_div,
        "timestamp": timestamp,
    }])
    new_games = new_games[cg_cols]
    new_games = new_games.set_index('g_id')

    new_players = pd.DataFrame([{
        "a_id": a_ids[i],
        "region": region,
        "division": divisions[i],
        "g_id": g_id,
        "timestamp": timestamp,
    } for i in range(len(a_ids))])
    new_players = new_players[cp_cols]
    new_players = new_players.set_index('a_id')

    games_added[game_div] += 1

    # Save csvs
    curr_games, curr_players = load_current_data(r_conds, region)
    if curr_games is not None and g_id in curr_games.index:
        return "501 Game already added"
    with r_conds['cg']:
        curr_games = db_add(new_games, curr_games_path, region, index_col=0)
    with r_conds['cp']:
        curr_players = db_add(new_players,curr_players_path,region,index_col=0)

    # Return success + game elo
    return succ_msg + " [" + str(game_div) + ']', curr_games, curr_players


