#
#  Monitoring script - creates combined log file, traffic history data file, and automatically scales the site according to demand
#


from Monitoring import *


# FOR EACH regions:
# create fd for logs/[regions].log
# get public ips for all servers
# connect pipes with popen
# every 0.5s, get wlogs pg entries, construct next graph datapoint, append to csvs (one for total, one with n_columns * n_workers entries)


def pr_fl(obj):
    with open(logs_dir + lorb_log_file, 'a') as f:
        f.write(str(obj) + '\n')

def get_wlogs(engine, meta, table):
    query = sqlalchemy.sql.expression.select([table.c[k] \
        for k in ['worker_key', 'timestamp', 'log']])
    res = pg_exec(engine, query)
    if not res:
        return None
    res = list(res)
    if len(res) < 1:
        return None
    return res


# AWS CLI methods
def get_spot_capacity(host):
    res = subprocess.run("aws --region " + host["aws_region"] + " ec2 describe-spot-fleet-requests",
                        stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    res = json.loads(res)

    curr_n = None
    # curr_conf = None
    for conf in res["SpotFleetRequestConfigs"]:
        if conf["SpotFleetRequestId"] == host["aws_spot"]:
            # curr_conf = conf
            if conf["SpotFleetRequestState"] != "active":
                pr_fl("Error: spot fleet request state is not 'active'")
                curr_n = -1
            else:
                curr_n = conf["SpotFleetRequestConfig"]["TargetCapacity"]
            break
    return curr_n

def adjust_spot_capacity(curr, new, host):
    res = subprocess.run("aws --region " + host["aws_region"] + \
        " ec2 modify-spot-fleet-request --target-capacity " + str(new) + " --spot-fleet-request-id " + host["aws_spot"],
        stdout=subprocess.PIPE, shell=True)
    res = res.stdout.decode('utf-8')
    res = json.loads(res)
    if res["Return"] == True:
        pr_fl('[' + host["regions"] + "]  Adjustment of target capacity from " + str(curr) + " to " + str(new) + " successful.")


# Create traffic history csvs if not present
create_folder(logs_dir)
for host in hosts:
    path = traffic_history_csv + '_' + host["regions"] + '.csv'
    if not os.path.exists(path):
        with open(path, 'w') as f:
            f.write('')

# Connect stuff
pg_str_ = 'postgresql://' + pg_user + ':' + pg_password + '@'
dbs = [sqlalchemy.create_engine(pg_str_ + host['addr'] + ':' + str(pg_port) + '/' + pg_name) for host in hosts]
engines = [connect_pg_lg(db, Condition()) for db in dbs]
traffic_files = [open(traffic_history_csv + '_' + host["regions"] + '.csv', 'a') for host in hosts]


logger_last_restart = 0
last_aws_change = defaultdict(int)
# last_aws_change = defaultdict(lambda: time.time())
n_workers = defaultdict(int)
try:
    while True:

        for i in range(len(hosts)):
            host = hosts[i]
            wlogs = get_wlogs(*engines[i])
            if wlogs is None:
                pr_fl("wlogs is None...")
                continue

            cpu_percs = []
            ram_percs = []
            mbit_ups = []
            mbit_downs = []
            mp_actives = []
            pl_actives = []
            op_actives = []
            pg_actives = []
            mp_qsizes = []
            op_qsizes = []
            pg_qsizes = []
            mp_threads = []
            op_threads = []
            pg_threads = []

            new_n = 0
            unique_hostnames = set()
            # pr_fl(host["addr"], wlogs)
            for key, timestamp, log_str in wlogs:
                ts = timestamp / 1000
                if time.time() - ts > worker_log_expiry:
                    continue
                new_n += 1

                worker_key, log_str = log_str.split('@')  # Get rid of worker_key in log_str
                worker_hostname, worker_index = worker_key.split(':')
                log_data = {k: v for k, v in map(lambda x: x.split(':'), log_str.split('|'))}

                mp_actives.append(float(log_data["mp_a"]))
                pl_actives.append(float(log_data["pl_a"]))
                op_actives.append(float(log_data["op_a"]))
                pg_actives.append(float(log_data["pg_a"]))
                mp_qsizes.append(float(log_data["mp_qsize"]))
                op_qsizes.append(float(log_data["op_qsize"]))
                pg_qsizes.append(float(log_data["pg_qsize"]))
                mp_threads.append(float(log_data["mp_threads"]))
                op_threads.append(float(log_data["op_threads"]))
                pg_threads.append(float(log_data["pg_threads"]))

                if worker_hostname not in unique_hostnames:
                    cpu_percs.append(float(log_data["cpu%"]))
                    ram_percs.append(float(log_data["ram%"]))
                    # mbit_ups.append(float(77))
                    mbit_ups.append(float(log_data["MbitUp"]))
                    # mbit_downs.append(float(55))
                    mbit_downs.append(float(log_data["MbitDown"]))
                    unique_hostnames.add(worker_hostname)


            # Get current number of workers
            if new_n != n_workers[host["regions"]]:
                # Workers have appeared/disappeared - restart logger
                if time.time() - logger_last_restart > logger_restart_cooldown:
                    n_workers[host["regions"]] = new_n
                    if enable_logging:
                        logger_last_restart = time.time()
                        start_command = pyex + " start_logger.py"
                        subprocess.Popen(start_command, close_fds=True, creationflags=DETACHED_PROCESS, shell=True)


            cpu_perc = np.mean(cpu_percs)  # Get mean cpu & ram
            ram_perc = np.mean(ram_percs)
            mbit_up = sum(mbit_ups)        # Get total network usage
            mbit_down = sum(mbit_downs)
            mp_active = sum(mp_actives)    # Get total active threads for each task
            pl_active = sum(pl_actives)
            op_active = sum(op_actives)
            pg_active = sum(pg_actives)
            mp_qsize = sum(mp_qsizes)
            op_qsize = sum(op_qsizes)
            pg_qsize = sum(pg_qsizes)
            mp_threads = sum(mp_threads)
            op_threads = sum(op_threads)
            pg_threads = sum(pg_threads)

            max_mp_threads = new_n * n_mp_threads
            max_pl_threads = new_n * n_mp_threads * N_PL
            max_op_threads = new_n * n_opgg_threads
            max_pg_threads = new_n * n_pgcn_threads

            traffic_log = [
                time.time(),
                new_n,
                cpu_perc,
                ram_perc,
                mbit_up,
                mbit_down,
                mp_active,
                pl_active,
                op_active,
                pg_active,
                mp_qsize,
                op_qsize,
                pg_qsize,
                mp_threads,
                op_threads,
                pg_threads,
                max_mp_threads,
                max_pl_threads,
                max_op_threads,
                max_pg_threads,
            ]
            traffic_files[i].write(','.join([str(v) for v in traffic_log]) + '\n')
            traffic_files[i].flush()

            if autoscale and time.time() - last_aws_change[host["regions"]] > aws_cooldown:
                if cpu_perc > undercap: # If under-capacity
                    curr_n = get_spot_capacity(host)
                    if curr_n is None:
                        pr_fl("Error: Spot fleet not found (check request id)")
                    elif curr_n >= 0:
                        new_capacity = curr_n + 1 # Add 1 to the target capacity
                        adjust_spot_capacity(curr_n, new_capacity, host)
                    last_aws_change[host["regions"]] = time.time()

                elif cpu_perc < overcap and new_n > host_n_workers: # If over-capacity
                    curr_n = get_spot_capacity(host)
                    if curr_n is None:
                        pr_fl("Error: Spot fleet not found (check request id)")
                    elif curr_n > 0:
                        new_capacity = curr_n - 1 # Take 1 from the target capacity
                        adjust_spot_capacity(curr_n, new_capacity, host)
                    last_aws_change[host["regions"]] = time.time()

        time.sleep(monitoring_interval)

except Exception as e:
    for line in traceback.format_tb(e.__traceback__):
        pr_fl(line)
    pr_fl(type(e))
    pr_fl(e)

finally:
    for engine, _, _ in engines:
        engine.close()
    for db in dbs:
        db.dispose()


