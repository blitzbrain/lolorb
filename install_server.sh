#
#  AWS Ubuntu 16.x Lorb.gg server install script
#

#
#  Once this script has been run for an AWS instance, a reusable AMI and launch template can be created,
#  at which point this script is no longer required.
#
#  This script should be run from within the lolorb repo folder.
#  This script should work from start to finish but has not been tested.
#  Running each subsection separately is recommended. Remember to set the constants using ./Constants.sh
#  First command (copying over pem key file) runs locally, then we ssh into new server machine
#


. ./Constants.sh






scp -i $PEM_FILE -o StrictHostKeyChecking=no $PEM_FILE ubuntu@$HOST_URL:~/
scp -i $PEM_FILE -o StrictHostKeyChecking=no worker_config_default.json ubuntu@$HOST_URL:/home/ubuntu/lolorb/worker_config.json

# NOTE: If installing for Korean server, manually set "regions": "kr" in worker_config.json


ssh -i $PEM_FILE -o StrictHostKeyChecking=no ubuntu@$HOST_URL -c "git clone https://$GIT_USER:$GIT_PASS@$REPO_HOST/$REPO_NAMESPACE/lolorb.git -b $REPO_BRANCH"
ssh -i $PEM_FILE -o StrictHostKeyChecking=no ubuntu@$HOST_URL

# Install dos2unix
sudo apt-get install dos2unix

# Configure git for lolorb
cd lolorb
git config credential.helper store
git config core.filemode true
git pull
dos2unix *.sh
cp Constants_default.sh Constants.sh
cp worker_config_default.json worker_config.json
cd ~

echo "Please open the file on the server at"
echo
echo "~/lolorb/Constants.sh"
echo 
echo "and set the \$GIT_USER and \$GIT_PASS credential variables."
echo "You may also wish to edit the server role configuration in worker_config.json"
echo "Press enter once this is complete..."
echo
read -p ""

. ./lolorb/Constants.sh


# Copy startup script
sudo cp lorb/rc.local /etc/rc.local


# Set pem key permissions
chmod 600 $PEM_FILE

sudo apt-get update
# sudo apt-get upgrade # Not sure if we want to try this on AWS ubuntu


###
###  Download Lorb respositories
###
cd ~
git config --global user.name $GIT_USER
echo "https://$GIT_USER:$GIT_PASS@$REPO_HOST" | sudo tee -a .git-credentials >/dev/null
# git clone https://$GIT_USER:$GIT_PASS@$REPO_HOST/$REPO_NAMESPACE/lolorb.git -b $REPO_BRANCH
git clone https://$GIT_USER:$GIT_PASS@$REPO_HOST/$REPO_NAMESPACE/lorb.git -b $REPO_BRANCH
git clone https://$GIT_USER:$GIT_PASS@$REPO_HOST/$REPO_NAMESPACE/opggapi.git -b $REPO_BRANCH


###
###  Install postgres
###
sudo apt-get -y install postgresql=9.5+173ubuntu0.2 postgresql-contrib=9.5+173ubuntu0.2
sudo -u postgres psql -c "CREATE DATABASE $PG_NAME;"
sudo -u postgres psql -c "CREATE USER $PG_USER WITH ENCRYPTED PASSWORD '$PG_PASS';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $PG_NAME TO $PG_USER;"

# Set local connections to use md5 password encryption (copy pg_hba.conf)
sudo mv /etc/postgresql/9.5/main/pg_hba.conf /etc/postgresql/9.5/main/pg_hba.conf.bak
sudo cp lolorb/pg_hba.conf /etc/postgresql/9.5/main/pg_hba.conf

# Set postgres to listen on all addresses "*" (copy postgresql.conf)
sudo mv /etc/postgresql/9.5/main/postgresql.conf /etc/postgresql/9.5/main/postgresql.conf.bak
sudo cp lolorb/postgresql.conf /etc/postgresql/9.5/main/postgresql.conf


###
### Install RabbmitMQ
###
apt-key adv --keyserver "hkps.pool.sks-keyservers.net" --recv-keys "0x6B73A36E6026DFCA"
wget -O - "https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc" | sudo apt-key add -
# echo "deb https://dl.bintray.com/rabbitmq/debian xenial main" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list
echo "deb https://dl.bintray.com/rabbitmq/debian xenial main erlang" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list
sudo apt-get update
sudo apt-get -y install rabbitmq-server
# sudo rabbitmq-server start
sudo rabbitmqctl add_user $RMQ_USER $RMQ_PASS
sudo rabbitmqctl set_permissions -p / $RMQ_USER ".*" ".*" ".*"
sudo rabbitmqctl set_user_tags $RMQ_USER administrator
sudo rabbitmq-plugins enable rabbitmq_management


###
### Install Node.js Version Manager
###
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
. ~/.nvm/nvm.sh
export NVM_DIR="/home/ubuntu/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"


###
### Install Python and required packages
###
sudo apt-get -y install python3 python3-pip
cd lolorb
pip3 install -r requirements.txt


# Configure git for lorb
cd ~/lorb
git config credential.helper store
git config core.filemode true
git pull


###
### Install Node.js and lorb required packages
###
nvm install $NPM_VER
nvm alias default node
sudo ln -s "$(which node)" /usr/local/bin/node
npm install forever -g
npm install


###
### Configure git for opggapi and install required packages
###
cd ~/opggapi
git config credential.helper store
git config core.filemode true
git pull
npm install


###
### Create swapfile for python subprocess
###
sudo dd if=/dev/zero of=/swapfile bs=1024 count=1024k
sudo mkswap /swapfile
sudo swapon /swapfile
sudo echo "/swapfile    none    swap    sw  0   0" | sudo tee -a /etc/fstab >/dev/null


###
### Install Nginx web server
###
sudo wget http://nginx.org/keys/nginx_signing.key
sudo apt-key add nginx_signing.key
cd /etc/apt

echo "#" | sudo tee -a sources.list >/dev/null
echo "deb http://nginx.org/packages/ubuntu xenial nginx" | sudo tee -a sources.list >/dev/null
echo "deb-src http://nginx.org/packages/ubuntu xenial nginx" | sudo tee -a sources.list >/dev/null
echo "#" | sudo tee -a sources.list >/dev/null

sudo apt-get update
sudo apt-get -y install nginx

sudo mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bak
sudo cp ~/lorb/$NGINX_CONF /etc/nginx/conf.d/server.conf

sudo service nginx start

# Add SSL certificate
sudo apt-get update
sudo apt-get -y install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get -y install python-certbot-nginx
sudo certbot --nginx -d $HOST_URL

sudo service nginx restart


# Create postgres tables
cd ~/lolorb/
./create_tables.sh $PYEXEC


# setup folders to copy over data
mkdir data
cd data
mkdir chgg_cache
mkdir opgg_cache
mkdir learning_data
mkdir infill_data
cd chgg_cache
mkdir champions
mkdir matchups



