#
#  Create postgres player past table
#


from sqlalchemy import Column, BigInteger, Text, DateTime
from sqlalchemy.dialects.postgresql import JSON, JSONB
import sqlalchemy

from ServerConstants import *


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()  
meta = sqlalchemy.MetaData(engine)

try:
    mhistories = sqlalchemy.Table('mhistories', meta, autoload=True, autoload_with=engine)
    mhistories.drop()
    print("dropped")
except sqlalchemy.exc.NoSuchTableError as e:
    pass

meta = sqlalchemy.MetaData(engine)

sqlalchemy.Table("mhistories", meta,  
    Column('id', Text, primary_key=True),
    Column('player', Text),
    Column('timestamp', BigInteger),
    Column('mh_start', BigInteger),
    Column('mh_end', BigInteger),
    Column('mhistory', JSON),
)

meta.create_all()
engine.close()
db.dispose()



# con = connect(dbname='postgres', user='postgres', host='localhost', password='turbo station doctor')
# cur = con.cursor()

# cur.execute("CREATE DATABASE lorb2")
# cur.execute("CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar);")

# query = \
#     "SELECT EXISTS (" + \
#     "   SELECT 1 " + \
#     "   FROM   pg_tables" + \
#     "   WHERE  schemaname = 'schema_name'" + \
#     "   AND    tablename = 'table_name'" + \
#     "   );"

# query = \
#     " CREATE TABLE tags" + \
#     "      (" + \
#     "               region VARCHAR(4) NOT NULL," + \
#     "               name VARCHAR(16) NOT NULL," + \
#     "               summonerId INTEGER," + \
#     "               timestamp TIMESTAMP," + \
#     "               rankedSummary VARCHAR(20)," + \
#     "               PRIMARY KEY(question_id, tag_id)" + \
#     "      );"

# query = "CREATE TABLE word( word CHARACTER VARYING NOT NULL, word CHARACTER VARYING NOT NULL);"

# print(cur.execute(query))


