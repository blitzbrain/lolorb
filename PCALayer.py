
import tensorflow as tf


# PCA Layer following the implementation: https://ewanlee.github.io/2018/01/17/PCA-With-Tensorflow/
class PCA_Layer():
    
    def __init__(self, dim, channel, batch_size=256):
        
        # self.alpha = tf.Variable(tf.random_normal(shape=[dim//2,dim//2,channel],dtype=tf.float32,stddev=0.05))
        # self.beta  = tf.Variable(tf.ones(shape=[channel],dtype=tf.float32))

        self.current_sigma = None
        self.moving_sigma = tf.Variable(tf.zeros(shape=[dim * channel], dtype=tf.float32))

        self.dim = dim
        self.channel = channel
        self.batch_size = batch_size

    def feedforward(self, inp, pars):
        update_sigma = []

        # 1. Get the input Shape and reshape the tensor into [Batch, Dim]
        # width, channel = tf.shape(inp)[1], tf.shape(inp)[3]
        # reshape_input = tf.reshape(inp, [self.batch_size, -1])
        # trans_input = tf.shape(inp)[1]

        # 2. Perform SVD and get the sigma value and get the sigma value
        singular_values, u, _ = tf.svd(inp, full_matrices=False)

        def training_fn(): 
            # 3. Training 
            sigma = tf.diag(singular_values)
            # sigma = tf.slice(sigma1, [0,0], [trans_input, self.dim * self.channel])
            pca = tf.matmul(u, sigma)
            update_sigma.append(tf.assign(self.moving_sigma, (self.moving_sigma * 0.9) + (sigma * 0.1) ))
            return pca, update_sigma

        def testing_fn(): 
            # 4. Testing calculate hte pca using the Exponentially Weighted Moving Averages  
            pca = tf.matmul(u, self.moving_sigma)
            return pca, update_sigma

        pca, update_sigma = tf.cond(pars["is_train"], true_fn=training_fn, false_fn=testing_fn)
        # pca_reshaped = tf.reshape(pca,[self.batch_size,(width//2),(width//2),channel])
        # out_put = self.alpha * pca_reshaped +self.beta 
        
        return out_put, update_sigma