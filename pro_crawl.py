#
#  Train pro prediction model
#


from Server import *


remodel = sys.argv[1] == "--remodel" if len(sys.argv) == 2 else False # If present, recompute sq model output
if remodel: pr_fl("Remodelling existing matches...")


# Get process metadata
worker_index = 777
worker_key = socket.gethostname() + ':' + str(worker_index)
create_folder(logs_dir) # Create logs directory


# Load globals
model_dict, model_strs, scales, n_train, n_test, n_pcal = load_ld(models_fn)  # Models
pro_sq_models = load_ld(pro_sq_models_fn)          # Regular models used in the first stage of pro model
pro_model_nsqo, pro_scores_nsqo = load_ld(pro_model_fn + '_nsqo')       # Pro model
pro_model_sqo, pro_scores_sqo = load_ld(pro_model_fn + '_sqo')          # Pro model when we don't have pro stats
pr(("Pro model accuracy (nsqo):", pro_scores_nsqo))
pr(("Pro model accuracy  (sqo):", pro_scores_sqo))
pro_model = {'nsqo': pro_model_nsqo, 'sqo': pro_model_sqo}

infill_data = load_json(infill_data_dir + infill_data_fn)                     # Infill data
rec_infill_data = load_json(infill_data_dir + rec_infill_data_fn)
gsdb = load_global_stats(del_old=worker_index == 0 and not crawl_server)      # Global champion stats

# Connect to postgres
db = sqlalchemy.create_engine(pg_conn_str, pool_size=cauldron_size, max_overflow=cauldron_overflow)

# Define threadpools and access conditions for bandwidth-limited concurrent IO operations
opgg_threadpool = ThreadPoolExecutor(max_workers=n_opgg_threads)  # op.gg HTTP request thread pool
pgdb_threadpool = ThreadPoolExecutor(max_workers=n_pgcn_threads)  # postgres connection thread pool
gsdb_cond = Condition()              # global champion statistics database cache write access condition
pgdb_cond = Condition()              # postgres new connection database object access condition
r_conds = make_r_conds()             # For rito requests
r_tss = make_r_timestamps()

log_vars = OrderedDict([
    ("mp_active", [Condition(), 0]),  # Active match prediction threads counter & write condition
    ("pl_active", [Condition(), 0]),  # Active player profile requests counter & write condition
    ("op_active", [Condition(), 0]),  # Active op.gg HTTP request threads counter & write condition
    ("pg_active", [Condition(), 0]),  # Active postgres connection threads counter & write condition
    ("renew_active", [Condition(), 0]),  # Active profile op.gg renewal threads counter & write condition
    ("pgdb_threadpool", pgdb_threadpool),  # postgres connection thread pool
    ("pgdb_db", db),                  # postgres database object (SQLAlchemy)
    ("pgdb_cond", pgdb_cond),         # postgres new connection database object access condition
])

# Define global variables shared by all threads
X_scales, X_scalesdict, Yr_scales, Yr_scalesdict, X_scaler, X_autolog, X_mins = scales
full_args = (opgg_threadpool, pgdb_threadpool, pgdb_cond,
    db, gsdb, gsdb_cond, r_conds, r_tss, infill_data, rec_infill_data, model_dict,
    model_strs, scales, n_train, n_test, n_pcal, X_scales, X_scalesdict, X_scaler, X_autolog, X_mins,
    Yr_scales, Yr_scalesdict, pro_model, pro_sq_models)

pl_threadpool = ThreadPoolExecutor(max_workers=N_PL)



# Get games from csvs
pro_csvs = glob.glob(data_dir + pro_csvs_dir + "*.csv")
dfs = [pd.read_csv(f, encoding="ISO-8859-1", low_memory=False) for f in pro_csvs]
dfs = [d[d["date"] != ' '] if d["date"].dtype == 'O' else d for d in dfs]
for d_i in range(len(dfs)):
    d = dfs[d_i]
    if d["date"].dtype != 'O':
        # d["date"] = pd.to_numeric(d["date"])
        d["date"] = d["date"].astype(str)
df = pd.concat(dfs)
df = df.sort_values('date')
def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]
gids = f7(list(df["gameid"]))


def ts_dos2unix(ts):
    return 24 * 60 * 60 * (ts - 25569)


data, missing_names = [], set()

# Create list of datapoints:
for gid in gids[len(data):]:
    # if any(g.isalpha() for g in gid):
    #     continue

    d = df[df["gameid"] == gid]
    if all(d["date"].values == ' '):
        pr_fl("Game date unknown... " + gid)
        continue
    # if all(d["league"].values == 'LPL'):
    #     continue
    if all(d["patchno"].values < patch_s2n(patches_all[n_pro_patches - 1])):
        continue

    timestamp = ts_dos2unix(float(d["date"].values[0]))
    d_blue_team = d[d["playerid"] == 100]
    d_red_team = d[d["playerid"] == 200]
    league = d["league"].values[0]
    patch = patch_n2s(d["patchno"].values[0])
    duration = d["gamelength"].values[0] * 60
    split = d["split"].values[0]
    week = d["week"].values[0]
    series_game = d["game"].values[0]

    blue_win = d_blue_team["result"].values[0]
    blue_name = d_blue_team["team"].values[0]

    meta = {l: d_blue_team[l].values[0] for l in pro_metalabels_}
    blue_team = {l: d_blue_team[l].values[0] for l in pro_team_labels}
    red_team = {l: d_red_team[l].values[0] for l in pro_team_labels}
    teams = [blue_team["team"], red_team["team"]]
    teams_sorted = deepcopy(teams)
    teams_sorted.sort()

    # Get series scenario encoded by W-L standing and winstreak
    if '.' in str(series_game):
        series_game = str(series_game).split('.')[1]
    series_count = 0 if series_game == 'T' else int(series_game) - 1
    series_ingame_time = 0
    series_blue_wins = 0
    series_red_wins = 0
    series_blue_winstreak = 0
    series_red_winstreak = 0
    if series_game != '1' and series_game != 'T':       # If this is not the first game in a series
        # First, find the previous datapoint that belongs to the same series
        # (occurred in the past 7 hours, and is between the same teams)
        p_ts = timestamp
        p_i = 0
        x_final = None
        p_series_count = None
        while x_final is None and p_ts > timestamp - (7 * 60 * 60):
            p_i -= 1
            if abs(p_i) > len(data):
                break
            x = data[p_i]
            p_ts = x["meta"]["timestamp"]
            p_teams = [x["blue_team"]["team"], x["red_team"]["team"]]
            p_teams.sort()
            if not teams_sorted == p_teams:
                continue
            p_series_count = x["meta"]["series_count"]
            if p_series_count >= series_count:
                break
            x_final = x

        if x_final is None or p_series_count >= series_count:
            pr_fl("Previous series game not found (series_game", series_game, "teams", teams_sorted)
            series_ingame_time = avg_pro_duration * series_count # Average game time = 1989 seconds
        else:
            pr_fl("Previous series game found!")
            series_ingame_time = x_final["meta"]["duration"] + x_final["meta"]["series_ingame_time"]
            p_blue_win = x_final["meta"]["blue_win"]
            same_sides = x_final["blue_team"]["team"] == blue_name
            same_win = int(not (same_sides ^ bool(p_blue_win)))
            # Set the series standing and winstreak from the previous game
            series_blue_wins = x_final["meta"]["series_" + ("blue" if same_sides else "red") + "_wins"]
            series_red_wins = x_final["meta"]["series_" + ("red" if same_sides else "blue") + "_wins"]
            series_blue_winstreak = x_final["meta"]["series_" + ("blue" if same_sides else "red") + "_winstreak"]
            series_red_winstreak = x_final["meta"]["series_" + ("red" if same_sides else "blue") + "_winstreak"]
            # Update
            if same_win:
                series_blue_wins += 1
                series_blue_winstreak += 1
                series_red_winstreak = 0
            else:
                series_red_wins += 1
                series_red_winstreak += 1
                series_blue_winstreak = 0

    players = []
    n_missing = 0
    for i in range(10):
        playerid = i + 1
        role = pro_role_ord[i]
        d_pl = d[d["playerid"] == playerid]
        player, champion, team = d_pl[["player", "champion", "team"]].values[0]
        if player == 'Jiizuk\x8e':
            player = "Jiizuke"

        # Get solo queue name for player (if known)
        sq_name = "?unknown~ " + player.lower()
        sq_region = "NA"
        if player.lower() in sq_dict:
            sq_region, sq_name = sq_dict[player.lower()]
        else:
            missing_names.add((team, player))
            # pr_fl("MISSING NAME:", team, player)
            n_missing += 1

        pl = {l: d_pl[l].values[0] for l in pro_player_labels_}
        pl = {**pl, **{
            "sq_name": sq_name,
            "sq_region": sq_region.lower(),
            "champion": pro_cd[champion.lower()],
        }}
        players.append(pl)

    if n_missing > pro_missing_max:
        continue

    meta = {**meta, **{
        "timestamp": timestamp,
        "patch": patch,
        "duration": duration,
        "blue_win": blue_win,
        "n_sqnames_missing": n_missing,
        "series_count": series_count,
        "series_ingame_time": series_ingame_time,
        "series_blue_wins": series_blue_wins,
        "series_blue_winstreak": series_blue_winstreak,
        "series_red_wins": series_red_wins,
        "series_red_winstreak": series_red_winstreak,
    }}

    data.append({
        "meta": meta,
        "blue_team": blue_team,
        "red_team": red_team,
        "players": players,
    })

# save_ld(data, pro_data_fn + "_unf")
print("Missing names:")
pr(missing_names)
pr(len(data))

# Define data download function for a league
def dl_league(data, league):
    # for d_i in range(start_index, len(data)):
    data = deepcopy(data)

    # Load existing data if it exists
    if os.path.exists("data/learning_data/" + pro_data_fn + "_unf_" + league + '.data'):
        pr_fl("Loading existing", league, "dataset...")
        data_ = load_ld(pro_data_fn + "_unf_" + league)
        ids_ = [d["meta"]["gameid"] for d in data_]
        for d_i in range(len(data)):
            gid = data[d_i]["meta"]["gameid"]
            # Check we haven't found more solo queue names
            if gid in ids_ and \
              'X' in data_[ids_.index(gid)] and \
              data_[ids_.index(gid)]["meta"]["n_sqnames_missing"] <= data[d_i]["meta"]["n_sqnames_missing"]:
              # not any([data_[ids_.index(gid)]["players"][i]["player"] == "Shad0w" for i in range(10)]) and \
              # data[d_i]['meta']['patch'] not in ['10.4'] and \
              # not any([data_[ids_.index(gid)]["players"][i]["player"] == "Kobbe" for i in range(10)]) and \
              # not any([data_[ids_.index(gid)]["players"][i]["player"] == "Amazing" for i in range(10)]):
              # "n_missing" in data_[ids_.index(gid)]["X"] and \
              # data_[ids_.index(gid)]["X"]["n_missing"] <= data[d_i]["meta"]["n_sqnames_missing"]:
                data[d_i] = data_[ids_.index(gid)]

    for d_i in range(len(data)):
        d = data[d_i]
        flags = []
        n_miss = 0
        preloaded = False

        if d["meta"]["timestamp"] < time.time() - (pro_retrospect_months * 30 * 24 * 60 * 60):
            continue
        if 'X' in d:
            # # if d["meta"]["patch"] != patches_all[0]:
            # # if d["meta"]["patch"] in ['9.12', '9.13', '9.14']:  # Old patches we want to use cached X data for
            # # if d["meta"]["patch"] not in ['9.15', '9.16', "9.17"]:
            if remodel:
                cached_d = {
                    'X': d['X']['X'],
                    'X_labels': d['X']['X_labels'],
                }
                flags.append('ptr_cached:' + json.dumps(parsePyJ(cached_d)))
                pr_fl("Adding precomputed X features to ptr_match request")
                n_miss = d['X']['n_missing']
                preloaded = True
            else:
                continue
            # continue

        meta = d["meta"]
        timestamp, patch, blue_win = meta["timestamp"], meta["patch"], meta["blue_win"]
        pls = d["players"]
        champions = [d_["champion"] for d_ in pls]
        regions = [d_["sq_region"] for d_ in pls]
        sq_names = [d_["sq_name"] for d_ in pls]

        # Create data array
        arr = []
        regs = []
        for i in range(10):
            r = pro_role_ord[i]
            side, role = r.split('_')
            arr.append([cols_all.index(side), roles_all.index(role), champions[i], sq_names[i]])
            regs.append(opgg_regions.index(regions[i]))

        req_d = {
            "data": arr,
            "region_i": regs[0],
            "avg_elo_i": 0,
            "req_i": 7,
            "session_id": "ptr_" + make_shortlink(16),
            "shortlink": -1,
            "comp": -1,
            "timestamp": timestamp,
            "patch": patch,
            "regions": regs,
            "flags": ['ptr_', 'no_player_spec'] + flags,
            "source": "localhost",
        }
        response = None
        try:
            # pr_fl("Submitting request... " + str(regs[0]))
            response = prediction_request_handler(log_vars, req_d, full_args, pl_threadpool)

            perc, perc_ch, perc_pl, req_i, opgg_rcodes, err_code, renewals, pl_percs, _, _, ts, out = response
            # while any(renewals):
            #     time.sleep(1)
            #     req_d["req_i"] = req_i 
            #     req_i += 1
            #     try:
            #         response = prediction_request_handler(log_vars, req_d, full_args, pl_threadpool)
            #     except Exception as e:
            #         pr_fl(type(e))
            #         pr_fl(e)
            #         pr_fl(e.args)
            #         break
            #     perc, perc_ch, perc_pl, req_i, opgg_rcodes, err_code, renewals, pl_percs, _, _, ts, out = response

            errs = [rcode != 200 and rcode != 201 for rcode in opgg_rcodes]
            if not preloaded:
                n_miss = sum(errs)
                if n_miss > 0:
                    # pr_fl("1+ missing players for game:", req_d)
                    pr_fl(n_miss, "missing players for game:", opgg_rcodes)
                    # continue

            
            X, X_labels, X_final, x_ls, pre_feats, output = out
            # if n_succ == 0:
            #     X_ls = x_ls
            # n_succ += 1

            d['X'] = {
                'X': X,
                'X_labels': X_labels,
                'X_final': X_final,
                'x_ls': x_ls,
                'pre_feats': pre_feats,
                'output': output,
                'sq_pred': perc / 100.0,
                'n_missing': n_miss,
            }
        except Exception as e:
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl(type(e))
            pr_fl(e)
            continue

        # if round(perc / 100.0) == blue_win:
        #     correct += 1
        # corr_perc = 100.0 * correct / n_succ

        # pr_fl()
        # pr_fl()
        # pr_fl("Added", n_succ, "/", len(data))
        # pr_fl("correct:", correct, '/', n_succ, '(', corr_perc, ')')
        # pr_fl()
        # pr_fl()

    save_ld(data, pro_data_fn + "_unf_" + league)

    return data



if os.path.exists("data/learning_data/" + pro_data_fn + ".data"):
    pr_fl("Loading previous pro dataset...")
    prev_data = load_ld(pro_data_fn) # Load previous dataset & infill current data variable
    prev_data_dict = {d["meta"]["gameid"]: d for d in prev_data}
    count = 0
    for i in range(len(data)):
        gid = data[i]["meta"]["gameid"]
        # if gid in prev_data_dict and 'X' in prev_data_dict[gid]:
        if gid in prev_data_dict and 'X' in prev_data_dict[gid] and prev_data_dict[gid]["meta"]["patch"] != patches_all[0]:
            count += 1
            data[i] = prev_data_dict[gid]
            pr_fl("Infilled game", gid, "count:", count)

# Group data by pro league (should distribute across riot regions for more requests per second)
d_groups = []
for league in pro_leagues:
    d_groups.append([d for d in data if d["meta"]["league"] == league])

with ThreadPoolExecutor(max_workers=n_parallel_cpu) as executor:
    d_futures = [executor.submit(dl_league,
                                 d_groups[i], pro_leagues[i]) for i in range(len(pro_leagues))]
    # pr_fl("Submitted pro crawling threads...")
    data = sum([d_fut.result() for d_fut in d_futures], [])


save_ld(data, pro_data_fn)
# X_dict = load_ld(pro_data_fn)

save_ld((model_dict, model_strs, scales, n_train, n_test, n_pcal), pro_sq_models_fn)


