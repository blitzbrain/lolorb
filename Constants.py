'''
 
    This file contains shared constants

'''

from UniversalUtils import *


# Import global shell script constants
sh_consts_fn = "Constants.sh"
sh_consts_fn_default = "Constants_default.sh"
sh_consts_prefix = "export "
store_folder = "H:/Code/lolorb/data/learning_data/"

# Copy defaults file if not found and we're running in the lolorb folder
in_lolorb = os.path.exists(sh_consts_fn_default)
if not in_lolorb:
    prev_wd = os.getcwd()
    os.chdir(os.path.dirname(sys.argv[0]))
if not os.path.exists(sh_consts_fn):
    shutil.copyfile(sh_consts_fn_default, sh_consts_fn)
with open(sh_consts_fn, 'r') as f:
    lines = f.readlines()
for line in lines:
    line = line.strip()
    if line[:len(sh_consts_prefix)] == sh_consts_prefix:
        key, value = line.split(sh_consts_prefix)[1].split('=')
        if value[0] == '$':
            value = globals()[value[1:]]
        elif value[0] == '"':
            value = value[1:value[1:].index('"') + 1]
        globals()[key] = value
if not in_lolorb:
    os.chdir(prev_wd)

# Rito API key
rito_key = "RGAPI-7d0ce015-7040-4814-9055-d4821c8a3f0b"

# Champion.gg API key
chgg_key = "01cb835fae51e42ecc3ff928a2517ce0"


########## Options #########

#### Global
n_parallel_cpu = os.cpu_count()  # Number of logical cores to use in parallel
curr_season_id = 15              # Current (pre)season id
n_seasons_to_use = 4             # Number of season ids to use for op.gg champion proficiency (n_seasons + n_preseasons)
n_recent_pcs_seasons = 15        # Number of recent (pre)seasons to download player past data of
# season_start = 1548201600        # Current (pre)season start timestamp (00:00 23rd January 2019)
season_start = 1578614400        # Current (pre)season start timestamp (00:00 10th January 2020)
pyex = PYEXEC                    # Python executable command
crawl_server = os.name == 'nt'   # Whether or not we are running on the crawler backend machine (assuming Windows)

#### Logging (mainly for server monitoring)
conf_fn = "worker_conf.json"
conf_fn_default = "worker_conf_default.json"

#### Data crawling
queues = ["Ranked Solo", "Ranked Flex"] # Queue types we want to collect for
skill_queue = "Ranked Solo" # Default queue to use for player skill estimate
# For initialising from 0 games - ignores existing distribution of games (across
# leagues) and just adds games indiscriminately up to n_per_div per division
# Also works around 1.7 times faster than add_games (use add_games to even
# out the distribution and/or add games for rarer, unsampled divisions)
use_get_games = True # If false, uses add_games
n_per_div = 5000 # Number of games to collect per division & region for get_games
n_per_div_increment = 5 # How much to increase n_per_div by when we fill up
init_games_earliest = n_weeks_ago(70) # Filter seed & current games for get_games by timestamp
n_mh_games = 3 # Number of recent MH games to investigate for each player in a game
# In add_games, Try to find a game to add from one player's match history while the
# number of players encountered from this match history is below this number
# (then switch to looking in a new player's match history) (includes dupes)
n_new_pl = 2000 # Number of players to see before switching to new root in add_games
challenger_cd = 15.0 * 60.0 # add_games cooldown between unsuccessful attempts to
                            # obtain rare division matches
challenger_old_count = 5 # N unsuccessful rare_div requests before going on cooldown
also_get_seed_games = False # Whether to also download seed games (get_matches) concurrently
only_get_seed_games = False # Whether to only download seed games
get_matches_rr_sleep = 1.5 # Time to sleep between get_matches Riot requests (for other threads)
n_recent_m = 2   # Number of recent matches of each player to explore (get_matches)
n_pl_cycle = 25  # Number of random players' to sample the match (get_matches)
                 # histories of, from the most recent games' players
# If > 0, only add games for which we've seen the participants less than this many times
# TODO: Maybe alternate this in order to cycle exploration/exploitation?
new_players_only = 500
# Rare divisions to find games in - allow duplicate players & search cooldown
# Start with just challenger (26), then add master/diamond leagues as we exhaust them
# rare_divs = [26]
# rare_divs = [25, 26]
rare_divs = [24, 25, 26]
# rare_divs = [23, 24, 25, 26]
# rare_divs = [22, 23, 24, 25, 26]
# rare_divs = [21, 22, 23, 24, 25, 26]
# rare_divs = [20, 21, 22, 23, 24, 25, 26]
N_divs = 27 # Number of elo_brackets
# rare_divs = range(N_divs)
always_add_rare = True # Whether get_games should always get games in rare_divs
# Number of concurrent threads per crawl region for op.gg requests - adjust based
# on system network capacity (start at ~150, then reduce until errors stop)
opgg_thread_pool = 20
# Number of threads for concurrent requests for champion.gg webpages
n_chgg_reqpool_workers = 8
# (mininum) request cooldowns for each source server in seconds ( 1 / rate limit )
opgg_cd = 0.2                            # No known rate limit
cwgg_cd = 0.01                           # No known rate limit
blitz_cd = 0.1
# opgg_cd = 0.0001                       # No known rate limit
chgg_cd = (10.0 / 50.0) * 1.05
rito_cd = (2.0 * 60.0 / 100.0) * 1.05
rito_err_cd = 10.0
# Wait randomly up to this many seconds before op.gg requests to avoid spam dos (THEY BLOCKED ME, RIP)
opgg_max_wait = 1
req_fail_reattempts = 5 # Request exception number of reattemps & cooldown period
req_fail_cooldown = 2
max_n_retries = 5  # Champion.gg global stats max number of retries
# Regions to crawl (can have duplicates, one thread for each)
crawl_regions = \
[
    # "EUW1"
    'EUW1','EUN1','NA1','KR','OC1','BR1','RU','LA1','LA2','JP1','TR1',
    'EUW1','EUN1','NA1','KR','OC1','BR1','RU','LA1','LA2','JP1','TR1',
    'EUW1','EUN1','NA1','KR','OC1','BR1','RU','LA1','LA2','JP1','TR1',
    # 'EUW1','EUN1','NA1','KR','OC1','BR1','RU','LA1','LA2','JP1','TR1',
]

# crawl_regions = \
#     ['EUW1','EUN1','NA1','KR','KR',#'KR','KR',
#      'OC1','BR1','RU','LA1','LA2','JP1','TR1']
# Regions for which to add an extra add_games thread to even out their sample distribution
# even_out_regions = []
# even_out_regions = ['KR', 'RU', 'OC1', 'JP1']
even_out_regions = \
[
    # "EUW1"
    # 'EUW1','EUN1','NA1','KR','OC1','BR1','RU','LA1','LA2','JP1','TR1',
    # 'EUW1','EUN1','NA1','KR','OC1','BR1','RU','LA1','LA2','JP1','TR1',
]

# Data files/folders
data_dir = "data/"                     # Data folder name (ignored by .gitignore)
logs_dir = "logs/"                     # Logs folder
curr_games_csv = "curr_games.csv"      # Current fully-collected-info games (id+elo)
curr_players_csv = "curr_players.csv"  # Players in these games (w/ their elo)
curr_games_jsons = "H:\\Code/lolorb/data/curr_games_jsons/" # Jsons for those games (full requests)
curr_data_jsons = "H:\\Code/lolorb/data/curr_data_jsons/"   # Full collected data jsons (combined)
# curr_games_jsons = data_dir + "curr_games_jsons/" # Jsons for those games (full requests)
# curr_data_jsons = data_dir + "curr_data_jsons/"   # Full collected data jsons (combined)
init_games_csv = "init_games.csv"      # Initial games
init_games_dir = "init_games/"         # Initial games folder
matchlists_csv = "init_matchlists.csv" # Initial matchlists
matchlists_dir = "init_matchlists/"    # Initial matchlists folder
infill_data_dir = "infill_data/"       # Saved intermediate learning data
learning_data_dir = "learning_data/"   # Saved intermediate learning data
pro_csvs_dir = "pro_history/"          # Pro match history from oracleselixir.com/match-data (converted from xlsx)
soloqueue_names_json = "sq_names"      # Solo queue pro player names
rito_cache = "riot_cache"              # Rito requests cache
opgg_cache = "opgg_cache"              # op.gg requests cache
chgg_cache = "chgg_cache"              # Champion.gg requests cache
graphs_folder = "graphs/"              # Saved graphs
all_patches_file = "patches_all.json"  # Data patches list (updated automatically)
all_patches_file_default = "patches_all_default.json"
# datadragon files (todo: automatically update this)
# ddrag_ver = "8.23.1"
# ddrag_dir = "datadragon_cache/dragontail-" + ddrag_ver + "/" + ddrag_ver + "/data/en_US/"
ddrag_dir = "datadragon_cache/"
selected_feats_json = "selected_feats.json" # Selected features for final models

r_cache = {                       # Runtime cache store
    "champions": {
        "path": rito_cache + '/' + "champions",
        "data": None,
        "ts": 0,                  # Timestamp of most recent response
        "ls": 9999.0 * 24.0 * 60.0 * 60.0,# Lifespan of response (before expiry)
        "del_old": True,          # Whether to only keep most recent response
        "archive": {},            # Previous loaded data indexed by timestamp
        "loaded": False,          # Whether an attempt has been made to load
    },
    "opgg_stats": {
        "path": opgg_cache + '/' + "stats",
        "data": None,
        "ts": 0,
        "ls": 1.0 * 24.0 * 60.0 * 60.0,
        "del_old": False,
        "archive": {},
        "loaded": False,
    },
    "chgg_champions": {
        "path": chgg_cache + '/' + "champions",
        "data": None,
        "ts": 0,
        "ls": 1.0 * 24.0 * 60.0 * 60.0,
        "del_old": False,
        "archive": {},
        "loaded": False,
    },
    "chgg_matchups": {
        "path": chgg_cache + '/' + "matchups",
        "data": None,
        "ts": 0,
        "ls": 1.0 * 24.0 * 60.0 * 60.0,
        "del_old": False,
        "archive": {},
        "loaded": False,
    },
}
dirs_to_create = [rito_cache, opgg_cache, chgg_cache]


########## Constants #########

N_PL = 10   # Number of players in a match
N_T = 2     # Number of teams

rito_queues = {
    "Ranked Solo" : "RANKED_SOLO_5x5",
    "Ranked Flex": "RANKED_FLEX_SR",
}
rito_queue_ids = {
    "CUSTOM_GAME": 0,
    "NORMAL_BLIND_OLD": 2,
    "RANKED_SOLO_5x5_OLD": 4,
    "Legend of the Poro King": 300,
    "NORMAL_DRAFT": 400,
    "RANKED_DYNAMIC_5x5": 410,
    "RANKED_SOLO_5x5": 420,
    "NORMAL_BLIND": 430,
    "RANKED_FLEX_SR": 440,
    "ARAM": 450,
}

# Note: need to make this automatic
seasons_all = [ 13, 12, 11 ]

# seasons_all = [ 8, 9, 10, 11 ]
# patches_all = [ "8.3", "8.2", 
#     "8.1", "7.24", "7.23", "7.22", "7.21", "7.20", "7.19", "7.18", "7.17", 
#     "7.16", "7.15", "7.14", "7.13", "7.12", "7.11", "7.10","7.9", "7.8",
#     "7.7", "7.6", "7.4", "7.5", "7.3", "7.2", "7.1", "6.24", "6.23", "6.22" ]

league_ns = {               # League numbers
    "challenger": 26,
    "grandmaster": 25,
    "master": 24,
    "diamond": 20,
    "platinum": 16,
    "gold": 12,
    "silver": 8,
    "bronze": 4,
    "iron": 0,
    "unranked": -1
}

# league_ns = {               # League numbers
#     "challenger": 26,
#     "master": 25,
#     "diamond": 20,
#     "platinum": 15,
#     "gold": 10,
#     "silver": 5,
#     "bronze": 0,
#     "unranked": -1
# }

league_ns_avgs = {
    "challenger": 26,
    "grandmaster": 25,
    "master": 24,
    "diamond": 21,
    "platinum": 17,
    "gold": 13,
    "silver": 9,
    "bronze": 5,
    "iron": 1,
    "unranked": -1
}

# league_ns_avgs = {
#     "challenger": 26,
#     "master": 25,
#     "diamond": 22,
#     "platinum": 17,
#     "gold": 12,
#     "silver": 7,
#     "bronze": 2,
#     "unranked": -1
# }

leag_tiers = ["unranked", "iron", "bronze", "silver", "gold", "platinum", "diamond",
              "master", "grandmaster", "challenger"]
opgg_elos = ['all', 'iron', 'bronze', 'silver', 'gold', 'platinum', 'diamond',
             'master', 'grandmaster', 'challenger']
chgg_elos = ['IRON', 'BRONZE', 'SILVER', 'GOLD', 'PLATINUM', '']
    #'PLATINUM,DIAMOND,MASTER,CHALLENGER' ] # changed to '' for some reason

# leag_tiers = ["unranked", "bronze", "silver", "gold", "platinum", "diamond",
#               "master", "challenger"]
# opgg_elos = ['all', 'bronze', 'silver', 'gold', 'platinum', \
#               'diamond', 'master', 'challenger']
# chgg_elos = ['BRONZE', 'SILVER', 'GOLD', 'PLATINUM', '']
#     #'PLATINUM,DIAMOND,MASTER,CHALLENGER' ] # changed to '' for some reason

# op.gg short league dict
opgg_shortleag = {
    "C1": 26,
    "GM1": 25,
    "M1": 24,
    "D1": 23,
    "D2": 22,
    "D3": 21,
    "D4": 20,
    "P1": 19,
    "P2": 18,
    "P3": 17,
    "P4": 16,
    "G1": 15,
    "G2": 14,
    "G3": 13,
    "G4": 12,
    "S1": 11,
    "S2": 10,
    "S3": 9,
    "S4": 8,
    "B1": 7,
    "B2": 6,
    "B3": 5,
    "B4": 4,
    "I1": 3,
    "I2": 2,
    "I3": 1,
    "I4": 0,
}

# Average elos for each league division
league_elo_avg_dict = OrderedDict([
    ("challenger",  2400 + 500),
    ("grandmaster", 2400 + 300),
    ("master", 2400 + 100),
])
for league in leag_tiers[1:7][::-1]:
    for i in range(1, 5):
        league_elo_avg_dict[league + '_' + str(i)] = \
            (league_ns[league] * 100) + (100 * (4 - i)) + 50
league_elo_avgs = list(league_elo_avg_dict.values())

# Server & request details
opgg_serv = "http://localhost:1337/"
chgg_serv = "http://api.champion.gg/v2/"
cwgg_serv = "https://champion.gg/"
rito_serv = ".api.riotgames.com/lol/"
https = "https://"

matchlist_by_acc_id = rito_serv + "match/v4/matchlists/by-account/"
league_pos_by_summ_id = rito_serv + "league/v4/entries/by-summoner/"
game_by_game_id = rito_serv + "match/v4/matches/"
summ_by_acc_id = rito_serv + "summoner/v4/summoners/by-account/"
summ_by_name = rito_serv + "summoner/v4/summoners/by-name/"
v3_summ_by_acc_id = rito_serv + "summoner/v3/summoners/by-account/"
v3_summ_by_summ_id = rito_serv + "summoner/v3/summoners/"
# champions_url = rito_serv + "static-data/v3/champions?dataById=true"

# Rito Response fields shortcuts
g_acc_id = "currentAccountId"
g_summ_id = "summonerId"
g_name = "summonerName"
g_pis = "participantIdentities"

# opgg/chgg shortcuts
opgg_rs_ = "RankedSummary"
opgg_pcs_ = "Champions"
opgg_gcs_ = "Stats"
opgg_ca_ = "AnalyticsByChampion"
opgg_ma_ = "MatchupAnalytics"
chgg_gcs_ = "Champions"
chgg_gms_ = "Matchups"

# Server request variables
opgg_mapId = 1              # One of [1, 10, 12]
opgg_queue = "ranked"       # One of ["ranked", "normal", "aram"]
opgg_periods = ["month", "week", "today"]
opgg_types = ["win"]  # Only useful metric right now
# opgg_types = ["win", "lose", "picked", "banned"]

rito_regions = \
  ['EUW1','EUN1','NA1','KR','OC1','BR1','RU','LA1','LA2','JP1','TR1']#,'PBE1']
# opgg_regions = ['euw','eune','na','kr','oce','br','ru','lan','las','jp','tr','sg','ph','tw','vn','th']
opgg_regions = ['euw','eune','na','kr','oce','br','ru','lan','las','jp','tr','sg','id','ph','tw','vn','th'] # id - indonesia, 
opgg_regions_old = ['euw','eune','na','kr','oce','br','ru','lan','las','jp','tr','sg','id','ph','tw','vn','th'] # id - indonesia, 
# opgg_regions = ['euw','eune','na','kr','oce','br','ru','lan','las','jp','tr']

region_timezones = {  # Approximate time zone for each region
    "euw": 0.5,
    "eune": 1.5,
    "na": -6.5,
    "kr": 9,
    "oce": 9.5,
    "br": -3,
    "ru": 3,
    "lan": -6,
    "las": -5,
    "jp": 9,
    "tr": 3,
    "sg:": 8,
    "id": 7,
    "ph": 8,
    "tw": 8,
    "vn": 7,
    "th": 7,
}

seed_acc_ids = { # Seed account ids to start riot data crawler
    "EUW1": [227439557, ],
    "EUN1": [35675853, ],
    "NA1": [233967184, ],
    "KR": [202571162, ],
    "OC1": [200299699, ],
    "BR1": [202166203, ],
    "RU": [204075860, ],
    "LA1": [202321021, ],
    "LA2": [200765841, ],
    "TR1": [207729635, ],  # RB Closer
    "JP1": [200686449, ],
    # "PBE1": [227439557, ],
}

# seed_acc_ids = { # Seed account ids to start riot data crawler
#     "EUW1": ["AVPKmG4Z5-i3AaE4PON8FZJ_8FdYpE71I19bqOu8rwDPT44", ],
#     "EUN1": [35675853, ],
#     "NA1": [233967184, ],
#     "KR": [202571162, ],
#     "OC1": [200299699, ],
#     "BR1": [202166203, ],
#     "RU": [204075860, ],
#     "LA1": [202321021, ],
#     "LA2": [200765841, ],
#     "TR1": [207729635, ],  # RB Closer
#     "JP1": [200686449, ],
#     # "PBE1": [227439557, ],
# }

# Access condition identifiers
# (
#   gf = games csv db (just riot data)
#   mf = matchlists csv db
#   cg = current games csv db (current dataset = riot + op.gg + champion.gg datapoints)
#   cp = current players csv db
#   ga = games being added set cond
#   cr = Champion.gg API requests
#   cw = Champion.gg requests
#   bz = Blitz iesdev requests
#   or = op.gg requests
#   rr_[region] = Riot API requests
# )
r_ids = ['gf', 'mf', 'cg', 'cp', 'ga', 'cr', 'cw', 'bz', 'or'] + \
        ['rr_' + reg.lower() for reg in rito_regions]

chgg_roles = {
    'TOP': 'top',
    'JUNGLE': 'jungle',
    'MIDDLE': 'middle',
    'DUO_SUPPORT': 'support',
    'DUO_CARRY': 'adc',
    'SYNERGY': 'synergy',
    'ADCSUPPORT': 'adc_support',
}
chgg_role_pref_transitions = {
    "middle": ["top","adc","jungle","support"],
    "top": ["middle","adc","jungle","support"],
    "adc": ["middle","top","jungle","support"],
    "jungle": ["top", "adc", "middle", "support"],
    "support": ["adc", "top", "middle", "jungle"],
    "synergy": ["synergy"],
    "adc_support": ["adc_support"],
    "support_adc": ["support_adc"],
}
roles_all = [
    "top",
    "jungle",
    "middle",
    "support",
    "adc",
]

cols_all = ["blue", "red"]
blue_team_id, red_team_id = 100, 200
sr_map_id = 11

spells_lib = OrderedDict([
    (4, "flash"),
    (6, "ghost"),
    (7, "heal"),
    (3, "exhaust"),
    (1, "cleanse"),
    (12, "teleport"),
    (14, "ignite"),
    (21, "barrier"),
    (13, "clarity"),
    (11, "smite"),
])

pick_order_map = OrderedDict([
    (1, 1),
    (2, 4),
    (3, 5),
    (4, 8),
    (5, 9),
    (6, 2),
    (7, 3),
    (8, 6),
    (9, 7),
    (10, 10),
])

# List to distinguish between adc & support champions bot lane (not currently used)
support_champs = [
    'Leona',
    'Nautilus',
    'Sejuani',
    'Lulu',
    'Malzahar',
    'Ornn',
    'Kayle',
    'Shen',
    'Lux',
    'Alistar',
    'Sion',
    'Soraka',
    'Teemo',
    'Warwick',
    'Kled',
    'Hecarim',
    'Annie',
    'Ekko',
    'Galio',
    'Lissandra',
    'LeBlanc',
    'Fiddlesticks',
    'Nunu & Willump',
    'Jax',
    'Morgana',
    'Zilean',
    'Singed',
    'Vi',
    'Rakan',
    'Syndra',
    'Aurelion Sol',
    'Thresh',
    'Karthus',
    "Cho'Gath",
    'Amumu',
    'Rammus',
    'Anivia',
    'Shaco',
    'Dr. Mundo',
    'Sona',
    'Kassadin',
    'Zoe',
    'Zyra',
    'Illaoi',
    'Nami',
    "Rek'Sai",
    'Ivern',
    'Janna',
    'Gangplank',
    'Karma',
    'Taric',
    'Veigar',
    'Trundle',
    'Zac',
    'Bard',
    'Swain',
    'Blitzcrank',
    'Malphite',
    'Katarina',
    'Nocturne',
    'Maokai',
    'Renekton',
    'Jarvan IV',
    "Vel'Koz",
    'Taliyah',
    'Camille',
    'Braum',
    'Elise',
    'Wukong',
    'Brand',
    'Lee Sin',
    'Rumble',
    'Skarner',
    'Heimerdinger',
    'Nidalee',
    'Udyr',
    'Poppy',
    'Gragas',
    'Xerath',
    'Shyvana',
    'Tahm Kench',
    'Fizz',
    'Volibear',
    'Pantheon',
    'Rengar',
    'Yorick',
    'Akali',
    'Kennen',
    'Garen',
    'Pyke',
]

seen_code = "205"
succ_msg = "200 Success"

mlists_cols = ['a_id', "region", "timestamp"]
games_cols = ['g_id', "region", "timestamp"]
cg_cols = ['g_id', "region", "division", "timestamp"]
cp_cols = ['a_id', "region", "division", 'g_id', "timestamp"]

r_queues = [rito_queues[q] for q in queues]
r_queue_ids = [rito_queue_ids[q] for q in r_queues]
r_queue = rito_queues[skill_queue]
r_queue_id = rito_queue_ids[r_queue]
queue_names = invert_dict(rito_queue_ids)
spell_ids = invert_dict(spells_lib)
smite_id = spell_ids["smite"]

curr_games_path = data_dir + curr_games_csv
curr_players_path = data_dir + curr_players_csv

N_PL_r = range(N_PL)
N_T_r = range(N_T)

opgg_region_dict = dict([(rito_regions[i],
                          opgg_regions[i]) for i in range(len(rito_regions))])
leag_tier_is = dict(zip(leag_tiers, [leag_tiers.index(t) for t in leag_tiers]))
t_roles = sum([[c + '_' + r for r in roles_all] for c in cols_all], [])


# op.gg API data labels/keys

opgg_rs_keys = [
    'summonerId',
    'league',
    # 'leagueName',
    'lp',
    'wins',
    'losses',
    'winRatio',

    'recent',
    'games',
]

opgg_rs_recent_keys = [
    'winRatio',
    'wins',
    'losses',
    'games',
    'killsAverage',
    'deathsAverage',
    'assistsAverage',
    'kdaRatio',
    'killParticipation',
]

opgg_rs_game_keys = [
    'type',
    'timestamp',
    'length',
    'result',
    'champion',
    'spell1',
    'spell2',
    'kills',
    'deaths',
    'assists',
    'ratio',
    'level',
    'cs',
    'csps',
    'pinksPurchased',
    'killParticipation',
    # 'items',
    # 'team1',
    # 'team2',
]

# opgg_rs_game_keys_strs = [
    
# ]

opgg_pcs_keys = [
    'rank',
    'name',
    'wins',
    'losses',
    'winRatio',
    'kills',
    'deaths',
    'assists',
    'ratio',
    'gold',
    'cs',
    # 'turrets',
    'maxKills',
    'maxDeaths',
    'damageDealt',
    'damageTaken',
    'doubleKill',
    'tripleKill',
    'quadraKill',
]

opgg_rs_db_keys = opgg_rs_keys[:-2] + \
    ["recent_" + k for k in opgg_rs_recent_keys] + \
    ["games"]


recent_keys_dict_thing = {
    "kills": "killsAverage",
    "deaths": "deathsAverage",
    "assists": "assistsAverage",
    "kda_ratio": "kdaRatio",
    "kill_participation": "killParticipation",
}


cdata_ = None


unique_caps_chars = ['Ø', 'ø', 'Δ', 'δ', '𝛿', 'º', 'ª', 'É', 'é', 'Ô', 'ô', 'Ð', 'ð', 'Ł', 'ł', 'Ä', 'ä', 'Ά', 'ά',
                     'Ή', 'ή', 'Ξ', 'ξ', 'Ê', 'ê', 'Ç', 'Ą', 'Ü', 'Ï', 'ï', 'Χ', 'χ', 'ῖ', 'Κ', 'κ', 'ϰ', 'ι', 'Ñ']
def lowercasify(s):
    return s
    # return ''.join([c.lower() if c not in unique_caps_chars else c for c in s])

