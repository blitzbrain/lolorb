#
#  This file is simply a cheatsheet for common commands used when setting up the site
#


# nginx config
sudo nano /etc/nginx/conf.d/server.conf
sudo nginx -s reload
sudo cp /etc/nginx/conf.d/server.conf ~/lorb/server.conf
sudo cp ~/lorb/server.conf /etc/nginx/conf.d/server.conf

# Non-korean update procedure
sudo mkdir /www
sudo mkdir /www/public
sudo rm -rd /www/public
sudo cp -rd ~/lorb/site/public /www/

# Korean update procedure
sudo rm -rdf /home/ubuntu/staging
cp -rd /home/ubuntu/lorb/site/public /home/ubuntu/staging
python3 kr_modify.py
sudo rm -rdf /www/public
sudo cp -rd /home/ubuntu/staging /www/public

# Regionless update procedure
cd /home/ubuntu/lorb/
/usr/bin/sudo -u ubuntu /usr/bin/git pull
/usr/bin/sudo rm -rdf /home/ubuntu/staging
/usr/bin/sudo -u ubuntu cp -rd /home/ubuntu/lorb/site/public /home/ubuntu/staging
cd /home/ubuntu/lolorb/
/usr/bin/sudo -u ubuntu /usr/bin/python3 kr_modify.py
/usr/bin/sudo rm -rdf /www/public
/usr/bin/sudo cp -rd /home/ubuntu/staging /www/public

# startup script
sudo nano /etc/rc.local
sudo cp /etc/rc.local ~/lorb/rc.local
sudo cp ~/lorb/rc.local /etc/rc.local
sudo cp /etc/rc.local ~/lolorb/rc.local
sudo cp ~/lolorb/rc.local /etc/rc.local

# Copy over key file
scp -i $PEM_FILE $PEM_FILE ubuntu@$HOST_URL:~/
chmod 600 $PEM_FILE


