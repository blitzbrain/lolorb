#
#  Modify index.html and index.js for korean server
#


from ServerConstants import *


if worker_conf["regions"] != 'kr':
    sys.exit()


site_folder = "/home/ubuntu/staging/"
html_file = "index.html"
js_file = "index.js"


new_lines = []
with open(site_folder + html_file, 'r') as f:
    for line in f.readlines():
        if '<option value="KR">' in line:
            new_lines.append(line.replace('<option value="KR">', '<option value="KR" selected="selected">'))
        else:
            new_lines.append(line)
with open(site_folder + html_file, 'w') as f:
    f.writelines(new_lines)


new_lines = []
with open(site_folder + js_file, 'r') as f:
    for line in f.readlines():
        if 'const in_korea = false;' in line:
            new_lines.append(line.replace('const in_korea = false;', 'const in_korea = true;'))
        else:
            new_lines.append(line)
with open(site_folder + js_file, 'w') as f:
    f.writelines(new_lines)


