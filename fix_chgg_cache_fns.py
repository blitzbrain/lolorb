#
#  Add patches to champion.gg cache filenames
#


import glob

from Utils import *


file_list = sorted(glob.glob(data_dir + chgg_cache + "/champions/*.json"))
# print(file_list)
for file in file_list: #[::-1]: #[:1]:
    d = json.load(open(file))
    patch = d[list(d.keys())[0]][0][0]["patch"]
    print(patch)
    fn = file.split('.json')[0]
    if '_' + patch not in fn:
        fn += '_' + patch + '.json'
        os.rename(file, fn)


file_list = sorted(glob.glob(data_dir + chgg_cache + "/matchups/*.json"))
for file in file_list: #[::-1]: #[:1]:
    d = json.load(open(file))
    patch = d[list(d.keys())[0]][0][0]["patch"]
    print(patch)
    fn = file.split('.json')[0]
    if '_' + patch not in fn:
        fn += '_' + patch + '.json'
        os.rename(file, fn)


