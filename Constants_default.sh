#
#  Shell script constants
#


export GIT_USER="lorbgg"
# export GIT_USER="jigawhat"
export GIT_PASS="???"


export REPO_NAMESPACE="blitzbrain"
export REPO_HOST="gitlab.com"
# export REPO_BRANCH="production"
export REPO_BRANCH="master"

export PG_USER="lorb"
export PG_PASS="turbo phoenix lightning"
export PG_NAME="lorbgg"

export RMQ_USER="lorb"
export RMQ_PASS="googargoyle"

export NPM_VER="10.13.0"

export PYEXEC="python3"


export PEM_FILE_NA="~/lorb-sf.pem" # San Francisco (us-west-1) AWS key
export PEM_FILE_KR="~/lorb-kr.pem" # Seoul (ap-northeast-2) AWS key
export HOST_URL_NA="lorb.gg"
export HOST_URL_KR="www.lorb.gg"
export NGINX_CONF_NA="server.conf"
export NGINX_CONF_KR="server_kr.conf"


### Config for Korea (KR) or all other regions (NA)
export HOST_URL=$HOST_URL_NA
export PEM_FILE=$PEM_FILE_NA
export NGINX_CONF=$NGINX_CONF_NA
# export HOST_URL=$HOST_URL_KR
# export PEM_FILE=$PEM_FILE_KR
# export NGINX_CONF=$NGINX_CONF_KR


