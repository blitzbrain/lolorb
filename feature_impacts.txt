16.11393014992825,Momentum/Tilt win rate
4.091523902682513,Momentum/Tilt Player champion avg games
3.978045704331125,Player champion avg deaths
3.4318309782892342,Momentum/Tilt deaths NORMALIZED BY Global champion deaths (Champion.gg)
2.9724315522810594,Player champion avg assists
2.85303506697364,Momentum/Tilt assists NORMALIZED BY Global champion assists (Champion.gg)
2.7448100924562016,Momentum/Tilt kill participation
2.744543795067687,Momentum/Tilt deaths NORMALIZED BY Player champion avg deaths
2.6950276919964185,Momentum/Tilt kills NORMALIZED BY Global champion kills (Champion.gg)
2.386646802506076,Momentum/Tilt assists NORMALIZED BY Player champion avg assists
2.3464323422962883,Momentum/Tilt assists
2.238954618181665,Momentum/Tilt duration
2.2107555528033798,Momentum/Tilt creep score NORMALIZED BY Player champion avg creep score
2.1450388949739096,Momentum/Tilt Player champion avg mean rank
2.0501936077971665,Momentum/Tilt creep score NORMALIZED BY Global champion creep score (today)
1.9998748609448365,Momentum/Tilt creep score
1.987015386231934,Player champion avg gold
1.9681737533181327,Player champion avg damage taken
1.9298890651326452,Momentum/Tilt pinks purchased NORMALIZED BY Global champion wards placed (Champion.gg)
1.9070920149754478,Momentum/Tilt Player champion avg losses
1.8956177314252634,Momentum/Tilt Player champion avg wins
1.8182304211036855,Momentum/Tilt pinks purchased
1.8111522512881753,Momentum/Tilt Global champion win rate (Champion.gg)
1.78996834426907,Momentum/Tilt kills NORMALIZED BY Player champion avg kills
1.6993086268051971,Momentum/Tilt deaths
1.6797344454607563,Momentum/Tilt Global champion win rate (today)
1.676847787314241,Momentum/Tilt pinks purchased NORMALIZED BY Global champion wards killed (Champion.gg)
1.6763869627456018,Momentum/Tilt Player champion avg damage taken NORMALIZED BY Global champion total damage taken (Champion.gg)
1.6073164649077598,Momentum/Tilt kills
1.5900893466621482,Momentum/Tilt time since match
1.5096629031883144,Global champion assists (Champion.gg)
1.4915835879710997,Global champion total damage taken (Champion.gg)
1.396959463103664,Player champion avg games
1.3889186464389482,elodeviation
1.3663985066173043,Player champion avg losses
1.3316310598729488,Momentum/Tilt Player champion avg gold NORMALIZED BY Global champion gold (today)
1.3132101108003462,Player champion avg wins
1.252196992572506,Player champion avg alltotal games
1.112932394264951,Global champion deaths (Champion.gg)
0.9890248356673066,Player champion avg kills
0.8996918323497949,Player champion avg mean rank
0.7957119363276953,Player champion avg deaths
0.724001126249921,Player champion avg gold
0.7131338604888623,Momentum/Tilt Player champion avg Bayes-corrected win rate
0.6757874494200655,Global champion duration 30 to 35 win rate (Champion.gg)
0.6631292984541406,Global champion duration 30 to 35 games fraction (Champion.gg)
0.6623800523346696,Global champion gold (Champion.gg)
0.5848996024615006,Player champion avg creep score
0.56815070355245,Global champion play rate (Champion.gg)
0.5484698280059973,Player champion avg kills
0.5293875513514177,Player champion avg assists
0.5284035609089368,Global matchup creep score (Champion.gg)
0.49025048063334753,Global champion duration 35 to 40 win rate (Champion.gg)
0.4722261122743439,Player champion avg wins
0.46407423764720324,Global champion creep score (today)
0.446266779312063,Player champion avg losses
0.41839530623700383,Global champion gold (today)
0.4080684680047002,Player all Fighter champ losses
0.40723455151454496,Player champion avg creep score
0.40579262912669856,Global champion duration 25 to 30 games fraction (Champion.gg)
0.40102935505238857,Player champion avg damage taken
0.38294253297316666,Global champion total physical damage (Champion.gg)
0.3691279049410049,Global matchup gold (Champion.gg)
0.3689437400606657,Player all Assassin champ losses
0.36460803669047726,Global champion total damage (Champion.gg)
0.3515620987447947,Global champion win rate (today)
0.3509113745260495,Global champion duration 20 to 25 games fraction (Champion.gg)
0.3472201189118459,Global champion total heal (Champion.gg)
0.3459773354395059,Player all Tank champ wins
0.33478240302762463,Global champion duration 20 to 25 win rate (Champion.gg)
0.33347734259976264,season Bayes-corrected win rate
0.33061804054452887,Player champion avg damage dealt
0.32349537175257187,Global champion duration 35 to 40 games fraction (Champion.gg)
0.32308801398775994,Player all Assassin champ wins
0.32015716739812966,Global champion win rate (Champion.gg)
0.3183710260412071,Player all Tank champ losses
0.30796146071903985,Player all Fighter champ wins
0.2996953085498155,Player champion avg alltotal wins
0.29527239253827287,Global champion duration 25 to 30 win rate (Champion.gg)
0.2950709246685387,Global champion wards killed (Champion.gg)
0.2928687396274032,Global champion wards placed (Champion.gg)
0.2919226275389931,Global champion total magic damage (Champion.gg)
0.28454502571024987,Global champion duration 40 plus games fraction (Champion.gg)
0.2803970992426134,Player all Marksman champ losses
0.2749409246780702,Player all Mage champ wins
0.27143390710684706,Global champion kills (Champion.gg)
0.2710479939619407,Global champion duration 40 plus win rate (Champion.gg)
0.26542813926226316,Global champion duration 15 to 20 games fraction (Champion.gg)
0.2612387418933434,Player all Mage champ losses
0.25008726638918477,Player all Support champ losses
0.24992020518088487,Player champion avg alltotal wins
0.2444082773015151,Player all Support champ wins
0.2421772504535618,Player champion avg alltotal Bayes-corrected win rate
0.23968030618759292,Global champion kill sprees (Champion.gg)
0.2386767550639995,Player champion avg alltotal losses
0.21290625035727012,Global matchup weighed score (Champion.gg)
0.19463282834610546,Global matchup win rate (Champion.gg)
0.1941595040308045,Global champion total true damage (Champion.gg)
0.19379352006377737,Player champion avg alltotal losses
0.190786744711901,Global matchup total damage dealt to champions (Champion.gg)
0.18899387607547186,Global matchup wins (Champion.gg)
0.16781756914543658,Player all Marksman champ wins
0.1629520656670892,Player champion avg alltotal Bayes-corrected win rate
0.13321231526906843,Elo
0.11586180752388027,Global matchup adc support wins
0.104993445202058,Global matchup synergy support deaths
0.10469322774782415,Global champion duration 0 to 15 games fraction (Champion.gg)
0.10281930153419674,Global champion duration 15 to 20 win rate (Champion.gg)
0.1013727481243448,Player champion avg Bayes-corrected win rate
0.10105247851474336,Global matchup synergy support kills
0.08589899759215852,Global matchup synergy adc kills
0.0761298996288286,Global matchup synergy support wins
0.06764746371914183,Global matchup synergy support win rate
0.06560794049119095,Global champion duration 0 to 15 win rate (Champion.gg)
0.06546924144273819,Player champion avg Bayes-corrected win rate
0.06001554870787066,Global matchup adc support win rate
0.057916085286594365,Global matchup adc support kills
0.053323762617258554,Global matchup synergy adc wins
0.04774412658687965,Global matchup support adc win rate
0.047557365482458855,Global matchup synergy adc win rate
0.03912358445368432,Global matchup synergy adc deaths
0.03792870698267582,Global matchup support adc wins
0.024718854037702437,Global matchup adc support deaths