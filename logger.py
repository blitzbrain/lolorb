#
#  Logger
#


from Monitoring import *


# Get all public ips of worker servers
for host in hosts:
    host['ips'] = get_public_ips(host["addr"], host["pem"])

# Connect pipes for hosts
for host in hosts:
    subprocess.Popen(pyex + " piper.py " + host["addr"] + ' ' + host["pem"] + ' ' + \
                     json.dumps(host["ips"]).replace(' ', '').replace('"', "'") + ' 37420777')

while True:
    time.sleep(0.1)


