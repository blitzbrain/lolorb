#
#  Convert dataset infill cache for use in server
#


from Learning import *


ts = sorted(glob.glob(data_dir + infill_data_dir + '*.json'))[-1]
print(ts)
filler = load_json(ts, pad=False)
print(len(filler), type(filler))

fill = filler["fill"]
print("\nAveraging filler data...")
fl_i, max_fl = 0, str(len(fill))
for k in fill:
    lib = fill[k]
    if "_name" in lib:
        fkey = lib["_name"]
        fkey_vs1 = filler_keys[fkey]
        for v1 in fkey_vs1:
            lib1 = lib[str(v1)]
            if "_name" in lib1:
                fkey = lib1["_name"]
                if fkey not in filler_keys.keys():
                    print()
                    print(v1, fkey, filler_keys.keys())

                fkey_vs2 = filler_keys[fkey]
                for v2 in fkey_vs2:
                    if str(v2) not in lib1:
                        print()
                        print(v1, fkey, v2, lib1.keys())
                    lib2 = lib1[str(v2)]
                    if "_name" in lib2:
                        fkey = lib2["_name"]
                        fkey_vs3 = filler_keys[fkey]
                        for v3 in fkey_vs3:
                            lib3 = lib2[str(v3)]
                            lib3["_data"] = np.mean(lib3["_data"])
                    else:
                        lib2["_data"] = np.mean(lib2["_data"])
            else:
                lib1["_data"] = np.mean(lib1["_data"])
    else:
        lib["_data"] = np.mean(lib["_data"])
    fl_i += 1
    sys_print("\rFill value # " + str(fl_i) + '/' + max_fl)

del filler["flags"]
del filler["rec_flags"]

save_json(filler, infill_data_dir + "infill_data")


