''' 

    get_matches.py LoL Crawler

'''

#
# This script runs the crawler to continuously gather a bunch of recent games &
# matchlists, until the API key expires or the script is manually stopped
#

from DataRequests import *



# Get matches
def get_matches(r_conds, r_tss, region):

    # Collect data only for games occuring after this timestamp (some days ago)
    oldest_game_ts = time.time() - (5.0 * 24.0 * 60.0 * 60.0)

    # Define containers for results
    create_folder(data_dir)
    create_folder(data_dir + init_games_dir)
    create_folder(data_dir + matchlists_dir)
    games_df, mlists_df = [], []
    seeds = seed_acc_ids[region]

    games_path = data_dir + init_games_csv
    mlists_path = data_dir + matchlists_csv
    rtag = '[' + region + '] '
    init_games_exist = None

    # Import existing results csvs
    # with r_conds['gf']:
    #     init_games_exist = os.path.exists(games_path)
    # if init_games_exist:
    #     pr_fl("Found file, reading...")
    #     with r_conds['gf']:
    #         games_df = pd.read_csv(games_path, index_col=0)
    #         correct_region = games_df["region"] == region
    #         if sum(correct_region) == 0:
    #             pr_fl("Not enough init games in region")
    #             init_games_exist = False
    #             games_df, mlists_df = [], []
    #         else:
    #             init_games_exist = True
    #             games_df = games_df[correct_region]
    #     if init_games_exist:
    #         with r_conds['mf']:
    #             mlists_df = pd.read_csv(mlists_path, index_col=0)
    #             correct_region = mlists_df["region"] == region
    #             if sum(correct_region) == 0:
    #                 pr_fl("Not enough init matchlists in region")
    #                 init_games_exist = False
    #                 games_df, mlists_df = [], []
    #             else:
    #                 init_games_exist = True
    #                 pr_fl("Successfully read file")
    #                 mlists_df = mlists_df[correct_region]

    # Try with current dataset games/player lists
    using_curr_dataset = False
    if not init_games_exist:
        pr_fl("Trying current dataset...")
        curr_games, curr_players = None, None
        exists = False
        with r_conds['cg']:
            if os.path.exists(curr_games_path):
                curr_games = pd.read_csv(curr_games_path, index_col=0)
                exists = True
        if exists:
            with r_conds['cp']:
                curr_players = pd.read_csv(data_dir + curr_players_csv, index_col=0)
        if curr_games is not None:
            games_df = deepcopy(curr_games)
            correct_region = games_df["region"] == region
            if sum(correct_region) == 0:
                pr_fl("Not enough players in region")
                init_games_exist = False
                games_df, mlists_df = [], []
            else:
                init_games_exist = True
                games_df = games_df[correct_region]
            if init_games_exist:
                mlists_df = deepcopy(curr_players)
                correct_region = mlists_df["region"] == region
                if sum(correct_region) == 0:
                    pr_fl("Not enough init matchlists in region")
                    init_games_exist = False
                    games_df, mlists_df = [], []
                else:
                    using_curr_dataset = True
                    init_games_exist = True
                    pr_fl("Successfully read current dataset")
                    mlists_df = mlists_df[correct_region]

    # If no initial file or no games/matchlists in the correct region...
    if not init_games_exist:
        # Use seed account id(s) and get their recent matches
        for acc_id in seeds:
            mh = ranked_matchlist(r_conds, r_tss, region, acc_id)
            if mh is None:
                pr_fl("ERROR: Seed matchlist is None (check acc ids)")
                continue
            save_json(mh, matchlists_dir + region + '_' + str(acc_id))
            mlists_df += [ { "a_id": region + '_' + str(acc_id),
                             "region": region,
                             "timestamp": time.time() } ]

            # Get 20 recent matches from these accounts
            for i in range(min(20, len(mh["matches"]))):
                game_id = mh["matches"][i]["gameId"]
                game = rito_req(r_conds, r_tss, region,
                    game_by_game_id + str(game_id))
                if game is None:
                    continue
                save_json(game, init_games_dir + region + '_' + str(game_id))
                games_df += [ { "g_id": region + '_' + str(game_id),
                                "region": region,
                                "timestamp": game["gameCreation"] / 1000 } ]

        # Assemble dataframes
        games_df = pd.DataFrame(games_df)
        games_df = games_df[games_cols]
        mlists_df = pd.DataFrame(mlists_df)
        mlists_df = mlists_df[mlists_cols]
        games_df = games_df.set_index("g_id")
        mlists_df = mlists_df.set_index("a_id")
        with r_conds['gf']:
            games_df = db_add(games_df, games_path, region, index_col=0)
        with r_conds['mf']:
            mlists_df = db_add(mlists_df, mlists_path, region, index_col=0)

    # Filter out games before oldest_game_ts
    games_list = deepcopy(games_df[games_df["timestamp"] > oldest_game_ts * (1 if using_curr_dataset else 1)])
    games_list = games_list.sample(frac=1)

    # Loop over crawler code
    n_cycles_complete = 0
    while True:

        # Construct list of players from the games in games_list, ignoring
        # those for which we already have a recent match history entry
        players = []
        for game_id in games_list.index:
            game = load_json(init_games_dir + str(game_id))
            if game is None:
                continue
            for player in game["participantIdentities"]:
                acc_id = player["player"]["currentAccountId"]
                a_id = region + '_' + str(acc_id)
                thing = mlists_df.loc[a_id]["timestamp"] if a_id in mlists_df.index else None
                if thing is not None:
                    if type(thing) is pd.Series and len(thing) > 0:
                        thing = list(thing)[-1]
                    if thing > oldest_game_ts:
                        players += [ acc_id ]
        sys_print(rtag + "New players: " + str(len(players)) + \
            ", New games: " + str(len(games_list)) + \
            ", Crawling cycles completed: " + str(n_cycles_complete) + '\n')
        games_list, games_list_ids = [], []

        # Select a random subset of n_pl_cycle players from the list, use
        # these to obtain a new list of games (n_recent_m per player)
        np.random.shuffle(players)
        new_mlists, new_games = [], []
        for acc_id in players[:n_pl_cycle]:
            a_id = region + '_' + str(acc_id)
            mh = ranked_matchlist(r_conds, r_tss, region, acc_id)
            # Give time to other rito requesting threads
            time.sleep(get_matches_rr_sleep)

            if mh is None:
                break
            save_json(mh, matchlists_dir + a_id)
            new_mlists += [ { 'a_id': a_id,
                              'region': region,
                              'timestamp': time.time() } ]

            # Get n_recent_m from these accounts, saving new & unseen games
            for i in range(min(n_recent_m, len(mh["matches"]))):
                game_id = mh["matches"][i]["gameId"]
                g_id = region + '_' + str(game_id)
                timestamp = mh["matches"][i]["timestamp"] / 1000
                if g_id not in games_df.index and \
                  g_id not in games_list_ids and \
                  timestamp > oldest_game_ts:
                    game = rito_req(r_conds, r_tss, region,
                        game_by_game_id + str(game_id))
                    # Give time to other rito requesting threads
                    time.sleep(get_matches_rr_sleep)

                    if game is None:
                        break
                    save_json(game, init_games_dir + g_id)
                    games_list_ids += [g_id]
                    games_list += [ { 'g_id': g_id,
                                      'region': region,
                                      'timestamp': timestamp } ]

        if games_list == []:
            games_list = deepcopy(games_df[games_df["timestamp"] > oldest_game_ts * (1 if using_curr_dataset else 1)])
            games_list = games_list.sample(frac=1)
            continue

        # Save new games & matchlists into csv databases
        new_mlists_df = pd.DataFrame(new_mlists)
        new_mlists_df = new_mlists_df[mlists_cols]
        new_mlists_df = new_mlists_df.set_index('a_id')
        # if not using_curr_dataset:
        with r_conds['mf']:
            mlists_df = db_upd(new_mlists_df, mlists_path, region, index_col=0)

        games_list = pd.DataFrame(games_list)
        games_list = games_list[games_cols]
        games_list = games_list.set_index('g_id')
        # if not using_curr_dataset:
        with r_conds['gf']:
            games_df = db_add(games_list, games_path, region, index_col=0)

        n_cycles_complete += 1



if __name__ == "__main__":
    get_matches(make_r_conds(), make_r_timestamps(), 'EUW1')


