import threading, queue
import time
import sys
from threading import Condition

from threading_test_func import *

# def func1(num, q):
#     while num < sys.maxsize:
#         num =  num**2
#         q.put(num)

# def func2(num, q):
#     while num < sys.maxsize:
#         num = q.get()
#         time.sleep(1)
#         print (num)

# num = 2
# q = queue.Queue()
# # thread1 = threading.Thread(target=func1,args=(num,q))
# thread2 = threading.Thread(target=func2,args=(num,q))
# print ('setup')
# # thread1.start()
# thread2.start()

# q.put(num)
# q.put(num)

# def rito_reqr(inp_q, out_q):
#     while True:    # Continuosly acquire lock, wait 2, then release, wait 1
#         inp = inp_q.get()
#         time.sleep(2) # Simulate processing
#         out_q.put(inp**2)



# inp_q = queue.Queue()
# out_q = queue.Queue()


rt_ts_cond = Condition()
rt_timestamp = {'ts': 77}


def rr(cond):
    global rt_timestamp
    rito_reqr(cond, rt_timestamp)


rito_reqr_thread = threading.Thread(target=rr,
                            args=(rt_ts_cond,), daemon=True)
rito_reqr_thread.start()
print("Thread started")
time.sleep(1)

for i in range(3):
    with rt_ts_cond:
        print("B: " + str(rt_timestamp['ts']))
        rt_timestamp['ts'] = 77
        time.sleep(4)
    time.sleep(0.5)




