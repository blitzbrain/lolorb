import time

def rito_reqr(cond, rt_ts):
    while True:
        with cond:
            time.sleep(2) # Simulate processing
            print("A: " + str(rt_ts['ts']))
            rt_ts['ts'] = 88
        time.sleep(1)