#
#  Disable or enable op.gg regions manually
#


from ServerConstants import *


region = sys.argv[1]
enable = parseBool(sys.argv[2])
msg = 'Server issues' if len(sys.argv) < 4 else sys.argv[3]


db = sqlalchemy.create_engine(pg_conn_str)  
engine = db.connect()
meta = sqlalchemy.MetaData(engine)


sysconf = sqlalchemy.Table("sysconf", meta, autoload=True, autoload_with=engine)

submission = {
    "enabled": enable,
    "message": msg,
}
upd = sysconf.update().where(sysconf.c.region == region).values(**submission)

engine.execute(upd)
engine.close()
db.dispose()


