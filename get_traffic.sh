#!/bin/bash

# Initialize
[ -z "${1}" ] && IFACE="eth0" || IFACE="$1" # Set the name of the target interface
# LOG="/tmp/traffic-watch-$IFACE.log"         # Set the log file name
# LANG=C                                      # Set envvar $LANG to `C` due to grep, awk, etc.
# IPPT='[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+'       # Set IP address match pattern #IPPT='[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'

# NIC="$(/sbin/ethtool -i "$IFACE" | awk 'FS=": " {print $2; exit}')" # Get the $IFACE (NIC) driver

# Function: Get the current traffic
# get_traffic(){
RX="$(/sbin/ifconfig "$IFACE" | grep -Po "RX bytes:[0-9]+" | sed 's/RX bytes://')" # Get the incoming traffic
TX="$(/sbin/ifconfig "$IFACE" | grep -Po "TX bytes:[0-9]+" | sed 's/TX bytes://')" # Get the outgoing traffic
IMB=$(( RX / ( ( 1000 * 1000 ) * 8 ) ))                                            # Convert into Mbits
OMB=$(( TX / ( ( 1000 * 1000 ) * 8 ) ))
# }

# get_traffic

printf "$IMB\n"
printf "$OMB\n"


