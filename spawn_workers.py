#
#  Python script to spawn n_cpu Match Prediction worker processes
#


from ServerConstants import *


create_folder(logs_dir)
logf = open(combined_log_path, 'a')


for i in range(n_mp_workers):
    start_command = worker_start_cmd + ' ' + str(i)
    # start_command += ' 1> logs/worker_' + str(i) + '/output.log 2>&1'

    create_folder(logs_dir)
    create_folder(logs_dir + "worker_" + str(i) + '/')
    process_is_running = False

    for process in psutil.process_iter():
        cmd = None
        try:
            cmd = process.cmdline()
        except Exception as e:
            # print(e) # Access denied errors
            continue

        if type(cmd) is list and len(cmd) > 0 and isinstance(cmd[0], str) and ' '.join(cmd) == start_command:
            # print(cmd)
            process_is_running = True
            break

    if not process_is_running:
        subprocess.Popen(start_command, stdout=logf, stderr=logf, close_fds=True, shell=True)
        print("worker " + str(i) + " started")
        time.sleep(1)
    else:
        print("worker " + str(i) + " is already running")


