#
#  Print worker logs
#


from Server import *


db = sqlalchemy.create_engine(pg_conn_str)
pgdb_cond = Condition()

engine, meta, wlogs = connect_pg_lg(db, pgdb_cond)
query = sqlalchemy.sql.expression.select([wlogs.c[k] \
    for k in ['worker_key', 'timestamp', 'log']])
entry = list(pg_exec(engine, query))
engine.close()
db.dispose()

pr(entry)


